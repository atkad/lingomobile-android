// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.viewHolder.checkout;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class StoreAddresHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.viewHolder.checkout.StoreAddresHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624251, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624251, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624252, "field 'textViewStoreAddress'");
    target.textViewStoreAddress = finder.castView(view, 2131624252, "field 'textViewStoreAddress'");
    view = finder.findRequiredView(source, 2131624250, "field 'linearLayoutItemStoreAddress'");
    target.linearLayoutItemStoreAddress = finder.castView(view, 2131624250, "field 'linearLayoutItemStoreAddress'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.textViewStoreAddress = null;
    target.linearLayoutItemStoreAddress = null;
  }
}
