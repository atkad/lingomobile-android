// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MoreProductReviewsActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.MoreProductReviewsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624096, "field 'recyclerView'");
    target.recyclerView = finder.castView(view, 2131624096, "field 'recyclerView'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.recyclerView = null;
  }
}
