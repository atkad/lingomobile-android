// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class OrderHistoryRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.OrderHistoryRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624473, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624473, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624474, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624474, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624475, "field 'appCompatTextViewOrderTime'");
    target.appCompatTextViewOrderTime = finder.castView(view, 2131624475, "field 'appCompatTextViewOrderTime'");
    view = finder.findRequiredView(source, 2131624471, "field 'relativeLayoutRowItem'");
    target.relativeLayoutRowItem = finder.castView(view, 2131624471, "field 'relativeLayoutRowItem'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.appCompatTextViewDescription = null;
    target.appCompatTextViewOrderTime = null;
    target.relativeLayoutRowItem = null;
  }
}
