// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NoInternetAccessFragment$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.NoInternetAccessFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624209, "field 'appCompatButtonTryAgain'");
    target.appCompatButtonTryAgain = finder.castView(view, 2131624209, "field 'appCompatButtonTryAgain'");
  }

  @Override public void unbind(T target) {
    target.appCompatButtonTryAgain = null;
  }
}
