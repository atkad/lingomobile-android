// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LeafChildrenCategoryActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.LeafChildrenCategoryActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624092, "field 'recyclerViewLeafChildrenCategory'");
    target.recyclerViewLeafChildrenCategory = finder.castView(view, 2131624092, "field 'recyclerViewLeafChildrenCategory'");
    view = finder.findRequiredView(source, 2131624091, "field 'appCompatSpinnerDataFilter'");
    target.appCompatSpinnerDataFilter = finder.castView(view, 2131624091, "field 'appCompatSpinnerDataFilter'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.recyclerViewLeafChildrenCategory = null;
    target.appCompatSpinnerDataFilter = null;
  }
}
