// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeFragment$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.HomeFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624201, "field 'relativeLayoutLoading'");
    target.relativeLayoutLoading = finder.castView(view, 2131624201, "field 'relativeLayoutLoading'");
    view = finder.findRequiredView(source, 2131624196, "field 'autoScrollViewPager'");
    target.autoScrollViewPager = finder.castView(view, 2131624196, "field 'autoScrollViewPager'");
    view = finder.findRequiredView(source, 2131624132, "field 'circlePageIndicator'");
    target.circlePageIndicator = finder.castView(view, 2131624132, "field 'circlePageIndicator'");
    view = finder.findRequiredView(source, 2131624195, "field 'parallaxTopHeaderLinearLayout'");
    target.parallaxTopHeaderLinearLayout = finder.castView(view, 2131624195, "field 'parallaxTopHeaderLinearLayout'");
    view = finder.findRequiredView(source, 2131624197, "field 'smartTabLayoutIndustry'");
    target.smartTabLayoutIndustry = finder.castView(view, 2131624197, "field 'smartTabLayoutIndustry'");
    view = finder.findRequiredView(source, 2131624194, "field 'hackyViewPagerIndustry'");
    target.hackyViewPagerIndustry = finder.castView(view, 2131624194, "field 'hackyViewPagerIndustry'");
    view = finder.findRequiredView(source, 2131624193, "field 'touchCallbackLayout'");
    target.touchCallbackLayout = finder.castView(view, 2131624193, "field 'touchCallbackLayout'");
    view = finder.findRequiredView(source, 2131624200, "field 'appCompatImageViewClose'");
    target.appCompatImageViewClose = finder.castView(view, 2131624200, "field 'appCompatImageViewClose'");
    view = finder.findRequiredView(source, 2131624199, "field 'appCompatImageViewLingoAdvertising'");
    target.appCompatImageViewLingoAdvertising = finder.castView(view, 2131624199, "field 'appCompatImageViewLingoAdvertising'");
    view = finder.findRequiredView(source, 2131624198, "field 'relativeLayoutLingoAdvertising'");
    target.relativeLayoutLingoAdvertising = finder.castView(view, 2131624198, "field 'relativeLayoutLingoAdvertising'");
  }

  @Override public void unbind(T target) {
    target.relativeLayoutLoading = null;
    target.autoScrollViewPager = null;
    target.circlePageIndicator = null;
    target.parallaxTopHeaderLinearLayout = null;
    target.smartTabLayoutIndustry = null;
    target.hackyViewPagerIndustry = null;
    target.touchCallbackLayout = null;
    target.appCompatImageViewClose = null;
    target.appCompatImageViewLingoAdvertising = null;
    target.relativeLayoutLingoAdvertising = null;
  }
}
