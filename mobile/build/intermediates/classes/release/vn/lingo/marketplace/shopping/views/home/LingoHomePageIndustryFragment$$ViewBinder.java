// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LingoHomePageIndustryFragment$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.LingoHomePageIndustryFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624239, "field 'scrollView'");
    target.scrollView = finder.castView(view, 2131624239, "field 'scrollView'");
    view = finder.findRequiredView(source, 2131624346, "field 'autoScrollViewPagerGoldenBrand'");
    target.autoScrollViewPagerGoldenBrand = finder.castView(view, 2131624346, "field 'autoScrollViewPagerGoldenBrand'");
    view = finder.findRequiredView(source, 2131624352, "field 'appCompatTextViewBestIndustrialFirst'");
    target.appCompatTextViewBestIndustrialFirst = finder.castView(view, 2131624352, "field 'appCompatTextViewBestIndustrialFirst'");
    view = finder.findRequiredView(source, 2131624353, "field 'recyclerViewBestIndustrialFirst'");
    target.recyclerViewBestIndustrialFirst = finder.castView(view, 2131624353, "field 'recyclerViewBestIndustrialFirst'");
    view = finder.findRequiredView(source, 2131624358, "field 'appCompatTextViewBestIndustrialSecond'");
    target.appCompatTextViewBestIndustrialSecond = finder.castView(view, 2131624358, "field 'appCompatTextViewBestIndustrialSecond'");
    view = finder.findRequiredView(source, 2131624359, "field 'recyclerViewBestIndustrialSecond'");
    target.recyclerViewBestIndustrialSecond = finder.castView(view, 2131624359, "field 'recyclerViewBestIndustrialSecond'");
    view = finder.findRequiredView(source, 2131624361, "field 'appCompatTextViewBestIndustrialThird'");
    target.appCompatTextViewBestIndustrialThird = finder.castView(view, 2131624361, "field 'appCompatTextViewBestIndustrialThird'");
    view = finder.findRequiredView(source, 2131624362, "field 'recyclerViewBestIndustrialThird'");
    target.recyclerViewBestIndustrialThird = finder.castView(view, 2131624362, "field 'recyclerViewBestIndustrialThird'");
    view = finder.findRequiredView(source, 2131624355, "field 'appCompatTextViewBestIndustrialFourth'");
    target.appCompatTextViewBestIndustrialFourth = finder.castView(view, 2131624355, "field 'appCompatTextViewBestIndustrialFourth'");
    view = finder.findRequiredView(source, 2131624356, "field 'recyclerViewBestIndustrialFourth'");
    target.recyclerViewBestIndustrialFourth = finder.castView(view, 2131624356, "field 'recyclerViewBestIndustrialFourth'");
    view = finder.findRequiredView(source, 2131624348, "field 'appCompatTextViewBestIndustrialFifth'");
    target.appCompatTextViewBestIndustrialFifth = finder.castView(view, 2131624348, "field 'appCompatTextViewBestIndustrialFifth'");
    view = finder.findRequiredView(source, 2131624350, "field 'recyclerViewBestIndustrialFifth'");
    target.recyclerViewBestIndustrialFifth = finder.castView(view, 2131624350, "field 'recyclerViewBestIndustrialFifth'");
    view = finder.findRequiredView(source, 2131624241, "field 'appCompatImageViewBannerFirst'");
    target.appCompatImageViewBannerFirst = finder.castView(view, 2131624241, "field 'appCompatImageViewBannerFirst'");
    view = finder.findRequiredView(source, 2131624242, "field 'appCompatImageViewBannerSecond'");
    target.appCompatImageViewBannerSecond = finder.castView(view, 2131624242, "field 'appCompatImageViewBannerSecond'");
    view = finder.findRequiredView(source, 2131624243, "field 'appCompatImageViewBannerThird'");
    target.appCompatImageViewBannerThird = finder.castView(view, 2131624243, "field 'appCompatImageViewBannerThird'");
    view = finder.findRequiredView(source, 2131624244, "field 'appCompatImageViewBannerFourth'");
    target.appCompatImageViewBannerFourth = finder.castView(view, 2131624244, "field 'appCompatImageViewBannerFourth'");
    view = finder.findRequiredView(source, 2131624344, "field 'appCompatTextViewBigFriDayTitle'");
    target.appCompatTextViewBigFriDayTitle = finder.castView(view, 2131624344, "field 'appCompatTextViewBigFriDayTitle'");
    view = finder.findRequiredView(source, 2131624343, "field 'relativeLayoutBigFriDay'");
    target.relativeLayoutBigFriDay = finder.castView(view, 2131624343, "field 'relativeLayoutBigFriDay'");
    view = finder.findRequiredView(source, 2131624351, "field 'relativeLayoutFirstLayoutCardTitle'");
    target.relativeLayoutFirstLayoutCardTitle = finder.castView(view, 2131624351, "field 'relativeLayoutFirstLayoutCardTitle'");
    view = finder.findRequiredView(source, 2131624357, "field 'relativeLayoutSecondLayoutCardTitle'");
    target.relativeLayoutSecondLayoutCardTitle = finder.castView(view, 2131624357, "field 'relativeLayoutSecondLayoutCardTitle'");
    view = finder.findRequiredView(source, 2131624360, "field 'relativeLayoutThirdLayoutCardTitle'");
    target.relativeLayoutThirdLayoutCardTitle = finder.castView(view, 2131624360, "field 'relativeLayoutThirdLayoutCardTitle'");
    view = finder.findRequiredView(source, 2131624354, "field 'relativeLayoutFourthLayoutCardTitle'");
    target.relativeLayoutFourthLayoutCardTitle = finder.castView(view, 2131624354, "field 'relativeLayoutFourthLayoutCardTitle'");
    view = finder.findRequiredView(source, 2131624347, "field 'relativeLayoutFifthLayoutCardTitle'");
    target.relativeLayoutFifthLayoutCardTitle = finder.castView(view, 2131624347, "field 'relativeLayoutFifthLayoutCardTitle'");
  }

  @Override public void unbind(T target) {
    target.scrollView = null;
    target.autoScrollViewPagerGoldenBrand = null;
    target.appCompatTextViewBestIndustrialFirst = null;
    target.recyclerViewBestIndustrialFirst = null;
    target.appCompatTextViewBestIndustrialSecond = null;
    target.recyclerViewBestIndustrialSecond = null;
    target.appCompatTextViewBestIndustrialThird = null;
    target.recyclerViewBestIndustrialThird = null;
    target.appCompatTextViewBestIndustrialFourth = null;
    target.recyclerViewBestIndustrialFourth = null;
    target.appCompatTextViewBestIndustrialFifth = null;
    target.recyclerViewBestIndustrialFifth = null;
    target.appCompatImageViewBannerFirst = null;
    target.appCompatImageViewBannerSecond = null;
    target.appCompatImageViewBannerThird = null;
    target.appCompatImageViewBannerFourth = null;
    target.appCompatTextViewBigFriDayTitle = null;
    target.relativeLayoutBigFriDay = null;
    target.relativeLayoutFirstLayoutCardTitle = null;
    target.relativeLayoutSecondLayoutCardTitle = null;
    target.relativeLayoutThirdLayoutCardTitle = null;
    target.relativeLayoutFourthLayoutCardTitle = null;
    target.relativeLayoutFifthLayoutCardTitle = null;
  }
}
