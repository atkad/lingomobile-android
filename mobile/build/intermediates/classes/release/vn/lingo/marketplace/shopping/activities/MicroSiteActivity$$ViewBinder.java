// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MicroSiteActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.MicroSiteActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624079, "field 'linearLayoutMicroSite'");
    target.linearLayoutMicroSite = finder.castView(view, 2131624079, "field 'linearLayoutMicroSite'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.linearLayoutMicroSite = null;
  }
}
