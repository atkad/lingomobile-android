// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeMoreAllEventsActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.HomeMoreAllEventsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624080, "field 'megaSaleRecyclerView'");
    target.megaSaleRecyclerView = finder.castView(view, 2131624080, "field 'megaSaleRecyclerView'");
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
  }

  @Override public void unbind(T target) {
    target.megaSaleRecyclerView = null;
    target.toolbar = null;
  }
}
