// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductDetailsRelatedProductsRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.ProductDetailsRelatedProductsRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624517, "field 'relativeLayoutRowItem'");
    target.relativeLayoutRowItem = finder.castView(view, 2131624517, "field 'relativeLayoutRowItem'");
    view = finder.findRequiredView(source, 2131624519, "field 'productDetailsRelatedProductsImageViewMetaData'");
    target.productDetailsRelatedProductsImageViewMetaData = finder.castView(view, 2131624519, "field 'productDetailsRelatedProductsImageViewMetaData'");
    view = finder.findRequiredView(source, 2131624527, "field 'productDetailsRelatedProductsTitle'");
    target.productDetailsRelatedProductsTitle = finder.castView(view, 2131624527, "field 'productDetailsRelatedProductsTitle'");
    view = finder.findRequiredView(source, 2131624528, "field 'productDetailsRelatedProductsPrices'");
    target.productDetailsRelatedProductsPrices = finder.castView(view, 2131624528, "field 'productDetailsRelatedProductsPrices'");
    view = finder.findRequiredView(source, 2131624529, "field 'productDetailsRelatedProductsMarketPrice'");
    target.productDetailsRelatedProductsMarketPrice = finder.castView(view, 2131624529, "field 'productDetailsRelatedProductsMarketPrice'");
    view = finder.findRequiredView(source, 2131624520, "field 'linearLayoutPercent'");
    target.linearLayoutPercent = finder.castView(view, 2131624520, "field 'linearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624521, "field 'appCompatTextViewPercent'");
    target.appCompatTextViewPercent = finder.castView(view, 2131624521, "field 'appCompatTextViewPercent'");
    view = finder.findRequiredView(source, 2131624522, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624522, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624523, "field 'appCompatTextViewPercentHappyHourDiscount'");
    target.appCompatTextViewPercentHappyHourDiscount = finder.castView(view, 2131624523, "field 'appCompatTextViewPercentHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624524, "field 'linearLayoutGiftCards'");
    target.linearLayoutGiftCards = finder.castView(view, 2131624524, "field 'linearLayoutGiftCards'");
    view = finder.findRequiredView(source, 2131624525, "field 'appCompatTextViewGiftCards'");
    target.appCompatTextViewGiftCards = finder.castView(view, 2131624525, "field 'appCompatTextViewGiftCards'");
  }

  @Override public void unbind(T target) {
    target.relativeLayoutRowItem = null;
    target.productDetailsRelatedProductsImageViewMetaData = null;
    target.productDetailsRelatedProductsTitle = null;
    target.productDetailsRelatedProductsPrices = null;
    target.productDetailsRelatedProductsMarketPrice = null;
    target.linearLayoutPercent = null;
    target.appCompatTextViewPercent = null;
    target.linearLayoutHappyHourDiscount = null;
    target.appCompatTextViewPercentHappyHourDiscount = null;
    target.linearLayoutGiftCards = null;
    target.appCompatTextViewGiftCards = null;
  }
}
