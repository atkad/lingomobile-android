// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeHotDealBigFriDayViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.viewholder.HomeHotDealBigFriDayViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624232, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624232, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624231, "field 'linearLayout'");
    target.linearLayout = finder.castView(view, 2131624231, "field 'linearLayout'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.linearLayout = null;
  }
}
