// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SearchResultsActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.SearchResultsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624104, "field 'recyclerViewSearchResults'");
    target.recyclerViewSearchResults = finder.castView(view, 2131624104, "field 'recyclerViewSearchResults'");
    view = finder.findRequiredView(source, 2131624105, "field 'relativeLayoutNotFound'");
    target.relativeLayoutNotFound = finder.castView(view, 2131624105, "field 'relativeLayoutNotFound'");
    view = finder.findRequiredView(source, 2131624106, "field 'appCompatTextViewNotFoundErrorDescription'");
    target.appCompatTextViewNotFoundErrorDescription = finder.castView(view, 2131624106, "field 'appCompatTextViewNotFoundErrorDescription'");
    view = finder.findRequiredView(source, 2131624103, "field 'appCompatSpinnerDataFilter'");
    target.appCompatSpinnerDataFilter = finder.castView(view, 2131624103, "field 'appCompatSpinnerDataFilter'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.recyclerViewSearchResults = null;
    target.relativeLayoutNotFound = null;
    target.appCompatTextViewNotFoundErrorDescription = null;
    target.appCompatSpinnerDataFilter = null;
  }
}
