// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class OrderHistoryFragment$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.OrderHistoryFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624213, "field 'recyclerView'");
    target.recyclerView = finder.castView(view, 2131624213, "field 'recyclerView'");
    view = finder.findRequiredView(source, 2131624212, "field 'appCompatTextViewHotLine'");
    target.appCompatTextViewHotLine = finder.castView(view, 2131624212, "field 'appCompatTextViewHotLine'");
  }

  @Override public void unbind(T target) {
    target.recyclerView = null;
    target.appCompatTextViewHotLine = null;
  }
}
