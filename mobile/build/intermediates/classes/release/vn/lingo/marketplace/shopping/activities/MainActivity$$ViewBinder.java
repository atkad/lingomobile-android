// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.MainActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624082, "field 'relativeLayoutTutorialHelper'");
    target.relativeLayoutTutorialHelper = finder.castView(view, 2131624082, "field 'relativeLayoutTutorialHelper'");
    view = finder.findRequiredView(source, 2131624086, "field 'appCompatButtonTutorialHelper'");
    target.appCompatButtonTutorialHelper = finder.castView(view, 2131624086, "field 'appCompatButtonTutorialHelper'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.relativeLayoutTutorialHelper = null;
    target.appCompatButtonTutorialHelper = null;
  }
}
