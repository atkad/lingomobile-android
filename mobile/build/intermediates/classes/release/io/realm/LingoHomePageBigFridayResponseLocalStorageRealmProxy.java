package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.local.LingoHomePageBigFridayResponseLocalStorage;

public class LingoHomePageBigFridayResponseLocalStorageRealmProxy extends LingoHomePageBigFridayResponseLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_ID;
    private static long INDEX_MODULEID;
    private static long INDEX_PRODUCTPRICELAST;
    private static long INDEX_AVATAR;
    private static long INDEX_ISPAUSESAVEBUY;
    private static long INDEX_PRODUCTDISCOUNTLAST;
    private static long INDEX_DISCOUNTSTYPE;
    private static long INDEX_PRODUCTID;
    private static long INDEX_ISSTOPSAVEBUY;
    private static long INDEX_PRODUCTNAME;
    private static long INDEX_PRODUCTQUANTITY;
    private static long INDEX_MARKETPRICE;
    private static long INDEX_TOTALORDER;
    private static long INDEX_MARKETPRICEFORMAT;
    private static long INDEX_PRODUCTPRICELASTFORMAT;
    private static long INDEX_HAPPYHOURDISCOUNT;
    private static long INDEX_GIFTCARD;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("id");
        fieldNames.add("moduleId");
        fieldNames.add("productPriceLast");
        fieldNames.add("avatar");
        fieldNames.add("isPauseSaveBuy");
        fieldNames.add("productDiscountLast");
        fieldNames.add("discountsType");
        fieldNames.add("productId");
        fieldNames.add("isStopSaveBuy");
        fieldNames.add("productName");
        fieldNames.add("productQuantity");
        fieldNames.add("marketPrice");
        fieldNames.add("totalOrder");
        fieldNames.add("marketPriceFormat");
        fieldNames.add("productPriceLastFormat");
        fieldNames.add("happyHourDiscount");
        fieldNames.add("giftCard");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_ID);
    }

    @Override
    public void setId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_ID, (long) value);
    }

    @Override
    public int getModuleId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_MODULEID);
    }

    @Override
    public void setModuleId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_MODULEID, (long) value);
    }

    @Override
    public String getProductPriceLast() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICELAST);
    }

    @Override
    public void setProductPriceLast(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICELAST);
            return;
        }
        row.setString(INDEX_PRODUCTPRICELAST, (String) value);
    }

    @Override
    public String getAvatar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_AVATAR);
    }

    @Override
    public void setAvatar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_AVATAR);
            return;
        }
        row.setString(INDEX_AVATAR, (String) value);
    }

    @Override
    public String getIsPauseSaveBuy() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ISPAUSESAVEBUY);
    }

    @Override
    public void setIsPauseSaveBuy(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ISPAUSESAVEBUY);
            return;
        }
        row.setString(INDEX_ISPAUSESAVEBUY, (String) value);
    }

    @Override
    public String getProductDiscountLast() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTDISCOUNTLAST);
    }

    @Override
    public void setProductDiscountLast(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTDISCOUNTLAST);
            return;
        }
        row.setString(INDEX_PRODUCTDISCOUNTLAST, (String) value);
    }

    @Override
    public String getDiscountsType() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_DISCOUNTSTYPE);
    }

    @Override
    public void setDiscountsType(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_DISCOUNTSTYPE);
            return;
        }
        row.setString(INDEX_DISCOUNTSTYPE, (String) value);
    }

    @Override
    public String getProductId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTID);
    }

    @Override
    public void setProductId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTID);
            return;
        }
        row.setString(INDEX_PRODUCTID, (String) value);
    }

    @Override
    public String getIsStopSaveBuy() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ISSTOPSAVEBUY);
    }

    @Override
    public void setIsStopSaveBuy(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ISSTOPSAVEBUY);
            return;
        }
        row.setString(INDEX_ISSTOPSAVEBUY, (String) value);
    }

    @Override
    public String getProductName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTNAME);
    }

    @Override
    public void setProductName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTNAME);
            return;
        }
        row.setString(INDEX_PRODUCTNAME, (String) value);
    }

    @Override
    public String getProductQuantity() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTQUANTITY);
    }

    @Override
    public void setProductQuantity(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTQUANTITY);
            return;
        }
        row.setString(INDEX_PRODUCTQUANTITY, (String) value);
    }

    @Override
    public String getMarketPrice() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MARKETPRICE);
    }

    @Override
    public void setMarketPrice(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MARKETPRICE);
            return;
        }
        row.setString(INDEX_MARKETPRICE, (String) value);
    }

    @Override
    public String getTotalOrder() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_TOTALORDER);
    }

    @Override
    public void setTotalOrder(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_TOTALORDER);
            return;
        }
        row.setString(INDEX_TOTALORDER, (String) value);
    }

    @Override
    public String getMarketPriceFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MARKETPRICEFORMAT);
    }

    @Override
    public void setMarketPriceFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MARKETPRICEFORMAT);
            return;
        }
        row.setString(INDEX_MARKETPRICEFORMAT, (String) value);
    }

    @Override
    public String getProductPriceLastFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICELASTFORMAT);
    }

    @Override
    public void setProductPriceLastFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICELASTFORMAT);
            return;
        }
        row.setString(INDEX_PRODUCTPRICELASTFORMAT, (String) value);
    }

    @Override
    public String getHappyHourDiscount() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_HAPPYHOURDISCOUNT);
    }

    @Override
    public void setHappyHourDiscount(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_HAPPYHOURDISCOUNT);
            return;
        }
        row.setString(INDEX_HAPPYHOURDISCOUNT, (String) value);
    }

    @Override
    public String getGiftCard() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_GIFTCARD);
    }

    @Override
    public void setGiftCard(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_GIFTCARD);
            return;
        }
        row.setString(INDEX_GIFTCARD, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_LingoHomePageBigFridayResponseLocalStorage")) {
            Table table = transaction.getTable("class_LingoHomePageBigFridayResponseLocalStorage");
            table.addColumn(ColumnType.INTEGER, "id", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "moduleId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "productPriceLast", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "avatar", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "isPauseSaveBuy", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productDiscountLast", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "discountsType", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "isStopSaveBuy", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productName", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productQuantity", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "marketPrice", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "totalOrder", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "marketPriceFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productPriceLastFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "happyHourDiscount", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "giftCard", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("id"));
            table.setPrimaryKey("id");
            return table;
        }
        return transaction.getTable("class_LingoHomePageBigFridayResponseLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_LingoHomePageBigFridayResponseLocalStorage")) {
            Table table = transaction.getTable("class_LingoHomePageBigFridayResponseLocalStorage");
            if (table.getColumnCount() != 17) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 17 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 17; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type LingoHomePageBigFridayResponseLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_ID = table.getColumnIndex("id");
            INDEX_MODULEID = table.getColumnIndex("moduleId");
            INDEX_PRODUCTPRICELAST = table.getColumnIndex("productPriceLast");
            INDEX_AVATAR = table.getColumnIndex("avatar");
            INDEX_ISPAUSESAVEBUY = table.getColumnIndex("isPauseSaveBuy");
            INDEX_PRODUCTDISCOUNTLAST = table.getColumnIndex("productDiscountLast");
            INDEX_DISCOUNTSTYPE = table.getColumnIndex("discountsType");
            INDEX_PRODUCTID = table.getColumnIndex("productId");
            INDEX_ISSTOPSAVEBUY = table.getColumnIndex("isStopSaveBuy");
            INDEX_PRODUCTNAME = table.getColumnIndex("productName");
            INDEX_PRODUCTQUANTITY = table.getColumnIndex("productQuantity");
            INDEX_MARKETPRICE = table.getColumnIndex("marketPrice");
            INDEX_TOTALORDER = table.getColumnIndex("totalOrder");
            INDEX_MARKETPRICEFORMAT = table.getColumnIndex("marketPriceFormat");
            INDEX_PRODUCTPRICELASTFORMAT = table.getColumnIndex("productPriceLastFormat");
            INDEX_HAPPYHOURDISCOUNT = table.getColumnIndex("happyHourDiscount");
            INDEX_GIFTCARD = table.getColumnIndex("giftCard");

            if (!columnTypes.containsKey("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("id") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'id' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'id' does support null values in the existing Realm file. Use corresponding boxed type for field 'id' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'id' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("id"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("moduleId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'moduleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("moduleId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'moduleId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_MODULEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'moduleId' does support null values in the existing Realm file. Use corresponding boxed type for field 'moduleId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("productPriceLast")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPriceLast' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPriceLast") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPriceLast' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICELAST)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPriceLast' is required. Either set @Required to field 'productPriceLast' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("avatar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'avatar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("avatar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'avatar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_AVATAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'avatar' is required. Either set @Required to field 'avatar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("isPauseSaveBuy")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'isPauseSaveBuy' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("isPauseSaveBuy") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'isPauseSaveBuy' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ISPAUSESAVEBUY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'isPauseSaveBuy' is required. Either set @Required to field 'isPauseSaveBuy' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productDiscountLast")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productDiscountLast' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productDiscountLast") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productDiscountLast' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTDISCOUNTLAST)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productDiscountLast' is required. Either set @Required to field 'productDiscountLast' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("discountsType")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'discountsType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("discountsType") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'discountsType' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_DISCOUNTSTYPE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'discountsType' is required. Either set @Required to field 'discountsType' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productId' is required. Either set @Required to field 'productId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("isStopSaveBuy")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'isStopSaveBuy' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("isStopSaveBuy") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'isStopSaveBuy' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ISSTOPSAVEBUY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'isStopSaveBuy' is required. Either set @Required to field 'isStopSaveBuy' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productName")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productName") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productName' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTNAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productName' is required. Either set @Required to field 'productName' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productQuantity")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productQuantity' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productQuantity") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productQuantity' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTQUANTITY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productQuantity' is required. Either set @Required to field 'productQuantity' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("marketPrice")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'marketPrice' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("marketPrice") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'marketPrice' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MARKETPRICE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'marketPrice' is required. Either set @Required to field 'marketPrice' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("totalOrder")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'totalOrder' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("totalOrder") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'totalOrder' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_TOTALORDER)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'totalOrder' is required. Either set @Required to field 'totalOrder' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("marketPriceFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'marketPriceFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("marketPriceFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'marketPriceFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MARKETPRICEFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'marketPriceFormat' is required. Either set @Required to field 'marketPriceFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productPriceLastFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPriceLastFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPriceLastFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPriceLastFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICELASTFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPriceLastFormat' is required. Either set @Required to field 'productPriceLastFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("happyHourDiscount")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'happyHourDiscount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("happyHourDiscount") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'happyHourDiscount' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_HAPPYHOURDISCOUNT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'happyHourDiscount' is required. Either set @Required to field 'happyHourDiscount' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("giftCard")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'giftCard' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("giftCard") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'giftCard' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_GIFTCARD)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'giftCard' is required. Either set @Required to field 'giftCard' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The LingoHomePageBigFridayResponseLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_LingoHomePageBigFridayResponseLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static LingoHomePageBigFridayResponseLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        LingoHomePageBigFridayResponseLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(LingoHomePageBigFridayResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("id")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new LingoHomePageBigFridayResponseLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(LingoHomePageBigFridayResponseLocalStorage.class);
        }
        if (json.has("id")) {
            if (json.isNull("id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
            } else {
                obj.setId((int) json.getInt("id"));
            }
        }
        if (json.has("moduleId")) {
            if (json.isNull("moduleId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
            } else {
                obj.setModuleId((int) json.getInt("moduleId"));
            }
        }
        if (json.has("productPriceLast")) {
            if (json.isNull("productPriceLast")) {
                obj.setProductPriceLast(null);
            } else {
                obj.setProductPriceLast((String) json.getString("productPriceLast"));
            }
        }
        if (json.has("avatar")) {
            if (json.isNull("avatar")) {
                obj.setAvatar(null);
            } else {
                obj.setAvatar((String) json.getString("avatar"));
            }
        }
        if (json.has("isPauseSaveBuy")) {
            if (json.isNull("isPauseSaveBuy")) {
                obj.setIsPauseSaveBuy(null);
            } else {
                obj.setIsPauseSaveBuy((String) json.getString("isPauseSaveBuy"));
            }
        }
        if (json.has("productDiscountLast")) {
            if (json.isNull("productDiscountLast")) {
                obj.setProductDiscountLast(null);
            } else {
                obj.setProductDiscountLast((String) json.getString("productDiscountLast"));
            }
        }
        if (json.has("discountsType")) {
            if (json.isNull("discountsType")) {
                obj.setDiscountsType(null);
            } else {
                obj.setDiscountsType((String) json.getString("discountsType"));
            }
        }
        if (json.has("productId")) {
            if (json.isNull("productId")) {
                obj.setProductId(null);
            } else {
                obj.setProductId((String) json.getString("productId"));
            }
        }
        if (json.has("isStopSaveBuy")) {
            if (json.isNull("isStopSaveBuy")) {
                obj.setIsStopSaveBuy(null);
            } else {
                obj.setIsStopSaveBuy((String) json.getString("isStopSaveBuy"));
            }
        }
        if (json.has("productName")) {
            if (json.isNull("productName")) {
                obj.setProductName(null);
            } else {
                obj.setProductName((String) json.getString("productName"));
            }
        }
        if (json.has("productQuantity")) {
            if (json.isNull("productQuantity")) {
                obj.setProductQuantity(null);
            } else {
                obj.setProductQuantity((String) json.getString("productQuantity"));
            }
        }
        if (json.has("marketPrice")) {
            if (json.isNull("marketPrice")) {
                obj.setMarketPrice(null);
            } else {
                obj.setMarketPrice((String) json.getString("marketPrice"));
            }
        }
        if (json.has("totalOrder")) {
            if (json.isNull("totalOrder")) {
                obj.setTotalOrder(null);
            } else {
                obj.setTotalOrder((String) json.getString("totalOrder"));
            }
        }
        if (json.has("marketPriceFormat")) {
            if (json.isNull("marketPriceFormat")) {
                obj.setMarketPriceFormat(null);
            } else {
                obj.setMarketPriceFormat((String) json.getString("marketPriceFormat"));
            }
        }
        if (json.has("productPriceLastFormat")) {
            if (json.isNull("productPriceLastFormat")) {
                obj.setProductPriceLastFormat(null);
            } else {
                obj.setProductPriceLastFormat((String) json.getString("productPriceLastFormat"));
            }
        }
        if (json.has("happyHourDiscount")) {
            if (json.isNull("happyHourDiscount")) {
                obj.setHappyHourDiscount(null);
            } else {
                obj.setHappyHourDiscount((String) json.getString("happyHourDiscount"));
            }
        }
        if (json.has("giftCard")) {
            if (json.isNull("giftCard")) {
                obj.setGiftCard(null);
            } else {
                obj.setGiftCard((String) json.getString("giftCard"));
            }
        }
        return obj;
    }

    public static LingoHomePageBigFridayResponseLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        LingoHomePageBigFridayResponseLocalStorage obj = realm.createObject(LingoHomePageBigFridayResponseLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
                } else {
                    obj.setId((int) reader.nextInt());
                }
            } else if (name.equals("moduleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
                } else {
                    obj.setModuleId((int) reader.nextInt());
                }
            } else if (name.equals("productPriceLast")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPriceLast(null);
                } else {
                    obj.setProductPriceLast((String) reader.nextString());
                }
            } else if (name.equals("avatar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setAvatar(null);
                } else {
                    obj.setAvatar((String) reader.nextString());
                }
            } else if (name.equals("isPauseSaveBuy")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setIsPauseSaveBuy(null);
                } else {
                    obj.setIsPauseSaveBuy((String) reader.nextString());
                }
            } else if (name.equals("productDiscountLast")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductDiscountLast(null);
                } else {
                    obj.setProductDiscountLast((String) reader.nextString());
                }
            } else if (name.equals("discountsType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setDiscountsType(null);
                } else {
                    obj.setDiscountsType((String) reader.nextString());
                }
            } else if (name.equals("productId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductId(null);
                } else {
                    obj.setProductId((String) reader.nextString());
                }
            } else if (name.equals("isStopSaveBuy")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setIsStopSaveBuy(null);
                } else {
                    obj.setIsStopSaveBuy((String) reader.nextString());
                }
            } else if (name.equals("productName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductName(null);
                } else {
                    obj.setProductName((String) reader.nextString());
                }
            } else if (name.equals("productQuantity")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductQuantity(null);
                } else {
                    obj.setProductQuantity((String) reader.nextString());
                }
            } else if (name.equals("marketPrice")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMarketPrice(null);
                } else {
                    obj.setMarketPrice((String) reader.nextString());
                }
            } else if (name.equals("totalOrder")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setTotalOrder(null);
                } else {
                    obj.setTotalOrder((String) reader.nextString());
                }
            } else if (name.equals("marketPriceFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMarketPriceFormat(null);
                } else {
                    obj.setMarketPriceFormat((String) reader.nextString());
                }
            } else if (name.equals("productPriceLastFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPriceLastFormat(null);
                } else {
                    obj.setProductPriceLastFormat((String) reader.nextString());
                }
            } else if (name.equals("happyHourDiscount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setHappyHourDiscount(null);
                } else {
                    obj.setHappyHourDiscount((String) reader.nextString());
                }
            } else if (name.equals("giftCard")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setGiftCard(null);
                } else {
                    obj.setGiftCard((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static LingoHomePageBigFridayResponseLocalStorage copyOrUpdate(Realm realm, LingoHomePageBigFridayResponseLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        LingoHomePageBigFridayResponseLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(LingoHomePageBigFridayResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new LingoHomePageBigFridayResponseLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static LingoHomePageBigFridayResponseLocalStorage copy(Realm realm, LingoHomePageBigFridayResponseLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        LingoHomePageBigFridayResponseLocalStorage realmObject = realm.createObject(LingoHomePageBigFridayResponseLocalStorage.class, newObject.getId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setId(newObject.getId());
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setProductPriceLast(newObject.getProductPriceLast());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setIsPauseSaveBuy(newObject.getIsPauseSaveBuy());
        realmObject.setProductDiscountLast(newObject.getProductDiscountLast());
        realmObject.setDiscountsType(newObject.getDiscountsType());
        realmObject.setProductId(newObject.getProductId());
        realmObject.setIsStopSaveBuy(newObject.getIsStopSaveBuy());
        realmObject.setProductName(newObject.getProductName());
        realmObject.setProductQuantity(newObject.getProductQuantity());
        realmObject.setMarketPrice(newObject.getMarketPrice());
        realmObject.setTotalOrder(newObject.getTotalOrder());
        realmObject.setMarketPriceFormat(newObject.getMarketPriceFormat());
        realmObject.setProductPriceLastFormat(newObject.getProductPriceLastFormat());
        realmObject.setHappyHourDiscount(newObject.getHappyHourDiscount());
        realmObject.setGiftCard(newObject.getGiftCard());
        return realmObject;
    }

    static LingoHomePageBigFridayResponseLocalStorage update(Realm realm, LingoHomePageBigFridayResponseLocalStorage realmObject, LingoHomePageBigFridayResponseLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setProductPriceLast(newObject.getProductPriceLast());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setIsPauseSaveBuy(newObject.getIsPauseSaveBuy());
        realmObject.setProductDiscountLast(newObject.getProductDiscountLast());
        realmObject.setDiscountsType(newObject.getDiscountsType());
        realmObject.setProductId(newObject.getProductId());
        realmObject.setIsStopSaveBuy(newObject.getIsStopSaveBuy());
        realmObject.setProductName(newObject.getProductName());
        realmObject.setProductQuantity(newObject.getProductQuantity());
        realmObject.setMarketPrice(newObject.getMarketPrice());
        realmObject.setTotalOrder(newObject.getTotalOrder());
        realmObject.setMarketPriceFormat(newObject.getMarketPriceFormat());
        realmObject.setProductPriceLastFormat(newObject.getProductPriceLastFormat());
        realmObject.setHappyHourDiscount(newObject.getHappyHourDiscount());
        realmObject.setGiftCard(newObject.getGiftCard());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("LingoHomePageBigFridayResponseLocalStorage = [");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{moduleId:");
        stringBuilder.append(getModuleId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPriceLast:");
        stringBuilder.append(getProductPriceLast() != null ? getProductPriceLast() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{avatar:");
        stringBuilder.append(getAvatar() != null ? getAvatar() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{isPauseSaveBuy:");
        stringBuilder.append(getIsPauseSaveBuy() != null ? getIsPauseSaveBuy() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productDiscountLast:");
        stringBuilder.append(getProductDiscountLast() != null ? getProductDiscountLast() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{discountsType:");
        stringBuilder.append(getDiscountsType() != null ? getDiscountsType() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productId:");
        stringBuilder.append(getProductId() != null ? getProductId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{isStopSaveBuy:");
        stringBuilder.append(getIsStopSaveBuy() != null ? getIsStopSaveBuy() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productName:");
        stringBuilder.append(getProductName() != null ? getProductName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productQuantity:");
        stringBuilder.append(getProductQuantity() != null ? getProductQuantity() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{marketPrice:");
        stringBuilder.append(getMarketPrice() != null ? getMarketPrice() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{totalOrder:");
        stringBuilder.append(getTotalOrder() != null ? getTotalOrder() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{marketPriceFormat:");
        stringBuilder.append(getMarketPriceFormat() != null ? getMarketPriceFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPriceLastFormat:");
        stringBuilder.append(getProductPriceLastFormat() != null ? getProductPriceLastFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{happyHourDiscount:");
        stringBuilder.append(getHappyHourDiscount() != null ? getHappyHourDiscount() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{giftCard:");
        stringBuilder.append(getGiftCard() != null ? getGiftCard() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LingoHomePageBigFridayResponseLocalStorageRealmProxy aLingoHomePageBigFridayResponseLocalStorage = (LingoHomePageBigFridayResponseLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aLingoHomePageBigFridayResponseLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aLingoHomePageBigFridayResponseLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aLingoHomePageBigFridayResponseLocalStorage.row.getIndex()) return false;

        return true;
    }

}
