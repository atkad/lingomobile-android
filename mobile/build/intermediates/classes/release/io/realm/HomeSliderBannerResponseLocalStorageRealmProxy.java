package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.local.HomeSliderBannerResponseLocalStorage;

public class HomeSliderBannerResponseLocalStorageRealmProxy extends HomeSliderBannerResponseLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_ID;
    private static long INDEX_MODULEID;
    private static long INDEX_DESCRIPTION;
    private static long INDEX_NAME;
    private static long INDEX_GALLERYID;
    private static long INDEX_IMAGEPATH;
    private static long INDEX_URL;
    private static long INDEX_GALLERYKEY;
    private static long INDEX_PARAMETER1;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("id");
        fieldNames.add("moduleId");
        fieldNames.add("description");
        fieldNames.add("name");
        fieldNames.add("galleryId");
        fieldNames.add("imagePath");
        fieldNames.add("url");
        fieldNames.add("galleryKey");
        fieldNames.add("parameter1");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_ID);
    }

    @Override
    public void setId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_ID, (long) value);
    }

    @Override
    public int getModuleId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_MODULEID);
    }

    @Override
    public void setModuleId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_MODULEID, (long) value);
    }

    @Override
    public String getDescription() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_DESCRIPTION);
    }

    @Override
    public void setDescription(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_DESCRIPTION);
            return;
        }
        row.setString(INDEX_DESCRIPTION, (String) value);
    }

    @Override
    public String getName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_NAME);
    }

    @Override
    public void setName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_NAME);
            return;
        }
        row.setString(INDEX_NAME, (String) value);
    }

    @Override
    public String getGalleryId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_GALLERYID);
    }

    @Override
    public void setGalleryId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_GALLERYID);
            return;
        }
        row.setString(INDEX_GALLERYID, (String) value);
    }

    @Override
    public String getImagePath() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_IMAGEPATH);
    }

    @Override
    public void setImagePath(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_IMAGEPATH);
            return;
        }
        row.setString(INDEX_IMAGEPATH, (String) value);
    }

    @Override
    public String getUrl() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_URL);
    }

    @Override
    public void setUrl(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_URL);
            return;
        }
        row.setString(INDEX_URL, (String) value);
    }

    @Override
    public String getGalleryKey() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_GALLERYKEY);
    }

    @Override
    public void setGalleryKey(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_GALLERYKEY);
            return;
        }
        row.setString(INDEX_GALLERYKEY, (String) value);
    }

    @Override
    public String getParameter1() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PARAMETER1);
    }

    @Override
    public void setParameter1(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PARAMETER1);
            return;
        }
        row.setString(INDEX_PARAMETER1, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_HomeSliderBannerResponseLocalStorage")) {
            Table table = transaction.getTable("class_HomeSliderBannerResponseLocalStorage");
            table.addColumn(ColumnType.INTEGER, "id", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "moduleId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "description", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "name", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "galleryId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "imagePath", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "url", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "galleryKey", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "parameter1", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("id"));
            table.setPrimaryKey("id");
            return table;
        }
        return transaction.getTable("class_HomeSliderBannerResponseLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_HomeSliderBannerResponseLocalStorage")) {
            Table table = transaction.getTable("class_HomeSliderBannerResponseLocalStorage");
            if (table.getColumnCount() != 9) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 9 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 9; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type HomeSliderBannerResponseLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_ID = table.getColumnIndex("id");
            INDEX_MODULEID = table.getColumnIndex("moduleId");
            INDEX_DESCRIPTION = table.getColumnIndex("description");
            INDEX_NAME = table.getColumnIndex("name");
            INDEX_GALLERYID = table.getColumnIndex("galleryId");
            INDEX_IMAGEPATH = table.getColumnIndex("imagePath");
            INDEX_URL = table.getColumnIndex("url");
            INDEX_GALLERYKEY = table.getColumnIndex("galleryKey");
            INDEX_PARAMETER1 = table.getColumnIndex("parameter1");

            if (!columnTypes.containsKey("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("id") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'id' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'id' does support null values in the existing Realm file. Use corresponding boxed type for field 'id' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'id' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("id"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("moduleId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'moduleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("moduleId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'moduleId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_MODULEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'moduleId' does support null values in the existing Realm file. Use corresponding boxed type for field 'moduleId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("description")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'description' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("description") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'description' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_DESCRIPTION)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'description' is required. Either set @Required to field 'description' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("name")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'name' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("name") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'name' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_NAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'name' is required. Either set @Required to field 'name' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("galleryId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'galleryId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("galleryId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'galleryId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_GALLERYID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'galleryId' is required. Either set @Required to field 'galleryId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("imagePath")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'imagePath' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("imagePath") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'imagePath' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_IMAGEPATH)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'imagePath' is required. Either set @Required to field 'imagePath' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("url")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'url' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("url") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'url' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_URL)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'url' is required. Either set @Required to field 'url' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("galleryKey")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'galleryKey' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("galleryKey") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'galleryKey' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_GALLERYKEY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'galleryKey' is required. Either set @Required to field 'galleryKey' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("parameter1")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'parameter1' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("parameter1") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'parameter1' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PARAMETER1)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'parameter1' is required. Either set @Required to field 'parameter1' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The HomeSliderBannerResponseLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_HomeSliderBannerResponseLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static HomeSliderBannerResponseLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        HomeSliderBannerResponseLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(HomeSliderBannerResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("id")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new HomeSliderBannerResponseLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(HomeSliderBannerResponseLocalStorage.class);
        }
        if (json.has("id")) {
            if (json.isNull("id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
            } else {
                obj.setId((int) json.getInt("id"));
            }
        }
        if (json.has("moduleId")) {
            if (json.isNull("moduleId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
            } else {
                obj.setModuleId((int) json.getInt("moduleId"));
            }
        }
        if (json.has("description")) {
            if (json.isNull("description")) {
                obj.setDescription(null);
            } else {
                obj.setDescription((String) json.getString("description"));
            }
        }
        if (json.has("name")) {
            if (json.isNull("name")) {
                obj.setName(null);
            } else {
                obj.setName((String) json.getString("name"));
            }
        }
        if (json.has("galleryId")) {
            if (json.isNull("galleryId")) {
                obj.setGalleryId(null);
            } else {
                obj.setGalleryId((String) json.getString("galleryId"));
            }
        }
        if (json.has("imagePath")) {
            if (json.isNull("imagePath")) {
                obj.setImagePath(null);
            } else {
                obj.setImagePath((String) json.getString("imagePath"));
            }
        }
        if (json.has("url")) {
            if (json.isNull("url")) {
                obj.setUrl(null);
            } else {
                obj.setUrl((String) json.getString("url"));
            }
        }
        if (json.has("galleryKey")) {
            if (json.isNull("galleryKey")) {
                obj.setGalleryKey(null);
            } else {
                obj.setGalleryKey((String) json.getString("galleryKey"));
            }
        }
        if (json.has("parameter1")) {
            if (json.isNull("parameter1")) {
                obj.setParameter1(null);
            } else {
                obj.setParameter1((String) json.getString("parameter1"));
            }
        }
        return obj;
    }

    public static HomeSliderBannerResponseLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        HomeSliderBannerResponseLocalStorage obj = realm.createObject(HomeSliderBannerResponseLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
                } else {
                    obj.setId((int) reader.nextInt());
                }
            } else if (name.equals("moduleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
                } else {
                    obj.setModuleId((int) reader.nextInt());
                }
            } else if (name.equals("description")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setDescription(null);
                } else {
                    obj.setDescription((String) reader.nextString());
                }
            } else if (name.equals("name")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setName(null);
                } else {
                    obj.setName((String) reader.nextString());
                }
            } else if (name.equals("galleryId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setGalleryId(null);
                } else {
                    obj.setGalleryId((String) reader.nextString());
                }
            } else if (name.equals("imagePath")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setImagePath(null);
                } else {
                    obj.setImagePath((String) reader.nextString());
                }
            } else if (name.equals("url")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setUrl(null);
                } else {
                    obj.setUrl((String) reader.nextString());
                }
            } else if (name.equals("galleryKey")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setGalleryKey(null);
                } else {
                    obj.setGalleryKey((String) reader.nextString());
                }
            } else if (name.equals("parameter1")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setParameter1(null);
                } else {
                    obj.setParameter1((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static HomeSliderBannerResponseLocalStorage copyOrUpdate(Realm realm, HomeSliderBannerResponseLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        HomeSliderBannerResponseLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(HomeSliderBannerResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new HomeSliderBannerResponseLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static HomeSliderBannerResponseLocalStorage copy(Realm realm, HomeSliderBannerResponseLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        HomeSliderBannerResponseLocalStorage realmObject = realm.createObject(HomeSliderBannerResponseLocalStorage.class, newObject.getId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setId(newObject.getId());
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setDescription(newObject.getDescription());
        realmObject.setName(newObject.getName());
        realmObject.setGalleryId(newObject.getGalleryId());
        realmObject.setImagePath(newObject.getImagePath());
        realmObject.setUrl(newObject.getUrl());
        realmObject.setGalleryKey(newObject.getGalleryKey());
        realmObject.setParameter1(newObject.getParameter1());
        return realmObject;
    }

    static HomeSliderBannerResponseLocalStorage update(Realm realm, HomeSliderBannerResponseLocalStorage realmObject, HomeSliderBannerResponseLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setDescription(newObject.getDescription());
        realmObject.setName(newObject.getName());
        realmObject.setGalleryId(newObject.getGalleryId());
        realmObject.setImagePath(newObject.getImagePath());
        realmObject.setUrl(newObject.getUrl());
        realmObject.setGalleryKey(newObject.getGalleryKey());
        realmObject.setParameter1(newObject.getParameter1());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("HomeSliderBannerResponseLocalStorage = [");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{moduleId:");
        stringBuilder.append(getModuleId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{description:");
        stringBuilder.append(getDescription() != null ? getDescription() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(getName() != null ? getName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{galleryId:");
        stringBuilder.append(getGalleryId() != null ? getGalleryId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{imagePath:");
        stringBuilder.append(getImagePath() != null ? getImagePath() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{url:");
        stringBuilder.append(getUrl() != null ? getUrl() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{galleryKey:");
        stringBuilder.append(getGalleryKey() != null ? getGalleryKey() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{parameter1:");
        stringBuilder.append(getParameter1() != null ? getParameter1() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HomeSliderBannerResponseLocalStorageRealmProxy aHomeSliderBannerResponseLocalStorage = (HomeSliderBannerResponseLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aHomeSliderBannerResponseLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aHomeSliderBannerResponseLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aHomeSliderBannerResponseLocalStorage.row.getIndex()) return false;

        return true;
    }

}
