package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.HotDealsLocalStorage;

public class HotDealsLocalStorageRealmProxy extends HotDealsLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_ZONEID;
    private static long INDEX_ZONEAVATAR;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("zoneId");
        fieldNames.add("zoneAvatar");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public String getZoneId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEID);
    }

    @Override
    public void setZoneId(String value) {
        realm.checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field zoneId to null.");
        }
        row.setString(INDEX_ZONEID, (String) value);
    }

    @Override
    public String getZoneAvatar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEAVATAR);
    }

    @Override
    public void setZoneAvatar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEAVATAR);
            return;
        }
        row.setString(INDEX_ZONEAVATAR, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_HotDealsLocalStorage")) {
            Table table = transaction.getTable("class_HotDealsLocalStorage");
            table.addColumn(ColumnType.STRING, "zoneId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneAvatar", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("zoneId"));
            table.setPrimaryKey("zoneId");
            return table;
        }
        return transaction.getTable("class_HotDealsLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_HotDealsLocalStorage")) {
            Table table = transaction.getTable("class_HotDealsLocalStorage");
            if (table.getColumnCount() != 2) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 2 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 2; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type HotDealsLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_ZONEID = table.getColumnIndex("zoneId");
            INDEX_ZONEAVATAR = table.getColumnIndex("zoneAvatar");

            if (!columnTypes.containsKey("zoneId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ZONEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneId' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'zoneId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("zoneId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'zoneId' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("zoneId"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'zoneId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("zoneAvatar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneAvatar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneAvatar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneAvatar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEAVATAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneAvatar' is required. Either set @Required to field 'zoneAvatar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The HotDealsLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_HotDealsLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static HotDealsLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        HotDealsLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(HotDealsLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("zoneId")) {
                long rowIndex = table.findFirstString(pkColumnIndex, json.getString("zoneId"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new HotDealsLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(HotDealsLocalStorage.class);
        }
        if (json.has("zoneId")) {
            if (json.isNull("zoneId")) {
                obj.setZoneId(null);
            } else {
                obj.setZoneId((String) json.getString("zoneId"));
            }
        }
        if (json.has("zoneAvatar")) {
            if (json.isNull("zoneAvatar")) {
                obj.setZoneAvatar(null);
            } else {
                obj.setZoneAvatar((String) json.getString("zoneAvatar"));
            }
        }
        return obj;
    }

    public static HotDealsLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        HotDealsLocalStorage obj = realm.createObject(HotDealsLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("zoneId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneId(null);
                } else {
                    obj.setZoneId((String) reader.nextString());
                }
            } else if (name.equals("zoneAvatar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneAvatar(null);
                } else {
                    obj.setZoneAvatar((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static HotDealsLocalStorage copyOrUpdate(Realm realm, HotDealsLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        HotDealsLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(HotDealsLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (object.getZoneId() == null) {
                throw new IllegalArgumentException("Primary key value must not be null.");
            }
            long rowIndex = table.findFirstString(pkColumnIndex, object.getZoneId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new HotDealsLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static HotDealsLocalStorage copy(Realm realm, HotDealsLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        HotDealsLocalStorage realmObject = realm.createObject(HotDealsLocalStorage.class, newObject.getZoneId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setZoneId(newObject.getZoneId());
        realmObject.setZoneAvatar(newObject.getZoneAvatar());
        return realmObject;
    }

    static HotDealsLocalStorage update(Realm realm, HotDealsLocalStorage realmObject, HotDealsLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setZoneAvatar(newObject.getZoneAvatar());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("HotDealsLocalStorage = [");
        stringBuilder.append("{zoneId:");
        stringBuilder.append(getZoneId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneAvatar:");
        stringBuilder.append(getZoneAvatar() != null ? getZoneAvatar() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HotDealsLocalStorageRealmProxy aHotDealsLocalStorage = (HotDealsLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aHotDealsLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aHotDealsLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aHotDealsLocalStorage.row.getIndex()) return false;

        return true;
    }

}
