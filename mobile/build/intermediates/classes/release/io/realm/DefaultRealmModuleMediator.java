package io.realm;


import android.util.JsonReader;
import io.realm.exceptions.RealmException;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.City;
import vn.lingo.marketplace.shopping.models.District;
import vn.lingo.marketplace.shopping.models.HotDealsLocalStorage;
import vn.lingo.marketplace.shopping.models.OrderHistoryLocalStorage;
import vn.lingo.marketplace.shopping.models.ShipType;
import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;
import vn.lingo.marketplace.shopping.models.ShortcutInformationEntity;
import vn.lingo.marketplace.shopping.models.local.EventByTypeResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.HomeEventResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.HomeSliderBannerResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.IndustryResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.LingoHomePageBigFridayResponseLocalStorage;
import vn.lingo.marketplace.shopping.models.local.LingoHomePageGetBannerResponseLocalStorage;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final List<Class<? extends RealmObject>> MODEL_CLASSES;
    static {
        List<Class<? extends RealmObject>> modelClasses = new ArrayList<Class<? extends RealmObject>>();
        modelClasses.add(LingoHomePageBigFridayResponseLocalStorage.class);
        modelClasses.add(ShortcutInformationEntity.class);
        modelClasses.add(OrderHistoryLocalStorage.class);
        modelClasses.add(ShoppingCartLocalStorage.class);
        modelClasses.add(HotDealsLocalStorage.class);
        modelClasses.add(ShipType.class);
        modelClasses.add(EventByTypeResponseLocalStorage.class);
        modelClasses.add(HomeEventResponseLocalStorage.class);
        modelClasses.add(City.class);
        modelClasses.add(LingoHomePageGetBannerResponseLocalStorage.class);
        modelClasses.add(District.class);
        modelClasses.add(IndustryResponseLocalStorage.class);
        modelClasses.add(HomeSliderBannerResponseLocalStorage.class);
        MODEL_CLASSES = Collections.unmodifiableList(modelClasses);
    }

    @Override
    public Table createTable(Class<? extends RealmObject> clazz, ImplicitTransaction transaction) {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return LingoHomePageBigFridayResponseLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return ShortcutInformationEntityRealmProxy.initTable(transaction);
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return OrderHistoryLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return ShoppingCartLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return HotDealsLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(ShipType.class)) {
            return ShipTypeRealmProxy.initTable(transaction);
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return EventByTypeResponseLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return HomeEventResponseLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(City.class)) {
            return CityRealmProxy.initTable(transaction);
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return LingoHomePageGetBannerResponseLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(District.class)) {
            return DistrictRealmProxy.initTable(transaction);
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return IndustryResponseLocalStorageRealmProxy.initTable(transaction);
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return HomeSliderBannerResponseLocalStorageRealmProxy.initTable(transaction);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void validateTable(Class<? extends RealmObject> clazz, ImplicitTransaction transaction) {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            LingoHomePageBigFridayResponseLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            ShortcutInformationEntityRealmProxy.validateTable(transaction);
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            OrderHistoryLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            ShoppingCartLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            HotDealsLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(ShipType.class)) {
            ShipTypeRealmProxy.validateTable(transaction);
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            EventByTypeResponseLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            HomeEventResponseLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(City.class)) {
            CityRealmProxy.validateTable(transaction);
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            LingoHomePageGetBannerResponseLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(District.class)) {
            DistrictRealmProxy.validateTable(transaction);
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            IndustryResponseLocalStorageRealmProxy.validateTable(transaction);
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            HomeSliderBannerResponseLocalStorageRealmProxy.validateTable(transaction);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public List<String> getFieldNames(Class<? extends RealmObject> clazz) {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return LingoHomePageBigFridayResponseLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return ShortcutInformationEntityRealmProxy.getFieldNames();
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return OrderHistoryLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return ShoppingCartLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return HotDealsLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(ShipType.class)) {
            return ShipTypeRealmProxy.getFieldNames();
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return EventByTypeResponseLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return HomeEventResponseLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(City.class)) {
            return CityRealmProxy.getFieldNames();
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return LingoHomePageGetBannerResponseLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(District.class)) {
            return DistrictRealmProxy.getFieldNames();
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return IndustryResponseLocalStorageRealmProxy.getFieldNames();
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return HomeSliderBannerResponseLocalStorageRealmProxy.getFieldNames();
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public String getTableName(Class<? extends RealmObject> clazz) {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return LingoHomePageBigFridayResponseLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return ShortcutInformationEntityRealmProxy.getTableName();
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return OrderHistoryLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return ShoppingCartLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return HotDealsLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(ShipType.class)) {
            return ShipTypeRealmProxy.getTableName();
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return EventByTypeResponseLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return HomeEventResponseLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(City.class)) {
            return CityRealmProxy.getTableName();
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return LingoHomePageGetBannerResponseLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(District.class)) {
            return DistrictRealmProxy.getTableName();
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return IndustryResponseLocalStorageRealmProxy.getTableName();
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return HomeSliderBannerResponseLocalStorageRealmProxy.getTableName();
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public <E extends RealmObject> E newInstance(Class<E> clazz) {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return clazz.cast(new LingoHomePageBigFridayResponseLocalStorageRealmProxy());
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return clazz.cast(new ShortcutInformationEntityRealmProxy());
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return clazz.cast(new OrderHistoryLocalStorageRealmProxy());
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return clazz.cast(new ShoppingCartLocalStorageRealmProxy());
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return clazz.cast(new HotDealsLocalStorageRealmProxy());
        } else if (clazz.equals(ShipType.class)) {
            return clazz.cast(new ShipTypeRealmProxy());
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return clazz.cast(new EventByTypeResponseLocalStorageRealmProxy());
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return clazz.cast(new HomeEventResponseLocalStorageRealmProxy());
        } else if (clazz.equals(City.class)) {
            return clazz.cast(new CityRealmProxy());
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return clazz.cast(new LingoHomePageGetBannerResponseLocalStorageRealmProxy());
        } else if (clazz.equals(District.class)) {
            return clazz.cast(new DistrictRealmProxy());
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return clazz.cast(new IndustryResponseLocalStorageRealmProxy());
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return clazz.cast(new HomeSliderBannerResponseLocalStorageRealmProxy());
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public List<Class<? extends RealmObject>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public Map<String, Long> getColumnIndices(Class<? extends RealmObject> clazz) {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return LingoHomePageBigFridayResponseLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return ShortcutInformationEntityRealmProxy.getColumnIndices();
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return OrderHistoryLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return ShoppingCartLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return HotDealsLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(ShipType.class)) {
            return ShipTypeRealmProxy.getColumnIndices();
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return EventByTypeResponseLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return HomeEventResponseLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(City.class)) {
            return CityRealmProxy.getColumnIndices();
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return LingoHomePageGetBannerResponseLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(District.class)) {
            return DistrictRealmProxy.getColumnIndices();
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return IndustryResponseLocalStorageRealmProxy.getColumnIndices();
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return HomeSliderBannerResponseLocalStorageRealmProxy.getColumnIndices();
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public <E extends RealmObject> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmObject, RealmObjectProxy> cache) {
        // This cast is correct because obj is either 
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return clazz.cast(LingoHomePageBigFridayResponseLocalStorageRealmProxy.copyOrUpdate(realm, (LingoHomePageBigFridayResponseLocalStorage) obj, update, cache));
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return clazz.cast(ShortcutInformationEntityRealmProxy.copyOrUpdate(realm, (ShortcutInformationEntity) obj, update, cache));
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return clazz.cast(OrderHistoryLocalStorageRealmProxy.copyOrUpdate(realm, (OrderHistoryLocalStorage) obj, update, cache));
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return clazz.cast(ShoppingCartLocalStorageRealmProxy.copyOrUpdate(realm, (ShoppingCartLocalStorage) obj, update, cache));
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return clazz.cast(HotDealsLocalStorageRealmProxy.copyOrUpdate(realm, (HotDealsLocalStorage) obj, update, cache));
        } else if (clazz.equals(ShipType.class)) {
            return clazz.cast(ShipTypeRealmProxy.copyOrUpdate(realm, (ShipType) obj, update, cache));
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return clazz.cast(EventByTypeResponseLocalStorageRealmProxy.copyOrUpdate(realm, (EventByTypeResponseLocalStorage) obj, update, cache));
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return clazz.cast(HomeEventResponseLocalStorageRealmProxy.copyOrUpdate(realm, (HomeEventResponseLocalStorage) obj, update, cache));
        } else if (clazz.equals(City.class)) {
            return clazz.cast(CityRealmProxy.copyOrUpdate(realm, (City) obj, update, cache));
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return clazz.cast(LingoHomePageGetBannerResponseLocalStorageRealmProxy.copyOrUpdate(realm, (LingoHomePageGetBannerResponseLocalStorage) obj, update, cache));
        } else if (clazz.equals(District.class)) {
            return clazz.cast(DistrictRealmProxy.copyOrUpdate(realm, (District) obj, update, cache));
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return clazz.cast(IndustryResponseLocalStorageRealmProxy.copyOrUpdate(realm, (IndustryResponseLocalStorage) obj, update, cache));
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return clazz.cast(HomeSliderBannerResponseLocalStorageRealmProxy.copyOrUpdate(realm, (HomeSliderBannerResponseLocalStorage) obj, update, cache));
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public <E extends RealmObject> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return clazz.cast(LingoHomePageBigFridayResponseLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return clazz.cast(ShortcutInformationEntityRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return clazz.cast(OrderHistoryLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return clazz.cast(ShoppingCartLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return clazz.cast(HotDealsLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(ShipType.class)) {
            return clazz.cast(ShipTypeRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return clazz.cast(EventByTypeResponseLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return clazz.cast(HomeEventResponseLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(City.class)) {
            return clazz.cast(CityRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return clazz.cast(LingoHomePageGetBannerResponseLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(District.class)) {
            return clazz.cast(DistrictRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return clazz.cast(IndustryResponseLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return clazz.cast(HomeSliderBannerResponseLocalStorageRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public <E extends RealmObject> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(LingoHomePageBigFridayResponseLocalStorage.class)) {
            return clazz.cast(LingoHomePageBigFridayResponseLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(ShortcutInformationEntity.class)) {
            return clazz.cast(ShortcutInformationEntityRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(OrderHistoryLocalStorage.class)) {
            return clazz.cast(OrderHistoryLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(ShoppingCartLocalStorage.class)) {
            return clazz.cast(ShoppingCartLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(HotDealsLocalStorage.class)) {
            return clazz.cast(HotDealsLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(ShipType.class)) {
            return clazz.cast(ShipTypeRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(EventByTypeResponseLocalStorage.class)) {
            return clazz.cast(EventByTypeResponseLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(HomeEventResponseLocalStorage.class)) {
            return clazz.cast(HomeEventResponseLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(City.class)) {
            return clazz.cast(CityRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(LingoHomePageGetBannerResponseLocalStorage.class)) {
            return clazz.cast(LingoHomePageGetBannerResponseLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(District.class)) {
            return clazz.cast(DistrictRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(IndustryResponseLocalStorage.class)) {
            return clazz.cast(IndustryResponseLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(HomeSliderBannerResponseLocalStorage.class)) {
            return clazz.cast(HomeSliderBannerResponseLocalStorageRealmProxy.createUsingJsonStream(realm, reader));
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

}
