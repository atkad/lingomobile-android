package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.local.EventByTypeResponseLocalStorage;

public class EventByTypeResponseLocalStorageRealmProxy extends EventByTypeResponseLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_ID;
    private static long INDEX_MODULEID;
    private static long INDEX_EVENTAVATAR;
    private static long INDEX_DES;
    private static long INDEX_NAME;
    private static long INDEX_EVENTID;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("id");
        fieldNames.add("moduleId");
        fieldNames.add("eventAvatar");
        fieldNames.add("des");
        fieldNames.add("name");
        fieldNames.add("eventId");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_ID);
    }

    @Override
    public void setId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_ID, (long) value);
    }

    @Override
    public int getModuleId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_MODULEID);
    }

    @Override
    public void setModuleId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_MODULEID, (long) value);
    }

    @Override
    public String getEventAvatar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_EVENTAVATAR);
    }

    @Override
    public void setEventAvatar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_EVENTAVATAR);
            return;
        }
        row.setString(INDEX_EVENTAVATAR, (String) value);
    }

    @Override
    public String getDes() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_DES);
    }

    @Override
    public void setDes(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_DES);
            return;
        }
        row.setString(INDEX_DES, (String) value);
    }

    @Override
    public String getName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_NAME);
    }

    @Override
    public void setName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_NAME);
            return;
        }
        row.setString(INDEX_NAME, (String) value);
    }

    @Override
    public String getEventId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_EVENTID);
    }

    @Override
    public void setEventId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_EVENTID);
            return;
        }
        row.setString(INDEX_EVENTID, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_EventByTypeResponseLocalStorage")) {
            Table table = transaction.getTable("class_EventByTypeResponseLocalStorage");
            table.addColumn(ColumnType.INTEGER, "id", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "moduleId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "eventAvatar", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "des", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "name", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "eventId", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("id"));
            table.setPrimaryKey("id");
            return table;
        }
        return transaction.getTable("class_EventByTypeResponseLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_EventByTypeResponseLocalStorage")) {
            Table table = transaction.getTable("class_EventByTypeResponseLocalStorage");
            if (table.getColumnCount() != 6) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 6 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 6; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type EventByTypeResponseLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_ID = table.getColumnIndex("id");
            INDEX_MODULEID = table.getColumnIndex("moduleId");
            INDEX_EVENTAVATAR = table.getColumnIndex("eventAvatar");
            INDEX_DES = table.getColumnIndex("des");
            INDEX_NAME = table.getColumnIndex("name");
            INDEX_EVENTID = table.getColumnIndex("eventId");

            if (!columnTypes.containsKey("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("id") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'id' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'id' does support null values in the existing Realm file. Use corresponding boxed type for field 'id' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'id' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("id"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("moduleId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'moduleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("moduleId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'moduleId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_MODULEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'moduleId' does support null values in the existing Realm file. Use corresponding boxed type for field 'moduleId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("eventAvatar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'eventAvatar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("eventAvatar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'eventAvatar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_EVENTAVATAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'eventAvatar' is required. Either set @Required to field 'eventAvatar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("des")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'des' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("des") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'des' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_DES)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'des' is required. Either set @Required to field 'des' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("name")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'name' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("name") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'name' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_NAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'name' is required. Either set @Required to field 'name' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("eventId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'eventId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("eventId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'eventId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_EVENTID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'eventId' is required. Either set @Required to field 'eventId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The EventByTypeResponseLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_EventByTypeResponseLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static EventByTypeResponseLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        EventByTypeResponseLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(EventByTypeResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("id")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new EventByTypeResponseLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(EventByTypeResponseLocalStorage.class);
        }
        if (json.has("id")) {
            if (json.isNull("id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
            } else {
                obj.setId((int) json.getInt("id"));
            }
        }
        if (json.has("moduleId")) {
            if (json.isNull("moduleId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
            } else {
                obj.setModuleId((int) json.getInt("moduleId"));
            }
        }
        if (json.has("eventAvatar")) {
            if (json.isNull("eventAvatar")) {
                obj.setEventAvatar(null);
            } else {
                obj.setEventAvatar((String) json.getString("eventAvatar"));
            }
        }
        if (json.has("des")) {
            if (json.isNull("des")) {
                obj.setDes(null);
            } else {
                obj.setDes((String) json.getString("des"));
            }
        }
        if (json.has("name")) {
            if (json.isNull("name")) {
                obj.setName(null);
            } else {
                obj.setName((String) json.getString("name"));
            }
        }
        if (json.has("eventId")) {
            if (json.isNull("eventId")) {
                obj.setEventId(null);
            } else {
                obj.setEventId((String) json.getString("eventId"));
            }
        }
        return obj;
    }

    public static EventByTypeResponseLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        EventByTypeResponseLocalStorage obj = realm.createObject(EventByTypeResponseLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
                } else {
                    obj.setId((int) reader.nextInt());
                }
            } else if (name.equals("moduleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
                } else {
                    obj.setModuleId((int) reader.nextInt());
                }
            } else if (name.equals("eventAvatar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setEventAvatar(null);
                } else {
                    obj.setEventAvatar((String) reader.nextString());
                }
            } else if (name.equals("des")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setDes(null);
                } else {
                    obj.setDes((String) reader.nextString());
                }
            } else if (name.equals("name")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setName(null);
                } else {
                    obj.setName((String) reader.nextString());
                }
            } else if (name.equals("eventId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setEventId(null);
                } else {
                    obj.setEventId((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static EventByTypeResponseLocalStorage copyOrUpdate(Realm realm, EventByTypeResponseLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        EventByTypeResponseLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(EventByTypeResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new EventByTypeResponseLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static EventByTypeResponseLocalStorage copy(Realm realm, EventByTypeResponseLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        EventByTypeResponseLocalStorage realmObject = realm.createObject(EventByTypeResponseLocalStorage.class, newObject.getId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setId(newObject.getId());
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setEventAvatar(newObject.getEventAvatar());
        realmObject.setDes(newObject.getDes());
        realmObject.setName(newObject.getName());
        realmObject.setEventId(newObject.getEventId());
        return realmObject;
    }

    static EventByTypeResponseLocalStorage update(Realm realm, EventByTypeResponseLocalStorage realmObject, EventByTypeResponseLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setEventAvatar(newObject.getEventAvatar());
        realmObject.setDes(newObject.getDes());
        realmObject.setName(newObject.getName());
        realmObject.setEventId(newObject.getEventId());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("EventByTypeResponseLocalStorage = [");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{moduleId:");
        stringBuilder.append(getModuleId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{eventAvatar:");
        stringBuilder.append(getEventAvatar() != null ? getEventAvatar() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{des:");
        stringBuilder.append(getDes() != null ? getDes() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(getName() != null ? getName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{eventId:");
        stringBuilder.append(getEventId() != null ? getEventId() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventByTypeResponseLocalStorageRealmProxy aEventByTypeResponseLocalStorage = (EventByTypeResponseLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aEventByTypeResponseLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aEventByTypeResponseLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aEventByTypeResponseLocalStorage.row.getIndex()) return false;

        return true;
    }

}
