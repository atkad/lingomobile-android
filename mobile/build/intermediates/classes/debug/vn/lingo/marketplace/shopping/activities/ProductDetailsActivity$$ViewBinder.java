// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ProductDetailsActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.ProductDetailsActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624098, "field 'frameLayoutImageMetadata'");
    target.frameLayoutImageMetadata = finder.castView(view, 2131624098, "field 'frameLayoutImageMetadata'");
    view = finder.findRequiredView(source, 2131624508, "field 'autoScrollViewPager'");
    target.autoScrollViewPager = finder.castView(view, 2131624508, "field 'autoScrollViewPager'");
    view = finder.findRequiredView(source, 2131624509, "field 'circlePageIndicator'");
    target.circlePageIndicator = finder.castView(view, 2131624509, "field 'circlePageIndicator'");
    view = finder.findRequiredView(source, 2131623979, "field 'observableScrollView'");
    target.observableScrollView = finder.castView(view, 2131623979, "field 'observableScrollView'");
    view = finder.findRequiredView(source, 2131624476, "field 'productDetailsDescription'");
    target.productDetailsDescription = finder.castView(view, 2131624476, "field 'productDetailsDescription'");
    view = finder.findRequiredView(source, 2131624487, "field 'productBrand'");
    target.productBrand = finder.castView(view, 2131624487, "field 'productBrand'");
    view = finder.findRequiredView(source, 2131624480, "field 'productCode'");
    target.productCode = finder.castView(view, 2131624480, "field 'productCode'");
    view = finder.findRequiredView(source, 2131624481, "field 'productPrice'");
    target.productPrice = finder.castView(view, 2131624481, "field 'productPrice'");
    view = finder.findRequiredView(source, 2131624482, "field 'productPriceGoldenHourLabel'");
    target.productPriceGoldenHourLabel = finder.castView(view, 2131624482, "field 'productPriceGoldenHourLabel'");
    view = finder.findRequiredView(source, 2131624485, "field 'productMarketPrice'");
    target.productMarketPrice = finder.castView(view, 2131624485, "field 'productMarketPrice'");
    view = finder.findRequiredView(source, 2131624494, "field 'recyclerViewRelatedProducts'");
    target.recyclerViewRelatedProducts = finder.castView(view, 2131624494, "field 'recyclerViewRelatedProducts'");
    view = finder.findRequiredView(source, 2131624492, "field 'recyclerViewProductDetailsGeneralProductInformation'");
    target.recyclerViewProductDetailsGeneralProductInformation = finder.castView(view, 2131624492, "field 'recyclerViewProductDetailsGeneralProductInformation'");
    view = finder.findRequiredView(source, 2131624499, "field 'recyclerViewProductForReview'");
    target.recyclerViewProductForReview = finder.castView(view, 2131624499, "field 'recyclerViewProductForReview'");
    view = finder.findRequiredView(source, 2131624330, "field 'ratingBar'");
    target.ratingBar = finder.castView(view, 2131624330, "field 'ratingBar'");
    view = finder.findRequiredView(source, 2131624491, "field 'appCompatButtonBuyNow'");
    target.appCompatButtonBuyNow = finder.castView(view, 2131624491, "field 'appCompatButtonBuyNow'");
    view = finder.findRequiredView(source, 2131624490, "field 'appCompatButtonAddToCart'");
    target.appCompatButtonAddToCart = finder.castView(view, 2131624490, "field 'appCompatButtonAddToCart'");
    view = finder.findRequiredView(source, 2131624496, "field 'appCompatTextViewDetailedInformation'");
    target.appCompatTextViewDetailedInformation = finder.castView(view, 2131624496, "field 'appCompatTextViewDetailedInformation'");
    view = finder.findRequiredView(source, 2131624489, "field 'autoScrollViewPagerGiftProducts'");
    target.autoScrollViewPagerGiftProducts = finder.castView(view, 2131624489, "field 'autoScrollViewPagerGiftProducts'");
    view = finder.findRequiredView(source, 2131624486, "field 'appCompatTextViewScores'");
    target.appCompatTextViewScores = finder.castView(view, 2131624486, "field 'appCompatTextViewScores'");
    view = finder.findRequiredView(source, 2131624478, "field 'countdownViewGoldenHour'");
    target.countdownViewGoldenHour = finder.castView(view, 2131624478, "field 'countdownViewGoldenHour'");
    view = finder.findRequiredView(source, 2131624510, "field 'appCompatTextViewDiscount'");
    target.appCompatTextViewDiscount = finder.castView(view, 2131624510, "field 'appCompatTextViewDiscount'");
    view = finder.findRequiredView(source, 2131624477, "field 'linearLayoutCountDownGoldenHour'");
    target.linearLayoutCountDownGoldenHour = finder.castView(view, 2131624477, "field 'linearLayoutCountDownGoldenHour'");
    view = finder.findRequiredView(source, 2131624488, "field 'linearLayoutViewPagerAutoScrollGiftProducts'");
    target.linearLayoutViewPagerAutoScrollGiftProducts = finder.castView(view, 2131624488, "field 'linearLayoutViewPagerAutoScrollGiftProducts'");
    view = finder.findRequiredView(source, 2131624500, "field 'linearLayoutMoreProductReviews'");
    target.linearLayoutMoreProductReviews = finder.castView(view, 2131624500, "field 'linearLayoutMoreProductReviews'");
    view = finder.findRequiredView(source, 2131624497, "field 'appCompatTextViewProductDetailedInformationViewMore'");
    target.appCompatTextViewProductDetailedInformationViewMore = finder.castView(view, 2131624497, "field 'appCompatTextViewProductDetailedInformationViewMore'");
  }

  @Override public void unbind(T target) {
    target.toolbar = null;
    target.frameLayoutImageMetadata = null;
    target.autoScrollViewPager = null;
    target.circlePageIndicator = null;
    target.observableScrollView = null;
    target.productDetailsDescription = null;
    target.productBrand = null;
    target.productCode = null;
    target.productPrice = null;
    target.productPriceGoldenHourLabel = null;
    target.productMarketPrice = null;
    target.recyclerViewRelatedProducts = null;
    target.recyclerViewProductDetailsGeneralProductInformation = null;
    target.recyclerViewProductForReview = null;
    target.ratingBar = null;
    target.appCompatButtonBuyNow = null;
    target.appCompatButtonAddToCart = null;
    target.appCompatTextViewDetailedInformation = null;
    target.autoScrollViewPagerGiftProducts = null;
    target.appCompatTextViewScores = null;
    target.countdownViewGoldenHour = null;
    target.appCompatTextViewDiscount = null;
    target.linearLayoutCountDownGoldenHour = null;
    target.linearLayoutViewPagerAutoScrollGiftProducts = null;
    target.linearLayoutMoreProductReviews = null;
    target.appCompatTextViewProductDetailedInformationViewMore = null;
  }
}
