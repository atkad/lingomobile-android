// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder.search;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CustomerPaymentRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.search.CustomerPaymentRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624303, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624303, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624304, "field 'productName'");
    target.productName = finder.castView(view, 2131624304, "field 'productName'");
    view = finder.findRequiredView(source, 2131624308, "field 'productQuantity'");
    target.productQuantity = finder.castView(view, 2131624308, "field 'productQuantity'");
    view = finder.findRequiredView(source, 2131624309, "field 'addProduct'");
    target.addProduct = finder.castView(view, 2131624309, "field 'addProduct'");
    view = finder.findRequiredView(source, 2131624307, "field 'subProduct'");
    target.subProduct = finder.castView(view, 2131624307, "field 'subProduct'");
    view = finder.findRequiredView(source, 2131624301, "field 'closeProduct'");
    target.closeProduct = finder.castView(view, 2131624301, "field 'closeProduct'");
    view = finder.findRequiredView(source, 2131624306, "field 'removeProduct'");
    target.removeProduct = finder.castView(view, 2131624306, "field 'removeProduct'");
    view = finder.findRequiredView(source, 2131624305, "field 'productPrice'");
    target.productPrice = finder.castView(view, 2131624305, "field 'productPrice'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.productName = null;
    target.productQuantity = null;
    target.addProduct = null;
    target.subProduct = null;
    target.closeProduct = null;
    target.removeProduct = null;
    target.productPrice = null;
  }
}
