// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ShoppingCartRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.ShoppingCartRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624586, "field 'appCompatCheckBox'");
    target.appCompatCheckBox = finder.castView(view, 2131624586, "field 'appCompatCheckBox'");
    view = finder.findRequiredView(source, 2131624587, "field 'appCompatImageViewDeletedOrder'");
    target.appCompatImageViewDeletedOrder = finder.castView(view, 2131624587, "field 'appCompatImageViewDeletedOrder'");
    view = finder.findRequiredView(source, 2131624588, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624588, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624589, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624589, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624590, "field 'appCompatTextViewMarketPrice'");
    target.appCompatTextViewMarketPrice = finder.castView(view, 2131624590, "field 'appCompatTextViewMarketPrice'");
    view = finder.findRequiredView(source, 2131624591, "field 'appCompatTextViewPrice'");
    target.appCompatTextViewPrice = finder.castView(view, 2131624591, "field 'appCompatTextViewPrice'");
    view = finder.findRequiredView(source, 2131624593, "field 'appCompatTextViewLabelQuantity'");
    target.appCompatTextViewLabelQuantity = finder.castView(view, 2131624593, "field 'appCompatTextViewLabelQuantity'");
    view = finder.findRequiredView(source, 2131624584, "field 'relativeLayoutRowItem'");
    target.relativeLayoutRowItem = finder.castView(view, 2131624584, "field 'relativeLayoutRowItem'");
    view = finder.findRequiredView(source, 2131624592, "field 'discreteSeekBar'");
    target.discreteSeekBar = finder.castView(view, 2131624592, "field 'discreteSeekBar'");
  }

  @Override public void unbind(T target) {
    target.appCompatCheckBox = null;
    target.appCompatImageViewDeletedOrder = null;
    target.imageView = null;
    target.appCompatTextViewDescription = null;
    target.appCompatTextViewMarketPrice = null;
    target.appCompatTextViewPrice = null;
    target.appCompatTextViewLabelQuantity = null;
    target.relativeLayoutRowItem = null;
    target.discreteSeekBar = null;
  }
}
