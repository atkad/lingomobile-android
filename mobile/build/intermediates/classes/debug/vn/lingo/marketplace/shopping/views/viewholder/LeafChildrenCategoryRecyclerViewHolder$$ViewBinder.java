// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LeafChildrenCategoryRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.LeafChildrenCategoryRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624322, "field 'relativeLayoutRowItem'");
    target.relativeLayoutRowItem = finder.castView(view, 2131624322, "field 'relativeLayoutRowItem'");
    view = finder.findRequiredView(source, 2131624324, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624324, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624327, "field 'leafChildrenCategoryProductionTitle'");
    target.leafChildrenCategoryProductionTitle = finder.castView(view, 2131624327, "field 'leafChildrenCategoryProductionTitle'");
    view = finder.findRequiredView(source, 2131624328, "field 'leafChildrenCategoriesPrices'");
    target.leafChildrenCategoriesPrices = finder.castView(view, 2131624328, "field 'leafChildrenCategoriesPrices'");
    view = finder.findRequiredView(source, 2131624329, "field 'leafChildrenCategoriesMarketPrice'");
    target.leafChildrenCategoriesMarketPrice = finder.castView(view, 2131624329, "field 'leafChildrenCategoriesMarketPrice'");
    view = finder.findRequiredView(source, 2131624330, "field 'leafChildrenCategoryRatingBar'");
    target.leafChildrenCategoryRatingBar = finder.castView(view, 2131624330, "field 'leafChildrenCategoryRatingBar'");
    view = finder.findRequiredView(source, 2131624325, "field 'leafChildrenCategoryLinearLayoutPercent'");
    target.leafChildrenCategoryLinearLayoutPercent = finder.castView(view, 2131624325, "field 'leafChildrenCategoryLinearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624326, "field 'leafChildrenCategoryPercent'");
    target.leafChildrenCategoryPercent = finder.castView(view, 2131624326, "field 'leafChildrenCategoryPercent'");
  }

  @Override public void unbind(T target) {
    target.relativeLayoutRowItem = null;
    target.imageView = null;
    target.leafChildrenCategoryProductionTitle = null;
    target.leafChildrenCategoriesPrices = null;
    target.leafChildrenCategoriesMarketPrice = null;
    target.leafChildrenCategoryRatingBar = null;
    target.leafChildrenCategoryLinearLayoutPercent = null;
    target.leafChildrenCategoryPercent = null;
  }
}
