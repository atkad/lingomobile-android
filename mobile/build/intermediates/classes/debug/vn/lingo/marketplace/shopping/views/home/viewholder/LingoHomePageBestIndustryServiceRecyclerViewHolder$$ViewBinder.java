// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LingoHomePageBestIndustryServiceRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.viewholder.LingoHomePageBestIndustryServiceRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624331, "field 'cardView'");
    target.cardView = finder.castView(view, 2131624331, "field 'cardView'");
    view = finder.findRequiredView(source, 2131624333, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624333, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624342, "field 'marketPrice'");
    target.marketPrice = finder.castView(view, 2131624342, "field 'marketPrice'");
    view = finder.findRequiredView(source, 2131624341, "field 'price'");
    target.price = finder.castView(view, 2131624341, "field 'price'");
    view = finder.findRequiredView(source, 2131624340, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624340, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624334, "field 'linearLayoutPercent'");
    target.linearLayoutPercent = finder.castView(view, 2131624334, "field 'linearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624335, "field 'appCompatTextViewPercent'");
    target.appCompatTextViewPercent = finder.castView(view, 2131624335, "field 'appCompatTextViewPercent'");
    view = finder.findRequiredView(source, 2131624336, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624336, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624337, "field 'appCompatTextViewHappyHourDiscount'");
    target.appCompatTextViewHappyHourDiscount = finder.castView(view, 2131624337, "field 'appCompatTextViewHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624338, "field 'linearLayoutDiscountGiftCards'");
    target.linearLayoutDiscountGiftCards = finder.castView(view, 2131624338, "field 'linearLayoutDiscountGiftCards'");
    view = finder.findRequiredView(source, 2131624339, "field 'appCompatTextViewDiscountGiftCards'");
    target.appCompatTextViewDiscountGiftCards = finder.castView(view, 2131624339, "field 'appCompatTextViewDiscountGiftCards'");
  }

  @Override public void unbind(T target) {
    target.cardView = null;
    target.imageView = null;
    target.marketPrice = null;
    target.price = null;
    target.appCompatTextViewDescription = null;
    target.linearLayoutPercent = null;
    target.appCompatTextViewPercent = null;
    target.linearLayoutHappyHourDiscount = null;
    target.appCompatTextViewHappyHourDiscount = null;
    target.linearLayoutDiscountGiftCards = null;
    target.appCompatTextViewDiscountGiftCards = null;
  }
}
