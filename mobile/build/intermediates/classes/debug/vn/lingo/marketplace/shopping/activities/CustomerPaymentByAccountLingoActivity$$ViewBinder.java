// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CustomerPaymentByAccountLingoActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.CustomerPaymentByAccountLingoActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624283, "field 'webView'");
    target.webView = finder.castView(view, 2131624283, "field 'webView'");
  }

  @Override public void unbind(T target) {
    target.webView = null;
  }
}
