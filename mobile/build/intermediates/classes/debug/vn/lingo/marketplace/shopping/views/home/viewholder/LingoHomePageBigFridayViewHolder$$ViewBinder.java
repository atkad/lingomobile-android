// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LingoHomePageBigFridayViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.viewholder.LingoHomePageBigFridayViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624363, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624363, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624364, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624364, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624365, "field 'appCompatTextViewPrice'");
    target.appCompatTextViewPrice = finder.castView(view, 2131624365, "field 'appCompatTextViewPrice'");
    view = finder.findRequiredView(source, 2131624366, "field 'appCompatTextViewMarketPrice'");
    target.appCompatTextViewMarketPrice = finder.castView(view, 2131624366, "field 'appCompatTextViewMarketPrice'");
    view = finder.findRequiredView(source, 2131624367, "field 'appCompatTextViewPercent'");
    target.appCompatTextViewPercent = finder.castView(view, 2131624367, "field 'appCompatTextViewPercent'");
    view = finder.findRequiredView(source, 2131624369, "field 'appCompatTextViewHappyHourDiscount'");
    target.appCompatTextViewHappyHourDiscount = finder.castView(view, 2131624369, "field 'appCompatTextViewHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624371, "field 'appCompatTextViewDiscountGiftCard'");
    target.appCompatTextViewDiscountGiftCard = finder.castView(view, 2131624371, "field 'appCompatTextViewDiscountGiftCard'");
    view = finder.findRequiredView(source, 2131624372, "field 'view'");
    target.view = view;
    view = finder.findRequiredView(source, 2131624368, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624368, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624370, "field 'linearLayoutDiscountGiftCard'");
    target.linearLayoutDiscountGiftCard = finder.castView(view, 2131624370, "field 'linearLayoutDiscountGiftCard'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.appCompatTextViewDescription = null;
    target.appCompatTextViewPrice = null;
    target.appCompatTextViewMarketPrice = null;
    target.appCompatTextViewPercent = null;
    target.appCompatTextViewHappyHourDiscount = null;
    target.appCompatTextViewDiscountGiftCard = null;
    target.view = null;
    target.linearLayoutHappyHourDiscount = null;
    target.linearLayoutDiscountGiftCard = null;
  }
}
