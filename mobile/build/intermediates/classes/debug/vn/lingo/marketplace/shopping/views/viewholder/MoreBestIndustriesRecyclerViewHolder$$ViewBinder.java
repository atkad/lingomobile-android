// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MoreBestIndustriesRecyclerViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.viewholder.MoreBestIndustriesRecyclerViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624423, "field 'cardView'");
    target.cardView = finder.castView(view, 2131624423, "field 'cardView'");
    view = finder.findRequiredView(source, 2131624425, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624425, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624435, "field 'marketPrice'");
    target.marketPrice = finder.castView(view, 2131624435, "field 'marketPrice'");
    view = finder.findRequiredView(source, 2131624434, "field 'price'");
    target.price = finder.castView(view, 2131624434, "field 'price'");
    view = finder.findRequiredView(source, 2131624427, "field 'percent'");
    target.percent = finder.castView(view, 2131624427, "field 'percent'");
    view = finder.findRequiredView(source, 2131624433, "field 'appCompatTextViewDescription'");
    target.appCompatTextViewDescription = finder.castView(view, 2131624433, "field 'appCompatTextViewDescription'");
    view = finder.findRequiredView(source, 2131624429, "field 'appCompatTextViewHappyHourDiscount'");
    target.appCompatTextViewHappyHourDiscount = finder.castView(view, 2131624429, "field 'appCompatTextViewHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624431, "field 'appCompatTextViewDiscountGiftCards'");
    target.appCompatTextViewDiscountGiftCards = finder.castView(view, 2131624431, "field 'appCompatTextViewDiscountGiftCards'");
    view = finder.findRequiredView(source, 2131624426, "field 'linearLayoutPercent'");
    target.linearLayoutPercent = finder.castView(view, 2131624426, "field 'linearLayoutPercent'");
    view = finder.findRequiredView(source, 2131624428, "field 'linearLayoutHappyHourDiscount'");
    target.linearLayoutHappyHourDiscount = finder.castView(view, 2131624428, "field 'linearLayoutHappyHourDiscount'");
    view = finder.findRequiredView(source, 2131624430, "field 'linearLayoutDiscountGiftCards'");
    target.linearLayoutDiscountGiftCards = finder.castView(view, 2131624430, "field 'linearLayoutDiscountGiftCards'");
  }

  @Override public void unbind(T target) {
    target.cardView = null;
    target.imageView = null;
    target.marketPrice = null;
    target.price = null;
    target.percent = null;
    target.appCompatTextViewDescription = null;
    target.appCompatTextViewHappyHourDiscount = null;
    target.appCompatTextViewDiscountGiftCards = null;
    target.linearLayoutPercent = null;
    target.linearLayoutHappyHourDiscount = null;
    target.linearLayoutDiscountGiftCards = null;
  }
}
