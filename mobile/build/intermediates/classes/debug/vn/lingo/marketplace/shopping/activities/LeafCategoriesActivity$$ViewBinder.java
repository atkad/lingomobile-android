// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.activities;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class LeafCategoriesActivity$$ViewBinder<T extends vn.lingo.marketplace.shopping.activities.LeafCategoriesActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624092, "field 'searchResultList'");
    target.searchResultList = finder.castView(view, 2131624092, "field 'searchResultList'");
    view = finder.findRequiredView(source, 2131624099, "field 'toolbar'");
    target.toolbar = finder.castView(view, 2131624099, "field 'toolbar'");
    view = finder.findRequiredView(source, 2131624089, "field 'cardView'");
    target.cardView = finder.castView(view, 2131624089, "field 'cardView'");
  }

  @Override public void unbind(T target) {
    target.searchResultList = null;
    target.toolbar = null;
    target.cardView = null;
  }
}
