// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomePageCategoriesFragment$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.HomePageCategoriesFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624190, "field 'recyclerViewHomePageSubIndustries'");
    target.recyclerViewHomePageSubIndustries = finder.castView(view, 2131624190, "field 'recyclerViewHomePageSubIndustries'");
  }

  @Override public void unbind(T target) {
    target.recyclerViewHomePageSubIndustries = null;
  }
}
