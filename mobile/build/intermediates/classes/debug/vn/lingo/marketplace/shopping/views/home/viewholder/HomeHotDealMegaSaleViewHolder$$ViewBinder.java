// Generated code from Butter Knife. Do not modify!
package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeHotDealMegaSaleViewHolder$$ViewBinder<T extends vn.lingo.marketplace.shopping.views.home.viewholder.HomeHotDealMegaSaleViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131624227, "field 'imageView'");
    target.imageView = finder.castView(view, 2131624227, "field 'imageView'");
    view = finder.findRequiredView(source, 2131624226, "field 'relativeLayoutMegaSaleId'");
    target.relativeLayoutMegaSaleId = finder.castView(view, 2131624226, "field 'relativeLayoutMegaSaleId'");
  }

  @Override public void unbind(T target) {
    target.imageView = null;
    target.relativeLayoutMegaSaleId = null;
  }
}
