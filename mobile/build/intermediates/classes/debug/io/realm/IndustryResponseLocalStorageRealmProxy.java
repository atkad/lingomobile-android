package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.local.IndustryResponseLocalStorage;

public class IndustryResponseLocalStorageRealmProxy extends IndustryResponseLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_ID;
    private static long INDEX_MODULEID;
    private static long INDEX_EXTENTSTYLE;
    private static long INDEX_ZONEVISIBLEBOTTOM;
    private static long INDEX_ZONEFOOTER;
    private static long INDEX_ZONEAVATARMENU;
    private static long INDEX_MAPPINGURL;
    private static long INDEX_ZONEALIAS;
    private static long INDEX_ISUSEBACKGROUND;
    private static long INDEX_ZONEAVATAR;
    private static long INDEX_ZONETITLE;
    private static long INDEX_ZONELANG;
    private static long INDEX_ZONEID;
    private static long INDEX_ZONEMORE;
    private static long INDEX_ZONENAME;
    private static long INDEX_MAPPINGTYPE;
    private static long INDEX_ZONEVISIBLEINTOP;
    private static long INDEX_ZONEVISIBLEINNAVBAR;
    private static long INDEX_ZONEAVATARMAIN;
    private static long INDEX_PORTALTABCONTROLID;
    private static long INDEX_VISIBLEONHOME;
    private static long INDEX_BACKGROUNDURL;
    private static long INDEX_ZONELEVEL;
    private static long INDEX_ZONEKEYWORDS;
    private static long INDEX_MAPPINGZONEID;
    private static long INDEX_ZONETYPE;
    private static long INDEX_ZONEHEADER;
    private static long INDEX_ZONEDES;
    private static long INDEX_ZONEPARENTID;
    private static long INDEX_ZONEVISIBLECENTER;
    private static long INDEX_ZONEVISIBLEINTAB;
    private static long INDEX_ZONEPAGE;
    private static long INDEX_IMGCOORDINATE;
    private static long INDEX_ZONEPRIORITY;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("id");
        fieldNames.add("moduleId");
        fieldNames.add("extentStyle");
        fieldNames.add("zoneVisibleBottom");
        fieldNames.add("zoneFooter");
        fieldNames.add("zoneAvatarMenu");
        fieldNames.add("mappingUrl");
        fieldNames.add("zoneAlias");
        fieldNames.add("isUseBackGround");
        fieldNames.add("zoneAvatar");
        fieldNames.add("zoneTitle");
        fieldNames.add("zoneLang");
        fieldNames.add("zoneId");
        fieldNames.add("zoneMore");
        fieldNames.add("zoneName");
        fieldNames.add("mappingType");
        fieldNames.add("zoneVisibleInTop");
        fieldNames.add("zoneVisibleInNavBar");
        fieldNames.add("zoneAvatarMain");
        fieldNames.add("portalTabControlId");
        fieldNames.add("visibleOnHome");
        fieldNames.add("backgroundUrl");
        fieldNames.add("zoneLevel");
        fieldNames.add("zoneKeywords");
        fieldNames.add("mappingZoneId");
        fieldNames.add("zoneType");
        fieldNames.add("zoneHeader");
        fieldNames.add("zoneDes");
        fieldNames.add("zoneParentId");
        fieldNames.add("zoneVisibleCenter");
        fieldNames.add("zoneVisibleInTab");
        fieldNames.add("zonePage");
        fieldNames.add("imgCoordinate");
        fieldNames.add("zonePriority");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_ID);
    }

    @Override
    public void setId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_ID, (long) value);
    }

    @Override
    public int getModuleId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_MODULEID);
    }

    @Override
    public void setModuleId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_MODULEID, (long) value);
    }

    @Override
    public String getExtentStyle() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_EXTENTSTYLE);
    }

    @Override
    public void setExtentStyle(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_EXTENTSTYLE);
            return;
        }
        row.setString(INDEX_EXTENTSTYLE, (String) value);
    }

    @Override
    public String getZoneVisibleBottom() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEVISIBLEBOTTOM);
    }

    @Override
    public void setZoneVisibleBottom(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEVISIBLEBOTTOM);
            return;
        }
        row.setString(INDEX_ZONEVISIBLEBOTTOM, (String) value);
    }

    @Override
    public String getZoneFooter() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEFOOTER);
    }

    @Override
    public void setZoneFooter(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEFOOTER);
            return;
        }
        row.setString(INDEX_ZONEFOOTER, (String) value);
    }

    @Override
    public String getZoneAvatarMenu() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEAVATARMENU);
    }

    @Override
    public void setZoneAvatarMenu(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEAVATARMENU);
            return;
        }
        row.setString(INDEX_ZONEAVATARMENU, (String) value);
    }

    @Override
    public String getMappingUrl() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MAPPINGURL);
    }

    @Override
    public void setMappingUrl(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MAPPINGURL);
            return;
        }
        row.setString(INDEX_MAPPINGURL, (String) value);
    }

    @Override
    public String getZoneAlias() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEALIAS);
    }

    @Override
    public void setZoneAlias(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEALIAS);
            return;
        }
        row.setString(INDEX_ZONEALIAS, (String) value);
    }

    @Override
    public String getIsUseBackGround() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ISUSEBACKGROUND);
    }

    @Override
    public void setIsUseBackGround(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ISUSEBACKGROUND);
            return;
        }
        row.setString(INDEX_ISUSEBACKGROUND, (String) value);
    }

    @Override
    public String getZoneAvatar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEAVATAR);
    }

    @Override
    public void setZoneAvatar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEAVATAR);
            return;
        }
        row.setString(INDEX_ZONEAVATAR, (String) value);
    }

    @Override
    public String getZoneTitle() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONETITLE);
    }

    @Override
    public void setZoneTitle(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONETITLE);
            return;
        }
        row.setString(INDEX_ZONETITLE, (String) value);
    }

    @Override
    public String getZoneLang() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONELANG);
    }

    @Override
    public void setZoneLang(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONELANG);
            return;
        }
        row.setString(INDEX_ZONELANG, (String) value);
    }

    @Override
    public String getZoneId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEID);
    }

    @Override
    public void setZoneId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEID);
            return;
        }
        row.setString(INDEX_ZONEID, (String) value);
    }

    @Override
    public String getZoneMore() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEMORE);
    }

    @Override
    public void setZoneMore(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEMORE);
            return;
        }
        row.setString(INDEX_ZONEMORE, (String) value);
    }

    @Override
    public String getZoneName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONENAME);
    }

    @Override
    public void setZoneName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONENAME);
            return;
        }
        row.setString(INDEX_ZONENAME, (String) value);
    }

    @Override
    public String getMappingType() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MAPPINGTYPE);
    }

    @Override
    public void setMappingType(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MAPPINGTYPE);
            return;
        }
        row.setString(INDEX_MAPPINGTYPE, (String) value);
    }

    @Override
    public String getZoneVisibleInTop() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEVISIBLEINTOP);
    }

    @Override
    public void setZoneVisibleInTop(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEVISIBLEINTOP);
            return;
        }
        row.setString(INDEX_ZONEVISIBLEINTOP, (String) value);
    }

    @Override
    public String getZoneVisibleInNavBar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEVISIBLEINNAVBAR);
    }

    @Override
    public void setZoneVisibleInNavBar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEVISIBLEINNAVBAR);
            return;
        }
        row.setString(INDEX_ZONEVISIBLEINNAVBAR, (String) value);
    }

    @Override
    public String getZoneAvatarMain() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEAVATARMAIN);
    }

    @Override
    public void setZoneAvatarMain(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEAVATARMAIN);
            return;
        }
        row.setString(INDEX_ZONEAVATARMAIN, (String) value);
    }

    @Override
    public String getPortalTabControlId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PORTALTABCONTROLID);
    }

    @Override
    public void setPortalTabControlId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PORTALTABCONTROLID);
            return;
        }
        row.setString(INDEX_PORTALTABCONTROLID, (String) value);
    }

    @Override
    public String getVisibleOnHome() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_VISIBLEONHOME);
    }

    @Override
    public void setVisibleOnHome(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_VISIBLEONHOME);
            return;
        }
        row.setString(INDEX_VISIBLEONHOME, (String) value);
    }

    @Override
    public String getBackgroundUrl() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_BACKGROUNDURL);
    }

    @Override
    public void setBackgroundUrl(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_BACKGROUNDURL);
            return;
        }
        row.setString(INDEX_BACKGROUNDURL, (String) value);
    }

    @Override
    public String getZoneLevel() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONELEVEL);
    }

    @Override
    public void setZoneLevel(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONELEVEL);
            return;
        }
        row.setString(INDEX_ZONELEVEL, (String) value);
    }

    @Override
    public String getZoneKeywords() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEKEYWORDS);
    }

    @Override
    public void setZoneKeywords(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEKEYWORDS);
            return;
        }
        row.setString(INDEX_ZONEKEYWORDS, (String) value);
    }

    @Override
    public String getMappingZoneId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MAPPINGZONEID);
    }

    @Override
    public void setMappingZoneId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MAPPINGZONEID);
            return;
        }
        row.setString(INDEX_MAPPINGZONEID, (String) value);
    }

    @Override
    public String getZoneType() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONETYPE);
    }

    @Override
    public void setZoneType(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONETYPE);
            return;
        }
        row.setString(INDEX_ZONETYPE, (String) value);
    }

    @Override
    public String getZoneHeader() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEHEADER);
    }

    @Override
    public void setZoneHeader(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEHEADER);
            return;
        }
        row.setString(INDEX_ZONEHEADER, (String) value);
    }

    @Override
    public String getZoneDes() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEDES);
    }

    @Override
    public void setZoneDes(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEDES);
            return;
        }
        row.setString(INDEX_ZONEDES, (String) value);
    }

    @Override
    public String getZoneParentId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEPARENTID);
    }

    @Override
    public void setZoneParentId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEPARENTID);
            return;
        }
        row.setString(INDEX_ZONEPARENTID, (String) value);
    }

    @Override
    public String getZoneVisibleCenter() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEVISIBLECENTER);
    }

    @Override
    public void setZoneVisibleCenter(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEVISIBLECENTER);
            return;
        }
        row.setString(INDEX_ZONEVISIBLECENTER, (String) value);
    }

    @Override
    public String getZoneVisibleInTab() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEVISIBLEINTAB);
    }

    @Override
    public void setZoneVisibleInTab(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEVISIBLEINTAB);
            return;
        }
        row.setString(INDEX_ZONEVISIBLEINTAB, (String) value);
    }

    @Override
    public String getZonePage() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEPAGE);
    }

    @Override
    public void setZonePage(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEPAGE);
            return;
        }
        row.setString(INDEX_ZONEPAGE, (String) value);
    }

    @Override
    public String getImgCoordinate() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_IMGCOORDINATE);
    }

    @Override
    public void setImgCoordinate(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_IMGCOORDINATE);
            return;
        }
        row.setString(INDEX_IMGCOORDINATE, (String) value);
    }

    @Override
    public String getZonePriority() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEPRIORITY);
    }

    @Override
    public void setZonePriority(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEPRIORITY);
            return;
        }
        row.setString(INDEX_ZONEPRIORITY, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_IndustryResponseLocalStorage")) {
            Table table = transaction.getTable("class_IndustryResponseLocalStorage");
            table.addColumn(ColumnType.INTEGER, "id", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "moduleId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "extentStyle", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneVisibleBottom", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneFooter", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneAvatarMenu", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "mappingUrl", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneAlias", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "isUseBackGround", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneAvatar", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneTitle", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneLang", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneMore", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneName", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "mappingType", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneVisibleInTop", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneVisibleInNavBar", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneAvatarMain", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "portalTabControlId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "visibleOnHome", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "backgroundUrl", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneLevel", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneKeywords", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "mappingZoneId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneType", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneHeader", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneDes", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneParentId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneVisibleCenter", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneVisibleInTab", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zonePage", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "imgCoordinate", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zonePriority", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("id"));
            table.setPrimaryKey("id");
            return table;
        }
        return transaction.getTable("class_IndustryResponseLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_IndustryResponseLocalStorage")) {
            Table table = transaction.getTable("class_IndustryResponseLocalStorage");
            if (table.getColumnCount() != 34) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 34 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 34; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type IndustryResponseLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_ID = table.getColumnIndex("id");
            INDEX_MODULEID = table.getColumnIndex("moduleId");
            INDEX_EXTENTSTYLE = table.getColumnIndex("extentStyle");
            INDEX_ZONEVISIBLEBOTTOM = table.getColumnIndex("zoneVisibleBottom");
            INDEX_ZONEFOOTER = table.getColumnIndex("zoneFooter");
            INDEX_ZONEAVATARMENU = table.getColumnIndex("zoneAvatarMenu");
            INDEX_MAPPINGURL = table.getColumnIndex("mappingUrl");
            INDEX_ZONEALIAS = table.getColumnIndex("zoneAlias");
            INDEX_ISUSEBACKGROUND = table.getColumnIndex("isUseBackGround");
            INDEX_ZONEAVATAR = table.getColumnIndex("zoneAvatar");
            INDEX_ZONETITLE = table.getColumnIndex("zoneTitle");
            INDEX_ZONELANG = table.getColumnIndex("zoneLang");
            INDEX_ZONEID = table.getColumnIndex("zoneId");
            INDEX_ZONEMORE = table.getColumnIndex("zoneMore");
            INDEX_ZONENAME = table.getColumnIndex("zoneName");
            INDEX_MAPPINGTYPE = table.getColumnIndex("mappingType");
            INDEX_ZONEVISIBLEINTOP = table.getColumnIndex("zoneVisibleInTop");
            INDEX_ZONEVISIBLEINNAVBAR = table.getColumnIndex("zoneVisibleInNavBar");
            INDEX_ZONEAVATARMAIN = table.getColumnIndex("zoneAvatarMain");
            INDEX_PORTALTABCONTROLID = table.getColumnIndex("portalTabControlId");
            INDEX_VISIBLEONHOME = table.getColumnIndex("visibleOnHome");
            INDEX_BACKGROUNDURL = table.getColumnIndex("backgroundUrl");
            INDEX_ZONELEVEL = table.getColumnIndex("zoneLevel");
            INDEX_ZONEKEYWORDS = table.getColumnIndex("zoneKeywords");
            INDEX_MAPPINGZONEID = table.getColumnIndex("mappingZoneId");
            INDEX_ZONETYPE = table.getColumnIndex("zoneType");
            INDEX_ZONEHEADER = table.getColumnIndex("zoneHeader");
            INDEX_ZONEDES = table.getColumnIndex("zoneDes");
            INDEX_ZONEPARENTID = table.getColumnIndex("zoneParentId");
            INDEX_ZONEVISIBLECENTER = table.getColumnIndex("zoneVisibleCenter");
            INDEX_ZONEVISIBLEINTAB = table.getColumnIndex("zoneVisibleInTab");
            INDEX_ZONEPAGE = table.getColumnIndex("zonePage");
            INDEX_IMGCOORDINATE = table.getColumnIndex("imgCoordinate");
            INDEX_ZONEPRIORITY = table.getColumnIndex("zonePriority");

            if (!columnTypes.containsKey("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("id") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'id' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'id' does support null values in the existing Realm file. Use corresponding boxed type for field 'id' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'id' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("id"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("moduleId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'moduleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("moduleId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'moduleId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_MODULEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'moduleId' does support null values in the existing Realm file. Use corresponding boxed type for field 'moduleId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("extentStyle")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'extentStyle' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("extentStyle") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'extentStyle' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_EXTENTSTYLE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'extentStyle' is required. Either set @Required to field 'extentStyle' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneVisibleBottom")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneVisibleBottom' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneVisibleBottom") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneVisibleBottom' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEVISIBLEBOTTOM)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneVisibleBottom' is required. Either set @Required to field 'zoneVisibleBottom' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneFooter")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneFooter' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneFooter") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneFooter' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEFOOTER)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneFooter' is required. Either set @Required to field 'zoneFooter' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneAvatarMenu")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneAvatarMenu' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneAvatarMenu") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneAvatarMenu' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEAVATARMENU)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneAvatarMenu' is required. Either set @Required to field 'zoneAvatarMenu' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("mappingUrl")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'mappingUrl' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("mappingUrl") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'mappingUrl' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MAPPINGURL)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'mappingUrl' is required. Either set @Required to field 'mappingUrl' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneAlias")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneAlias' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneAlias") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneAlias' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEALIAS)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneAlias' is required. Either set @Required to field 'zoneAlias' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("isUseBackGround")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'isUseBackGround' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("isUseBackGround") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'isUseBackGround' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ISUSEBACKGROUND)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'isUseBackGround' is required. Either set @Required to field 'isUseBackGround' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneAvatar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneAvatar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneAvatar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneAvatar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEAVATAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneAvatar' is required. Either set @Required to field 'zoneAvatar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneTitle")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneTitle' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneTitle") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneTitle' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONETITLE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneTitle' is required. Either set @Required to field 'zoneTitle' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneLang")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneLang' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneLang") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneLang' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONELANG)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneLang' is required. Either set @Required to field 'zoneLang' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneId' is required. Either set @Required to field 'zoneId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneMore")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneMore' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneMore") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneMore' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEMORE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneMore' is required. Either set @Required to field 'zoneMore' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneName")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneName") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneName' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONENAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneName' is required. Either set @Required to field 'zoneName' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("mappingType")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'mappingType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("mappingType") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'mappingType' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MAPPINGTYPE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'mappingType' is required. Either set @Required to field 'mappingType' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneVisibleInTop")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneVisibleInTop' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneVisibleInTop") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneVisibleInTop' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEVISIBLEINTOP)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneVisibleInTop' is required. Either set @Required to field 'zoneVisibleInTop' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneVisibleInNavBar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneVisibleInNavBar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneVisibleInNavBar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneVisibleInNavBar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEVISIBLEINNAVBAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneVisibleInNavBar' is required. Either set @Required to field 'zoneVisibleInNavBar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneAvatarMain")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneAvatarMain' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneAvatarMain") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneAvatarMain' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEAVATARMAIN)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneAvatarMain' is required. Either set @Required to field 'zoneAvatarMain' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("portalTabControlId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'portalTabControlId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("portalTabControlId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'portalTabControlId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PORTALTABCONTROLID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'portalTabControlId' is required. Either set @Required to field 'portalTabControlId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("visibleOnHome")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'visibleOnHome' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("visibleOnHome") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'visibleOnHome' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_VISIBLEONHOME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'visibleOnHome' is required. Either set @Required to field 'visibleOnHome' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("backgroundUrl")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'backgroundUrl' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("backgroundUrl") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'backgroundUrl' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_BACKGROUNDURL)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'backgroundUrl' is required. Either set @Required to field 'backgroundUrl' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneLevel")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneLevel' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneLevel") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneLevel' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONELEVEL)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneLevel' is required. Either set @Required to field 'zoneLevel' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneKeywords")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneKeywords' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneKeywords") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneKeywords' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEKEYWORDS)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneKeywords' is required. Either set @Required to field 'zoneKeywords' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("mappingZoneId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'mappingZoneId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("mappingZoneId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'mappingZoneId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MAPPINGZONEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'mappingZoneId' is required. Either set @Required to field 'mappingZoneId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneType")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneType") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneType' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONETYPE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneType' is required. Either set @Required to field 'zoneType' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneHeader")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneHeader' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneHeader") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneHeader' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEHEADER)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneHeader' is required. Either set @Required to field 'zoneHeader' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneDes")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneDes' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneDes") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneDes' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEDES)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneDes' is required. Either set @Required to field 'zoneDes' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneParentId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneParentId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneParentId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneParentId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEPARENTID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneParentId' is required. Either set @Required to field 'zoneParentId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneVisibleCenter")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneVisibleCenter' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneVisibleCenter") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneVisibleCenter' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEVISIBLECENTER)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneVisibleCenter' is required. Either set @Required to field 'zoneVisibleCenter' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneVisibleInTab")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneVisibleInTab' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneVisibleInTab") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneVisibleInTab' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEVISIBLEINTAB)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneVisibleInTab' is required. Either set @Required to field 'zoneVisibleInTab' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zonePage")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zonePage' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zonePage") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zonePage' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEPAGE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zonePage' is required. Either set @Required to field 'zonePage' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("imgCoordinate")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'imgCoordinate' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("imgCoordinate") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'imgCoordinate' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_IMGCOORDINATE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'imgCoordinate' is required. Either set @Required to field 'imgCoordinate' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zonePriority")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zonePriority' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zonePriority") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zonePriority' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEPRIORITY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zonePriority' is required. Either set @Required to field 'zonePriority' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The IndustryResponseLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_IndustryResponseLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static IndustryResponseLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        IndustryResponseLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(IndustryResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("id")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new IndustryResponseLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(IndustryResponseLocalStorage.class);
        }
        if (json.has("id")) {
            if (json.isNull("id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
            } else {
                obj.setId((int) json.getInt("id"));
            }
        }
        if (json.has("moduleId")) {
            if (json.isNull("moduleId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
            } else {
                obj.setModuleId((int) json.getInt("moduleId"));
            }
        }
        if (json.has("extentStyle")) {
            if (json.isNull("extentStyle")) {
                obj.setExtentStyle(null);
            } else {
                obj.setExtentStyle((String) json.getString("extentStyle"));
            }
        }
        if (json.has("zoneVisibleBottom")) {
            if (json.isNull("zoneVisibleBottom")) {
                obj.setZoneVisibleBottom(null);
            } else {
                obj.setZoneVisibleBottom((String) json.getString("zoneVisibleBottom"));
            }
        }
        if (json.has("zoneFooter")) {
            if (json.isNull("zoneFooter")) {
                obj.setZoneFooter(null);
            } else {
                obj.setZoneFooter((String) json.getString("zoneFooter"));
            }
        }
        if (json.has("zoneAvatarMenu")) {
            if (json.isNull("zoneAvatarMenu")) {
                obj.setZoneAvatarMenu(null);
            } else {
                obj.setZoneAvatarMenu((String) json.getString("zoneAvatarMenu"));
            }
        }
        if (json.has("mappingUrl")) {
            if (json.isNull("mappingUrl")) {
                obj.setMappingUrl(null);
            } else {
                obj.setMappingUrl((String) json.getString("mappingUrl"));
            }
        }
        if (json.has("zoneAlias")) {
            if (json.isNull("zoneAlias")) {
                obj.setZoneAlias(null);
            } else {
                obj.setZoneAlias((String) json.getString("zoneAlias"));
            }
        }
        if (json.has("isUseBackGround")) {
            if (json.isNull("isUseBackGround")) {
                obj.setIsUseBackGround(null);
            } else {
                obj.setIsUseBackGround((String) json.getString("isUseBackGround"));
            }
        }
        if (json.has("zoneAvatar")) {
            if (json.isNull("zoneAvatar")) {
                obj.setZoneAvatar(null);
            } else {
                obj.setZoneAvatar((String) json.getString("zoneAvatar"));
            }
        }
        if (json.has("zoneTitle")) {
            if (json.isNull("zoneTitle")) {
                obj.setZoneTitle(null);
            } else {
                obj.setZoneTitle((String) json.getString("zoneTitle"));
            }
        }
        if (json.has("zoneLang")) {
            if (json.isNull("zoneLang")) {
                obj.setZoneLang(null);
            } else {
                obj.setZoneLang((String) json.getString("zoneLang"));
            }
        }
        if (json.has("zoneId")) {
            if (json.isNull("zoneId")) {
                obj.setZoneId(null);
            } else {
                obj.setZoneId((String) json.getString("zoneId"));
            }
        }
        if (json.has("zoneMore")) {
            if (json.isNull("zoneMore")) {
                obj.setZoneMore(null);
            } else {
                obj.setZoneMore((String) json.getString("zoneMore"));
            }
        }
        if (json.has("zoneName")) {
            if (json.isNull("zoneName")) {
                obj.setZoneName(null);
            } else {
                obj.setZoneName((String) json.getString("zoneName"));
            }
        }
        if (json.has("mappingType")) {
            if (json.isNull("mappingType")) {
                obj.setMappingType(null);
            } else {
                obj.setMappingType((String) json.getString("mappingType"));
            }
        }
        if (json.has("zoneVisibleInTop")) {
            if (json.isNull("zoneVisibleInTop")) {
                obj.setZoneVisibleInTop(null);
            } else {
                obj.setZoneVisibleInTop((String) json.getString("zoneVisibleInTop"));
            }
        }
        if (json.has("zoneVisibleInNavBar")) {
            if (json.isNull("zoneVisibleInNavBar")) {
                obj.setZoneVisibleInNavBar(null);
            } else {
                obj.setZoneVisibleInNavBar((String) json.getString("zoneVisibleInNavBar"));
            }
        }
        if (json.has("zoneAvatarMain")) {
            if (json.isNull("zoneAvatarMain")) {
                obj.setZoneAvatarMain(null);
            } else {
                obj.setZoneAvatarMain((String) json.getString("zoneAvatarMain"));
            }
        }
        if (json.has("portalTabControlId")) {
            if (json.isNull("portalTabControlId")) {
                obj.setPortalTabControlId(null);
            } else {
                obj.setPortalTabControlId((String) json.getString("portalTabControlId"));
            }
        }
        if (json.has("visibleOnHome")) {
            if (json.isNull("visibleOnHome")) {
                obj.setVisibleOnHome(null);
            } else {
                obj.setVisibleOnHome((String) json.getString("visibleOnHome"));
            }
        }
        if (json.has("backgroundUrl")) {
            if (json.isNull("backgroundUrl")) {
                obj.setBackgroundUrl(null);
            } else {
                obj.setBackgroundUrl((String) json.getString("backgroundUrl"));
            }
        }
        if (json.has("zoneLevel")) {
            if (json.isNull("zoneLevel")) {
                obj.setZoneLevel(null);
            } else {
                obj.setZoneLevel((String) json.getString("zoneLevel"));
            }
        }
        if (json.has("zoneKeywords")) {
            if (json.isNull("zoneKeywords")) {
                obj.setZoneKeywords(null);
            } else {
                obj.setZoneKeywords((String) json.getString("zoneKeywords"));
            }
        }
        if (json.has("mappingZoneId")) {
            if (json.isNull("mappingZoneId")) {
                obj.setMappingZoneId(null);
            } else {
                obj.setMappingZoneId((String) json.getString("mappingZoneId"));
            }
        }
        if (json.has("zoneType")) {
            if (json.isNull("zoneType")) {
                obj.setZoneType(null);
            } else {
                obj.setZoneType((String) json.getString("zoneType"));
            }
        }
        if (json.has("zoneHeader")) {
            if (json.isNull("zoneHeader")) {
                obj.setZoneHeader(null);
            } else {
                obj.setZoneHeader((String) json.getString("zoneHeader"));
            }
        }
        if (json.has("zoneDes")) {
            if (json.isNull("zoneDes")) {
                obj.setZoneDes(null);
            } else {
                obj.setZoneDes((String) json.getString("zoneDes"));
            }
        }
        if (json.has("zoneParentId")) {
            if (json.isNull("zoneParentId")) {
                obj.setZoneParentId(null);
            } else {
                obj.setZoneParentId((String) json.getString("zoneParentId"));
            }
        }
        if (json.has("zoneVisibleCenter")) {
            if (json.isNull("zoneVisibleCenter")) {
                obj.setZoneVisibleCenter(null);
            } else {
                obj.setZoneVisibleCenter((String) json.getString("zoneVisibleCenter"));
            }
        }
        if (json.has("zoneVisibleInTab")) {
            if (json.isNull("zoneVisibleInTab")) {
                obj.setZoneVisibleInTab(null);
            } else {
                obj.setZoneVisibleInTab((String) json.getString("zoneVisibleInTab"));
            }
        }
        if (json.has("zonePage")) {
            if (json.isNull("zonePage")) {
                obj.setZonePage(null);
            } else {
                obj.setZonePage((String) json.getString("zonePage"));
            }
        }
        if (json.has("imgCoordinate")) {
            if (json.isNull("imgCoordinate")) {
                obj.setImgCoordinate(null);
            } else {
                obj.setImgCoordinate((String) json.getString("imgCoordinate"));
            }
        }
        if (json.has("zonePriority")) {
            if (json.isNull("zonePriority")) {
                obj.setZonePriority(null);
            } else {
                obj.setZonePriority((String) json.getString("zonePriority"));
            }
        }
        return obj;
    }

    public static IndustryResponseLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        IndustryResponseLocalStorage obj = realm.createObject(IndustryResponseLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
                } else {
                    obj.setId((int) reader.nextInt());
                }
            } else if (name.equals("moduleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
                } else {
                    obj.setModuleId((int) reader.nextInt());
                }
            } else if (name.equals("extentStyle")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setExtentStyle(null);
                } else {
                    obj.setExtentStyle((String) reader.nextString());
                }
            } else if (name.equals("zoneVisibleBottom")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneVisibleBottom(null);
                } else {
                    obj.setZoneVisibleBottom((String) reader.nextString());
                }
            } else if (name.equals("zoneFooter")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneFooter(null);
                } else {
                    obj.setZoneFooter((String) reader.nextString());
                }
            } else if (name.equals("zoneAvatarMenu")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneAvatarMenu(null);
                } else {
                    obj.setZoneAvatarMenu((String) reader.nextString());
                }
            } else if (name.equals("mappingUrl")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMappingUrl(null);
                } else {
                    obj.setMappingUrl((String) reader.nextString());
                }
            } else if (name.equals("zoneAlias")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneAlias(null);
                } else {
                    obj.setZoneAlias((String) reader.nextString());
                }
            } else if (name.equals("isUseBackGround")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setIsUseBackGround(null);
                } else {
                    obj.setIsUseBackGround((String) reader.nextString());
                }
            } else if (name.equals("zoneAvatar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneAvatar(null);
                } else {
                    obj.setZoneAvatar((String) reader.nextString());
                }
            } else if (name.equals("zoneTitle")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneTitle(null);
                } else {
                    obj.setZoneTitle((String) reader.nextString());
                }
            } else if (name.equals("zoneLang")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneLang(null);
                } else {
                    obj.setZoneLang((String) reader.nextString());
                }
            } else if (name.equals("zoneId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneId(null);
                } else {
                    obj.setZoneId((String) reader.nextString());
                }
            } else if (name.equals("zoneMore")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneMore(null);
                } else {
                    obj.setZoneMore((String) reader.nextString());
                }
            } else if (name.equals("zoneName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneName(null);
                } else {
                    obj.setZoneName((String) reader.nextString());
                }
            } else if (name.equals("mappingType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMappingType(null);
                } else {
                    obj.setMappingType((String) reader.nextString());
                }
            } else if (name.equals("zoneVisibleInTop")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneVisibleInTop(null);
                } else {
                    obj.setZoneVisibleInTop((String) reader.nextString());
                }
            } else if (name.equals("zoneVisibleInNavBar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneVisibleInNavBar(null);
                } else {
                    obj.setZoneVisibleInNavBar((String) reader.nextString());
                }
            } else if (name.equals("zoneAvatarMain")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneAvatarMain(null);
                } else {
                    obj.setZoneAvatarMain((String) reader.nextString());
                }
            } else if (name.equals("portalTabControlId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setPortalTabControlId(null);
                } else {
                    obj.setPortalTabControlId((String) reader.nextString());
                }
            } else if (name.equals("visibleOnHome")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setVisibleOnHome(null);
                } else {
                    obj.setVisibleOnHome((String) reader.nextString());
                }
            } else if (name.equals("backgroundUrl")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setBackgroundUrl(null);
                } else {
                    obj.setBackgroundUrl((String) reader.nextString());
                }
            } else if (name.equals("zoneLevel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneLevel(null);
                } else {
                    obj.setZoneLevel((String) reader.nextString());
                }
            } else if (name.equals("zoneKeywords")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneKeywords(null);
                } else {
                    obj.setZoneKeywords((String) reader.nextString());
                }
            } else if (name.equals("mappingZoneId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMappingZoneId(null);
                } else {
                    obj.setMappingZoneId((String) reader.nextString());
                }
            } else if (name.equals("zoneType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneType(null);
                } else {
                    obj.setZoneType((String) reader.nextString());
                }
            } else if (name.equals("zoneHeader")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneHeader(null);
                } else {
                    obj.setZoneHeader((String) reader.nextString());
                }
            } else if (name.equals("zoneDes")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneDes(null);
                } else {
                    obj.setZoneDes((String) reader.nextString());
                }
            } else if (name.equals("zoneParentId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneParentId(null);
                } else {
                    obj.setZoneParentId((String) reader.nextString());
                }
            } else if (name.equals("zoneVisibleCenter")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneVisibleCenter(null);
                } else {
                    obj.setZoneVisibleCenter((String) reader.nextString());
                }
            } else if (name.equals("zoneVisibleInTab")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneVisibleInTab(null);
                } else {
                    obj.setZoneVisibleInTab((String) reader.nextString());
                }
            } else if (name.equals("zonePage")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZonePage(null);
                } else {
                    obj.setZonePage((String) reader.nextString());
                }
            } else if (name.equals("imgCoordinate")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setImgCoordinate(null);
                } else {
                    obj.setImgCoordinate((String) reader.nextString());
                }
            } else if (name.equals("zonePriority")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZonePriority(null);
                } else {
                    obj.setZonePriority((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static IndustryResponseLocalStorage copyOrUpdate(Realm realm, IndustryResponseLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        IndustryResponseLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(IndustryResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new IndustryResponseLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static IndustryResponseLocalStorage copy(Realm realm, IndustryResponseLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        IndustryResponseLocalStorage realmObject = realm.createObject(IndustryResponseLocalStorage.class, newObject.getId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setId(newObject.getId());
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setExtentStyle(newObject.getExtentStyle());
        realmObject.setZoneVisibleBottom(newObject.getZoneVisibleBottom());
        realmObject.setZoneFooter(newObject.getZoneFooter());
        realmObject.setZoneAvatarMenu(newObject.getZoneAvatarMenu());
        realmObject.setMappingUrl(newObject.getMappingUrl());
        realmObject.setZoneAlias(newObject.getZoneAlias());
        realmObject.setIsUseBackGround(newObject.getIsUseBackGround());
        realmObject.setZoneAvatar(newObject.getZoneAvatar());
        realmObject.setZoneTitle(newObject.getZoneTitle());
        realmObject.setZoneLang(newObject.getZoneLang());
        realmObject.setZoneId(newObject.getZoneId());
        realmObject.setZoneMore(newObject.getZoneMore());
        realmObject.setZoneName(newObject.getZoneName());
        realmObject.setMappingType(newObject.getMappingType());
        realmObject.setZoneVisibleInTop(newObject.getZoneVisibleInTop());
        realmObject.setZoneVisibleInNavBar(newObject.getZoneVisibleInNavBar());
        realmObject.setZoneAvatarMain(newObject.getZoneAvatarMain());
        realmObject.setPortalTabControlId(newObject.getPortalTabControlId());
        realmObject.setVisibleOnHome(newObject.getVisibleOnHome());
        realmObject.setBackgroundUrl(newObject.getBackgroundUrl());
        realmObject.setZoneLevel(newObject.getZoneLevel());
        realmObject.setZoneKeywords(newObject.getZoneKeywords());
        realmObject.setMappingZoneId(newObject.getMappingZoneId());
        realmObject.setZoneType(newObject.getZoneType());
        realmObject.setZoneHeader(newObject.getZoneHeader());
        realmObject.setZoneDes(newObject.getZoneDes());
        realmObject.setZoneParentId(newObject.getZoneParentId());
        realmObject.setZoneVisibleCenter(newObject.getZoneVisibleCenter());
        realmObject.setZoneVisibleInTab(newObject.getZoneVisibleInTab());
        realmObject.setZonePage(newObject.getZonePage());
        realmObject.setImgCoordinate(newObject.getImgCoordinate());
        realmObject.setZonePriority(newObject.getZonePriority());
        return realmObject;
    }

    static IndustryResponseLocalStorage update(Realm realm, IndustryResponseLocalStorage realmObject, IndustryResponseLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setExtentStyle(newObject.getExtentStyle());
        realmObject.setZoneVisibleBottom(newObject.getZoneVisibleBottom());
        realmObject.setZoneFooter(newObject.getZoneFooter());
        realmObject.setZoneAvatarMenu(newObject.getZoneAvatarMenu());
        realmObject.setMappingUrl(newObject.getMappingUrl());
        realmObject.setZoneAlias(newObject.getZoneAlias());
        realmObject.setIsUseBackGround(newObject.getIsUseBackGround());
        realmObject.setZoneAvatar(newObject.getZoneAvatar());
        realmObject.setZoneTitle(newObject.getZoneTitle());
        realmObject.setZoneLang(newObject.getZoneLang());
        realmObject.setZoneId(newObject.getZoneId());
        realmObject.setZoneMore(newObject.getZoneMore());
        realmObject.setZoneName(newObject.getZoneName());
        realmObject.setMappingType(newObject.getMappingType());
        realmObject.setZoneVisibleInTop(newObject.getZoneVisibleInTop());
        realmObject.setZoneVisibleInNavBar(newObject.getZoneVisibleInNavBar());
        realmObject.setZoneAvatarMain(newObject.getZoneAvatarMain());
        realmObject.setPortalTabControlId(newObject.getPortalTabControlId());
        realmObject.setVisibleOnHome(newObject.getVisibleOnHome());
        realmObject.setBackgroundUrl(newObject.getBackgroundUrl());
        realmObject.setZoneLevel(newObject.getZoneLevel());
        realmObject.setZoneKeywords(newObject.getZoneKeywords());
        realmObject.setMappingZoneId(newObject.getMappingZoneId());
        realmObject.setZoneType(newObject.getZoneType());
        realmObject.setZoneHeader(newObject.getZoneHeader());
        realmObject.setZoneDes(newObject.getZoneDes());
        realmObject.setZoneParentId(newObject.getZoneParentId());
        realmObject.setZoneVisibleCenter(newObject.getZoneVisibleCenter());
        realmObject.setZoneVisibleInTab(newObject.getZoneVisibleInTab());
        realmObject.setZonePage(newObject.getZonePage());
        realmObject.setImgCoordinate(newObject.getImgCoordinate());
        realmObject.setZonePriority(newObject.getZonePriority());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("IndustryResponseLocalStorage = [");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{moduleId:");
        stringBuilder.append(getModuleId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{extentStyle:");
        stringBuilder.append(getExtentStyle() != null ? getExtentStyle() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneVisibleBottom:");
        stringBuilder.append(getZoneVisibleBottom() != null ? getZoneVisibleBottom() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneFooter:");
        stringBuilder.append(getZoneFooter() != null ? getZoneFooter() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneAvatarMenu:");
        stringBuilder.append(getZoneAvatarMenu() != null ? getZoneAvatarMenu() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mappingUrl:");
        stringBuilder.append(getMappingUrl() != null ? getMappingUrl() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneAlias:");
        stringBuilder.append(getZoneAlias() != null ? getZoneAlias() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{isUseBackGround:");
        stringBuilder.append(getIsUseBackGround() != null ? getIsUseBackGround() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneAvatar:");
        stringBuilder.append(getZoneAvatar() != null ? getZoneAvatar() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneTitle:");
        stringBuilder.append(getZoneTitle() != null ? getZoneTitle() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneLang:");
        stringBuilder.append(getZoneLang() != null ? getZoneLang() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneId:");
        stringBuilder.append(getZoneId() != null ? getZoneId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneMore:");
        stringBuilder.append(getZoneMore() != null ? getZoneMore() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneName:");
        stringBuilder.append(getZoneName() != null ? getZoneName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mappingType:");
        stringBuilder.append(getMappingType() != null ? getMappingType() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneVisibleInTop:");
        stringBuilder.append(getZoneVisibleInTop() != null ? getZoneVisibleInTop() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneVisibleInNavBar:");
        stringBuilder.append(getZoneVisibleInNavBar() != null ? getZoneVisibleInNavBar() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneAvatarMain:");
        stringBuilder.append(getZoneAvatarMain() != null ? getZoneAvatarMain() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{portalTabControlId:");
        stringBuilder.append(getPortalTabControlId() != null ? getPortalTabControlId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{visibleOnHome:");
        stringBuilder.append(getVisibleOnHome() != null ? getVisibleOnHome() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{backgroundUrl:");
        stringBuilder.append(getBackgroundUrl() != null ? getBackgroundUrl() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneLevel:");
        stringBuilder.append(getZoneLevel() != null ? getZoneLevel() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneKeywords:");
        stringBuilder.append(getZoneKeywords() != null ? getZoneKeywords() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{mappingZoneId:");
        stringBuilder.append(getMappingZoneId() != null ? getMappingZoneId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneType:");
        stringBuilder.append(getZoneType() != null ? getZoneType() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneHeader:");
        stringBuilder.append(getZoneHeader() != null ? getZoneHeader() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneDes:");
        stringBuilder.append(getZoneDes() != null ? getZoneDes() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneParentId:");
        stringBuilder.append(getZoneParentId() != null ? getZoneParentId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneVisibleCenter:");
        stringBuilder.append(getZoneVisibleCenter() != null ? getZoneVisibleCenter() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneVisibleInTab:");
        stringBuilder.append(getZoneVisibleInTab() != null ? getZoneVisibleInTab() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zonePage:");
        stringBuilder.append(getZonePage() != null ? getZonePage() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{imgCoordinate:");
        stringBuilder.append(getImgCoordinate() != null ? getImgCoordinate() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zonePriority:");
        stringBuilder.append(getZonePriority() != null ? getZonePriority() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndustryResponseLocalStorageRealmProxy aIndustryResponseLocalStorage = (IndustryResponseLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aIndustryResponseLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aIndustryResponseLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aIndustryResponseLocalStorage.row.getIndex()) return false;

        return true;
    }

}
