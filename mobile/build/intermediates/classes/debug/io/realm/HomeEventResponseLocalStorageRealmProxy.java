package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.lingo.marketplace.shopping.models.local.HomeEventResponseLocalStorage;

public class HomeEventResponseLocalStorageRealmProxy extends HomeEventResponseLocalStorage
    implements RealmObjectProxy {

    private static long INDEX_ID;
    private static long INDEX_MODULEID;
    private static long INDEX_EVENTID;
    private static long INDEX_AVATAR;
    private static long INDEX_PRODUCTID;
    private static long INDEX_PRODUCTNAME;
    private static long INDEX_PRODUCTQUANTITY;
    private static long INDEX_MARKETPRICE;
    private static long INDEX_ZONEALIAS;
    private static long INDEX_ATTRIBUTEID;
    private static long INDEX_NUMBERVALUE;
    private static long INDEX_DISCOUNTSTYPE;
    private static long INDEX_PROMOTIONDISCOUNT;
    private static long INDEX_PRODUCTPRICE;
    private static long INDEX_PRODUCTPRICEDISCOUNT;
    private static long INDEX_ISINSTALLMENT;
    private static long INDEX_TRADEMARK;
    private static long INDEX_MARKETPRICEFORMAT;
    private static long INDEX_PRODUCTPRICEFORMAT;
    private static long INDEX_PRODUCTPRICEDISCOUNTFORMAT;
    private static long INDEX_PROMOTIONPERCENT;
    private static long INDEX_HAPPYHOURDISCOUNT;
    private static long INDEX_GIFTCARD;
    private static Map<String, Long> columnIndices;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("id");
        fieldNames.add("moduleId");
        fieldNames.add("eventId");
        fieldNames.add("avatar");
        fieldNames.add("productId");
        fieldNames.add("productName");
        fieldNames.add("productQuantity");
        fieldNames.add("marketPrice");
        fieldNames.add("zoneAlias");
        fieldNames.add("attributeId");
        fieldNames.add("numberValue");
        fieldNames.add("discountsType");
        fieldNames.add("promotionDiscount");
        fieldNames.add("productPrice");
        fieldNames.add("productPriceDiscount");
        fieldNames.add("isInstallment");
        fieldNames.add("trademark");
        fieldNames.add("marketPriceFormat");
        fieldNames.add("productPriceFormat");
        fieldNames.add("productPriceDiscountFormat");
        fieldNames.add("promotionPercent");
        fieldNames.add("happyHourDiscount");
        fieldNames.add("giftCard");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    @Override
    public int getId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_ID);
    }

    @Override
    public void setId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_ID, (long) value);
    }

    @Override
    public int getModuleId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_MODULEID);
    }

    @Override
    public void setModuleId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_MODULEID, (long) value);
    }

    @Override
    public int getEventId() {
        realm.checkIfValid();
        return (int) row.getLong(INDEX_EVENTID);
    }

    @Override
    public void setEventId(int value) {
        realm.checkIfValid();
        row.setLong(INDEX_EVENTID, (long) value);
    }

    @Override
    public String getAvatar() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_AVATAR);
    }

    @Override
    public void setAvatar(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_AVATAR);
            return;
        }
        row.setString(INDEX_AVATAR, (String) value);
    }

    @Override
    public String getProductId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTID);
    }

    @Override
    public void setProductId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTID);
            return;
        }
        row.setString(INDEX_PRODUCTID, (String) value);
    }

    @Override
    public String getProductName() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTNAME);
    }

    @Override
    public void setProductName(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTNAME);
            return;
        }
        row.setString(INDEX_PRODUCTNAME, (String) value);
    }

    @Override
    public String getProductQuantity() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTQUANTITY);
    }

    @Override
    public void setProductQuantity(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTQUANTITY);
            return;
        }
        row.setString(INDEX_PRODUCTQUANTITY, (String) value);
    }

    @Override
    public String getMarketPrice() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MARKETPRICE);
    }

    @Override
    public void setMarketPrice(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MARKETPRICE);
            return;
        }
        row.setString(INDEX_MARKETPRICE, (String) value);
    }

    @Override
    public String getZoneAlias() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ZONEALIAS);
    }

    @Override
    public void setZoneAlias(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ZONEALIAS);
            return;
        }
        row.setString(INDEX_ZONEALIAS, (String) value);
    }

    @Override
    public String getAttributeId() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ATTRIBUTEID);
    }

    @Override
    public void setAttributeId(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ATTRIBUTEID);
            return;
        }
        row.setString(INDEX_ATTRIBUTEID, (String) value);
    }

    @Override
    public String getNumberValue() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_NUMBERVALUE);
    }

    @Override
    public void setNumberValue(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_NUMBERVALUE);
            return;
        }
        row.setString(INDEX_NUMBERVALUE, (String) value);
    }

    @Override
    public String getDiscountsType() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_DISCOUNTSTYPE);
    }

    @Override
    public void setDiscountsType(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_DISCOUNTSTYPE);
            return;
        }
        row.setString(INDEX_DISCOUNTSTYPE, (String) value);
    }

    @Override
    public String getPromotionDiscount() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PROMOTIONDISCOUNT);
    }

    @Override
    public void setPromotionDiscount(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PROMOTIONDISCOUNT);
            return;
        }
        row.setString(INDEX_PROMOTIONDISCOUNT, (String) value);
    }

    @Override
    public String getProductPrice() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICE);
    }

    @Override
    public void setProductPrice(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICE);
            return;
        }
        row.setString(INDEX_PRODUCTPRICE, (String) value);
    }

    @Override
    public String getProductPriceDiscount() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICEDISCOUNT);
    }

    @Override
    public void setProductPriceDiscount(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICEDISCOUNT);
            return;
        }
        row.setString(INDEX_PRODUCTPRICEDISCOUNT, (String) value);
    }

    @Override
    public String getIsInstallment() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_ISINSTALLMENT);
    }

    @Override
    public void setIsInstallment(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_ISINSTALLMENT);
            return;
        }
        row.setString(INDEX_ISINSTALLMENT, (String) value);
    }

    @Override
    public String getTrademark() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_TRADEMARK);
    }

    @Override
    public void setTrademark(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_TRADEMARK);
            return;
        }
        row.setString(INDEX_TRADEMARK, (String) value);
    }

    @Override
    public String getMarketPriceFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_MARKETPRICEFORMAT);
    }

    @Override
    public void setMarketPriceFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_MARKETPRICEFORMAT);
            return;
        }
        row.setString(INDEX_MARKETPRICEFORMAT, (String) value);
    }

    @Override
    public String getProductPriceFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICEFORMAT);
    }

    @Override
    public void setProductPriceFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICEFORMAT);
            return;
        }
        row.setString(INDEX_PRODUCTPRICEFORMAT, (String) value);
    }

    @Override
    public String getProductPriceDiscountFormat() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PRODUCTPRICEDISCOUNTFORMAT);
    }

    @Override
    public void setProductPriceDiscountFormat(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PRODUCTPRICEDISCOUNTFORMAT);
            return;
        }
        row.setString(INDEX_PRODUCTPRICEDISCOUNTFORMAT, (String) value);
    }

    @Override
    public String getPromotionPercent() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_PROMOTIONPERCENT);
    }

    @Override
    public void setPromotionPercent(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_PROMOTIONPERCENT);
            return;
        }
        row.setString(INDEX_PROMOTIONPERCENT, (String) value);
    }

    @Override
    public String getHappyHourDiscount() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_HAPPYHOURDISCOUNT);
    }

    @Override
    public void setHappyHourDiscount(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_HAPPYHOURDISCOUNT);
            return;
        }
        row.setString(INDEX_HAPPYHOURDISCOUNT, (String) value);
    }

    @Override
    public String getGiftCard() {
        realm.checkIfValid();
        return (java.lang.String) row.getString(INDEX_GIFTCARD);
    }

    @Override
    public void setGiftCard(String value) {
        realm.checkIfValid();
        if (value == null) {
            row.setNull(INDEX_GIFTCARD);
            return;
        }
        row.setString(INDEX_GIFTCARD, (String) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_HomeEventResponseLocalStorage")) {
            Table table = transaction.getTable("class_HomeEventResponseLocalStorage");
            table.addColumn(ColumnType.INTEGER, "id", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "moduleId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.INTEGER, "eventId", Table.NOT_NULLABLE);
            table.addColumn(ColumnType.STRING, "avatar", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productName", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productQuantity", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "marketPrice", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "zoneAlias", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "attributeId", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "numberValue", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "discountsType", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "promotionDiscount", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productPrice", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productPriceDiscount", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "isInstallment", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "trademark", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "marketPriceFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productPriceFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "productPriceDiscountFormat", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "promotionPercent", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "happyHourDiscount", Table.NULLABLE);
            table.addColumn(ColumnType.STRING, "giftCard", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("id"));
            table.setPrimaryKey("id");
            return table;
        }
        return transaction.getTable("class_HomeEventResponseLocalStorage");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_HomeEventResponseLocalStorage")) {
            Table table = transaction.getTable("class_HomeEventResponseLocalStorage");
            if (table.getColumnCount() != 23) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 23 but was " + table.getColumnCount());
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for (long i = 0; i < 23; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            columnIndices = new HashMap<String, Long>();
            for (String fieldName : getFieldNames()) {
                long index = table.getColumnIndex(fieldName);
                if (index == -1) {
                    throw new RealmMigrationNeededException(transaction.getPath(), "Field '" + fieldName + "' not found for type HomeEventResponseLocalStorage");
                }
                columnIndices.put(fieldName, index);
            }
            INDEX_ID = table.getColumnIndex("id");
            INDEX_MODULEID = table.getColumnIndex("moduleId");
            INDEX_EVENTID = table.getColumnIndex("eventId");
            INDEX_AVATAR = table.getColumnIndex("avatar");
            INDEX_PRODUCTID = table.getColumnIndex("productId");
            INDEX_PRODUCTNAME = table.getColumnIndex("productName");
            INDEX_PRODUCTQUANTITY = table.getColumnIndex("productQuantity");
            INDEX_MARKETPRICE = table.getColumnIndex("marketPrice");
            INDEX_ZONEALIAS = table.getColumnIndex("zoneAlias");
            INDEX_ATTRIBUTEID = table.getColumnIndex("attributeId");
            INDEX_NUMBERVALUE = table.getColumnIndex("numberValue");
            INDEX_DISCOUNTSTYPE = table.getColumnIndex("discountsType");
            INDEX_PROMOTIONDISCOUNT = table.getColumnIndex("promotionDiscount");
            INDEX_PRODUCTPRICE = table.getColumnIndex("productPrice");
            INDEX_PRODUCTPRICEDISCOUNT = table.getColumnIndex("productPriceDiscount");
            INDEX_ISINSTALLMENT = table.getColumnIndex("isInstallment");
            INDEX_TRADEMARK = table.getColumnIndex("trademark");
            INDEX_MARKETPRICEFORMAT = table.getColumnIndex("marketPriceFormat");
            INDEX_PRODUCTPRICEFORMAT = table.getColumnIndex("productPriceFormat");
            INDEX_PRODUCTPRICEDISCOUNTFORMAT = table.getColumnIndex("productPriceDiscountFormat");
            INDEX_PROMOTIONPERCENT = table.getColumnIndex("promotionPercent");
            INDEX_HAPPYHOURDISCOUNT = table.getColumnIndex("happyHourDiscount");
            INDEX_GIFTCARD = table.getColumnIndex("giftCard");

            if (!columnTypes.containsKey("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("id") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'id' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_ID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'id' does support null values in the existing Realm file. Use corresponding boxed type for field 'id' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'id' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("id"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("moduleId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'moduleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("moduleId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'moduleId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_MODULEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'moduleId' does support null values in the existing Realm file. Use corresponding boxed type for field 'moduleId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("eventId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'eventId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("eventId") != ColumnType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'eventId' in existing Realm file.");
            }
            if (table.isColumnNullable(INDEX_EVENTID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'eventId' does support null values in the existing Realm file. Use corresponding boxed type for field 'eventId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (!columnTypes.containsKey("avatar")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'avatar' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("avatar") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'avatar' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_AVATAR)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'avatar' is required. Either set @Required to field 'avatar' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productId' is required. Either set @Required to field 'productId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productName")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productName") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productName' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTNAME)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productName' is required. Either set @Required to field 'productName' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productQuantity")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productQuantity' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productQuantity") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productQuantity' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTQUANTITY)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productQuantity' is required. Either set @Required to field 'productQuantity' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("marketPrice")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'marketPrice' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("marketPrice") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'marketPrice' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MARKETPRICE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'marketPrice' is required. Either set @Required to field 'marketPrice' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("zoneAlias")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'zoneAlias' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("zoneAlias") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'zoneAlias' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ZONEALIAS)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'zoneAlias' is required. Either set @Required to field 'zoneAlias' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("attributeId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'attributeId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("attributeId") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'attributeId' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ATTRIBUTEID)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'attributeId' is required. Either set @Required to field 'attributeId' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("numberValue")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'numberValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("numberValue") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'numberValue' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_NUMBERVALUE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'numberValue' is required. Either set @Required to field 'numberValue' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("discountsType")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'discountsType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("discountsType") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'discountsType' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_DISCOUNTSTYPE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'discountsType' is required. Either set @Required to field 'discountsType' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("promotionDiscount")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'promotionDiscount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("promotionDiscount") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'promotionDiscount' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PROMOTIONDISCOUNT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'promotionDiscount' is required. Either set @Required to field 'promotionDiscount' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productPrice")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPrice' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPrice") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPrice' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICE)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPrice' is required. Either set @Required to field 'productPrice' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productPriceDiscount")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPriceDiscount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPriceDiscount") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPriceDiscount' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICEDISCOUNT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPriceDiscount' is required. Either set @Required to field 'productPriceDiscount' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("isInstallment")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'isInstallment' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("isInstallment") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'isInstallment' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_ISINSTALLMENT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'isInstallment' is required. Either set @Required to field 'isInstallment' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("trademark")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'trademark' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("trademark") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'trademark' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_TRADEMARK)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'trademark' is required. Either set @Required to field 'trademark' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("marketPriceFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'marketPriceFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("marketPriceFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'marketPriceFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_MARKETPRICEFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'marketPriceFormat' is required. Either set @Required to field 'marketPriceFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productPriceFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPriceFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPriceFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPriceFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICEFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPriceFormat' is required. Either set @Required to field 'productPriceFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("productPriceDiscountFormat")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'productPriceDiscountFormat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("productPriceDiscountFormat") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'productPriceDiscountFormat' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PRODUCTPRICEDISCOUNTFORMAT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'productPriceDiscountFormat' is required. Either set @Required to field 'productPriceDiscountFormat' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("promotionPercent")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'promotionPercent' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("promotionPercent") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'promotionPercent' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_PROMOTIONPERCENT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'promotionPercent' is required. Either set @Required to field 'promotionPercent' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("happyHourDiscount")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'happyHourDiscount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("happyHourDiscount") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'happyHourDiscount' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_HAPPYHOURDISCOUNT)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'happyHourDiscount' is required. Either set @Required to field 'happyHourDiscount' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
            if (!columnTypes.containsKey("giftCard")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'giftCard' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("giftCard") != ColumnType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'giftCard' in existing Realm file.");
            }
            if (!table.isColumnNullable(INDEX_GIFTCARD)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'giftCard' is required. Either set @Required to field 'giftCard' or migrate using io.realm.internal.Table.convertColumnToNullable().");
            }
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The HomeEventResponseLocalStorage class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_HomeEventResponseLocalStorage";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static Map<String,Long> getColumnIndices() {
        return columnIndices;
    }

    public static HomeEventResponseLocalStorage createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        HomeEventResponseLocalStorage obj = null;
        if (update) {
            Table table = realm.getTable(HomeEventResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("id")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new HomeEventResponseLocalStorageRealmProxy();
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            obj = realm.createObject(HomeEventResponseLocalStorage.class);
        }
        if (json.has("id")) {
            if (json.isNull("id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
            } else {
                obj.setId((int) json.getInt("id"));
            }
        }
        if (json.has("moduleId")) {
            if (json.isNull("moduleId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
            } else {
                obj.setModuleId((int) json.getInt("moduleId"));
            }
        }
        if (json.has("eventId")) {
            if (json.isNull("eventId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field eventId to null.");
            } else {
                obj.setEventId((int) json.getInt("eventId"));
            }
        }
        if (json.has("avatar")) {
            if (json.isNull("avatar")) {
                obj.setAvatar(null);
            } else {
                obj.setAvatar((String) json.getString("avatar"));
            }
        }
        if (json.has("productId")) {
            if (json.isNull("productId")) {
                obj.setProductId(null);
            } else {
                obj.setProductId((String) json.getString("productId"));
            }
        }
        if (json.has("productName")) {
            if (json.isNull("productName")) {
                obj.setProductName(null);
            } else {
                obj.setProductName((String) json.getString("productName"));
            }
        }
        if (json.has("productQuantity")) {
            if (json.isNull("productQuantity")) {
                obj.setProductQuantity(null);
            } else {
                obj.setProductQuantity((String) json.getString("productQuantity"));
            }
        }
        if (json.has("marketPrice")) {
            if (json.isNull("marketPrice")) {
                obj.setMarketPrice(null);
            } else {
                obj.setMarketPrice((String) json.getString("marketPrice"));
            }
        }
        if (json.has("zoneAlias")) {
            if (json.isNull("zoneAlias")) {
                obj.setZoneAlias(null);
            } else {
                obj.setZoneAlias((String) json.getString("zoneAlias"));
            }
        }
        if (json.has("attributeId")) {
            if (json.isNull("attributeId")) {
                obj.setAttributeId(null);
            } else {
                obj.setAttributeId((String) json.getString("attributeId"));
            }
        }
        if (json.has("numberValue")) {
            if (json.isNull("numberValue")) {
                obj.setNumberValue(null);
            } else {
                obj.setNumberValue((String) json.getString("numberValue"));
            }
        }
        if (json.has("discountsType")) {
            if (json.isNull("discountsType")) {
                obj.setDiscountsType(null);
            } else {
                obj.setDiscountsType((String) json.getString("discountsType"));
            }
        }
        if (json.has("promotionDiscount")) {
            if (json.isNull("promotionDiscount")) {
                obj.setPromotionDiscount(null);
            } else {
                obj.setPromotionDiscount((String) json.getString("promotionDiscount"));
            }
        }
        if (json.has("productPrice")) {
            if (json.isNull("productPrice")) {
                obj.setProductPrice(null);
            } else {
                obj.setProductPrice((String) json.getString("productPrice"));
            }
        }
        if (json.has("productPriceDiscount")) {
            if (json.isNull("productPriceDiscount")) {
                obj.setProductPriceDiscount(null);
            } else {
                obj.setProductPriceDiscount((String) json.getString("productPriceDiscount"));
            }
        }
        if (json.has("isInstallment")) {
            if (json.isNull("isInstallment")) {
                obj.setIsInstallment(null);
            } else {
                obj.setIsInstallment((String) json.getString("isInstallment"));
            }
        }
        if (json.has("trademark")) {
            if (json.isNull("trademark")) {
                obj.setTrademark(null);
            } else {
                obj.setTrademark((String) json.getString("trademark"));
            }
        }
        if (json.has("marketPriceFormat")) {
            if (json.isNull("marketPriceFormat")) {
                obj.setMarketPriceFormat(null);
            } else {
                obj.setMarketPriceFormat((String) json.getString("marketPriceFormat"));
            }
        }
        if (json.has("productPriceFormat")) {
            if (json.isNull("productPriceFormat")) {
                obj.setProductPriceFormat(null);
            } else {
                obj.setProductPriceFormat((String) json.getString("productPriceFormat"));
            }
        }
        if (json.has("productPriceDiscountFormat")) {
            if (json.isNull("productPriceDiscountFormat")) {
                obj.setProductPriceDiscountFormat(null);
            } else {
                obj.setProductPriceDiscountFormat((String) json.getString("productPriceDiscountFormat"));
            }
        }
        if (json.has("promotionPercent")) {
            if (json.isNull("promotionPercent")) {
                obj.setPromotionPercent(null);
            } else {
                obj.setPromotionPercent((String) json.getString("promotionPercent"));
            }
        }
        if (json.has("happyHourDiscount")) {
            if (json.isNull("happyHourDiscount")) {
                obj.setHappyHourDiscount(null);
            } else {
                obj.setHappyHourDiscount((String) json.getString("happyHourDiscount"));
            }
        }
        if (json.has("giftCard")) {
            if (json.isNull("giftCard")) {
                obj.setGiftCard(null);
            } else {
                obj.setGiftCard((String) json.getString("giftCard"));
            }
        }
        return obj;
    }

    public static HomeEventResponseLocalStorage createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        HomeEventResponseLocalStorage obj = realm.createObject(HomeEventResponseLocalStorage.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field id to null.");
                } else {
                    obj.setId((int) reader.nextInt());
                }
            } else if (name.equals("moduleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field moduleId to null.");
                } else {
                    obj.setModuleId((int) reader.nextInt());
                }
            } else if (name.equals("eventId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field eventId to null.");
                } else {
                    obj.setEventId((int) reader.nextInt());
                }
            } else if (name.equals("avatar")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setAvatar(null);
                } else {
                    obj.setAvatar((String) reader.nextString());
                }
            } else if (name.equals("productId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductId(null);
                } else {
                    obj.setProductId((String) reader.nextString());
                }
            } else if (name.equals("productName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductName(null);
                } else {
                    obj.setProductName((String) reader.nextString());
                }
            } else if (name.equals("productQuantity")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductQuantity(null);
                } else {
                    obj.setProductQuantity((String) reader.nextString());
                }
            } else if (name.equals("marketPrice")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMarketPrice(null);
                } else {
                    obj.setMarketPrice((String) reader.nextString());
                }
            } else if (name.equals("zoneAlias")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setZoneAlias(null);
                } else {
                    obj.setZoneAlias((String) reader.nextString());
                }
            } else if (name.equals("attributeId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setAttributeId(null);
                } else {
                    obj.setAttributeId((String) reader.nextString());
                }
            } else if (name.equals("numberValue")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setNumberValue(null);
                } else {
                    obj.setNumberValue((String) reader.nextString());
                }
            } else if (name.equals("discountsType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setDiscountsType(null);
                } else {
                    obj.setDiscountsType((String) reader.nextString());
                }
            } else if (name.equals("promotionDiscount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setPromotionDiscount(null);
                } else {
                    obj.setPromotionDiscount((String) reader.nextString());
                }
            } else if (name.equals("productPrice")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPrice(null);
                } else {
                    obj.setProductPrice((String) reader.nextString());
                }
            } else if (name.equals("productPriceDiscount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPriceDiscount(null);
                } else {
                    obj.setProductPriceDiscount((String) reader.nextString());
                }
            } else if (name.equals("isInstallment")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setIsInstallment(null);
                } else {
                    obj.setIsInstallment((String) reader.nextString());
                }
            } else if (name.equals("trademark")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setTrademark(null);
                } else {
                    obj.setTrademark((String) reader.nextString());
                }
            } else if (name.equals("marketPriceFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setMarketPriceFormat(null);
                } else {
                    obj.setMarketPriceFormat((String) reader.nextString());
                }
            } else if (name.equals("productPriceFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPriceFormat(null);
                } else {
                    obj.setProductPriceFormat((String) reader.nextString());
                }
            } else if (name.equals("productPriceDiscountFormat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setProductPriceDiscountFormat(null);
                } else {
                    obj.setProductPriceDiscountFormat((String) reader.nextString());
                }
            } else if (name.equals("promotionPercent")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setPromotionPercent(null);
                } else {
                    obj.setPromotionPercent((String) reader.nextString());
                }
            } else if (name.equals("happyHourDiscount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setHappyHourDiscount(null);
                } else {
                    obj.setHappyHourDiscount((String) reader.nextString());
                }
            } else if (name.equals("giftCard")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    obj.setGiftCard(null);
                } else {
                    obj.setGiftCard((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static HomeEventResponseLocalStorage copyOrUpdate(Realm realm, HomeEventResponseLocalStorage object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        HomeEventResponseLocalStorage realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(HomeEventResponseLocalStorage.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new HomeEventResponseLocalStorageRealmProxy();
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static HomeEventResponseLocalStorage copy(Realm realm, HomeEventResponseLocalStorage newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        HomeEventResponseLocalStorage realmObject = realm.createObject(HomeEventResponseLocalStorage.class, newObject.getId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setId(newObject.getId());
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setEventId(newObject.getEventId());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setProductId(newObject.getProductId());
        realmObject.setProductName(newObject.getProductName());
        realmObject.setProductQuantity(newObject.getProductQuantity());
        realmObject.setMarketPrice(newObject.getMarketPrice());
        realmObject.setZoneAlias(newObject.getZoneAlias());
        realmObject.setAttributeId(newObject.getAttributeId());
        realmObject.setNumberValue(newObject.getNumberValue());
        realmObject.setDiscountsType(newObject.getDiscountsType());
        realmObject.setPromotionDiscount(newObject.getPromotionDiscount());
        realmObject.setProductPrice(newObject.getProductPrice());
        realmObject.setProductPriceDiscount(newObject.getProductPriceDiscount());
        realmObject.setIsInstallment(newObject.getIsInstallment());
        realmObject.setTrademark(newObject.getTrademark());
        realmObject.setMarketPriceFormat(newObject.getMarketPriceFormat());
        realmObject.setProductPriceFormat(newObject.getProductPriceFormat());
        realmObject.setProductPriceDiscountFormat(newObject.getProductPriceDiscountFormat());
        realmObject.setPromotionPercent(newObject.getPromotionPercent());
        realmObject.setHappyHourDiscount(newObject.getHappyHourDiscount());
        realmObject.setGiftCard(newObject.getGiftCard());
        return realmObject;
    }

    static HomeEventResponseLocalStorage update(Realm realm, HomeEventResponseLocalStorage realmObject, HomeEventResponseLocalStorage newObject, Map<RealmObject, RealmObjectProxy> cache) {
        realmObject.setModuleId(newObject.getModuleId());
        realmObject.setEventId(newObject.getEventId());
        realmObject.setAvatar(newObject.getAvatar());
        realmObject.setProductId(newObject.getProductId());
        realmObject.setProductName(newObject.getProductName());
        realmObject.setProductQuantity(newObject.getProductQuantity());
        realmObject.setMarketPrice(newObject.getMarketPrice());
        realmObject.setZoneAlias(newObject.getZoneAlias());
        realmObject.setAttributeId(newObject.getAttributeId());
        realmObject.setNumberValue(newObject.getNumberValue());
        realmObject.setDiscountsType(newObject.getDiscountsType());
        realmObject.setPromotionDiscount(newObject.getPromotionDiscount());
        realmObject.setProductPrice(newObject.getProductPrice());
        realmObject.setProductPriceDiscount(newObject.getProductPriceDiscount());
        realmObject.setIsInstallment(newObject.getIsInstallment());
        realmObject.setTrademark(newObject.getTrademark());
        realmObject.setMarketPriceFormat(newObject.getMarketPriceFormat());
        realmObject.setProductPriceFormat(newObject.getProductPriceFormat());
        realmObject.setProductPriceDiscountFormat(newObject.getProductPriceDiscountFormat());
        realmObject.setPromotionPercent(newObject.getPromotionPercent());
        realmObject.setHappyHourDiscount(newObject.getHappyHourDiscount());
        realmObject.setGiftCard(newObject.getGiftCard());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("HomeEventResponseLocalStorage = [");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{moduleId:");
        stringBuilder.append(getModuleId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{eventId:");
        stringBuilder.append(getEventId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{avatar:");
        stringBuilder.append(getAvatar() != null ? getAvatar() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productId:");
        stringBuilder.append(getProductId() != null ? getProductId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productName:");
        stringBuilder.append(getProductName() != null ? getProductName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productQuantity:");
        stringBuilder.append(getProductQuantity() != null ? getProductQuantity() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{marketPrice:");
        stringBuilder.append(getMarketPrice() != null ? getMarketPrice() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{zoneAlias:");
        stringBuilder.append(getZoneAlias() != null ? getZoneAlias() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{attributeId:");
        stringBuilder.append(getAttributeId() != null ? getAttributeId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{numberValue:");
        stringBuilder.append(getNumberValue() != null ? getNumberValue() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{discountsType:");
        stringBuilder.append(getDiscountsType() != null ? getDiscountsType() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{promotionDiscount:");
        stringBuilder.append(getPromotionDiscount() != null ? getPromotionDiscount() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPrice:");
        stringBuilder.append(getProductPrice() != null ? getProductPrice() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPriceDiscount:");
        stringBuilder.append(getProductPriceDiscount() != null ? getProductPriceDiscount() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{isInstallment:");
        stringBuilder.append(getIsInstallment() != null ? getIsInstallment() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{trademark:");
        stringBuilder.append(getTrademark() != null ? getTrademark() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{marketPriceFormat:");
        stringBuilder.append(getMarketPriceFormat() != null ? getMarketPriceFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPriceFormat:");
        stringBuilder.append(getProductPriceFormat() != null ? getProductPriceFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{productPriceDiscountFormat:");
        stringBuilder.append(getProductPriceDiscountFormat() != null ? getProductPriceDiscountFormat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{promotionPercent:");
        stringBuilder.append(getPromotionPercent() != null ? getPromotionPercent() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{happyHourDiscount:");
        stringBuilder.append(getHappyHourDiscount() != null ? getHappyHourDiscount() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{giftCard:");
        stringBuilder.append(getGiftCard() != null ? getGiftCard() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HomeEventResponseLocalStorageRealmProxy aHomeEventResponseLocalStorage = (HomeEventResponseLocalStorageRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aHomeEventResponseLocalStorage.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aHomeEventResponseLocalStorage.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aHomeEventResponseLocalStorage.row.getIndex()) return false;

        return true;
    }

}
