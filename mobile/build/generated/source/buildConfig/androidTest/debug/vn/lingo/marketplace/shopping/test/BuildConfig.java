/**
 * Automatically generated file. DO NOT MODIFY
 */
package vn.lingo.marketplace.shopping.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "vn.lingo.marketplace.shopping.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 21;
  public static final String VERSION_NAME = "1.1.5";
  // Fields from build type: debug
  public static final String ENDPOINT = "http://tapp.lingo.vn:8086";
  public static final String ENDPOINT_CHECK_OUT = "http://mobile-api.lingo.vn";
}
