package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 07/12/2015.
 */
public class LeafChildrenProductsByCategoryResponse implements Serializable {

    @SerializedName("count")
    public int count;
    @SerializedName("items")
    public List<LeafChildrenCategoryResponse> items;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<LeafChildrenCategoryResponse> getItems() {
        return items;
    }

    public void setItems(List<LeafChildrenCategoryResponse> items) {
        this.items = items;
    }
}
