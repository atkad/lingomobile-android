package vn.lingo.marketplace.shopping.presenters.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.adapter.home.LingoHomePageBestIndustryServiceRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.OnRcvScrollListener;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.EventByTypesAPI;
import vn.lingo.marketplace.shopping.models.HomeEventResponse;
import vn.lingo.marketplace.shopping.models.ListHomeEventsResponse;
import vn.lingo.marketplace.shopping.models.local.HomeEventResponseLocalStorage;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 13/11/2015.
 */
public class LingoHomePageBestIndustryService implements RecyclerViewItemClickListener {

    private String TAG = LingoHomePageBestIndustryService.class.getName();

    private final int totalDocumentOfPage = 5;
    private int currentPage = 1;
    private Retrofit retrofit;
    private Context context;
    private DisplayImageOptions displayImageOptions;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private boolean isProcessingLoadMore = false;
    private Realm realm;
    private List<HomeEventResponseLocalStorage> listHomeEventResponseLocalStorage;
    private LingoHomePageBestIndustryServiceRecyclerViewAdapter lingoHomePageBestIndustryServiceRecyclerViewAdapter;
    private boolean reloadhomePageBestIndustry = false;
    private RecyclerView recyclerView;

    /****
     * @param context
     * @param retrofit
     * @param displayImageOptions
     * @param recyclerViewItemClickListener
     */
    public LingoHomePageBestIndustryService(Context context, Retrofit retrofit, DisplayImageOptions displayImageOptions,
                                            RecyclerViewItemClickListener recyclerViewItemClickListener, RecyclerView recyclerView) {
        this.context = context;
        this.retrofit = retrofit;
        this.displayImageOptions = displayImageOptions;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        this.recyclerView = recyclerView;
        realm = Realm.getDefaultInstance();
        listHomeEventResponseLocalStorage = new ArrayList<HomeEventResponseLocalStorage>();
        lingoHomePageBestIndustryServiceRecyclerViewAdapter
                = new LingoHomePageBestIndustryServiceRecyclerViewAdapter(context, listHomeEventResponseLocalStorage,
                LingoHomePageBestIndustryService.this, displayImageOptions);
        recyclerView.setAdapter(lingoHomePageBestIndustryServiceRecyclerViewAdapter);
    }

    /****
     * @param eventId
     */
    private void setBestIndustryAdapter(int eventId) {
        listHomeEventResponseLocalStorage.clear();
        lingoHomePageBestIndustryServiceRecyclerViewAdapter.notifyDataSetChanged();
        RealmQuery<HomeEventResponseLocalStorage> realmQueryHomeEventResponseLocalStorage =
                realm.where(HomeEventResponseLocalStorage.class)
                        .equalTo("moduleId", Constant.MODULE_BEST_INDUSTRY)
                        .equalTo("eventId", eventId);
        RealmResults<HomeEventResponseLocalStorage> realmResultsHomeEventResponseLocalStorage = realmQueryHomeEventResponseLocalStorage.findAll();
        Iterator<HomeEventResponseLocalStorage> iteratorHomeEventResponseLocalStorage = realmResultsHomeEventResponseLocalStorage.iterator();
        while (iteratorHomeEventResponseLocalStorage.hasNext()) {
            HomeEventResponseLocalStorage homeEventResponseLocalStorage = iteratorHomeEventResponseLocalStorage.next();
            listHomeEventResponseLocalStorage.add(homeEventResponseLocalStorage);
            lingoHomePageBestIndustryServiceRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    /***
     *
     */
    public void fetchingBestIndustry(final int eventId) {
        try {
            RealmQuery<HomeEventResponseLocalStorage> realmQueryHomeEventResponseLocalStorage =
                    realm.where(HomeEventResponseLocalStorage.class)
                            .equalTo("moduleId", Constant.MODULE_BEST_INDUSTRY)
                            .equalTo("eventId", eventId);
            RealmResults<HomeEventResponseLocalStorage> realmResultsHomeEventResponseLocalStorage = realmQueryHomeEventResponseLocalStorage.findAll();
            if (realmResultsHomeEventResponseLocalStorage.size() > 0) {
                Log.i(TAG, "LingoHomePageBestIndustryService load data from cache.............");
                setBestIndustryAdapter(eventId);
                reloadhomePageBestIndustry = false;
            } else {
                Log.i(TAG, "LingoHomePageBestIndustryService load data from server.............");
                reloadhomePageBestIndustry = true;
            }
            final EventByTypesAPI eventByTypesAPI = retrofit.create(EventByTypesAPI.class);
            Call<ListHomeEventsResponse> callListHomeEventsResponse = null;
            if (eventId == 5) {
                callListHomeEventsResponse = eventByTypesAPI.getDetailItemInAllEvent(eventId, totalDocumentOfPage, currentPage);
            } else {
                callListHomeEventsResponse = eventByTypesAPI.getDetailItemInOneEvent(eventId, totalDocumentOfPage, currentPage);
            }
            if (null != callListHomeEventsResponse) {
                callListHomeEventsResponse.enqueue(new Callback<ListHomeEventsResponse>() {
                    @Override
                    public void onResponse(Response<ListHomeEventsResponse> response, Retrofit retrofit) {
                        if (response.code() == 200 && null != response.body().getListHomeEventResponse() &&
                                response.body().getListHomeEventResponse().size() > 0) {
                            List<HomeEventResponse> listHomeEventResponseTemporary = response.body().getListHomeEventResponse();
                            final int count = 15/*response.body().getCount()*/;
                            for (HomeEventResponse homeEventResponse : listHomeEventResponseTemporary) {
                                realm.beginTransaction();
                                HomeEventResponseLocalStorage homeEventResponseLocalStorage = HomeEventResponseLocalStorage.convertFromHomeEventResponse(
                                        eventId, homeEventResponse);
                                realm.copyToRealmOrUpdate(homeEventResponseLocalStorage);
                                realm.commitTransaction();
                            }
                            if (reloadhomePageBestIndustry) {
                                setBestIndustryAdapter(eventId);
                            }
                            recyclerView.addOnScrollListener(new OnRcvScrollListener() {
                                @Override
                                public void onLoadMore(int totalItemCount) {
                                    super.onLoadMore(totalItemCount);
                                    if ((currentPage * totalDocumentOfPage) < count && !isProcessingLoadMore) {
                                        isProcessingLoadMore = true;
                                        currentPage++;
                                        listHomeEventResponseLocalStorage.add(null);
                                        lingoHomePageBestIndustryServiceRecyclerViewAdapter.notifyItemInserted(listHomeEventResponseLocalStorage.size() - 1);
                                        Call<ListHomeEventsResponse> callListHomeEventsResponseLoadMore = null;
                                        if (eventId == 5) {
                                            callListHomeEventsResponseLoadMore = eventByTypesAPI.getDetailItemInAllEvent(eventId, totalDocumentOfPage, currentPage);
                                        } else {
                                            callListHomeEventsResponseLoadMore = eventByTypesAPI.getDetailItemInOneEvent(eventId, totalDocumentOfPage, currentPage);
                                        }
                                        callListHomeEventsResponseLoadMore.enqueue(new Callback<ListHomeEventsResponse>() {
                                            @Override
                                            public void onResponse(Response<ListHomeEventsResponse> response, Retrofit retrofit) {
                                                listHomeEventResponseLocalStorage.remove(null);
                                                lingoHomePageBestIndustryServiceRecyclerViewAdapter.notifyDataSetChanged();
                                                if (response.code() == 200 && null != response.body().getListHomeEventResponse() &&
                                                        response.body().getListHomeEventResponse().size() > 0) {
                                                    for (HomeEventResponse homeEventResponse : response.body().getListHomeEventResponse()) {
                                                        HomeEventResponseLocalStorage homeEventResponseLocalStorage = HomeEventResponseLocalStorage.convertFromHomeEventResponse(
                                                                eventId, homeEventResponse);
                                                        listHomeEventResponseLocalStorage.add(homeEventResponseLocalStorage);
                                                        lingoHomePageBestIndustryServiceRecyclerViewAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                                isProcessingLoadMore = false;
                                            }

                                            @Override
                                            public void onFailure(Throwable t) {
                                                isProcessingLoadMore = false;
                                                listHomeEventResponseLocalStorage.remove(null);
                                                lingoHomePageBestIndustryServiceRecyclerViewAdapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                }
                            });
                        }

                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            }

        } catch (Exception exception) {
            Log.i(TAG, exception.toString());
        }
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        recyclerViewItemClickListener.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
