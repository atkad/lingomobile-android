package vn.lingo.marketplace.shopping.models;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 07/12/2015.
 */
public interface LeafChildrenCategoryAPI {

    @GET("/v1/mobile/Home/GetProductsByCategory")
    Call<LeafChildrenProductsByCategoryResponse> getProductsByCategory(@Query("i_id") int i_id, @Query("i_sort_name") String i_sort_name,
                                                                       @Query("i_sort_type") String i_sort_type,
                                                                       @Query("i_size") int i_size, @Query("i_page") int i_page);
}
