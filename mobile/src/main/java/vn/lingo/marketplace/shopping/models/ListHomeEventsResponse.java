package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 16/11/2015.
 */
public class ListHomeEventsResponse implements Serializable {

    @SerializedName("count")
    private int count;

    @SerializedName("items")
    private List<HomeEventResponse> listHomeEventResponse;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<HomeEventResponse> getListHomeEventResponse() {
        return listHomeEventResponse;
    }

    public void setListHomeEventResponse(List<HomeEventResponse> listHomeEventResponse) {
        this.listHomeEventResponse = listHomeEventResponse;
    }
}
