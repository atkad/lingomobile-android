package vn.lingo.marketplace.shopping.listeners;

import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;

/**
 * Created by longtran on 11/12/2015.
 */
public interface ShoppingCartListener {

    public void setOnListener(int actionID, int position, ShoppingCartEntity shoppingCartEntity);

}
