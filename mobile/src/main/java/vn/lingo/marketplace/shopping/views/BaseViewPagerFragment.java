package vn.lingo.marketplace.shopping.views;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import me.relex.seamlessviewpagerheader.tools.ScrollableFragmentListener;
import me.relex.seamlessviewpagerheader.tools.ScrollableListener;
import vn.lingo.marketplace.shopping.R;

public abstract class BaseViewPagerFragment extends AbstractFragment implements ScrollableListener {

    private static final String TAG = "BaseViewPagerFragment";
    protected ScrollableFragmentListener mListener;
    protected static final String BUNDLE_FRAGMENT_INDEX = "BaseFragment.BUNDLE_FRAGMENT_INDEX";
    protected int mFragmentIndex;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mListener = (ScrollableFragmentListener) getActivity();
            Bundle bundle = getArguments();
            if (bundle != null) {
                mFragmentIndex = bundle.getInt(BUNDLE_FRAGMENT_INDEX, 0);
            }
            if (mListener != null) {
                mListener.onFragmentAttached(this, mFragmentIndex);
            }
        } catch (ClassCastException e) {
            Log.e(TAG, getActivity().toString() + " must implement ScrollableFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        if (mListener != null) {
            mListener.onFragmentDetached(this, mFragmentIndex);
        }
        super.onDetach();
        mListener = null;
    }

    /***
     * @return
     */
    public MaterialDialog getMaterialDialog() {
        MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                //.title(R.string.progress_dialog)
                //.content(R.string.please_wait)
                .customView(R.layout.progress_indicator_layout, true)
                //.progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .backgroundColorRes(R.color.transparent)
                .titleColorRes(R.color.transparent)
                .contentColor(Color.TRANSPARENT)
                .progressIndeterminateStyle(false)
                .show();
        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return materialDialog;
    }
}
