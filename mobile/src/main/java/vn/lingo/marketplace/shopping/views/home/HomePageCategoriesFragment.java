package vn.lingo.marketplace.shopping.views.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.jsoup.helper.StringUtil;

import butterknife.Bind;
import me.relex.seamlessviewpagerheader.delegate.AbsRecyclerViewDelegate;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.LeafChildrenCategoryActivity;
import vn.lingo.marketplace.shopping.activities.SubCategoriesActivity;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.presenters.home.HomePageCategoriesPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.utils.JVMRuntime;
import vn.lingo.marketplace.shopping.views.BaseViewPagerFragment;
import vn.lingo.marketplace.shopping.widget.JRecyclerView;

/**
 * Created by longtran on 12/11/2015.
 */
public class HomePageCategoriesFragment extends BaseViewPagerFragment implements HomePageSubIndustriesView {

    private final String TAG = HomePageCategoriesFragment.class.getName();

    @Bind(R.id.home_page_categories_fragment_j_recycler_view_id)
    JRecyclerView recyclerViewHomePageSubIndustries;
    private AbsRecyclerViewDelegate absRecyclerViewDelegate;
    private HomePageCategoriesPresenter homePageCategoriesPresenter;

    public HomePageCategoriesFragment() {
        super();
        Log.i(TAG, "HomePageCategoriesFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homePageCategoriesPresenter = new HomePageCategoriesPresenter(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");
        absRecyclerViewDelegate = new AbsRecyclerViewDelegate();
        String eventId = getArguments().getString(Constant.Categories.ZONE_ID);
        int zoneLevel = 0;
        try {
            zoneLevel = Integer.parseInt(getArguments().getString(Constant.Categories.ZONE_LEVEL));
        } catch (Exception exception) {
            Log.e(TAG, exception.toString());
        }
        CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
        categoriesParcelable.setId(Integer.parseInt(eventId));
        categoriesParcelable.setZoneLevel(zoneLevel);
        Log.i(TAG, "categoriesParcelable : " + categoriesParcelable.getId());
        homePageCategoriesPresenter.fetchingHomePageSubIndustries(categoriesParcelable);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home_page_categories_layout;
    }

    @Override
    public int getFragmentViewId() {
        return R.id.fragment_home_page_categories_layout_id;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public RecyclerView getRecyclerViewSubIndustries() {
        recyclerViewHomePageSubIndustries.addItemDecoration(new DividerItemDecoration(1));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewHomePageSubIndustries.setLayoutManager(linearLayoutManager);
        return recyclerViewHomePageSubIndustries;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        Log.i(TAG, "position : " + position + "mappingZoneId :" + mappingZoneId);
        if (!StringUtil.isBlank(title)) {
            AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", title, true);
        }
        if (zoneLevel == 3) {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setZoneLevel(zoneLevel);
            categoriesParcelable.setTitle(title);
            categoriesParcelable.setId(id);
            categoriesParcelable.setMappingZoneId(mappingZoneId);
            Intent intent = new Intent();
            intent.setClassName(getActivity(), LeafChildrenCategoryActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            this.startActivity(intent);
        } else {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setId(id);
            categoriesParcelable.setZoneLevel(zoneLevel);
            categoriesParcelable.setTitle(title);
            Intent intent = new Intent();
            intent.setClassName(getActivity(), SubCategoriesActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            this.startActivity(intent);
        }
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {

    }

    @Override
    public void hideProcessing() {

    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getActivity().getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public DisplayImageOptions getDisplayImageOptions() {
        if (getActivity() == null) {
            return null;
        }
        LingoApplication lingoApplication = (LingoApplication) getActivity().getApplication();
        return lingoApplication.getDisplayImageOptions();
    }

    @Override
    public void onDestroy() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onDestroy();
    }

    @Override
    public boolean isViewBeingDragged(MotionEvent event) {
        return absRecyclerViewDelegate.isViewBeingDragged(event, getRecyclerViewSubIndustries());
    }
}
