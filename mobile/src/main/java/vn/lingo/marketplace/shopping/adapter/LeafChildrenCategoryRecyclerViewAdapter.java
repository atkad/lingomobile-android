package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.LeafChildrenCategoryResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.home.viewholder.ProgressViewHolder;
import vn.lingo.marketplace.shopping.views.viewholder.LeafChildrenCategoryRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class LeafChildrenCategoryRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = LeafChildrenCategoryRecyclerViewAdapter.class.getName();
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private List<LeafChildrenCategoryResponse> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    public LeafChildrenCategoryRecyclerViewAdapter(Context context, List<LeafChildrenCategoryResponse> itemList,
                                                   RecyclerViewItemClickListener recyclerViewItemClickListener, DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.context = context;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaf_children_category_item_layout, null);
            LeafChildrenCategoryRecyclerViewHolder viewHolder = new LeafChildrenCategoryRecyclerViewHolder(view);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);
            ProgressViewHolder progressViewHolder = new ProgressViewHolder(view);
            return progressViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof LeafChildrenCategoryRecyclerViewHolder) {
            LeafChildrenCategoryRecyclerViewHolder leafChildrenCategoryRecyclerViewHolder = (LeafChildrenCategoryRecyclerViewHolder) holder;
            final LeafChildrenCategoryResponse leafChildrenCategoryResponse = itemList.get(position);
            bindDataViewHolder(position, leafChildrenCategoryRecyclerViewHolder, leafChildrenCategoryResponse);
        } else {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBarLoadMore.setIndeterminate(true);
        }
    }

    /***
     * @param position
     * @param leafChildrenCategoryRecyclerViewHolder
     * @param leafChildrenCategoryResponse
     */
    private void bindDataViewHolder(final int position, final LeafChildrenCategoryRecyclerViewHolder leafChildrenCategoryRecyclerViewHolder,
                                    final LeafChildrenCategoryResponse leafChildrenCategoryResponse) {
        /***
         * cancel any loading images on this view
         */
        try {
            leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoryProductionTitle.setText(leafChildrenCategoryResponse.getProductName());
            leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoriesPrices.setText(String.format(context.getResources().getString(R.string.prices),
                    leafChildrenCategoryResponse.getProductPriceFormat()));
            if (StringUtils.isBlank(leafChildrenCategoryResponse.getMarketPriceFormat())) {
                leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoriesMarketPrice.setVisibility(View.GONE);
            } else {
                leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoriesMarketPrice.setVisibility(View.VISIBLE);
                leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoriesMarketPrice.setText(String.format(context.getResources().getString(R.string.market_price),
                        leafChildrenCategoryResponse.getMarketPriceFormat()));
            }
            leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoryRatingBar.setNumStars(Constant.TOTAL_STAR);
            leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoryRatingBar.setRating(Float.parseFloat(leafChildrenCategoryResponse.getRatingStar()));
            if (StringUtils.isBlank(leafChildrenCategoryResponse.getPromotionPercent())) {
                leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoryLinearLayoutPercent.setVisibility(View.GONE);
            } else {
                leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoryLinearLayoutPercent.setVisibility(View.VISIBLE);
                leafChildrenCategoryRecyclerViewHolder.leafChildrenCategoryPercent.setText(leafChildrenCategoryResponse.getPromotionPercent());
            }
        } catch (Exception exception) {
            Log.e(TAG, exception.toString());
        }
//        try {
//            ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + leafChildrenCategoryResponse.getAvatar(),
//                    leafChildrenCategoryRecyclerViewHolder.imageView, displayImageOptions);
//        } catch (OutOfMemoryError outOfMemoryError) {
//            Log.e(TAG, outOfMemoryError.toString());
//        }
        String urlAvatar = Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + leafChildrenCategoryResponse.getAvatar();
        try {
            ImageSize targetSize = new ImageSize(256, 256);
            ImageLoader.getInstance().loadImage(urlAvatar, targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            leafChildrenCategoryRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            leafChildrenCategoryRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                            leafChildrenCategoryRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            leafChildrenCategoryRecyclerViewHolder.imageView.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            Log.e(TAG, outOfMemoryError.toString());
        }
        leafChildrenCategoryRecyclerViewHolder.relativeLayoutRowItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(leafChildrenCategoryResponse.getProductId()),
                            Integer.MAX_VALUE, Integer.MAX_VALUE, leafChildrenCategoryResponse.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
    }
}
