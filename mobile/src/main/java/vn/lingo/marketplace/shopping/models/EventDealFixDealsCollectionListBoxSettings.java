package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 09/12/2015.
 */
public class EventDealFixDealsCollectionListBoxSettings implements Serializable {

    @SerializedName("BOX_URL")
    private String boxUrl;
    @SerializedName("CREATEDBY")
    private String createdBy;
    @SerializedName("BOX_TITLE")
    private String boxTitle;
    @SerializedName("MODIFYON")
    private String modifyOn;
    @SerializedName("BOX_TEMPLATE_ID")
    private String boxTemplateId;
    @SerializedName("CREATEDON")
    private String createdOn;
    @SerializedName("MICROSITE_ID")
    private String microSiteId;
    @SerializedName("MODIFYBY")
    private String modifyBy;
    @SerializedName("DETAIL_ID")
    private String detailId;
    @SerializedName("BOX_PRIORITY")
    private String boxPriority;

    public String getBoxUrl() {
        return boxUrl;
    }

    public void setBoxUrl(String boxUrl) {
        this.boxUrl = boxUrl;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getBoxTitle() {
        return boxTitle;
    }

    public void setBoxTitle(String boxTitle) {
        this.boxTitle = boxTitle;
    }

    public String getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;
    }

    public String getBoxTemplateId() {
        return boxTemplateId;
    }

    public void setBoxTemplateId(String boxTemplateId) {
        this.boxTemplateId = boxTemplateId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getMicroSiteId() {
        return microSiteId;
    }

    public void setMicroSiteId(String microSiteId) {
        this.microSiteId = microSiteId;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getBoxPriority() {
        return boxPriority;
    }

    public void setBoxPriority(String boxPriority) {
        this.boxPriority = boxPriority;
    }
}
