package vn.lingo.marketplace.shopping.listeners;

import android.view.View;

/**
 * Created by longtran on 14/11/2015.
 */
public interface StickyEventListener {

    public void startStickingViewListener(View viewThatShouldStick);

    public void stopStickingCurrentlyStickingViewListener();
}
