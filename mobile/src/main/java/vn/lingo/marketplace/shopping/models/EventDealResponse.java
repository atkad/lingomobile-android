package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 08/12/2015.
 */
public class EventDealResponse implements Serializable {

    @SerializedName("AVATAR")
    private String avatar;
    @SerializedName("NAME")
    private String name;
    @SerializedName("TO_DATE")
    private String toDate;
    @SerializedName("DESCRIPTION")
    private String description;
    @SerializedName("MICROSITE_TYPE")
    private String microSiteType;
    @SerializedName("MICROSITE_KEY")
    private String microSiteKey;
    @SerializedName("STATUS")
    private String status;
    @SerializedName("FROM_DATE")
    private String fromDate;
    @SerializedName("CREATED_BY")
    private String createdBy;
    @SerializedName("CREATED_AT")
    private String createdAt;
    @SerializedName("UPDATED_BY")
    private String updatedBy;
    @SerializedName("ID")
    private String id;
    @SerializedName("UPDATED_AT")
    private String updatedAt;
    @SerializedName("TYPE")
    private String type;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMicroSiteType() {
        return microSiteType;
    }

    public void setMicroSiteType(String microSiteType) {
        this.microSiteType = microSiteType;
    }

    public String getMicroSiteKey() {
        return microSiteKey;
    }

    public void setMicroSiteKey(String microSiteKey) {
        this.microSiteKey = microSiteKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
