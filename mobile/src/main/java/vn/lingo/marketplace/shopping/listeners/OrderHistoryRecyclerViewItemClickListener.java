package vn.lingo.marketplace.shopping.listeners;

import vn.lingo.marketplace.shopping.models.OrderHistoryLocalStorage;

/**
 * Created by longtran on 12/01/2016.
 */
public interface OrderHistoryRecyclerViewItemClickListener {
    public void onItemClicked(int position, OrderHistoryLocalStorage orderHistoryLocalStorage);
}
