package vn.lingo.marketplace.shopping.views.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 06/12/2015.
 */
public class PromotionProgramViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.promotion_program_card_view_item)
    public RelativeLayout relativeLayout;

    @Bind(R.id.promotion_program_image_view_header)
    public ImageView imageViewPromotionProgram;

    public PromotionProgramViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
