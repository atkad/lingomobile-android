package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.presenters.MoreBigFriDayPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.MoreBigFriDayView;

/**
 * Created by longtran on 23/12/2015.
 */
public class MoreBigFriDayActivity extends AbstractAppCompatActivity implements MoreBigFriDayView {

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.activity_more_big_fri_day_layout_recycle_view_id)
    RecyclerView recyclerView;

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    private MoreBigFriDayPresenter moreBigFriDayPresenter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_big_fri_day_layout);
        ButterKnife.bind(this);
        moreBigFriDayPresenter = new MoreBigFriDayPresenter(this);
        moreBigFriDayPresenter.fetchingMoreBigFriDay();
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public RecyclerView getRecyclerView() {
        recyclerView.addItemDecoration(new DividerItemDecoration(1));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        return recyclerView;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Intent intent = new Intent();
        intent.setClassName(this, ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        moreBigFriDayPresenter.invalidateToolbar();
        super.onResume();
    }
}
