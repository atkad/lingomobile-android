package vn.lingo.marketplace.shopping.models;

import java.io.Serializable;

/**
 * Created by longtran on 06/12/2015.
 */
public class PromotionProgramResponse implements Serializable {

    private String id;

    private String promotionProgramAvatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPromotionProgramAvatar() {
        return promotionProgramAvatar;
    }

    public void setPromotionProgramAvatar(String promotionProgramAvatar) {
        this.promotionProgramAvatar = promotionProgramAvatar;
    }
}
