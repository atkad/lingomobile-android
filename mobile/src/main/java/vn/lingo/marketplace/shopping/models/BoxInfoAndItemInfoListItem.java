package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 12/12/2015.
 */
public class BoxInfoAndItemInfoListItem implements Serializable {

    @SerializedName("AVATAR")
    private String avatar;
    @SerializedName("PRODUCT_ID")
    private String productId;
    @SerializedName("PRODUCT_NAME")
    private String productName;
    @SerializedName("PROMOTION_ID")
    private String promotionId;
    @SerializedName("ATTRIBUTES_SET_ID")
    private String attributesSetId;
    @SerializedName("DETAIL_ID")
    private String detailId;
    @SerializedName("RATING_NUM")
    private String ratingNum;
    @SerializedName("DISCOUNTS_TYPE")
    private String discountsType;
    @SerializedName("WEIGHT")
    private String weight;
    @SerializedName("PRODUCT_PRICE")
    private String productPrice;
    @SerializedName("PRODUCT_PRICEDISCOUNT")
    private String productPriceDiscount;
    @SerializedName("DISCOUNT")
    private String discount;
    @SerializedName("PARTNER_NAME")
    private String partnerName;
    @SerializedName("PARTNER_ID")
    private String partnerId;
    @SerializedName("ITEM_PRIORITY")
    private String itemPriority;
    @SerializedName("SKU")
    private String sku;
    @SerializedName("PRODUCT_QUANTITY")
    private String productQuantity;
    @SerializedName("ISVAT")
    private String isVat;
    @SerializedName("MARKET_PRICE")
    private String marketPrice;
    @SerializedName("RATING_STAR")
    private String ratingStar;
    @SerializedName("PRODUCT_TYPE")
    private String productType;
    @SerializedName("PROMOTION_DISCOUNT")
    private String promotionDiscount;
    @SerializedName("SHIP_FREE")
    private String shipFree;
    @SerializedName("VAT")
    private String vat;
    @SerializedName("TRADEMARK")
    private String tradeMark;
    @SerializedName("ISACTIVE")
    private String isActive;
    @SerializedName("MARKET_PRICE_FORMAT")
    private String marketPriceFormat;
    @SerializedName("PRODUCT_PRICE_FORMAT")
    private String productPriceFormat;
    @SerializedName("PRODUCT_PRICEDISCOUNT_FORMAT")
    private String productPriceDiscountFormat;
    @SerializedName("PROMOTIONPERCENT")
    private String promotionPercent;
    @SerializedName("M_GOLD_HOUR")
    private String happyHourDiscount;
    @SerializedName("M_GIFT")
    private String giftCard;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getAttributesSetId() {
        return attributesSetId;
    }

    public void setAttributesSetId(String attributesSetId) {
        this.attributesSetId = attributesSetId;
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getRatingNum() {
        return ratingNum;
    }

    public void setRatingNum(String ratingNum) {
        this.ratingNum = ratingNum;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductPriceDiscount() {
        return productPriceDiscount;
    }

    public void setProductPriceDiscount(String productPriceDiscount) {
        this.productPriceDiscount = productPriceDiscount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getItemPriority() {
        return itemPriority;
    }

    public void setItemPriority(String itemPriority) {
        this.itemPriority = itemPriority;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getIsVat() {
        return isVat;
    }

    public void setIsVat(String isVat) {
        this.isVat = isVat;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getRatingStar() {
        return ratingStar;
    }

    public void setRatingStar(String ratingStar) {
        this.ratingStar = ratingStar;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(String promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public String getShipFree() {
        return shipFree;
    }

    public void setShipFree(String shipFree) {
        this.shipFree = shipFree;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getTradeMark() {
        return tradeMark;
    }

    public void setTradeMark(String tradeMark) {
        this.tradeMark = tradeMark;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getMarketPriceFormat() {
        return marketPriceFormat;
    }

    public void setMarketPriceFormat(String marketPriceFormat) {
        this.marketPriceFormat = marketPriceFormat;
    }

    public String getProductPriceFormat() {
        return productPriceFormat;
    }

    public void setProductPriceFormat(String productPriceFormat) {
        this.productPriceFormat = productPriceFormat;
    }

    public String getProductPriceDiscountFormat() {
        return productPriceDiscountFormat;
    }

    public void setProductPriceDiscountFormat(String productPriceDiscountFormat) {
        this.productPriceDiscountFormat = productPriceDiscountFormat;
    }

    public String getPromotionPercent() {
        return promotionPercent;
    }

    public void setPromotionPercent(String promotionPercent) {
        this.promotionPercent = promotionPercent;
    }

    public String getHappyHourDiscount() {
        return happyHourDiscount;
    }

    public void setHappyHourDiscount(String happyHourDiscount) {
        this.happyHourDiscount = happyHourDiscount;
    }

    public String getGiftCard() {
        return giftCard;
    }

    public void setGiftCard(String giftCard) {
        this.giftCard = giftCard;
    }
}
