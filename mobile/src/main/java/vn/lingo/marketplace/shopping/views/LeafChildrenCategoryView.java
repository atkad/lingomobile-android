package vn.lingo.marketplace.shopping.views;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

/**
 * Created by longtran on 13/11/2015.
 */
public interface LeafChildrenCategoryView extends AbstractView {

    public AppCompatSpinner getAppCompatSpinnerDataFilter();

    public AppCompatActivity getAppCompatActivity();

    public RecyclerView getRecyclerViewLeafChildrenCategory();

    public Toolbar getToolbar();

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);

}
