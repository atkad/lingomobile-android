package vn.lingo.marketplace.shopping.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by longtran on 10/11/2015.
 */
public class HotDealsLocalStorage extends RealmObject {

    @PrimaryKey
    private String zoneId;

    private String zoneAvatar;

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneAvatar() {
        return zoneAvatar;
    }

    public void setZoneAvatar(String zoneAvatar) {
        this.zoneAvatar = zoneAvatar;
    }
}
