package vn.lingo.marketplace.shopping.views.viewholder;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class OrderHistoryRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.order_history_recycler_view_row_item_image_view)
    public ImageView imageView;

    @Bind(R.id.order_history_recycler_view_row_item_description)
    public AppCompatTextView appCompatTextViewDescription;

    @Bind(R.id.app_compat_text_view_order_time)
    public AppCompatTextView appCompatTextViewOrderTime;

    @Bind(R.id.order_history_recycler_view_row_item)
    public RelativeLayout relativeLayoutRowItem;

    public OrderHistoryRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
