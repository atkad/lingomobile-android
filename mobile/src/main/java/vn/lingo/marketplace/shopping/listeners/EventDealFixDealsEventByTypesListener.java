package vn.lingo.marketplace.shopping.listeners;

/**
 * Created by longtran on 12/12/2015.
 */
public interface EventDealFixDealsEventByTypesListener {
    public void onItemClickedEventDealFixDealsEventByTypes(int position, int id, String micrositeKey, String micrositeType);
}
