package vn.lingo.marketplace.shopping.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by longtran on 12/11/2015.
 */
public class CategoriesParcelable implements Parcelable {

    private int categoriesID;

    private int mappingZoneId;

    private String title = "Lingo";

    private String sortName;

    private String sortType;

    private int zoneLevel;

    public int getId() {
        return categoriesID;
    }

    public void setId(int categoriesID) {
        this.categoriesID = categoriesID;
    }

    public int getMappingZoneId() {
        return mappingZoneId;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public void setMappingZoneId(int mappingZoneId) {
        this.mappingZoneId = mappingZoneId;
    }

    public String getTitle() {
        return title;
    }

    public int getZoneLevel() {
        return zoneLevel;
    }

    public void setZoneLevel(int zoneLevel) {
        this.zoneLevel = zoneLevel;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static final Parcelable.Creator<CategoriesParcelable> CREATOR = new Creator<CategoriesParcelable>() {
        public CategoriesParcelable createFromParcel(Parcel source) {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.categoriesID = source.readInt();
            categoriesParcelable.sortName = source.readString();
            categoriesParcelable.sortType = source.readString();
            categoriesParcelable.mappingZoneId = source.readInt();
            categoriesParcelable.title = source.readString();
            categoriesParcelable.zoneLevel = source.readInt();
            return categoriesParcelable;
        }

        public CategoriesParcelable[] newArray(int size) {
            return new CategoriesParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoriesID);
        dest.writeString(sortName);
        dest.writeString(sortType);
        dest.writeInt(mappingZoneId);
        dest.writeString(title);
        dest.writeInt(zoneLevel);
    }
}
