package vn.lingo.marketplace.shopping.views;

import android.view.View;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 05/11/2015.
 */
public class ProductDetailsImageMetadataViewHolder {

    @Bind(R.id.image_view_metadata)
    public ImageView imageView;

    public ProductDetailsImageMetadataViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }
}
