//package vn.lingo.marketplace.shopping.views.home.viewholder;
//
//import android.support.v7.widget.AppCompatTextView;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//import vn.lingo.marketplace.shopping.R;
//
///**
// * Created by longtran on 09/11/2015.
// */
//public class HomeFixDealsRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//    @Bind(R.id.image_view_one)
//    public ImageView imageView;
//
//    @Bind(R.id.hot_deal_title)
//    public AppCompatTextView hotDealTitle;
//
//    @Bind(R.id.home_fix_deal_item_id)
//    public FrameLayout cardView;
//
//    public HomeFixDealsRecyclerViewHolder(View itemView) {
//        super(itemView);
//        ButterKnife.bind(this, itemView);
//        itemView.setOnClickListener(this);
//    }
//
//    @Override
//    public void onClick(View v) {
//
//    }
//}
