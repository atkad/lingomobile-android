package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 10/12/2015.
 */
public class ProductClientInfoCheckoutRequest implements Serializable {

    @SerializedName("devLocalTime")
    private String devLocalTime;
    @SerializedName("devId")
    private int devId;
    @SerializedName("devOS")
    private String devOS;

    public String getDevLocalTime() {
        return devLocalTime;
    }

    public void setDevLocalTime(String devLocalTime) {
        this.devLocalTime = devLocalTime;
    }

    public int getDevId() {
        return devId;
    }

    public void setDevId(int devId) {
        this.devId = devId;
    }

    public String getDevOS() {
        return devOS;
    }

    public void setDevOS(String devOS) {
        this.devOS = devOS;
    }
}
