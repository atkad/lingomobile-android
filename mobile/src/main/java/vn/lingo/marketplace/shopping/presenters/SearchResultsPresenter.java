package vn.lingo.marketplace.shopping.presenters;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.adapter.LeafChildrenCategoryFilterAdapter;
import vn.lingo.marketplace.shopping.adapter.search.SearchResultsRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.OnRcvScrollListener;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.ProductionFilterEntity;
import vn.lingo.marketplace.shopping.models.search.SearchProductResults;
import vn.lingo.marketplace.shopping.models.search.SearchResultsAPI;
import vn.lingo.marketplace.shopping.models.search.SearchResultsResponse;
import vn.lingo.marketplace.shopping.views.SearchResultsView;

/**
 * Created by longtran on 06/12/2015.
 */
public class SearchResultsPresenter implements RecyclerViewItemClickListener {

    private final String TAG = SearchResultsPresenter.class.getName();
    private final int totalDocumentOfPage = 15;
    private int currentPage = 1;
    private int count;
    private SearchResultsView searchResultsView;
    private List<SearchResultsResponse> listSearchResultsResponseGlobal;
    private SearchResultsRecyclerViewAdapter searchResultsRecyclerViewAdapter;
    private SearchResultsAPI searchResultsAPI;
    private String sortName;
    private String sortType;
    /***
     * @param searchResultsView
     */
    public SearchResultsPresenter(SearchResultsView searchResultsView) {
        this.searchResultsView = searchResultsView;
        searchResultsAPI = searchResultsView.getRetrofit().create(SearchResultsAPI.class);
        searchResultsView.getRelativeLayoutNotFound().setVisibility(View.GONE);
        listSearchResultsResponseGlobal = new ArrayList<SearchResultsResponse>();
        searchResultsRecyclerViewAdapter = new SearchResultsRecyclerViewAdapter(searchResultsView.getContext(),
                listSearchResultsResponseGlobal, SearchResultsPresenter.this,
                searchResultsView.getDisplayImageOptions());
        searchResultsView.getRecyclerSearchResults().setAdapter(searchResultsRecyclerViewAdapter);
        searchResultsView.getToolbar().setTitle("");
        searchResultsView.getToolbar().setTitleTextColor(searchResultsView.getContext().getResources().getColor(R.color.primary));
        searchResultsView.getAppCompatActivity().setSupportActionBar(searchResultsView.getToolbar());
        searchResultsView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        searchResultsView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        searchResultsView.getToolbar().invalidate();// restore toolbar
    }

    /***
     *
     */
    public void invalidateToolbar() {
        //mainView.getToolbar().setLogo(R.drawable.lingo_logo_tool_bar);
        searchResultsView.getToolbar().setTitle("");
        searchResultsView.getToolbar().setTitleTextColor(searchResultsView.getContext().getResources().getColor(R.color.primary));
        searchResultsView.getToolbar().invalidate();// restore toolbar
    }

    /****
     *
     */
    public void fetchingFilter(final String queryGlobal) {
        List<ProductionFilterEntity> listProductionFilterEntity = new ArrayList<ProductionFilterEntity>();

        ProductionFilterEntity productionFilterEntityAscending = new ProductionFilterEntity();
        productionFilterEntityAscending.setTitle("Giá tăng dần");
        productionFilterEntityAscending.setSortName("PRODUCT_PRICE");
        productionFilterEntityAscending.setSortType("ASC");
        listProductionFilterEntity.add(productionFilterEntityAscending);

        ProductionFilterEntity productionFilterEntityDescending = new ProductionFilterEntity();
        productionFilterEntityDescending.setTitle("Giá giảm dần");
        productionFilterEntityDescending.setSortName("PRODUCT_PRICE");
        productionFilterEntityDescending.setSortType("DESC");
        listProductionFilterEntity.add(productionFilterEntityDescending);

        ProductionFilterEntity productionFilterCreatedOnAscending = new ProductionFilterEntity();
        productionFilterCreatedOnAscending.setTitle("Theo ngày tăng dần");
        productionFilterCreatedOnAscending.setSortName("CREATEDON");
        productionFilterCreatedOnAscending.setSortType("ASC");
        listProductionFilterEntity.add(productionFilterCreatedOnAscending);

        ProductionFilterEntity productionFilterCreatedOnDescending = new ProductionFilterEntity();
        productionFilterCreatedOnDescending.setTitle("Theo ngày giảm dần");
        productionFilterCreatedOnDescending.setSortName("CREATEDON");
        productionFilterCreatedOnDescending.setSortType("DESC");
        listProductionFilterEntity.add(productionFilterCreatedOnDescending);

        LeafChildrenCategoryFilterAdapter leafChildrenCategoryFilterAdapter = new LeafChildrenCategoryFilterAdapter(searchResultsView.getContext(),
                listProductionFilterEntity);
        searchResultsView.getAppCompatSpinnerDataFilter().setAdapter(leafChildrenCategoryFilterAdapter);
        searchResultsView.getAppCompatSpinnerDataFilter().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listSearchResultsResponseGlobal.clear();
                searchResultsRecyclerViewAdapter.notifyDataSetChanged();
                searchResultsView.showProcessing();
                ProductionFilterEntity productionFilterEntity = (ProductionFilterEntity) parent.getItemAtPosition(position);
                sortName = productionFilterEntity.getSortName();
                sortType = productionFilterEntity.getSortType();
                Call<SearchProductResults> call = searchResultsAPI.searchProduct(queryGlobal, totalDocumentOfPage, currentPage, sortName, sortType);
                call.enqueue(new Callback<SearchProductResults>() {
                    @Override
                    public void onResponse(Response<SearchProductResults> response, Retrofit retrofit) {
                        if (response.code() == 200 && response.body().getItems().size() > 0) {
                            searchResultsView.getRelativeLayoutNotFound().setVisibility(View.GONE);
                            count = response.body().getCount();
                            final List<SearchResultsResponse> listSearchResultsResponseRetrofit = response.body().getItems();
                            Iterator<SearchResultsResponse> iteratorSearchResultsResponse = listSearchResultsResponseRetrofit.iterator();
                            while (iteratorSearchResultsResponse.hasNext()) {
                                listSearchResultsResponseGlobal.add(iteratorSearchResultsResponse.next());
                                searchResultsRecyclerViewAdapter.notifyDataSetChanged();
                            }
                        } else {
                            searchResultsView.getRelativeLayoutNotFound().setVisibility(View.VISIBLE);
                            searchResultsView.getAppCompatTextViewNotFoundErrorDescription().setText(
                                    String.format(searchResultsView.getContext().getResources().getString(R.string.not_found_error_description), queryGlobal));
                            Log.i(TAG, "fetchingSearchResults onResponse : " + queryGlobal);
                        }
                        searchResultsView.hideProcessing();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        searchResultsView.hideProcessing();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                searchResultsView.hideProcessing();
            }
        });

        searchResultsView.getRecyclerSearchResults().addOnScrollListener(new OnRcvScrollListener() {
            @Override
            public void onLoadMore(int totalItemCount) {
                super.onLoadMore(totalItemCount);
                if ((currentPage * totalDocumentOfPage) < count) {
                    currentPage++;
                    listSearchResultsResponseGlobal.add(null);
                    searchResultsRecyclerViewAdapter.notifyItemInserted(listSearchResultsResponseGlobal.size() - 1);
                    Call<SearchProductResults> call = searchResultsAPI.searchProduct(queryGlobal, totalDocumentOfPage, currentPage, sortName, sortType);
                    call.enqueue(new Callback<SearchProductResults>() {
                        @Override
                        public void onResponse(Response<SearchProductResults> response, Retrofit retrofit) {
                            listSearchResultsResponseGlobal.remove(null);
                            searchResultsRecyclerViewAdapter.notifyDataSetChanged();
                            final List<SearchResultsResponse> listSearchResultsResponseRetrofit = response.body().getItems();
                            Iterator<SearchResultsResponse> iteratorSearchResultsResponse = listSearchResultsResponseRetrofit.iterator();
                            while (iteratorSearchResultsResponse.hasNext()) {
                                listSearchResultsResponseGlobal.add(iteratorSearchResultsResponse.next());
                                searchResultsRecyclerViewAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            listSearchResultsResponseGlobal.remove(null);
                            searchResultsRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        searchResultsView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
