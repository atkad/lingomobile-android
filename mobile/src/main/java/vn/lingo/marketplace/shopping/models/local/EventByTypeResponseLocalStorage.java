package vn.lingo.marketplace.shopping.models.local;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vn.lingo.marketplace.shopping.models.EventByTypeResponse;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 11/11/2015.
 */
public class EventByTypeResponseLocalStorage extends RealmObject {

    @PrimaryKey
    private int id;
    private int moduleId;
    private String eventAvatar;
    private String des;
    private String name;
    private String eventId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getEventAvatar() {
        return eventAvatar;
    }

    public void setEventAvatar(String eventAvatar) {
        this.eventAvatar = eventAvatar;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public static EventByTypeResponseLocalStorage convertFromEventByTypeResponse(EventByTypeResponse eventByTypeResponse) {
        EventByTypeResponseLocalStorage eventByTypeResponseLocalStorage = new EventByTypeResponseLocalStorage();
        try {
            eventByTypeResponseLocalStorage.setId(Integer.parseInt(eventByTypeResponse.getEventId()));
        } catch (Exception exception) {
            eventByTypeResponseLocalStorage.setId(Integer.MAX_VALUE);
        }
        eventByTypeResponseLocalStorage.setModuleId(Constant.MODULE_EVENT_BY_TYPE);
        eventByTypeResponseLocalStorage.setEventAvatar(eventByTypeResponse.getEventAvatar());
        eventByTypeResponseLocalStorage.setDes(eventByTypeResponse.getDes());
        eventByTypeResponseLocalStorage.setName(eventByTypeResponse.getName());
        eventByTypeResponseLocalStorage.setEventId(eventByTypeResponse.getEventId());
        return eventByTypeResponseLocalStorage;
    }
}
