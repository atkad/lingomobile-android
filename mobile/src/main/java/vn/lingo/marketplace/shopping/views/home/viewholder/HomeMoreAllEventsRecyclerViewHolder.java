package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class HomeMoreAllEventsRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.image_view_banner_event)
    public ImageView imageView;

    @Bind(R.id.banner_event_layout_item_id)
    public CardView cardView;

    public HomeMoreAllEventsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
