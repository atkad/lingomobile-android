package vn.lingo.marketplace.shopping.views.viewholder.search;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class SearchResultsRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.search_results_item_layout_id)
    public RelativeLayout relativeLayoutRowItem;

    @Bind(R.id.search_results_image_view)
    public ImageView imageView;

    @Bind(R.id.search_results_production_title)
    public AppCompatTextView searchResultsProductionTitle;

    @Bind(R.id.search_results_prices)
    public AppCompatTextView searchResultsPrices;

    @Bind(R.id.search_results_market_price)
    public AppCompatTextView searchResultsMarketPrice;

    @Bind(R.id.search_results_rating_bar)
    public RatingBar searchResultsRatingBar;

    public SearchResultsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        // Set TextView text strike through
        searchResultsMarketPrice.setPaintFlags(searchResultsMarketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
