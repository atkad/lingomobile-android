package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.EventDealFixDealsEventByTypesListener;
import vn.lingo.marketplace.shopping.models.EventDealResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.home.viewholder.HomeMoreAllEventsRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class HomeMoreAllEventsRecyclerViewAdapter extends RecyclerView.Adapter<HomeMoreAllEventsRecyclerViewHolder> {

    private String TAG = HomeMoreAllEventsRecyclerViewAdapter.class.getName();

    private List<EventDealResponse> itemList;
    private Context context;
    private EventDealFixDealsEventByTypesListener eventDealFixDealsEventByTypesListener;
    private DisplayImageOptions displayImageOptions;

    public HomeMoreAllEventsRecyclerViewAdapter(Context context, List<EventDealResponse> itemList,
                                                EventDealFixDealsEventByTypesListener eventDealFixDealsEventByTypesListener,
                                                DisplayImageOptions displayImageOptions) {
        this.eventDealFixDealsEventByTypesListener = eventDealFixDealsEventByTypesListener;
        this.itemList = itemList;
        this.context = context;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public HomeMoreAllEventsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_more_all_events_item_layout, null);
        HomeMoreAllEventsRecyclerViewHolder viewHolder = new HomeMoreAllEventsRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HomeMoreAllEventsRecyclerViewHolder holder, final int position) {
        final EventDealResponse eventDealResponse = itemList.get(position);
//        ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + eventDealResponse.getAvatar(),
//                holder.imageView, displayImageOptions);
        String urlAvatar = Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + eventDealResponse.getAvatar();
        try {
            ImageSize targetSize = new ImageSize(256, 256);
            ImageLoader.getInstance().loadImage(urlAvatar, targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.imageView.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            Log.e(TAG, outOfMemoryError.toString());
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    eventDealFixDealsEventByTypesListener.onItemClickedEventDealFixDealsEventByTypes(position, Integer.parseInt(eventDealResponse.getId()),
                            eventDealResponse.getMicroSiteKey(), eventDealResponse.getMicroSiteType());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}
