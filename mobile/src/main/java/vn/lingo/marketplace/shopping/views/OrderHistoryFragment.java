package vn.lingo.marketplace.shopping.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import butterknife.Bind;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.presenters.OrderHistoryPresenter;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.utils.JVMRuntime;

/**
 * Created by longtran on 01/01/2016.
 */
public class OrderHistoryFragment extends BaseViewPagerFragment implements OrderHistoryView {

    private final String TAG = OrderHistoryFragment.class.getName();

    @Bind(R.id.fragment_order_history_layout_recycler_view_id)
    RecyclerView recyclerView;

    @Bind(R.id.app_compat_text_view_hot_line)
    AppCompatTextView appCompatTextViewHotLine;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_order_history_layout;
    }

    @Override
    public int getFragmentViewId() {
        return R.id.frame_container;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewHotLine() {
        return appCompatTextViewHotLine;
    }

    private OrderHistoryPresenter orderHistoryPresenter;
    private static final int PERMISSIONS_REQUEST_CALL_PHONE = 201;
    private String mManifestPersmission;
    private int mRequestCode;

    @Override
    public FragmentActivity getFragmentActivity() {
        return getActivity();
    }

    /***
     *
     */
    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{mManifestPersmission}, mRequestCode);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        orderHistoryPresenter = new OrderHistoryPresenter(this);
        orderHistoryPresenter.fetchingOrderHistoryRecyclerViewAdapter();
        getAppCompatTextViewHotLine().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mManifestPersmission = Manifest.permission.CALL_PHONE;
                mRequestCode = PERMISSIONS_REQUEST_CALL_PHONE;
                int checkSelfPermission = ActivityCompat.checkSelfPermission(getContext(), mManifestPersmission);
                boolean should = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), mManifestPersmission);
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    requestPermission();
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:19002025"));
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onDestroy();
    }

    @Override
    public boolean isViewBeingDragged(MotionEvent event) {
        return false;
    }

    @Override
    public RecyclerView getRecyclerViewOrderHistory() {
        recyclerView.addItemDecoration(new DividerItemDecoration(1));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        return recyclerView;
    }

    @Override
    public void onItemClicked(int position, int id, String endpoint) {

    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {

    }

    @Override
    public void hideProcessing() {

    }

    @Override
    public Retrofit getRetrofit() {
        return null;
    }

    @Override
    public DisplayImageOptions getDisplayImageOptions() {
        return null;
    }
}
