package vn.lingo.marketplace.shopping.models.local;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vn.lingo.marketplace.shopping.models.HomeEventResponse;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 23/01/2016.
 */
public class HomeEventResponseLocalStorage extends RealmObject {

    @PrimaryKey
    private int id;
    private int moduleId;
    private int eventId;
    private String avatar;
    private String productId;
    private String productName;
    private String productQuantity;
    private String marketPrice;
    private String zoneAlias;
    private String attributeId;
    private String numberValue;
    private String discountsType;
    private String promotionDiscount;
    private String productPrice;
    private String productPriceDiscount;
    private String isInstallment;
    private String trademark;
    private String marketPriceFormat;
    private String productPriceFormat;
    private String productPriceDiscountFormat;
    private String promotionPercent;
    private String happyHourDiscount;
    private String giftCard;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getZoneAlias() {
        return zoneAlias;
    }

    public void setZoneAlias(String zoneAlias) {
        this.zoneAlias = zoneAlias;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(String numberValue) {
        this.numberValue = numberValue;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public String getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(String promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductPriceDiscount() {
        return productPriceDiscount;
    }

    public void setProductPriceDiscount(String productPriceDiscount) {
        this.productPriceDiscount = productPriceDiscount;
    }

    public String getIsInstallment() {
        return isInstallment;
    }

    public void setIsInstallment(String isInstallment) {
        this.isInstallment = isInstallment;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public String getMarketPriceFormat() {
        return marketPriceFormat;
    }

    public void setMarketPriceFormat(String marketPriceFormat) {
        this.marketPriceFormat = marketPriceFormat;
    }

    public String getProductPriceFormat() {
        return productPriceFormat;
    }

    public void setProductPriceFormat(String productPriceFormat) {
        this.productPriceFormat = productPriceFormat;
    }

    public String getProductPriceDiscountFormat() {
        return productPriceDiscountFormat;
    }

    public void setProductPriceDiscountFormat(String productPriceDiscountFormat) {
        this.productPriceDiscountFormat = productPriceDiscountFormat;
    }

    public String getPromotionPercent() {
        return promotionPercent;
    }

    public void setPromotionPercent(String promotionPercent) {
        this.promotionPercent = promotionPercent;
    }

    public String getHappyHourDiscount() {
        return happyHourDiscount;
    }

    public void setHappyHourDiscount(String happyHourDiscount) {
        this.happyHourDiscount = happyHourDiscount;
    }

    public String getGiftCard() {
        return giftCard;
    }

    public void setGiftCard(String giftCard) {
        this.giftCard = giftCard;
    }

    public static HomeEventResponseLocalStorage convertFromHomeEventResponse(int eventId, HomeEventResponse homeEventResponse) {
        HomeEventResponseLocalStorage homeEventResponseLocalStorage = new HomeEventResponseLocalStorage();
        try {
            homeEventResponseLocalStorage.setId(Integer.parseInt(homeEventResponse.getProductId()));
        } catch (Exception exception) {
            homeEventResponseLocalStorage.setId(Integer.MAX_VALUE);
        }
        homeEventResponseLocalStorage.setModuleId(Constant.MODULE_BEST_INDUSTRY);
        homeEventResponseLocalStorage.setEventId(eventId);
        homeEventResponseLocalStorage.setAvatar(homeEventResponse.getAvatar());
        homeEventResponseLocalStorage.setProductId(homeEventResponse.getProductId());
        homeEventResponseLocalStorage.setProductName(homeEventResponse.getProductName());
        homeEventResponseLocalStorage.setProductQuantity(homeEventResponse.getProductQuantity());
        homeEventResponseLocalStorage.setMarketPrice(homeEventResponse.getMarketPrice());
        homeEventResponseLocalStorage.setZoneAlias(homeEventResponse.getZoneAlias());
        homeEventResponseLocalStorage.setAttributeId(homeEventResponse.getAttributeId());
        homeEventResponseLocalStorage.setNumberValue(homeEventResponse.getNumberValue());
        homeEventResponseLocalStorage.setDiscountsType(homeEventResponse.getDiscountsType());
        homeEventResponseLocalStorage.setPromotionDiscount(homeEventResponse.getPromotionDiscount());
        homeEventResponseLocalStorage.setProductPrice(homeEventResponse.getProductPrice());
        homeEventResponseLocalStorage.setProductPriceDiscount(homeEventResponse.getProductPriceDiscount());
        homeEventResponseLocalStorage.setIsInstallment(homeEventResponse.getIsInstallment());
        homeEventResponseLocalStorage.setTrademark(homeEventResponse.getTrademark());
        homeEventResponseLocalStorage.setMarketPriceFormat(homeEventResponse.getMarketPriceFormat());
        homeEventResponseLocalStorage.setProductPriceFormat(homeEventResponse.getProductPriceFormat());
        homeEventResponseLocalStorage.setProductPriceDiscountFormat(homeEventResponse.getProductPriceDiscountFormat());
        homeEventResponseLocalStorage.setPromotionPercent(homeEventResponse.getPromotionPercent());
        homeEventResponseLocalStorage.setHappyHourDiscount(homeEventResponse.getHappyHourDiscount());
        homeEventResponseLocalStorage.setGiftCard(homeEventResponse.getGiftCard());
        return homeEventResponseLocalStorage;
    }
}
