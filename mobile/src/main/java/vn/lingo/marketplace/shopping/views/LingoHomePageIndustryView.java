package vn.lingo.marketplace.shopping.views;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by longtran on 18/12/2015.
 */
public interface LingoHomePageIndustryView extends AbstractView {

    public ScrollView getScrollView();

    public AutoScrollViewPager getAutoScrollViewPagerGoldenBrand();

    public AppCompatImageView getAppCompatImageViewBannerFirst();

    public AppCompatImageView getAppCompatImageViewBannerSecond();

    public AppCompatImageView getAppCompatImageViewBannerThird();

    public AppCompatImageView getAppCompatImageViewBannerFourth();

    public AppCompatTextView getAppCompatTextViewBigFriDayTitle();

    public RelativeLayout getRelativeLayoutBigFriDay();

    public RelativeLayout getRelativeLayoutFirstLayoutCardTitle();

    public AppCompatTextView getAppCompatTextViewBestIndustrialFirst();

    public RecyclerView getRecyclerViewBestIndustrialFirst();

    public RelativeLayout getRelativeLayoutSecondLayoutCardTitle();

    public AppCompatTextView getAppCompatTextViewBestIndustrialSecond();

    public RecyclerView getRecyclerViewBestIndustrialSecond();

    public RelativeLayout getRelativeLayoutThirdLayoutCardTitle();

    public AppCompatTextView getAppCompatTextViewBestIndustrialThird();

    public RecyclerView getRecyclerViewBestIndustrialThird();

    public RelativeLayout getRelativeLayoutFourthLayoutCardTitle();

    public AppCompatTextView getAppCompatTextViewBestIndustrialFourth();

    public RecyclerView getRecyclerViewBestIndustrialFourth();

    public RelativeLayout getRelativeLayoutFifthLayoutCardTitle();

    public AppCompatTextView getAppCompatTextViewBestIndustrialFifth();

    public RecyclerView getRecyclerViewBestIndustrialFifth();

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);

}
