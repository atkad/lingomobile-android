package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.LingoHomePageBigFridayResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.home.viewholder.ProgressViewHolder;
import vn.lingo.marketplace.shopping.views.viewholder.MoreBigFriDayRecyclerViewHolder;


/**
 * Created by longtran on 09/11/2015.
 */
public class MoreBigFridayRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    private List<LingoHomePageBigFridayResponse> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    public MoreBigFridayRecyclerViewAdapter(Context context, List<LingoHomePageBigFridayResponse> itemList,
                                            RecyclerViewItemClickListener recyclerViewItemClickListener,
                                            DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        if (null != context) {
            this.context = context;
            this.displayImageOptions = displayImageOptions;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_big_fri_day_recycler_view_item_layout, null);
            MoreBigFriDayRecyclerViewHolder viewHolder =
                    new MoreBigFriDayRecyclerViewHolder(view);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);
            ProgressViewHolder progressViewHolder = new ProgressViewHolder(view);
            return progressViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MoreBigFriDayRecyclerViewHolder) {
            final MoreBigFriDayRecyclerViewHolder moreBigFriDayRecyclerViewHolder
                    = (MoreBigFriDayRecyclerViewHolder) holder;
            final LingoHomePageBigFridayResponse lingoHomePageBigFridayResponse = itemList.get(position);
            try {
                ImageSize targetSize = new ImageSize((int) context.getResources().getDimension(R.dimen.lingo_home_page_big_fri_day_image_height),
                        (int) context.getResources().getDimension(R.dimen.lingo_home_page_big_fri_day_image_height));
                ImageLoader.getInstance().loadImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + lingoHomePageBigFridayResponse.getAvatar(), targetSize,
                        displayImageOptions, new SimpleImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                super.onLoadingStarted(imageUri, view);
                                moreBigFriDayRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                super.onLoadingFailed(imageUri, view, failReason);
                                moreBigFriDayRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                super.onLoadingCancelled(imageUri, view);
                                moreBigFriDayRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                moreBigFriDayRecyclerViewHolder.imageView.setImageBitmap(loadedImage);
                            }
                        });
            } catch (OutOfMemoryError outOfMemoryError) {
                outOfMemoryError.printStackTrace();
            }
//            ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + homeEventResponse.getAvatar(),
//                    homeFragmentHotDealsFestivalRecyclerViewHolder.imageView, displayImageOptions);
            moreBigFriDayRecyclerViewHolder.appCompatTextViewDescription.setText(lingoHomePageBigFridayResponse.getProductName());
            moreBigFriDayRecyclerViewHolder.price.setText(lingoHomePageBigFridayResponse.getProductPriceLastFormat());
            if (StringUtils.isBlank(lingoHomePageBigFridayResponse.getMarketPriceFormat())) {
                moreBigFriDayRecyclerViewHolder.marketPrice.setVisibility(View.GONE);
            } else {
                moreBigFriDayRecyclerViewHolder.marketPrice.setVisibility(View.VISIBLE);
                moreBigFriDayRecyclerViewHolder.marketPrice.setText(lingoHomePageBigFridayResponse.getMarketPriceFormat());
            }
            if (!StringUtils.isBlank(lingoHomePageBigFridayResponse.getProductDiscountLast())) {
                moreBigFriDayRecyclerViewHolder.linearLayoutPercent.setVisibility(View.VISIBLE);
                moreBigFriDayRecyclerViewHolder.percent.setText(lingoHomePageBigFridayResponse.getProductDiscountLast());
            } else {
                moreBigFriDayRecyclerViewHolder.linearLayoutPercent.setVisibility(View.GONE);
            }
            if (!StringUtils.isBlank(lingoHomePageBigFridayResponse.getHappyHourDiscount())) {
                moreBigFriDayRecyclerViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.VISIBLE);
                moreBigFriDayRecyclerViewHolder.appCompatTextViewHappyHourDiscount.setText("Giờ vàng");
            } else {
                moreBigFriDayRecyclerViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.GONE);
            }
            if (!StringUtils.isBlank(lingoHomePageBigFridayResponse.getGiftCard())) {
                moreBigFriDayRecyclerViewHolder.linearLayoutDiscountGiftCards.setVisibility(View.VISIBLE);
                moreBigFriDayRecyclerViewHolder.appCompatTextViewDiscountGiftCards.setText("Kèm theo quà tặng");
            } else {
                moreBigFriDayRecyclerViewHolder.linearLayoutDiscountGiftCards.setVisibility(View.GONE);
            }
            moreBigFriDayRecyclerViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(lingoHomePageBigFridayResponse.getProductId()),
                                Integer.MAX_VALUE, Integer.MAX_VALUE,
                                lingoHomePageBigFridayResponse.getProductName());
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            });
        } else {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBarLoadMore.setIndeterminate(true);
        }
    }
}
