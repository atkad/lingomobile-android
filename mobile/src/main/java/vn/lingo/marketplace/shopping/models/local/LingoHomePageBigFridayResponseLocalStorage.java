package vn.lingo.marketplace.shopping.models.local;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vn.lingo.marketplace.shopping.models.LingoHomePageBigFridayResponse;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 23/01/2016.
 */
public class LingoHomePageBigFridayResponseLocalStorage extends RealmObject {

    @PrimaryKey
    private int id;
    private int moduleId;
    private String productPriceLast;
    private String avatar;
    private String isPauseSaveBuy;
    private String productDiscountLast;
    private String discountsType;
    private String productId;
    private String isStopSaveBuy;
    private String productName;
    private String productQuantity;
    private String marketPrice;
    private String totalOrder;
    private String marketPriceFormat;
    private String productPriceLastFormat;
    private String happyHourDiscount;
    private String giftCard;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getProductPriceLast() {
        return productPriceLast;
    }

    public void setProductPriceLast(String productPriceLast) {
        this.productPriceLast = productPriceLast;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIsPauseSaveBuy() {
        return isPauseSaveBuy;
    }

    public void setIsPauseSaveBuy(String isPauseSaveBuy) {
        this.isPauseSaveBuy = isPauseSaveBuy;
    }

    public String getProductDiscountLast() {
        return productDiscountLast;
    }

    public void setProductDiscountLast(String productDiscountLast) {
        this.productDiscountLast = productDiscountLast;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getIsStopSaveBuy() {
        return isStopSaveBuy;
    }

    public void setIsStopSaveBuy(String isStopSaveBuy) {
        this.isStopSaveBuy = isStopSaveBuy;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(String totalOrder) {
        this.totalOrder = totalOrder;
    }

    public String getMarketPriceFormat() {
        return marketPriceFormat;
    }

    public void setMarketPriceFormat(String marketPriceFormat) {
        this.marketPriceFormat = marketPriceFormat;
    }

    public String getProductPriceLastFormat() {
        return productPriceLastFormat;
    }

    public void setProductPriceLastFormat(String productPriceLastFormat) {
        this.productPriceLastFormat = productPriceLastFormat;
    }

    public String getHappyHourDiscount() {
        return happyHourDiscount;
    }

    public void setHappyHourDiscount(String happyHourDiscount) {
        this.happyHourDiscount = happyHourDiscount;
    }

    public String getGiftCard() {
        return giftCard;
    }

    public void setGiftCard(String giftCard) {
        this.giftCard = giftCard;
    }

    public static LingoHomePageBigFridayResponseLocalStorage convertFromLingoHomePageBigFridayResponse(LingoHomePageBigFridayResponse lingoHomePageBigFridayResponse) {
        LingoHomePageBigFridayResponseLocalStorage lingoHomePageBigFridayResponseLocalStorage = new LingoHomePageBigFridayResponseLocalStorage();
        try {
            lingoHomePageBigFridayResponseLocalStorage.setId(Integer.parseInt(lingoHomePageBigFridayResponse.getProductId()));
        } catch (Exception exception) {
            lingoHomePageBigFridayResponseLocalStorage.setId(Integer.MAX_VALUE);
        }
        lingoHomePageBigFridayResponseLocalStorage.setModuleId(Constant.MODULE_HOME_PAGE_BIG_FRIDAY);
        lingoHomePageBigFridayResponseLocalStorage.setProductPriceLast(lingoHomePageBigFridayResponse.getProductPriceLast());
        lingoHomePageBigFridayResponseLocalStorage.setAvatar(lingoHomePageBigFridayResponse.getAvatar());
        lingoHomePageBigFridayResponseLocalStorage.setIsPauseSaveBuy(lingoHomePageBigFridayResponse.getIsPauseSaveBuy());
        lingoHomePageBigFridayResponseLocalStorage.setProductDiscountLast(lingoHomePageBigFridayResponse.getProductDiscountLast());
        lingoHomePageBigFridayResponseLocalStorage.setDiscountsType(lingoHomePageBigFridayResponse.getDiscountsType());
        lingoHomePageBigFridayResponseLocalStorage.setProductId(lingoHomePageBigFridayResponse.getProductId());
        lingoHomePageBigFridayResponseLocalStorage.setIsStopSaveBuy(lingoHomePageBigFridayResponse.getIsStopSaveBuy());
        lingoHomePageBigFridayResponseLocalStorage.setProductName(lingoHomePageBigFridayResponse.getProductName());
        lingoHomePageBigFridayResponseLocalStorage.setProductQuantity(lingoHomePageBigFridayResponse.getProductQuantity());
        lingoHomePageBigFridayResponseLocalStorage.setMarketPrice(lingoHomePageBigFridayResponse.getMarketPrice());
        lingoHomePageBigFridayResponseLocalStorage.setTotalOrder(lingoHomePageBigFridayResponse.getTotalOrder());
        lingoHomePageBigFridayResponseLocalStorage.setMarketPriceFormat(lingoHomePageBigFridayResponse.getMarketPriceFormat());
        lingoHomePageBigFridayResponseLocalStorage.setProductPriceLastFormat(lingoHomePageBigFridayResponse.getProductPriceLastFormat());
        lingoHomePageBigFridayResponseLocalStorage.setHappyHourDiscount(lingoHomePageBigFridayResponse.getHappyHourDiscount());
        lingoHomePageBigFridayResponseLocalStorage.setGiftCard(lingoHomePageBigFridayResponse.getGiftCard());
        return lingoHomePageBigFridayResponseLocalStorage;
    }
}
