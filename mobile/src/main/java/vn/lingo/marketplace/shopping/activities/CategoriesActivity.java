package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;

import org.jsoup.helper.StringUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.presenters.CategoriesPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.CategoriesView;

/**
 * This activity controls the maint flow
 */
public class CategoriesActivity extends AbstractAppCompatActivity implements CategoriesView {

    @Bind(R.id.activity_categories_layout_categories_result_list)
    RecyclerView recyclerView;

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    private CategoriesPresenter categoriesPresenter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_layout);
        ButterKnife.bind(this);
        categoriesPresenter = new CategoriesPresenter(this);
        CategoriesParcelable categoriesParcelable = getIntent().getParcelableExtra(Constant.Categories.CATEGORY_KEY);
        if (null != categoriesParcelable) {
            toolbar.setTitle(categoriesParcelable.getTitle());
            categoriesPresenter.setAdapter(categoriesParcelable);
        }
    }

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public RecyclerView getRecyclerViewCategories() {
//        recyclerView.addItemDecoration(new MarginDecoration(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(1));
        recyclerView.setHasFixedSize(true);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));//new GridLayoutManager(getContext(), 2)
        return recyclerView;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        if (!StringUtil.isBlank(title)) {
            AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY", title, true);
            AnalyticsHelper.logEvent(this, title, String.valueOf(mappingZoneId));
        }
        if (zoneLevel == 3) {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setTitle(title);
            categoriesParcelable.setId(id);
            categoriesParcelable.setZoneLevel(zoneLevel);
            categoriesParcelable.setMappingZoneId(mappingZoneId);
            Intent intent = new Intent();
            intent.setClassName(this, LeafChildrenCategoryActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            this.startActivity(intent);
        } else {
            CategoriesParcelable categoriesParcelable = new CategoriesParcelable();
            categoriesParcelable.setId(id);
            categoriesParcelable.setTitle(title);
            categoriesParcelable.setZoneLevel(zoneLevel);
            Intent intent = new Intent();
            intent.setClassName(this, SubCategoriesActivity.class.getName());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constant.Categories.CATEGORY_KEY, categoriesParcelable);
            intent.putExtras(bundle);
            this.startActivity(intent);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        super.onResume();
    }
}

