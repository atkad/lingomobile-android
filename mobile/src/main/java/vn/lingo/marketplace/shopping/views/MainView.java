package vn.lingo.marketplace.shopping.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;

import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.DrawerBuilder;

/**
 * Created by longtran on 07/11/2015.
 */
public interface MainView extends AbstractView {

    public AppCompatActivity getAppCompatActivity();

    public Toolbar getToolbar();

    public AccountHeaderBuilder getAccountHeaderBuilder();

    public DrawerBuilder getDrawerBuilder();

    public void redirectShoppingCartActivity();

    public void switchFragments(Fragment newFragment, Bundle args, String fragmentByTag);

    public boolean isInternetConnected(Context context);

    public RelativeLayout getRelativeLayoutTutorialHelper();

    public AppCompatButton getAppCompatButtonTutorialHelper();
}
