package vn.lingo.marketplace.shopping.models.local;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vn.lingo.marketplace.shopping.models.IndustryResponse;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 08/11/2015.
 */
public class IndustryResponseLocalStorage extends RealmObject {

    @PrimaryKey
    private int id;
    private int moduleId;
    private String extentStyle;
    private String zoneVisibleBottom;
    private String zoneFooter;
    private String zoneAvatarMenu;
    private String mappingUrl;
    private String zoneAlias;
    private String isUseBackGround;
    private String zoneAvatar;
    private String zoneTitle;
    private String zoneLang;
    private String zoneId;
    private String zoneMore;
    private String zoneName;
    private String mappingType;
    private String zoneVisibleInTop;
    private String zoneVisibleInNavBar;
    private String zoneAvatarMain;
    private String portalTabControlId;
    private String visibleOnHome;
    private String backgroundUrl;
    private String zoneLevel;
    private String zoneKeywords;
    private String mappingZoneId;
    private String zoneType;
    private String zoneHeader;
    private String zoneDes;
    private String zoneParentId;
    private String zoneVisibleCenter;
    private String zoneVisibleInTab;
    private String zonePage;
    private String imgCoordinate;
    private String zonePriority;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getExtentStyle() {
        return extentStyle;
    }

    public void setExtentStyle(String extentStyle) {
        this.extentStyle = extentStyle;
    }

    public String getZoneVisibleBottom() {
        return zoneVisibleBottom;
    }

    public void setZoneVisibleBottom(String zoneVisibleBottom) {
        this.zoneVisibleBottom = zoneVisibleBottom;
    }

    public String getZoneFooter() {
        return zoneFooter;
    }

    public void setZoneFooter(String zoneFooter) {
        this.zoneFooter = zoneFooter;
    }

    public String getZoneAvatarMenu() {
        return zoneAvatarMenu;
    }

    public void setZoneAvatarMenu(String zoneAvatarMenu) {
        this.zoneAvatarMenu = zoneAvatarMenu;
    }

    public String getMappingUrl() {
        return mappingUrl;
    }

    public void setMappingUrl(String mappingUrl) {
        this.mappingUrl = mappingUrl;
    }

    public String getZoneAlias() {
        return zoneAlias;
    }

    public void setZoneAlias(String zoneAlias) {
        this.zoneAlias = zoneAlias;
    }

    public String getIsUseBackGround() {
        return isUseBackGround;
    }

    public void setIsUseBackGround(String isUseBackGround) {
        this.isUseBackGround = isUseBackGround;
    }

    public String getZoneAvatar() {
        return zoneAvatar;
    }

    public void setZoneAvatar(String zoneAvatar) {
        this.zoneAvatar = zoneAvatar;
    }

    public String getZoneTitle() {
        return zoneTitle;
    }

    public void setZoneTitle(String zoneTitle) {
        this.zoneTitle = zoneTitle;
    }

    public String getZoneLang() {
        return zoneLang;
    }

    public void setZoneLang(String zoneLang) {
        this.zoneLang = zoneLang;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneMore() {
        return zoneMore;
    }

    public void setZoneMore(String zoneMore) {
        this.zoneMore = zoneMore;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getMappingType() {
        return mappingType;
    }

    public void setMappingType(String mappingType) {
        this.mappingType = mappingType;
    }

    public String getZoneVisibleInTop() {
        return zoneVisibleInTop;
    }

    public void setZoneVisibleInTop(String zoneVisibleInTop) {
        this.zoneVisibleInTop = zoneVisibleInTop;
    }

    public String getZoneVisibleInNavBar() {
        return zoneVisibleInNavBar;
    }

    public void setZoneVisibleInNavBar(String zoneVisibleInNavBar) {
        this.zoneVisibleInNavBar = zoneVisibleInNavBar;
    }

    public String getZoneAvatarMain() {
        return zoneAvatarMain;
    }

    public void setZoneAvatarMain(String zoneAvatarMain) {
        this.zoneAvatarMain = zoneAvatarMain;
    }

    public String getPortalTabControlId() {
        return portalTabControlId;
    }

    public void setPortalTabControlId(String portalTabControlId) {
        this.portalTabControlId = portalTabControlId;
    }

    public String getVisibleOnHome() {
        return visibleOnHome;
    }

    public void setVisibleOnHome(String visibleOnHome) {
        this.visibleOnHome = visibleOnHome;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public void setBackgroundUrl(String backgroundUrl) {
        this.backgroundUrl = backgroundUrl;
    }

    public String getZoneLevel() {
        return zoneLevel;
    }

    public void setZoneLevel(String zoneLevel) {
        this.zoneLevel = zoneLevel;
    }

    public String getZoneKeywords() {
        return zoneKeywords;
    }

    public void setZoneKeywords(String zoneKeywords) {
        this.zoneKeywords = zoneKeywords;
    }

    public String getMappingZoneId() {
        return mappingZoneId;
    }

    public void setMappingZoneId(String mappingZoneId) {
        this.mappingZoneId = mappingZoneId;
    }

    public String getZoneType() {
        return zoneType;
    }

    public void setZoneType(String zoneType) {
        this.zoneType = zoneType;
    }

    public String getZoneHeader() {
        return zoneHeader;
    }

    public void setZoneHeader(String zoneHeader) {
        this.zoneHeader = zoneHeader;
    }

    public String getZoneDes() {
        return zoneDes;
    }

    public void setZoneDes(String zoneDes) {
        this.zoneDes = zoneDes;
    }

    public String getZoneParentId() {
        return zoneParentId;
    }

    public void setZoneParentId(String zoneParentId) {
        this.zoneParentId = zoneParentId;
    }

    public String getZoneVisibleCenter() {
        return zoneVisibleCenter;
    }

    public void setZoneVisibleCenter(String zoneVisibleCenter) {
        this.zoneVisibleCenter = zoneVisibleCenter;
    }

    public String getZoneVisibleInTab() {
        return zoneVisibleInTab;
    }

    public void setZoneVisibleInTab(String zoneVisibleInTab) {
        this.zoneVisibleInTab = zoneVisibleInTab;
    }

    public String getZonePage() {
        return zonePage;
    }

    public void setZonePage(String zonePage) {
        this.zonePage = zonePage;
    }

    public String getImgCoordinate() {
        return imgCoordinate;
    }

    public void setImgCoordinate(String imgCoordinate) {
        this.imgCoordinate = imgCoordinate;
    }

    public String getZonePriority() {
        return zonePriority;
    }

    public void setZonePriority(String zonePriority) {
        this.zonePriority = zonePriority;
    }

    public static IndustryResponseLocalStorage getIndustryResponseLocalStorageFromIndustryResponse(IndustryResponse industryResponse) {
        IndustryResponseLocalStorage industryResponseLocalStorage = new IndustryResponseLocalStorage();
        try {
            industryResponseLocalStorage.setId(Integer.parseInt(industryResponse.getZoneId()));
        } catch (Exception exception) {
            industryResponseLocalStorage.setId(Integer.MIN_VALUE);
        }
        industryResponseLocalStorage.setModuleId(Constant.MODULE_INDUSTRY);
        industryResponseLocalStorage.setExtentStyle(industryResponse.getExtentStyle());
        industryResponseLocalStorage.setZoneVisibleBottom(industryResponse.getZoneVisibleBottom());
        industryResponseLocalStorage.setZoneFooter(industryResponse.getZoneFooter());
        industryResponseLocalStorage.setZoneAvatarMenu(industryResponse.getZoneAvatarMenu());
        industryResponseLocalStorage.setMappingUrl(industryResponse.getMappingUrl());
        industryResponseLocalStorage.setZoneAlias(industryResponse.getZoneAlias());
        industryResponseLocalStorage.setIsUseBackGround(industryResponse.getIsUseBackGround());
        industryResponseLocalStorage.setZoneAvatar(industryResponse.getZoneAvatar());
        industryResponseLocalStorage.setZoneTitle(industryResponse.getZoneTitle());
        industryResponseLocalStorage.setZoneLang(industryResponse.getZoneLang());
        industryResponseLocalStorage.setZoneId(industryResponse.getZoneId());
        industryResponseLocalStorage.setZoneMore(industryResponse.getZoneMore());
        industryResponseLocalStorage.setZoneName(industryResponse.getZoneName());
        industryResponseLocalStorage.setMappingType(industryResponse.getMappingType());
        industryResponseLocalStorage.setZoneVisibleInTop(industryResponse.getZoneVisibleInTop());
        industryResponseLocalStorage.setZoneVisibleInNavBar(industryResponse.getZoneVisibleInNavBar());
        industryResponseLocalStorage.setZoneAvatarMain(industryResponse.getZoneAvatarMain());
        industryResponseLocalStorage.setVisibleOnHome(industryResponse.getVisibleOnHome());
        industryResponseLocalStorage.setBackgroundUrl(industryResponse.getBackgroundUrl());
        industryResponseLocalStorage.setZoneLevel(industryResponse.getZoneLevel());
        industryResponseLocalStorage.setZoneKeywords(industryResponse.getZoneKeywords());
        industryResponseLocalStorage.setMappingZoneId(industryResponse.getMappingZoneId());
        industryResponseLocalStorage.setZoneType(industryResponse.getZoneType());
        industryResponseLocalStorage.setZoneHeader(industryResponse.getZoneHeader());
        industryResponseLocalStorage.setZoneDes(industryResponse.getZoneDes());
        industryResponseLocalStorage.setZoneParentId(industryResponse.getZoneParentId());
        industryResponseLocalStorage.setZoneVisibleCenter(industryResponse.getZoneVisibleCenter());
        industryResponseLocalStorage.setZoneVisibleInTab(industryResponse.getZoneVisibleInTab());
        industryResponseLocalStorage.setZonePage(industryResponse.getZonePage());
        industryResponseLocalStorage.setImgCoordinate(industryResponse.getImgCoordinate());
        industryResponseLocalStorage.setZonePriority(industryResponse.getZonePriority());
        return industryResponseLocalStorage;
    }
}
