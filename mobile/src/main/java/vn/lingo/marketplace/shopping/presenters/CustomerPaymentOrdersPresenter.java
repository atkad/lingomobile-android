package vn.lingo.marketplace.shopping.presenters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.activities.CustomerPaymentByAccountLingoActivity;
import vn.lingo.marketplace.shopping.adapter.checkout.BankVATAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.CustomerPaymentRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.LocationCityAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.LocationDistrictAdapter;
import vn.lingo.marketplace.shopping.adapter.checkout.ShipTypeAdapter;
import vn.lingo.marketplace.shopping.apicheckout.ApiCheckout;
import vn.lingo.marketplace.shopping.delegate.Singleton;
import vn.lingo.marketplace.shopping.interfaces.CustomerPaymentOrdersView;
import vn.lingo.marketplace.shopping.interfaces.EventClickListenerProductOrder;
import vn.lingo.marketplace.shopping.listeners.CheckoutListeners;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.listeners.ShoppingCartListener;
import vn.lingo.marketplace.shopping.models.City;
import vn.lingo.marketplace.shopping.models.District;
import vn.lingo.marketplace.shopping.models.ReceiveType;
import vn.lingo.marketplace.shopping.models.ShipType;
import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;
import vn.lingo.marketplace.shopping.models.checkout.BankVAT;
import vn.lingo.marketplace.shopping.models.checkout.OrderShippingInfor;
import vn.lingo.marketplace.shopping.models.checkout.StoreAddress;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.NumberFormat;
import vn.lingo.marketplace.shopping.views.ShoppingCartView;

/**
 * Created by Administrator on 3/23/2016.
 */
public class CustomerPaymentOrdersPresenter extends AbstractPresenter<ShoppingCartView> implements CheckoutListeners, RecyclerViewItemClickListener, ShoppingCartListener, EventClickListenerProductOrder {
    CustomerPaymentOrdersView customerPaymentOrdersView;
    CustomerPaymentRecyclerViewAdapter customerPaymentRecyclerViewAdapter;
    private ApiCheckout apiCheckout;
    private RadioGroup.LayoutParams layoutParams;
    private List<District> lstDistricts;
    private List<ShoppingCartEntity> listShoppingCartEntity;
    private float totalMoneyPayment = 0;
    private float moneyPayment = 0;
    private float changePrice = 0;

    public CustomerPaymentOrdersPresenter(final CustomerPaymentOrdersView customerPaymentOrdersView) {
        this.customerPaymentOrdersView = customerPaymentOrdersView;

        listShoppingCartEntity = (ArrayList<ShoppingCartEntity>) customerPaymentOrdersView.getAppCompatActivity()
                .getIntent().getSerializableExtra(Constant.LIST_SHOPPING_CART_ENTITY);

        customerPaymentRecyclerViewAdapter = new CustomerPaymentRecyclerViewAdapter
                (customerPaymentOrdersView.getAppCompatActivity()
                        , listShoppingCartEntity, CustomerPaymentOrdersPresenter.this
                        , CustomerPaymentOrdersPresenter.this, customerPaymentOrdersView.getDisplayImageOptions(), CustomerPaymentOrdersPresenter.this);

        customerPaymentOrdersView.getRecyclerViewProductPurchase().setAdapter(customerPaymentRecyclerViewAdapter);
        customerPaymentRecyclerViewAdapter.notifyDataSetChanged();

//        Total money payment
        for (ShoppingCartEntity shoppingCartEntity : listShoppingCartEntity
                ) {
            int totalPriceProduct = shoppingCartEntity.getProductPrice() * shoppingCartEntity.getOrderQuantity();
            moneyPayment = totalMoneyPayment = totalMoneyPayment + totalPriceProduct;
        }
        String attribute= Singleton.getSingleton().getAttribute();
        changePrice=totalMoneyPayment;
        BigDecimal bigDecimalAllMoneyProduct = new BigDecimal(totalMoneyPayment);

        customerPaymentOrdersView.getTextViewAllMoneyProduct().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                R.string.prices), NumberFormat.priceToString(bigDecimalAllMoneyProduct.floatValue())));

        BigDecimal bigDecimalMoneyPayment = new BigDecimal(totalMoneyPayment);
        customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                R.string.prices), NumberFormat.priceToString(bigDecimalMoneyPayment.floatValue())));
//        event button Apply Coupon code
        customerPaymentOrdersView.getButtonApplyCodeSale().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerPaymentOrdersView.getEditTextCodeSale().setText("");
            }
        });

        customerPaymentOrdersView.getRadioGroupPayments().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioID = customerPaymentOrdersView.getRadioGroupPayments()
                        .indexOfChild(customerPaymentOrdersView.getAppCompatActivity().findViewById(checkedId));
                switch (checkedId) {
                    case R.id.customer_payment_method_radio_internet_banking:
                        customerPaymentOrdersView.getSpinnerInternetBanking().setVisibility(View.VISIBLE);
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        break;
                    case R.id.customer_payment_method_radio_transfer_get_bill:
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.VISIBLE);
                        customerPaymentOrdersView.getSpinnerInternetBanking().setVisibility(View.GONE);
                        break;
                    case R.id.customer_payment_method_radio_account_lingo:
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        customerPaymentOrdersView.getSpinnerInternetBanking().setVisibility(View.GONE);
                        Intent intent = new Intent();
                        intent.setClassName(customerPaymentOrdersView.getContext(), CustomerPaymentByAccountLingoActivity.class.getName());
                        customerPaymentOrdersView.getContext().startActivity(intent);

                        break;
                    case R.id.customer_payment_method_radio_visa_mater:
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        customerPaymentOrdersView.getSpinnerInternetBanking().setVisibility(View.GONE);
                        break;
                    case R.id.customer_payment_method_radio_payment_receipt_goods:
                        customerPaymentOrdersView.getLinearLayoutViewTransferReceiveBill().setVisibility(View.GONE);
                        customerPaymentOrdersView.getSpinnerInternetBanking().setVisibility(View.GONE);
                        break;
                }
            }
        });

        customerPaymentOrdersView.getRadioGroupSippingAdress().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.customer_authentication_radio_button_shipping_address_option:
                        customerPaymentOrdersView.getRadioGroupStoreAddress().setVisibility(View.GONE);
                        customerPaymentOrdersView.getLinearLayoutReceiveGoodsLocationDesired().setVisibility(View.VISIBLE);
                        break;
                    case R.id.customer_authentication_radio_button_shipping_lingo_address:
                        customerPaymentOrdersView.getRadioGroupStoreAddress().setVisibility(View.VISIBLE);
                        customerPaymentOrdersView.getLinearLayoutReceiveGoodsLocationDesired().setVisibility(View.GONE);
                        break;
                }
            }
        });

        customerPaymentOrdersView.getCheckBoxGrabBill().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    customerPaymentOrdersView.getLinearLayoutViewReceiveBill().setVisibility(View.VISIBLE);
                } else {
                    customerPaymentOrdersView.getLinearLayoutViewReceiveBill().setVisibility(View.GONE);
                }
            }
        });

        fetchDataSpinner();
        getReceiveType();
        getStoreAddress();
        getBankVAT();
        customerPaymentOrdersView.getButtonApplyCodeSale().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCouponCode();
            }
        });
    }

    public void shippingFree(){
        OrderShippingInfor orderShippingInfor=null;
        if (customerPaymentOrdersView.getRetrofit()!=null){
            ApiCheckout apiCheckout=customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
            Call<JsonElement> call=apiCheckout.getShipFree(orderShippingInfor);
        }
    }

    public void fetchingShoppingCart() {
        customerPaymentOrdersView.getToolbar().setTitle("");
        customerPaymentOrdersView.getToolbar().setTitleTextColor(customerPaymentOrdersView.getContext().getResources().getColor(R.color.primary));
        customerPaymentOrdersView.getAppCompatActivity().setSupportActionBar(customerPaymentOrdersView.getToolbar());
        customerPaymentOrdersView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        customerPaymentOrdersView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        customerPaymentOrdersView.getToolbar().setNavigationIcon(R.drawable.close_delete_remove_icon);
        customerPaymentOrdersView.getToolbar().invalidate();// restore toolbar
    }

    public void invalidateToolbar() {
        customerPaymentOrdersView.getToolbar().setTitle("");
        customerPaymentOrdersView.getToolbar().setTitleTextColor(customerPaymentOrdersView.getContext().getResources().getColor(R.color.primary));
        customerPaymentOrdersView.getToolbar().invalidate();// restore toolbar
    }

    public void getShippingType(final int cityId) {
        final List<ShipType> listShipType = new ArrayList<>();
        apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        final Call<JsonElement> call = apiCheckout.getShipType(cityId);
        customerPaymentOrdersView.showProcessing();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                if (response.code() == Constant.REQUEST_SEREVE) {
                    JsonElement jsonElement = response.body();
                    JsonArray jsonArray = jsonElement.getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                        int id = jsonObject.get("ID").getAsInt();
                        String name = jsonObject.get("NAME").getAsString();
                        ShipType shipType = new ShipType(id, name, String.valueOf(cityId));
                        listShipType.add(shipType);
                    }

                    ShipTypeAdapter shipTypeAdapter = new ShipTypeAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, listShipType);
                    customerPaymentOrdersView.getSpinnerMethodShippingGoods().setAdapter(shipTypeAdapter);
                }
                customerPaymentOrdersView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                customerPaymentOrdersView.hideProcessing();
            }
        });
    }

    public void getLocation() {
        final List<City> lstCities = new ArrayList<>();
        lstDistricts = new ArrayList<>();
        apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        Call<JsonElement> call = apiCheckout.getLocation();
        customerPaymentOrdersView.showProcessing();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                if (response.code() == Constant.REQUEST_SEREVE) {
                    JsonElement jsonElement = response.body();
                    JsonArray jsonArray = jsonElement.getAsJsonArray();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                        int id = jsonObject.get("LOCATION_ID").getAsInt();
                        String name = jsonObject.get("LOCATION_NAME").getAsString();
                        City city = new City(id, name);
                        writeRealmCity(city);
                        lstCities.add(city);
                        JsonArray arrayDistrict = jsonObject.get("LIST_DISTRICT").getAsJsonArray();
                        for (int j = 0; j < arrayDistrict.size(); j++) {
                            JsonObject objectDistrict = arrayDistrict.get(j).getAsJsonObject();
                            int dId = objectDistrict.get("LOCATION_ID").getAsInt();
                            String dName = objectDistrict.get("LOCATION_NAME").getAsString();
                            District district = new District(dId, dName, String.valueOf(id));
                            writeRealmDistrict(district);
                            lstDistricts.add(district);
                        }
                    }
                    LocationCityAdapter locationCityAdapter = new LocationCityAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, lstCities);
                    customerPaymentOrdersView.getSpinnerCityReceiveGoods().setAdapter(locationCityAdapter);

                    customerPaymentOrdersView.getSpinnerCityCompany().setAdapter(locationCityAdapter);
                    locationCityAdapter.notifyDataSetChanged();
                    LocationDistrictAdapter locationDistrictAdapter = new LocationDistrictAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, lstDistricts);
                    customerPaymentOrdersView.getSpinnerDistrictCompany().setAdapter(locationDistrictAdapter);
                    locationDistrictAdapter.notifyDataSetChanged();

                    customerPaymentOrdersView.getSpinnerCityReceiveGoods().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            City city = lstCities.get(position);
                            LocationDistrictAdapter locationDistrictAdapter = new LocationDistrictAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, findDistrictByCityId(city.getId()));
                            customerPaymentOrdersView.getSpinnerDistrictReceiptGoods().setAdapter(locationDistrictAdapter);

                            getShippingType(city.getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
                customerPaymentOrdersView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                customerPaymentOrdersView.hideProcessing();
            }
        });
    }

    public void fetchDataSpinner() {
        if (getAllCity().isEmpty()) {
            getLocation();
        } else {
            final LocationCityAdapter locationCityAdapter = new LocationCityAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, getAllCity());
            customerPaymentOrdersView.getSpinnerCityReceiveGoods().setAdapter(locationCityAdapter);

            customerPaymentOrdersView.getSpinnerCityReceiveGoods().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    City city = getAllCity().get(position);
                    LocationDistrictAdapter locationDistrictAdapter = new LocationDistrictAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, getAllDistrict());
                    customerPaymentOrdersView.getSpinnerDistrictReceiptGoods().setAdapter(locationDistrictAdapter);

                    customerPaymentOrdersView.getSpinnerCityCompany().setAdapter(locationCityAdapter);
                    locationCityAdapter.notifyDataSetChanged();
                    customerPaymentOrdersView.getSpinnerDistrictCompany().setAdapter(locationDistrictAdapter);
                    locationDistrictAdapter.notifyDataSetChanged();
                    getShippingType(city.getId());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    public void getBankVAT() {
        if (customerPaymentOrdersView.getRetrofit() != null) {
            ApiCheckout apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
            Call<List<BankVAT>> call = apiCheckout.getbankVAT();
            call.enqueue(new Callback<List<BankVAT>>() {
                @Override
                public void onResponse(Response<List<BankVAT>> response, Retrofit retrofit) {
                    if (response.code() == Constant.REQUEST_SEREVE && response.body() != null) {
                        final List<BankVAT> listBankVAT = response.body();
                        List<String> listNameBank = new ArrayList<String>();
                        for (BankVAT bankVAT : listBankVAT) {
                            listNameBank.add(bankVAT.getBankName().split("-")[0]);
                        }
                        BankVATAdapter bankVATAdapter = new BankVATAdapter(customerPaymentOrdersView.getContext(), R.layout.layout_item_ship_type, listNameBank);
                        customerPaymentOrdersView.getSpinnerNameBanking().setAdapter(bankVATAdapter);
                        bankVATAdapter.notifyDataSetChanged();

                        customerPaymentOrdersView.getSpinnerNameBanking().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                BankVAT bankVAT = listBankVAT.get(position);
                                customerPaymentOrdersView.getTextViewNameBanking().setText(bankVAT.getBankName().toString().trim());
                                customerPaymentOrdersView.getTextViewNumberAccountBanking().setText(bankVAT.getBankNunber().toString().trim());
                                customerPaymentOrdersView.getTextViewAccountOwner().setText(bankVAT.getAccountName().toString().trim());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }

    public void checkCouponCode() {
        String couponCode = customerPaymentOrdersView.getEditTextCodeSale().getText().toString().trim();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        Calendar cal = Calendar.getInstance();
        String dataTime = dateFormat.format(cal.getTime());
        if (couponCode != null) {
            if (customerPaymentOrdersView.getRetrofit() != null) {

                customerPaymentOrdersView.showProcessing();

                ApiCheckout apiCheckout = customerPaymentOrdersView.getRetrofitCheckCoupon().create(ApiCheckout.class);
                Call<JsonElement> call = apiCheckout.getCheckCouponCode(couponCode, dataTime);
                call.enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Response<JsonElement> response, Retrofit retrofit) {
                        customerPaymentOrdersView.hideProcessing();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-M-d HH:mm:ss");
                        Date date = new Date();
                        System.out.println(dateFormat.format(date));
                        Calendar cal = Calendar.getInstance();
                        String dataTime = dateFormat.format(cal.getTime());
                        if (response.code() == Constant.REQUEST_SEREVE) {
                            JsonElement jsonElement = response.body();
                            JsonObject jsonObject = jsonElement.getAsJsonObject();
                            int codeSuccess = jsonObject.get("code").getAsInt();
                            if (codeSuccess == Constant.REQUEST_SEREVE) {
                                JsonArray jsonArray = jsonObject.get("item").getAsJsonArray();
                                for (int i = 0; i < jsonArray.size(); i++) {
                                    JsonObject object = jsonArray.get(i).getAsJsonObject();
                                    String scope = object.get("SCOPE").getAsString();
                                    String sStarDate = object.get("ISSUE_DATE").getAsString();
                                    String sEndDate = object.get("ISSUE_END_DATE").getAsString();
                                    Date issueStartDate = converStringToDateTime(sStarDate);
                                    Date issueEndDate = converStringToDateTime(sEndDate);
                                    if (Integer.parseInt(scope) > 0 && Integer.parseInt(scope) < 3) {
                                        Date currentDate = converStringToDateTime(dataTime);
                                        if (currentDate.compareTo(issueStartDate) > 0 && currentDate.compareTo(issueEndDate) < 0) {
                                            int discounted = Integer.parseInt(object.get("DISCOUNT_VALUE").getAsString());
                                            customerPaymentOrdersView.getTextViewPriceDiscounted().setText(discounted + " VND");
                                            moneyPayment = totalMoneyPayment - discounted;
                                            BigDecimal bigDecimalPayment = new BigDecimal(moneyPayment);
                                            customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(
                                                    R.string.prices), NumberFormat.priceToString(bigDecimalPayment.floatValue())));

                                        }
                                    }
                                }
                            } else {
                                showDialog("Mã giảm giá không chính xác.Xin vui lòng nhập lại !");
                            }

                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        customerPaymentOrdersView.hideProcessing();
                    }
                });
            }
        }
    }

    public Date converStringToDateTime(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void writeRealmCity(City city) {
        Realm realmCity = Realm.getDefaultInstance();
        realmCity.beginTransaction();
        realmCity.copyToRealmOrUpdate(city);
        realmCity.commitTransaction();
    }

    public void deleteRealmCity() {
        Realm realmCity = Realm.getDefaultInstance();
        realmCity.beginTransaction();
        RealmResults<City> results = realmCity.where(City.class).findAll();
        results.removeAll(results);
        realmCity.commitTransaction();
    }

    public void writeRealmDistrict(District district) {
        Realm realmDistrict = Realm.getDefaultInstance();
        realmDistrict.beginTransaction();
        realmDistrict.copyToRealmOrUpdate(district);
        realmDistrict.commitTransaction();
    }

    public void deleteRealmDistrict() {
        Realm realmDistrict = Realm.getDefaultInstance();
        realmDistrict.beginTransaction();
        RealmResults<District> results = realmDistrict.where(District.class).findAll();
        results.removeAll(results);
        realmDistrict.commitTransaction();
    }

    public List<City> getAllCity() {
        List<City> lstCities = new ArrayList<>();
        Realm realmAllCity = Realm.getDefaultInstance();
        RealmResults<City> results = realmAllCity.where(City.class).findAll();

        for (City city : results) {
            lstCities.add(city);
        }
        return lstCities;
    }

    public List<District> findDistrictByCityId(int id) {
        List<District> lstDistricts = new ArrayList<>();
        Realm realmDistrict = Realm.getDefaultInstance();
        RealmResults<District> results = realmDistrict.where(District.class).equalTo("cityId", String.valueOf(id)).findAll();

        for (District district : results) {
            lstDistricts.add(district);
        }

        return lstDistricts;
    }

    public List<District> getAllDistrict() {
        List<District> lstDistricts = new ArrayList<>();
        Realm realmDistrict = Realm.getDefaultInstance();
        RealmResults<District> results = realmDistrict.where(District.class).findAll();

        for (District district : results) {
            lstDistricts.add(district);
        }

        return lstDistricts;
    }

    public void getReceiveType() {
        apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
        Call<List<ReceiveType>> call = apiCheckout.getReceiveType();
        call.enqueue(new Callback<List<ReceiveType>>() {
            @Override
            public void onResponse(Response<List<ReceiveType>> response, Retrofit retrofit) {
                if (response.code() == Constant.REQUEST_SEREVE && response.body() != null) {
                    List<ReceiveType> listReceiveType = response.body();
                    for (ReceiveType receiveType : listReceiveType) {
                        if (receiveType.getId() == 1) {
                            customerPaymentOrdersView.getRadioButtonReceiveAtLingo().setText(receiveType.getName());
                        } else if (receiveType.getId() == 2) {
                            customerPaymentOrdersView.getRadioButtonReceiveAtHouse().setText(receiveType.getName());
                        }
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    private void getStoreAddress() {
        if (customerPaymentOrdersView.getRetrofit() != null) {
            apiCheckout = customerPaymentOrdersView.getRetrofit().create(ApiCheckout.class);
            Call<List<StoreAddress>> call = apiCheckout.getStoreAddress();
            call.enqueue(new Callback<List<StoreAddress>>() {
                @Override
                public void onResponse(Response<List<StoreAddress>> response, Retrofit retrofit) {
                    if (response.code() == Constant.REQUEST_SEREVE && response.body() != null) {
                        List<StoreAddress> listStoreAddress = response.body();
                        for (int i = 0; i < listStoreAddress.size(); i++) {
                            StoreAddress storeAddress = listStoreAddress.get(i);
                            RadioButton radioButton = new RadioButton(customerPaymentOrdersView.getContext());
                            radioButton.setText(storeAddress.getStoreName() + "  (" + storeAddress.getStoreAddress() + ")");
                            radioButton.setId(i);
                            layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            customerPaymentOrdersView.getRadioGroupStoreAddress().addView(radioButton, layoutParams);

                        }

                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }

    @Override
    public void onclickItemRecyclerView(int position) {

    }

    public void addradiobutton(int count) {
        for (int i = 0; i < count; i++) {
            RadioButton radioButton = new RadioButton(customerPaymentOrdersView.getContext());

        }

    }

    public void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(customerPaymentOrdersView.getContext());
        builder.setTitle("Thông báo !");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {

    }

    @Override
    public void setOnListener(int actionID, int position, ShoppingCartEntity shoppingCartEntity) {
//        listShoppingCartEntity.remove(position);
//        customerPaymentRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void addProduct(float price) {
        changePrice=changePrice+price;
        BigDecimal bigDecimal = new BigDecimal(changePrice);
        customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(R.string.prices)
                , NumberFormat.priceToString(bigDecimal.floatValue())));

    }

    @Override
    public void subProduct(float price) {
        changePrice=changePrice-price;
        BigDecimal bigDecimal = new BigDecimal(changePrice);
        customerPaymentOrdersView.getTextViewMoneyPay().setText(String.format(customerPaymentOrdersView.getContext().getResources().getString(R.string.prices)
                , NumberFormat.priceToString(bigDecimal.floatValue())));
    }
}
