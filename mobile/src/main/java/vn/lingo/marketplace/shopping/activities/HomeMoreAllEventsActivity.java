package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionParcelable;
import vn.lingo.marketplace.shopping.presenters.home.HomeMoreAllEventsPresenter;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.home.HomeMoreAllEventsView;

/**
 * Created by longtran on 25/11/2015.
 */
public class HomeMoreAllEventsActivity extends AbstractAppCompatActivity implements
        HomeMoreAllEventsView {

    private static final String TAG = HomeMoreAllEventsActivity.class.getName();

    @Bind(R.id.recycler_view_more_all_events_id)
    RecyclerView megaSaleRecyclerView;

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    private HomeMoreAllEventsPresenter homeMoreAllEventsPresenter;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_more_events_screen);
        ButterKnife.bind(this);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        toolbar.setTitle(getResources().getString(R.string.promotion_program_toolbar_title));
        homeMoreAllEventsPresenter = new HomeMoreAllEventsPresenter(this);
        homeMoreAllEventsPresenter.fetchingHomeMoreAllEventsRecyclerViewAdapter();
    }

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    public RecyclerView getRecyclerView() {
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
//        megaSaleRecyclerView.setLayoutManager(gridLayoutManager);
        megaSaleRecyclerView.setHasFixedSize(true);
        // First param is number of columns and second param is orientation i.e Vertical or Horizontal
        StaggeredGridLayoutManager staggeredGridLayoutManager =
                new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
// Attach the layout manager to the recycler view
        megaSaleRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        return megaSaleRecyclerView;
    }

    @Override
    public void onItemClickedEventDealFixDealsEventByTypes(int position, int id, String micrositeKey, String micrositeType) {
        EventDealFixDealsCollectionParcelable eventDealFixDealsCollectionParcelable = new EventDealFixDealsCollectionParcelable();
        eventDealFixDealsCollectionParcelable.setId(id);
        eventDealFixDealsCollectionParcelable.setPosition(position);
        eventDealFixDealsCollectionParcelable.setMicrositeKey(micrositeKey);
        eventDealFixDealsCollectionParcelable.setMicrositeType(micrositeType);
        Intent intent = new Intent();
        intent.setClassName(this, MicroSiteActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.EventDeal.EVENT_DEAL_FIX_DEALS_COLLECTION, eventDealFixDealsCollectionParcelable);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {

    }

    @Override
    public void hideProcessing() {

    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }
}
