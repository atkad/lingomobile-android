package vn.lingo.marketplace.shopping.views;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import butterknife.Bind;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.utils.JVMRuntime;
import vn.lingo.marketplace.shopping.views.home.HomeFragment;

/**
 * Created by longtran on 24/12/2015.
 */
public class NoInternetAccessFragment extends AbstractFragment {

    @Bind(R.id.fragment_no_internet_access_app_compat_button_try_again_id)
    AppCompatButton appCompatButtonTryAgain;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_no_internet_access_layout;
    }

    @Override
    public int getFragmentViewId() {
        return R.id.frame_container;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appCompatButtonTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetConnected(getActivity())) {
                    switchFragments(new HomeFragment(), null, HomeFragment.class.getName());
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onDestroy();
    }
}
