/*
 *  Copyright 2015 Yahoo Inc.
 *  Licensed under the terms of the zLib license. Please see LICENSE file for terms.
 */
package vn.lingo.marketplace.shopping.utils;

import android.app.Activity;
import android.content.Context;

import com.flurry.android.FlurryAgent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vn.lingo.marketplace.shopping.models.ShoppingCartLocalStorage;

/**
 * Helps with logging custom events and errors to Flurry
 */
public class AnalyticsHelper {
    public static final String EVENT_VISITOR = "screen_visitor";
    public static final String EVENT_PRODUCT_ORDER = "product_order";
    public static final String EVENT_PRODUCT_ORDER_DATA_REQUEST = "product_order_data_request";

    /**
     * Logs an event for AppsFlyerLib analytics.
     */
    public static void logEventOrder(Context context, String eventName, List<ShoppingCartLocalStorage> listShoppingCartLocalStorage) {
        Map<String, String> event = new HashMap<String, String>();
        for (int i = 0; i < listShoppingCartLocalStorage.size(); i++) {
            ShoppingCartLocalStorage shoppingCartLocalStorage = listShoppingCartLocalStorage.get(i);
            event.put("CONTENT_ID", String.valueOf(shoppingCartLocalStorage.getCartID()));
            event.put("QUANTITY", String.valueOf(shoppingCartLocalStorage.getOrderQuantity()));
            event.put("PRICE", String.valueOf(shoppingCartLocalStorage.getProductPrice()));
            event.put("DESCRIPTION", String.valueOf(shoppingCartLocalStorage.getProductName()));
        }
//        AppsFlyerLib.trackEvent(context, eventName, event);
        FlurryAgent.logEvent(eventName, event, true);
    }

    /**
     * Logs an event for AppsFlyerLib analytics.
     */
    public static void logEvent(Context context, String eventName, String eventParams) {
        Map<String, String> event = new HashMap<String, String>();
        event.put(eventName, eventParams);
//        AppsFlyerLib.trackEvent(context, eventName, event);
        FlurryAgent.logEvent(eventName, event, true);
    }

    /**
     * Logs an event for AppsFlyerLib analytics.
     */
    public static void logEvent(Context context, String eventName, Map<String, String> eventParams) {
//        AppsFlyerLib.trackEvent(context, eventName, eventParams);
        FlurryAgent.logEvent(eventName, eventParams, true);
    }

    /**
     * Logs an event for analytics.
     *
     * @param eventName   name of the event
     * @param eventParams event parameters (can be null)
     * @param timed       <code>true</code> if the event should be timed, false otherwise
     */
    public static void logEvent(String eventName, String eventParams, boolean timed) {
        FlurryAgent.logEvent(eventParams, timed);
    }

    /**
     * Logs an event for analytics.
     *
     * @param eventName   name of the event
     * @param eventParams event parameters (can be null)
     * @param timed       <code>true</code> if the event should be timed, false otherwise
     */
    public static void logEvent(String eventName, Map<String, String> eventParams, boolean timed) {
        FlurryAgent.logEvent(eventName, eventParams, timed);
    }

    /**
     * Ends a timed event that was previously started.
     *
     * @param eventName   name of the event
     * @param eventParams event parameters (can be null)
     */
    public static void endTimedEvent(String eventName, Map<String, String> eventParams) {
        FlurryAgent.endTimedEvent(eventName, eventParams);
    }


    /**
     * Ends a timed event without event parameters.
     *
     * @param eventName name of the event
     */
    public static void endTimedEvent(String eventName) {
        FlurryAgent.endTimedEvent(eventName);
    }

    /**
     * Logs an error.
     *
     * @param errorId          error ID
     * @param errorDescription error description
     * @param throwable        a {@link Throwable} that describes the error
     */
    public static void logError(String errorId, String errorDescription, Throwable throwable) {
        FlurryAgent.onError(errorId, errorDescription, throwable);
    }

    /**
     * Logs location.
     *
     * @param latitude  latitude of location
     * @param longitude longitude of location
     */
    public static void logLocation(double latitude, double longitude) {
        FlurryAgent.setLocation((float) latitude, (float) longitude);
    }

    /**
     * Logs page view counts.
     */
    public static void logPageViews() {
        FlurryAgent.onPageView();
    }

    /**
     * Logs page view counts.
     */
    public static void onActivityResume(Activity activity) {
//        AppsFlyerLib.onActivityResume(activity);
    }

    /**
     * Logs page view counts.
     */
    public static void onActivityPause(Activity activity) {
//        AppsFlyerLib.onActivityPause(activity);
    }
}
