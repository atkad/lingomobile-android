package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 10/11/2015.
 */
public class HotDealResponse implements Serializable {

    @SerializedName("ADVERTISE_ENABLE")
    private String advertiseEnable;
    @SerializedName("ADVERTISE_ENDDATE")
    private String advertiseEnddate;
    @SerializedName("ADVERTISE_PARAMS")
    private String advertiseParams;
    @SerializedName("ADVERTISE_NAME")
    private String advertiseName;
    @SerializedName("ADVERTISE_HIDENAME")
    private String advertiseHideName;
    @SerializedName("ADVERTISE_STARTDATE")
    private String advertiseStartDate;
    @SerializedName("ADVERTISE_POSITIONID")
    private String advertisePositionId;
    @SerializedName("ZONE_MAPPING_ID")
    private String zoneMappingId;
    @SerializedName("ADVERTISE_CLICK")
    private String advertiseClick;
    @SerializedName("ADVERTISE_HEIGHT")
    private String advertiseHeight;
    @SerializedName("ADVERTISE_TYPE")
    private String advertiseType;
    @SerializedName("ADVERTISE_LANG")
    private String advertiseLang;
    @SerializedName("ADVERTISE_WIDTH")
    private String advertiseWidth;
    @SerializedName("ADVERTISE_REDIRECTURL")
    private String advertiseRedirectURL;
    @SerializedName("ADVERTISE_PATH")
    private String advertisePath;
    @SerializedName("LOCATION_ID")
    private String locationId;
    @SerializedName("ADVERTISE_PRIORITY")
    private String advertisePriority;
    @SerializedName("ZONE_ID")
    private String zoneId;
    @SerializedName("ADVERTISE_TARGET")
    private String advertiseTarget;
    @SerializedName("ADVERTISE_ID")
    private String advertiseId;

    public String getAdvertiseEnable() {
        return advertiseEnable;
    }

    public void setAdvertiseEnable(String advertiseEnable) {
        this.advertiseEnable = advertiseEnable;
    }

    public String getAdvertiseEnddate() {
        return advertiseEnddate;
    }

    public void setAdvertiseEnddate(String advertiseEnddate) {
        this.advertiseEnddate = advertiseEnddate;
    }

    public String getAdvertiseParams() {
        return advertiseParams;
    }

    public void setAdvertiseParams(String advertiseParams) {
        this.advertiseParams = advertiseParams;
    }

    public String getAdvertiseName() {
        return advertiseName;
    }

    public void setAdvertiseName(String advertiseName) {
        this.advertiseName = advertiseName;
    }

    public String getAdvertiseHideName() {
        return advertiseHideName;
    }

    public void setAdvertiseHideName(String advertiseHideName) {
        this.advertiseHideName = advertiseHideName;
    }

    public String getAdvertiseStartDate() {
        return advertiseStartDate;
    }

    public void setAdvertiseStartDate(String advertiseStartDate) {
        this.advertiseStartDate = advertiseStartDate;
    }

    public String getAdvertisePositionId() {
        return advertisePositionId;
    }

    public void setAdvertisePositionId(String advertisePositionId) {
        this.advertisePositionId = advertisePositionId;
    }

    public String getZoneMappingId() {
        return zoneMappingId;
    }

    public void setZoneMappingId(String zoneMappingId) {
        this.zoneMappingId = zoneMappingId;
    }

    public String getAdvertiseClick() {
        return advertiseClick;
    }

    public void setAdvertiseClick(String advertiseClick) {
        this.advertiseClick = advertiseClick;
    }

    public String getAdvertiseHeight() {
        return advertiseHeight;
    }

    public void setAdvertiseHeight(String advertiseHeight) {
        this.advertiseHeight = advertiseHeight;
    }

    public String getAdvertiseType() {
        return advertiseType;
    }

    public void setAdvertiseType(String advertiseType) {
        this.advertiseType = advertiseType;
    }

    public String getAdvertiseLang() {
        return advertiseLang;
    }

    public void setAdvertiseLang(String advertiseLang) {
        this.advertiseLang = advertiseLang;
    }

    public String getAdvertiseWidth() {
        return advertiseWidth;
    }

    public void setAdvertiseWidth(String advertiseWidth) {
        this.advertiseWidth = advertiseWidth;
    }

    public String getAdvertiseRedirectURL() {
        return advertiseRedirectURL;
    }

    public void setAdvertiseRedirectURL(String advertiseRedirectURL) {
        this.advertiseRedirectURL = advertiseRedirectURL;
    }

    public String getAdvertisePath() {
        return advertisePath;
    }

    public void setAdvertisePath(String advertisePath) {
        this.advertisePath = advertisePath;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getAdvertisePriority() {
        return advertisePriority;
    }

    public void setAdvertisePriority(String advertisePriority) {
        this.advertisePriority = advertisePriority;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getAdvertiseTarget() {
        return advertiseTarget;
    }

    public void setAdvertiseTarget(String advertiseTarget) {
        this.advertiseTarget = advertiseTarget;
    }

    public String getAdvertiseId() {
        return advertiseId;
    }

    public void setAdvertiseId(String advertiseId) {
        this.advertiseId = advertiseId;
    }
}
