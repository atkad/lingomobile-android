package vn.lingo.marketplace.shopping.views;

import com.viewpagerindicator.CirclePageIndicator;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;

import butterknife.Bind;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 05/11/2015.
 */
public interface CarouselFeaturesRollOutRegularlyView extends AbstractView {

    public AutoScrollViewPager getAutoScrollViewPager();

    public CirclePageIndicator circlePageIndicator();

}
