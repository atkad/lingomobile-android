package vn.lingo.marketplace.shopping.models.search;

/**
 * Provides constants to be used through the application
 */
public class CustomSearchableConstants {
    public static final String CLICKED_RESULT_ITEM = "clicked_result_item";
    public static final int UNSET_RESOURCES = -1;
}
