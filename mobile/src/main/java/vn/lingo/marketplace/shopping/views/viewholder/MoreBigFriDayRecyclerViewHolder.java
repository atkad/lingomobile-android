package vn.lingo.marketplace.shopping.views.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class MoreBigFriDayRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.more_big_fri_day_activity_relative_layout_item_layout_id)
    public RelativeLayout cardView;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_image_view_id)
    public ImageView imageView;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_market_price)
    public AppCompatTextView marketPrice;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_price)
    public AppCompatTextView price;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_percent)
    public AppCompatTextView percent;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_description_id)
    public AppCompatTextView appCompatTextViewDescription;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_percent_happy_hour_discount_id)
    public AppCompatTextView appCompatTextViewHappyHourDiscount;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_discount_gift_cards)
    public AppCompatTextView appCompatTextViewDiscountGiftCards;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_percent_linear_layout_id)
    public LinearLayout linearLayoutPercent;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_percent_linear_layout_happy_hour_discount_id)
    public LinearLayout linearLayoutHappyHourDiscount;

    @Bind(R.id.more_big_fri_day_activity_recycler_view_item_discount_gift_cards_linear_layout_id)
    public LinearLayout linearLayoutDiscountGiftCards;


    public MoreBigFriDayRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        marketPrice.setPaintFlags(marketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
