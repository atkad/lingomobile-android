package vn.lingo.marketplace.shopping.views;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 05/11/2015.
 */
public class ProductDetailsGiftProductsViewPagerHolder {

    @Bind(R.id.product_details_gift_products_view_pager_layout_item_id)
    public View view;

    @Bind(R.id.product_details_gift_products_view_pager_item_layout_image_view_id)
    public AppCompatImageView imageView;

    @Bind(R.id.product_details_gift_products_view_pager_item_layout_gift_name_id)
    public AppCompatTextView appCompatTextViewGiftName;

    @Bind(R.id.product_details_gift_products_view_pager_item_layout_product_name_id)
    public AppCompatTextView appCompatTextViewProductName;

    @Bind(R.id.product_details_gift_products_view_pager_item_layout_product_price_id)
    public AppCompatTextView appCompatTextViewProductPrice;

    public ProductDetailsGiftProductsViewPagerHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }
}
