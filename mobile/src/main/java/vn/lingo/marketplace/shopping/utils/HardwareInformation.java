package vn.lingo.marketplace.shopping.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by longtran on 14/01/2016.
 */
public class HardwareInformation {

    private final String TAG = HardwareInformation.class.getName();

    private final Activity activity;
    private JSONObject jObjectData;
    private String activityName;
    private Class<?> name;
    private String className;
    private String message;
    private String localizedMessage;
    private String causes;
    private String stackTrace;
    private String brandName;
    private String deviceName;
    private String modelNumber;
    private String productName;
    private String sdkVersion;
    private String release;
    private String incremental;
    private String height;
    private String width;
    private String appVersion;
    private String tablet;
    private String deviceOrientation;
    private String screenLayout;
    private String vmHeapSize;
    private String allocatedVMSize;
    private String vmMaxHeapSize;
    private String vmFreeHeapSize;
    private String nativeAllocatedSize;
    private String batteryPercentage;
    private String batteryCharging;
    private String batteryChargingVia;
    private String sdCardStatus;
    private String internalMemorySize;
    private String externalMemorySize;
    private String internalFreeSpace;
    private String externalFreeSpace;
    private String packageName;
    private String deviceRooted;
    private String networkMode;
    private String country;
    /****
     * @param activity
     * @param name
     */
    public HardwareInformation(Activity activity, Class<?> name) {
        this.activity = activity;
        this.name = name;
    }
    /****
     * @param context
     * @return
     */
    private String getAppVersion(Context context) {
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.toString());
        }
        return packageInfo.versionName;
    }
    /****
     * @param con
     * @return
     */
    private boolean isTablet(Context con) {
        boolean xlarge = ((con.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((con.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return xlarge || large;
    }
    /****
     * @param activity
     * @return
     */
    @SuppressWarnings("deprecation")
    private String getScreenOrientation(Activity activity) {
        Display getOrient = activity.getWindowManager().getDefaultDisplay();
        if (getOrient.getWidth() == getOrient.getHeight()) {
            return "Square";
        } else {
            if (getOrient.getWidth() < getOrient.getHeight()) {
                return "Portrait";
            } else {
                return "Landscape";
            }
        }
    }
    /***
     * @param act
     * @return
     */
    private String getScreenLayout(Activity act) {
        int screenSize = act.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "Large Screen";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "Normal Screen";
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "Small Screen";
            default:
                return "Screen size is neither large, normal or small";
        }
    }
    /****
     * @param size
     * @return
     */
    private String convertSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
    /****
     * @param activity
     * @return
     */
    private String getBatteryStatus(Activity activity) {
        int status = activity.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED)).getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        if (status == BatteryManager.BATTERY_STATUS_CHARGING) {
            return "Charging";
        } else if (status == BatteryManager.BATTERY_STATUS_DISCHARGING) {
            return "Discharging";
        } else if (status == BatteryManager.BATTERY_STATUS_FULL) {
            return "Full";
        }
        return "NULL";
    }
    /****
     * @param activity
     * @return
     */
    private String getBatteryChargingMode(Activity activity) {
        int plugged = activity.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED)).getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        if (plugged == BatteryManager.BATTERY_PLUGGED_AC)
            return "AC";
        else if (plugged == BatteryManager.BATTERY_PLUGGED_USB)
            return "USB";
        else if (plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS)
            return "WireLess";
        return "NULL";
    }
    /****
     * @param activity
     * @return
     */
    private String getSDCardStatus(Activity activity) {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if (isSDPresent)
            return "Mounted";
        else
            return "Not mounted";
    }
    /***
     * @return
     */
    @SuppressWarnings("deprecation")
    private String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return convertSize(availableBlocks * blockSize);
    }
    /***
     * @return
     */
    @SuppressWarnings("deprecation")
    private String getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return convertSize(totalBlocks * blockSize);
    }
    /****
     * @return
     */
    @SuppressWarnings("deprecation")
    private String getAvailableExternalMemorySize() {
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return convertSize(availableBlocks * blockSize);
        } else {
            return "SDCard not present";
        }
    }
    /***
     * @return
     */
    @SuppressWarnings("deprecation")
    private String getTotalExternalMemorySize() {
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return convertSize(totalBlocks * blockSize);
        } else {
            return "SDCard not present";
        }
    }
    /****
     * @return
     */
    private boolean isRooted() {
        boolean found = false;
        if (!found) {
            String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
                    "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
            for (String where : places) {
                if (new File(where + "su").exists()) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }
    /*****
     * @param activity
     * @return
     */
    @SuppressWarnings("deprecation")
    private String getNetworkMode(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            return "Wifi";
        } else if (mobile.isConnectedOrConnecting()) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR_MR1) {
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_1xRTT)
                    return "1xRTT";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_CDMA)
                    return "CDMA";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE)
                    return "EDGE";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_0)
                    return "EVDO 0";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_A)
                    return "EVDO A";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS)
                    return "GPRS";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_HSDPA)
                    return "HSDPA";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_HSPA)
                    return "HSPA";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_HSUPA)
                    return "HSUPA";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_UMTS)
                    return "UMTS";
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB)
                    if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_EHRPD)
                        return "EHRPD";
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO)
                    if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_IDEN)
                        return "IDEN";
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1)
                    if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_B)
                        return "EVDO B";
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB)
                    if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_LTE)
                        return "LTE";
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR2)
                    if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_HSPAP)
                        return "HSPAP";
                if (mobile.getSubtype() == TelephonyManager.NETWORK_TYPE_UNKNOWN)
                    return "UNKNOWN";
            }
        } else {
            return "No Network";
        }
        return "NULL";
    }
    /****
     * @param context
     * @return
     */
    private boolean isSimSupport(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return !(telephonyManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT);
    }
    /****
     *
     */
    public void fetchingInformation() {
        jObjectData = new JSONObject();
        try {

            jObjectData.put("Package_Name", activity.getPackageName());
            jObjectData.put("Class", activityName);
            jObjectData.put("Stack_Trace", stackTrace.toString());
            jObjectData.put("Brand", Build.BRAND);
            jObjectData.put("Device", Build.DEVICE);
            jObjectData.put("Model", Build.MODEL);
            jObjectData.put("Product", Build.PRODUCT);
            jObjectData.put("SDK", Build.VERSION.SDK);
            jObjectData.put("Release", Build.VERSION.RELEASE);
            jObjectData.put("Incremental", Build.VERSION.INCREMENTAL);
            jObjectData.put("Height", activity.getResources().getDisplayMetrics().heightPixels);
            jObjectData.put("Width", activity.getResources().getDisplayMetrics().widthPixels);
            jObjectData.put("App_Version", getAppVersion(activity));
            jObjectData.put("Tablet", isTablet(activity));
            jObjectData.put("Device_Orientation", getScreenOrientation(activity));
            jObjectData.put("Screen_Layout", getScreenLayout(activity));
            jObjectData.put("VM_Heap_Size", convertSize(Runtime.getRuntime().totalMemory()));
            jObjectData.put("Allocated_VM_Size", convertSize(Runtime.getRuntime().freeMemory()));
            jObjectData.put("VM_Max_Heap_Size", convertSize((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())));
            jObjectData.put("VM_free_Heap_Size", convertSize(Runtime.getRuntime().maxMemory()));
            jObjectData.put("Native_Allocated_Size", convertSize(Debug.getNativeHeapAllocatedSize()));
            jObjectData.put("Battery_Percentage", activity.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED)).getIntExtra(BatteryManager.EXTRA_LEVEL, 0));
            jObjectData.put("Battery_Charging_Status", getBatteryStatus(activity));
            jObjectData.put("Battery_Charging_Via", getBatteryChargingMode(activity));
            jObjectData.put("SDCard_Status", getSDCardStatus(activity));
            jObjectData.put("Internal_Memory_Size", getTotalInternalMemorySize());
            jObjectData.put("External_Memory_Size", getTotalExternalMemorySize());
            jObjectData.put("Internal_Free_Space", getAvailableInternalMemorySize());
            jObjectData.put("External_Free_Space", getAvailableExternalMemorySize());
            jObjectData.put("Device_IsRooted", isRooted());
            jObjectData.put("Network_Mode", getNetworkMode(activity));
            jObjectData.put("Country", new Locale("", activity.getResources().getConfiguration().locale.getCountry()).getDisplayCountry());
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
    }
}
