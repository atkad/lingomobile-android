package vn.lingo.marketplace.shopping.interfaces;

/**
 * Created by lenhan on 04/04/2016.
 */
public interface EventClickListenerProductOrder {

    public void addProduct(float price);
    public void subProduct(float price);
}
