//package vn.lingo.marketplace.shopping.models;
//
//import io.realm.RealmObject;
//import io.realm.annotations.PrimaryKey;
//
///**
// * Created by longtran on 17/12/2015.
// */
//public class TutorialHelperEntity extends RealmObject {
//
//    @PrimaryKey
//    private int tutorialHelperId;
//    private int tutorialHelper;
//    private int repeat;
//
//    public int getTutorialHelperId() {
//        return tutorialHelperId;
//    }
//
//    public void setTutorialHelperId(int tutorialHelperId) {
//        this.tutorialHelperId = tutorialHelperId;
//    }
//
//    public int getTutorialHelper() {
//        return tutorialHelper;
//    }
//
//    public void setTutorialHelper(int tutorialHelper) {
//        this.tutorialHelper = tutorialHelper;
//    }
//
//    public int getRepeat() {
//        return repeat;
//    }
//
//    public void setRepeat(int repeat) {
//        this.repeat = repeat;
//    }
//}
