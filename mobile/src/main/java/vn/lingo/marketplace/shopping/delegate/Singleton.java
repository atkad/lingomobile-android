package vn.lingo.marketplace.shopping.delegate;

/**
 * Created by lenhan on 06/04/2016.
 */
public class Singleton {
    private static Singleton singleton=null;

    public static Singleton getSingleton(){
        if (singleton==null){
            singleton=new Singleton();
        }
        return singleton;
    }

    private String attribute;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }
}
