package vn.lingo.marketplace.shopping.presenters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.adapter.MicroSiteRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.BoxInfoAndItemInfo;
import vn.lingo.marketplace.shopping.models.BoxInfoAndItemInfoListItem;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionAPI;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionListBoxSettings;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionParcelable;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionTemplateResponse;
import vn.lingo.marketplace.shopping.utils.MarginDecoration;
import vn.lingo.marketplace.shopping.views.MicroSiteView;

/**
 * Created by longtran on 06/12/2015.
 */
public class MicroSitePresenter implements RecyclerViewItemClickListener {

    private MicroSiteView microSiteView;

    public MicroSitePresenter(MicroSiteView microSiteView) {
        microSiteView.getToolbar().setTitle("");
        microSiteView.getToolbar().setTitleTextColor(microSiteView.getContext().getResources().getColor(R.color.primary));
        microSiteView.getAppCompatActivity().setSupportActionBar(microSiteView.getToolbar());
        microSiteView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        microSiteView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        microSiteView.getToolbar().invalidate();// restore toolbar
        this.microSiteView = microSiteView;
    }

    /***
     *
     */
    public void invalidateToolbar() {
        microSiteView.getToolbar().refreshDrawableState();
        microSiteView.getToolbar().setTitle("");
        microSiteView.getToolbar().setTitleTextColor(microSiteView.getContext().getResources().getColor(R.color.primary));
        microSiteView.getToolbar().invalidate();// restore toolbar
    }

    /****
     *
     */
    public void fetchingEventDealFixDealsCollection(EventDealFixDealsCollectionParcelable eventDealFixDealsCollectionParcelable) {
        try {
            microSiteView.showProcessing();
            EventDealFixDealsCollectionAPI eventDealDetailAPI = microSiteView.getRetrofit().create(EventDealFixDealsCollectionAPI.class);
            Call<EventDealFixDealsCollectionTemplateResponse> callEventDealDetail = eventDealDetailAPI.
                    getEventDealDetail(eventDealFixDealsCollectionParcelable.getMicrositeKey(),
                            Integer.parseInt(eventDealFixDealsCollectionParcelable.getMicrositeType()));
            callEventDealDetail.enqueue(new retrofit.Callback<EventDealFixDealsCollectionTemplateResponse>() {
                @Override
                public void onResponse(Response<EventDealFixDealsCollectionTemplateResponse> response, Retrofit retrofit) {
                    if (response.code() == 200 && null != response.body().getListBoxSettings()) {
                        Iterator<EventDealFixDealsCollectionListBoxSettings> iteratorEventDealFixDealsCollectionListBoxSettings = response.body().getListBoxSettings().iterator();
                        while (iteratorEventDealFixDealsCollectionListBoxSettings.hasNext()) {
                            EventDealFixDealsCollectionListBoxSettings eventDealFixDealsCollectionListBoxSettings = iteratorEventDealFixDealsCollectionListBoxSettings.next();
                             /* We get the inflator in the constructor */
                            LayoutInflater layoutInflater = (LayoutInflater) microSiteView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            /* We inflate the xml which gives us a view */
                            RelativeLayout microSiteBoxLayout = (RelativeLayout) layoutInflater.inflate(R.layout.layout_micro_site_box_layout,
                                    microSiteView.getLinearLayoutMicroSite(), false);
                            AppCompatTextView appCompatTextView = (AppCompatTextView) microSiteBoxLayout.findViewById(R.id.app_compat_text_view_micro_site_box_description);
                            appCompatTextView.setText(eventDealFixDealsCollectionListBoxSettings.getBoxTitle());
                            fetchingEventDealFixDealsCollectionRecyclerViewAdapter(microSiteBoxLayout, eventDealFixDealsCollectionListBoxSettings);
                            microSiteView.getLinearLayoutMicroSite().addView(microSiteBoxLayout);
                        }
                    }
                    microSiteView.hideProcessing();
                }

                @Override
                public void onFailure(Throwable t) {
                    microSiteView.hideProcessing();
                }
            });

        } catch (Exception exception) {
            microSiteView.hideProcessing();
        }

    }

    /*****
     *
     */
    private void fetchingEventDealFixDealsCollectionRecyclerViewAdapter(RelativeLayout microSiteBoxLayout,
                                                                        EventDealFixDealsCollectionListBoxSettings eventDealFixDealsCollectionListBoxSettings) {
        final List<BoxInfoAndItemInfoListItem> listBoxInfoAndItemInfoListItem = new ArrayList<BoxInfoAndItemInfoListItem>();
        final MicroSiteRecyclerViewAdapter microSiteRecyclerViewAdapter = new MicroSiteRecyclerViewAdapter(
                microSiteView.getContext(), listBoxInfoAndItemInfoListItem, MicroSitePresenter.this,
                microSiteView.getDisplayImageOptions());
        final RecyclerView recyclerView = (RecyclerView) microSiteBoxLayout.findViewById(R.id.recycler_view_micro_site_box_id);
        recyclerView.addItemDecoration(new MarginDecoration(microSiteView.getContext()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(microSiteView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(microSiteRecyclerViewAdapter);
        EventDealFixDealsCollectionAPI eventDealDetailAPI = microSiteView.getRetrofit().create(EventDealFixDealsCollectionAPI.class);
        Call<BoxInfoAndItemInfo> callBoxInfoAndItemInfo = eventDealDetailAPI.getBoxInfoAndItemInfo(
                Integer.parseInt(eventDealFixDealsCollectionListBoxSettings.getBoxTemplateId()), Integer.parseInt(eventDealFixDealsCollectionListBoxSettings.getDetailId())
        );
        callBoxInfoAndItemInfo.enqueue(new retrofit.Callback<BoxInfoAndItemInfo>() {
            @Override
            public void onResponse(Response<BoxInfoAndItemInfo> response, Retrofit retrofit) {
                if (response.code() == 200 && null != response.body()) {
                    Iterator<BoxInfoAndItemInfoListItem> iteratorBoxInfoAndItemInfoListItem = response.body().getListBoxInfoAndItemInfoListItem().iterator();
                    while (iteratorBoxInfoAndItemInfoListItem.hasNext()) {
                        listBoxInfoAndItemInfoListItem.add(iteratorBoxInfoAndItemInfoListItem.next());
                        microSiteRecyclerViewAdapter.notifyDataSetChanged();
                    }
                }
                microSiteView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                microSiteView.hideProcessing();
            }
        });
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        microSiteView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
