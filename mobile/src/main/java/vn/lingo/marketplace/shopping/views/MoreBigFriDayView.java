package vn.lingo.marketplace.shopping.views;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

/**
 * Created by longtran on 13/11/2015.
 */
public interface MoreBigFriDayView extends AbstractView {

    public AppCompatActivity getAppCompatActivity();

    public RecyclerView getRecyclerView();

    public Toolbar getToolbar();

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);

}
