package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 16/11/2015.
 */
public class HomeHotDealMegaSaleViewHolder {

    @Bind(R.id.home_hot_deal_mega_sale_image_view_id)
    public ImageView imageView;

    @Bind(R.id.panel_mega_sale_id)
    public RelativeLayout relativeLayoutMegaSaleId;

//    @Bind(R.id.home_hot_deal_mega_sale_app_compat_text_view_title_id)
//    public AppCompatTextView appCompatTextViewTitle;
//
//    @Bind(R.id.home_hot_deal_mega_sale_app_compat_text_view_description_id)
//    public AppCompatTextView appCompatTextViewDescription;

    public HomeHotDealMegaSaleViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }

}
