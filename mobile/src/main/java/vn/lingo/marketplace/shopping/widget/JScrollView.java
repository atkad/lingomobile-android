package vn.lingo.marketplace.shopping.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

/**
 * Created by longtran on 14/11/2015.
 */
public class JScrollView extends ScrollView {

    public JScrollView(Context context) {
        super(context);
    }

    public JScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        // Grab the last child placed in the ScrollView, we need it to determinate the bottom position.
        View view = (View) getChildAt(getChildCount() - 1);
        System.err.println(getChildCount()+ "view    " + view);
        // Calculate the scrolldiff
        int diff = (view.getBottom() - (getHeight() + getScrollY()));
        System.err.println("view.getBottom() " + view.getBottom());
        System.err.println("getHeight() " + getHeight());
        System.err.println("getScrollY() " + getScrollY());
        // if diff is zero, then the bottom has been reached
        if (diff == 0) {
            // notify that we have reached the bottom
            System.err.println("rrrrrrrrrrrrrrrrrrrrr");
        }
        super.onScrollChanged(l, t, oldl, oldt);
    }
}
