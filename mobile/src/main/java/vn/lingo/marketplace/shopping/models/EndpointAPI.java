package vn.lingo.marketplace.shopping.models;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by longtran on 30/12/2015.
 */
public interface EndpointAPI {

    @GET("/v1/mobile/Server/BaseUrl")
    Call<EndpointResponse> getBaseUrl();
}
