package vn.lingo.marketplace.shopping.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.sourceforge.android.view.autoscrollviewpager.RecyclingPagerAdapter;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.EventDealFixDealsEventByTypesListener;
import vn.lingo.marketplace.shopping.models.HomeSliderBannerResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.home.HomeSliderBannerViewHolder;

/**
 * Created by longtran on 04/11/2015.
 */
public class HomePageIndustrySliderBannerPagerAdapter extends RecyclingPagerAdapter {

    private Context context;
    private List<HomeSliderBannerResponse> listCarouselFeaturesRollOutRegularly;
    private EventDealFixDealsEventByTypesListener eventDealFixDealsEventByTypesListener;
    private DisplayImageOptions displayImageOptions;

    public HomePageIndustrySliderBannerPagerAdapter(Context context, List<HomeSliderBannerResponse> listCarouselFeaturesRollOutRegularly,
                                                    EventDealFixDealsEventByTypesListener eventDealFixDealsEventByTypesListener,
                                                    DisplayImageOptions displayImageOptions) {
        this.context = context;
        this.listCarouselFeaturesRollOutRegularly = listCarouselFeaturesRollOutRegularly;
        this.eventDealFixDealsEventByTypesListener = eventDealFixDealsEventByTypesListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public int getCount() {
        return listCarouselFeaturesRollOutRegularly.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup container) {
        HomeSliderBannerViewHolder carouselFeaturesRollOutRegularlyViewHolder;
        if (convertView == null) {
            /***
             * inflate the layout
             */
            convertView = LayoutInflater.from(container.getContext())
                    .inflate(R.layout.fragment_home_view_pager_slider_banner_item_image_layout, container, false);
            /***
             * well set up the HomeSliderBannerViewHolder
             */
            carouselFeaturesRollOutRegularlyViewHolder = new HomeSliderBannerViewHolder(convertView);
            /***
             * store the holder with the view.
             */
            convertView.setTag(carouselFeaturesRollOutRegularlyViewHolder);
        } else {
            /***
             * we've just avoided calling findViewById() on resource everytimejust use the viewHolder
             */
            carouselFeaturesRollOutRegularlyViewHolder = (HomeSliderBannerViewHolder) convertView.getTag();
        }
        final HomeSliderBannerResponse homeSliderBannerResponse = listCarouselFeaturesRollOutRegularly.get(position);
        ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + homeSliderBannerResponse.getImagePath(),
                carouselFeaturesRollOutRegularlyViewHolder.imageView, displayImageOptions);
        carouselFeaturesRollOutRegularlyViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventDealFixDealsEventByTypesListener.onItemClickedEventDealFixDealsEventByTypes(position,
                        Integer.parseInt(homeSliderBannerResponse.getGalleryId()), homeSliderBannerResponse.getParameter1(), "1");
            }
        });
        return convertView;
    }
}
