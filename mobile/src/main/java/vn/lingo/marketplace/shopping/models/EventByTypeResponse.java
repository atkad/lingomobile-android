package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 11/11/2015.
 */
public class EventByTypeResponse implements Serializable {

    @SerializedName("EVENT_AVATAR")
    private String eventAvatar;
    @SerializedName("DES")
    private String des;
    @SerializedName("NAME")
    private String name;
    @SerializedName("EVENT_ID")
    private String eventId;

    public String getEventAvatar() {
        return eventAvatar;
    }

    public void setEventAvatar(String eventAvatar) {
        this.eventAvatar = eventAvatar;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
