package vn.lingo.marketplace.shopping.presenters;

import android.support.annotation.Nullable;

import io.realm.Realm;
import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by longtran on 08/11/2015.
 */
public abstract class AbstractPresenter<T extends AbstractView> {

    private T abstractView;
    public Realm realm;

    public AbstractPresenter() {
        realm = Realm.getDefaultInstance();
    }

    public AbstractPresenter(T view) {
        abstractView = view;
        realm = Realm.getDefaultInstance();
    }

    /**
     * Returns the current bound {@link AbstractView} if any.
     *
     * @return The bound {@code AbstractView} or {@code null}.
     */
    @Nullable
    public T getView() {
        return abstractView;
    }

    /**
     * Must be called to bind the {@link AbstractView} and let the {@link AbstractPresenter} know that can start
     * {@code AbstractView} updates. Normally this method will be called from {@link Activity#onStart()}
     * or from {@link Fragment#onStart()}.
     *
     * @param abstractView A {@code Vista} instance to bind.
     */
    public final void setView(T abstractView) {
        this.abstractView = abstractView;
    }

    /**
     * Must be called un unbind the {@link AbstractView} and let the {@link AbstractPresenter} know that must
     * stop updating the {@code Vista}. Normally this method will be called from
     * {@link Activity#onStop()} or from {@link Fragment#onStop()}.
     */
    public final void stop() {
        abstractView = null;
    }

}
