package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 25/12/2015.
 */
public class ProductReviewsResponse implements Serializable {

    @SerializedName("lstcomments")
    private List<ProductReviewResponse> lisProductReviewsResponse;
    @SerializedName("o_data")
    private String data;
    @SerializedName("o_total_record")
    private String totalRecord;
    @SerializedName("o_code")
    private String code;

    public List<ProductReviewResponse> getLisProductReviewsResponse() {
        return lisProductReviewsResponse;
    }

    public void setLisProductReviewsResponse(List<ProductReviewResponse> lisProductReviewsResponse) {
        this.lisProductReviewsResponse = lisProductReviewsResponse;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
