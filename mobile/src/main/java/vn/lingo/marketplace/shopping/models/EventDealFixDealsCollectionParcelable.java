package vn.lingo.marketplace.shopping.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by longtran on 12/11/2015.
 */
public class EventDealFixDealsCollectionParcelable implements Parcelable {

    private int id;
    private int position;
    private String micrositeKey;
    private String micrositeType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getMicrositeKey() {
        return micrositeKey;
    }

    public void setMicrositeKey(String micrositeKey) {
        this.micrositeKey = micrositeKey;
    }

    public String getMicrositeType() {
        return micrositeType;
    }

    public void setMicrositeType(String micrositeType) {
        this.micrositeType = micrositeType;
    }

    public static final Creator<EventDealFixDealsCollectionParcelable> CREATOR = new Creator<EventDealFixDealsCollectionParcelable>() {
        public EventDealFixDealsCollectionParcelable createFromParcel(Parcel source) {
            EventDealFixDealsCollectionParcelable categoriesParcelable = new EventDealFixDealsCollectionParcelable();
            categoriesParcelable.id = source.readInt();
            categoriesParcelable.position = source.readInt();
            categoriesParcelable.micrositeKey = source.readString();
            categoriesParcelable.micrositeType = source.readString();
            return categoriesParcelable;
        }

        public EventDealFixDealsCollectionParcelable[] newArray(int size) {
            return new EventDealFixDealsCollectionParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(position);
        dest.writeString(micrositeKey);
        dest.writeString(micrositeType);
    }
}
