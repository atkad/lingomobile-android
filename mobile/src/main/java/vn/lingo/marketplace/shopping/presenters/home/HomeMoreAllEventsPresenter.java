package vn.lingo.marketplace.shopping.presenters.home;

import android.graphics.Color;

import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.adapter.HomeMoreAllEventsRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.EventDealFixDealsEventByTypesListener;
import vn.lingo.marketplace.shopping.models.AllEventsResponse;
import vn.lingo.marketplace.shopping.models.HomeAPI;
import vn.lingo.marketplace.shopping.views.home.HomeMoreAllEventsView;

/**
 * Created by longtran on 06/12/2015.
 */
public class HomeMoreAllEventsPresenter implements EventDealFixDealsEventByTypesListener {

    private HomeMoreAllEventsView homeMoreAllEventsView;

    public HomeMoreAllEventsPresenter(HomeMoreAllEventsView homeMoreAllEventsView) {
        this.homeMoreAllEventsView = homeMoreAllEventsView;
        homeMoreAllEventsView.getToolbar().setTitle("");
        homeMoreAllEventsView.getToolbar().setTitleTextColor(homeMoreAllEventsView.getContext().getResources().getColor(R.color.primary));
        homeMoreAllEventsView.getAppCompatActivity().setSupportActionBar(homeMoreAllEventsView.getToolbar());
        homeMoreAllEventsView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        homeMoreAllEventsView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        homeMoreAllEventsView.getToolbar().invalidate();// restore toolbar
    }

    /*****
     *
     */
    public void fetchingHomeMoreAllEventsRecyclerViewAdapter() {
        HomeAPI homeAPI = homeMoreAllEventsView.getRetrofit().create(HomeAPI.class);
        Call<AllEventsResponse> callAllEventsResponse = homeAPI.getAllEvent();
        callAllEventsResponse.enqueue(new retrofit.Callback<AllEventsResponse>() {
            @Override
            public void onResponse(Response<AllEventsResponse> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    HomeMoreAllEventsRecyclerViewAdapter homeMoreAllEventsRecyclerViewAdapter = new HomeMoreAllEventsRecyclerViewAdapter(
                            homeMoreAllEventsView.getContext(),
                            response.body().getListEventDealResponse(), HomeMoreAllEventsPresenter.this,
                            homeMoreAllEventsView.getDisplayImageOptions());
                    homeMoreAllEventsView.getRecyclerView().setAdapter(homeMoreAllEventsRecyclerViewAdapter);
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onItemClickedEventDealFixDealsEventByTypes(int position, int id, String micrositeKey, String micrositeType) {
        homeMoreAllEventsView.onItemClickedEventDealFixDealsEventByTypes(position, id, micrositeKey, micrositeType);
    }
}
