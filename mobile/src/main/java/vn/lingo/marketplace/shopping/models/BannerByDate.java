package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * MegaSaleAdvertisement
 * Created by longtran on 12/11/2015.
 */
public class BannerByDate implements Serializable {

    @SerializedName("COLOR_NUMBER")
    private String colorNumber;
    @SerializedName("NAME")
    private String name;
    @SerializedName("PRIORITY")
    private String priority;
    @SerializedName("LABEL")
    private String label;
    @SerializedName("BANNER_IMAGE")
    private String bannerImage;
    @SerializedName("MODIFIED_ON")
    private String modifiedOn;
    @SerializedName("IS_ACTIVE")
    private String isActive;
    @SerializedName("CREATED_BY")
    private String createdBy;
    @SerializedName("CREATED_ON")
    private String createdOn;
    @SerializedName("BANNER_ID")
    private String bannerId;
    @SerializedName("LINK")
    private String link;
    @SerializedName("TIME_MAIN")
    private String timeMain;
    @SerializedName("TIME1")
    private String time1;
    @SerializedName("TIME2")
    private String time2;
    @SerializedName("TIME3")
    private String time3;
    @SerializedName("MODIFIED_BY")
    private String modifiedBy;
    @SerializedName("TIME4")
    private String time4;
    @SerializedName("COLOR_TEXT")
    private String colorText;

    public String getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(String colorNumber) {
        this.colorNumber = colorNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTimeMain() {
        return timeMain;
    }

    public void setTimeMain(String timeMain) {
        this.timeMain = timeMain;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getTime3() {
        return time3;
    }

    public void setTime3(String time3) {
        this.time3 = time3;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getTime4() {
        return time4;
    }

    public void setTime4(String time4) {
        this.time4 = time4;
    }

    public String getColorText() {
        return colorText;
    }

    public void setColorText(String colorText) {
        this.colorText = colorText;
    }
}
