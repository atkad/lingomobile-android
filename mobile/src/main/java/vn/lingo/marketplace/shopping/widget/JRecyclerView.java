package vn.lingo.marketplace.shopping.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by longtran on 02/12/2015.
 */
public class JRecyclerView extends RecyclerView {

    private boolean isInterceptTouchEvent = true;

    public boolean isInterceptTouchEvent() {
        return isInterceptTouchEvent;
    }

    public void setIsInterceptTouchEvent(boolean isInterceptTouchEvent) {
        this.isInterceptTouchEvent = isInterceptTouchEvent;
    }

    public JRecyclerView(Context context) {
        super(context);
    }

    public JRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public JRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        if(isInterceptTouchEvent()){
            return super.onInterceptTouchEvent(e);
        } else {
            return false;
        }
    }
}
