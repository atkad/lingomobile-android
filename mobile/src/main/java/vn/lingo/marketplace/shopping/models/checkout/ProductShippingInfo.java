package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lenhan on 05/04/2016.
 */
public class ProductShippingInfo implements Serializable {
    @SerializedName("big_type")
        int bigType;
    @SerializedName("combo_id")
    int comboID;
    @SerializedName("combo_quantity")
    int comboQuantity;
    @SerializedName("online_order_id")
    int onlineOrderID;
    @SerializedName("product_id")
    int productID;
    @SerializedName("product_weight")
    int productWeight;
    @SerializedName("quantity")
    int quantity;
    @SerializedName("shippingExtraTimes")
    int shippingExtraTimes;
    @SerializedName("unit_price")
    int unitPrice;
    @SerializedName("combo_isfreeship")
    int comboIsFreeShip;
    @SerializedName("freeship")
    int freeShip;

    public ProductShippingInfo(int bigType, int comboID, int comboQuantity
            , int onlineOrderID, int productID, int productWeight, int quantity
            , int shippingExtraTimes, int unitPrice, int comboIsFreeShip, int freeShip) {
        this.bigType = bigType;
        this.comboID = comboID;
        this.comboQuantity = comboQuantity;
        this.onlineOrderID = onlineOrderID;
        this.productID = productID;
        this.productWeight = productWeight;
        this.quantity = quantity;
        this.shippingExtraTimes = shippingExtraTimes;
        this.unitPrice = unitPrice;
        this.comboIsFreeShip = comboIsFreeShip;
        this.freeShip = freeShip;
    }

    public int getBigType() {
        return bigType;
    }

    public void setBigType(int bigType) {
        this.bigType = bigType;
    }

    public int getComboID() {
        return comboID;
    }

    public void setComboID(int comboID) {
        this.comboID = comboID;
    }

    public int getComboQuantity() {
        return comboQuantity;
    }

    public void setComboQuantity(int comboQuantity) {
        this.comboQuantity = comboQuantity;
    }

    public int getOnlineOrderID() {
        return onlineOrderID;
    }

    public void setOnlineOrderID(int onlineOrderID) {
        this.onlineOrderID = onlineOrderID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(int productWeight) {
        this.productWeight = productWeight;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getShippingExtraTimes() {
        return shippingExtraTimes;
    }

    public void setShippingExtraTimes(int shippingExtraTimes) {
        this.shippingExtraTimes = shippingExtraTimes;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getComboIsFreeShip() {
        return comboIsFreeShip;
    }

    public void setComboIsFreeShip(int comboIsFreeShip) {
        this.comboIsFreeShip = comboIsFreeShip;
    }

    public int getFreeShip() {
        return freeShip;
    }

    public void setFreeShip(int freeShip) {
        this.freeShip = freeShip;
    }
}
