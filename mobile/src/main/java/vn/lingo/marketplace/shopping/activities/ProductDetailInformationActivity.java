package vn.lingo.marketplace.shopping.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.jsoup.helper.StringUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.utils.Constant;

/**
 * Created by longtran on 31/12/2015.
 */
public class ProductDetailInformationActivity extends AbstractAppCompatActivity {

    private final String TAG = ProductDetailInformationActivity.class.getName();

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.web_view_product_detail_information_layout_id)
    WebView webView;

    private String htmlContent;

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail_information_layout);
        ButterKnife.bind(this);
        getToolbar().setTitle("");
        getToolbar().setTitleTextColor(getResources().getColor(R.color.primary));
        this.setSupportActionBar(getToolbar());
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(false);
        getToolbar().setNavigationIcon(R.drawable.close_delete_remove_icon);
        getToolbar().invalidate();// restore toolbar
        htmlContent = getIntent().getStringExtra(Constant.ProductDetails.PRODUCT_DETAIL_INFORMATION);
        if (!StringUtil.isBlank(htmlContent)) {
            WebSettings settings = webView.getSettings();
            settings.setUseWideViewPort(false);
            settings.setLoadWithOverviewMode(true);
            settings.setJavaScriptEnabled(true);
            // wv.setBackgroundColor(0);
            webView.setVerticalScrollBarEnabled(false);
            webView.setHorizontalScrollBarEnabled(false);
            webView.setWebViewClient(new JWebViewClient());
            //get and format the path pointing to the internal storage
            String internalFilePath = "file://" + getFilesDir().getAbsolutePath() + "/";
            //load the html with the baseURL, all files relative to the baseURL will be found
            webView.loadDataWithBaseURL(internalFilePath, htmlContent, "text/html", "UTF-8", "");
        }
    }

    /****
     *
     */
    private class JWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return true;
        }
    }
}
