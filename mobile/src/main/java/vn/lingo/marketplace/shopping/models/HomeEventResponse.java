package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 13/11/2015.
 */
public class HomeEventResponse implements Serializable {

    @SerializedName("AVATAR")
    private String avatar;
    @SerializedName("PRODUCT_ID")
    private String productId;
    @SerializedName("PRODUCT_NAME")
    private String productName;
    @SerializedName("PRODUCT_QUANTITY")
    private String productQuantity;
    @SerializedName("MARKET_PRICE")
    private String marketPrice;
    @SerializedName("ZONE_ALIAS")
    private String zoneAlias;
    @SerializedName("ATTRIBUTE_ID")
    private String attributeId;
    @SerializedName("NUMBER_VALUE")
    private String numberValue;
    @SerializedName("DISCOUNTS_TYPE")
    private String discountsType;
    @SerializedName("PROMOTION_DISCOUNT")
    private String promotionDiscount;
    @SerializedName("PRODUCT_PRICE")
    private String productPrice;
    @SerializedName("PRODUCT_PRICEDISCOUNT")
    private String productPriceDiscount;
    @SerializedName("ISINSTALLMENT")
    private String isInstallment;
    @SerializedName("TRADEMARK")
    private String trademark;
    @SerializedName("MARKET_PRICE_FORMAT")
    private String marketPriceFormat;
    @SerializedName("PRODUCT_PRICE_FORMAT")
    private String productPriceFormat;
    @SerializedName("PRODUCT_PRICEDISCOUNT_FORMAT")
    private String productPriceDiscountFormat;
    @SerializedName("PROMOTIONPERCENT")
    private String promotionPercent;
    @SerializedName("M_GOLD_HOUR")
    private String happyHourDiscount;
    @SerializedName("M_GIFT")
    private String giftCard;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getZoneAlias() {
        return zoneAlias;
    }

    public void setZoneAlias(String zoneAlias) {
        this.zoneAlias = zoneAlias;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(String numberValue) {
        this.numberValue = numberValue;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public String getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(String promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductPriceDiscount() {
        return productPriceDiscount;
    }

    public void setProductPriceDiscount(String productPriceDiscount) {
        this.productPriceDiscount = productPriceDiscount;
    }

    public String getIsInstallment() {
        return isInstallment;
    }

    public void setIsInstallment(String isInstallment) {
        this.isInstallment = isInstallment;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public String getMarketPriceFormat() {
        return marketPriceFormat;
    }

    public void setMarketPriceFormat(String marketPriceFormat) {
        this.marketPriceFormat = marketPriceFormat;
    }

    public String getProductPriceFormat() {
        return productPriceFormat;
    }

    public void setProductPriceFormat(String productPriceFormat) {
        this.productPriceFormat = productPriceFormat;
    }

    public String getProductPriceDiscountFormat() {
        return productPriceDiscountFormat;
    }

    public void setProductPriceDiscountFormat(String productPriceDiscountFormat) {
        this.productPriceDiscountFormat = productPriceDiscountFormat;
    }

    public String getPromotionPercent() {
        return promotionPercent;
    }

    public void setPromotionPercent(String promotionPercent) {
        this.promotionPercent = promotionPercent;
    }

    public String getHappyHourDiscount() {
        return happyHourDiscount;
    }

    public void setHappyHourDiscount(String happyHourDiscount) {
        this.happyHourDiscount = happyHourDiscount;
    }

    public String getGiftCard() {
        return giftCard;
    }

    public void setGiftCard(String giftCard) {
        this.giftCard = giftCard;
    }
}
