package vn.lingo.marketplace.shopping.presenters;

import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.adapter.MoreBestIndustriesRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.adapter.MoreBigFridayRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.OnRcvScrollListener;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.BigFridayAPI;
import vn.lingo.marketplace.shopping.models.EventByTypesAPI;
import vn.lingo.marketplace.shopping.models.HomeEventResponse;
import vn.lingo.marketplace.shopping.models.LingoHomePageAllBigFridayResponse;
import vn.lingo.marketplace.shopping.models.LingoHomePageBigFridayResponse;
import vn.lingo.marketplace.shopping.models.ListHomeEventsResponse;
import vn.lingo.marketplace.shopping.views.MoreBigFriDayView;

/**
 * Created by longtran on 06/12/2015.
 */
public class MoreBigFriDayPresenter implements RecyclerViewItemClickListener {

    private String TAG = MoreBigFriDayPresenter.class.getName();
    private final int totalDocumentOfPage = 15;
    private int currentPage = 1;
    private MoreBigFriDayView moreBigFriDayView;

    /***
     * @param moreBigFriDayView
     */
    public MoreBigFriDayPresenter(MoreBigFriDayView moreBigFriDayView) {
        this.moreBigFriDayView = moreBigFriDayView;

    }

    /***
     *
     */
    public void fetchingMoreBigFriDay() {
        moreBigFriDayView.showProcessing();
        moreBigFriDayView.getToolbar().setTitle("");
        moreBigFriDayView.getToolbar().setTitleTextColor(moreBigFriDayView.getContext().getResources().getColor(R.color.primary));
        moreBigFriDayView.getAppCompatActivity().setSupportActionBar(moreBigFriDayView.getToolbar());
        moreBigFriDayView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        moreBigFriDayView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        moreBigFriDayView.getToolbar().invalidate();// restore toolbar
        fetchingMoreBigFriDayRecyclerViewAdapter();

    }

    /***
     *
     */
    public void invalidateToolbar() {
        //mainView.getToolbar().setLogo(R.drawable.lingo_logo_tool_bar);
        moreBigFriDayView.getToolbar().setTitle("");
        moreBigFriDayView.getToolbar().setTitleTextColor(moreBigFriDayView.getContext().getResources().getColor(R.color.primary));
        moreBigFriDayView.getToolbar().invalidate();// restore toolbar
    }

    /****
     *
     */
    private void fetchingMoreBigFriDayRecyclerViewAdapter() {
        try {
            final BigFridayAPI bigFridayAPI = moreBigFriDayView.getRetrofit().create(BigFridayAPI.class);
            Call<LingoHomePageAllBigFridayResponse>  call = bigFridayAPI.getBigFriday(currentPage, totalDocumentOfPage);
            call.enqueue(new Callback<LingoHomePageAllBigFridayResponse>() {
                @Override
                public void onResponse(Response<LingoHomePageAllBigFridayResponse> response, Retrofit retrofit) {
                    if (response.code() == 200 && null != response.body().getListLingoHomePageBigFridayResponse() &&
                            response.body().getListLingoHomePageBigFridayResponse().size() > 0) {
                        final List<LingoHomePageBigFridayResponse> listLingoHomePageBigFridayResponse = new ArrayList<LingoHomePageBigFridayResponse>();
                        final int count = response.body().getTotalRecord();
                        for (LingoHomePageBigFridayResponse lingoHomePageBigFridayResponse : response.body().getListLingoHomePageBigFridayResponse()) {
                            listLingoHomePageBigFridayResponse.add(lingoHomePageBigFridayResponse);
                        }
                        final MoreBigFridayRecyclerViewAdapter moreBigFridayRecyclerViewAdapter
                                = new MoreBigFridayRecyclerViewAdapter(moreBigFriDayView.getContext(), listLingoHomePageBigFridayResponse,
                                MoreBigFriDayPresenter.this, moreBigFriDayView.getDisplayImageOptions());
                        moreBigFriDayView.getRecyclerView().setAdapter(moreBigFridayRecyclerViewAdapter);
                        moreBigFriDayView.getRecyclerView().addOnScrollListener(new OnRcvScrollListener() {
                            @Override
                            public void onLoadMore(int totalItemCount) {
                                super.onLoadMore(totalItemCount);
                                if ((currentPage * totalDocumentOfPage) < count) {
                                    currentPage++;
                                    listLingoHomePageBigFridayResponse.add(null);
                                    moreBigFridayRecyclerViewAdapter.notifyItemInserted(listLingoHomePageBigFridayResponse.size() - 1);
                                    Call<LingoHomePageAllBigFridayResponse> callLoadMore  = bigFridayAPI.getBigFriday(totalDocumentOfPage, currentPage);
                                    callLoadMore.enqueue(new Callback<LingoHomePageAllBigFridayResponse>() {
                                        @Override
                                        public void onResponse(Response<LingoHomePageAllBigFridayResponse> response, Retrofit retrofit) {
                                            listLingoHomePageBigFridayResponse.remove(null);
                                            moreBigFridayRecyclerViewAdapter.notifyDataSetChanged();
                                            if (response.code() == 200 && null != response.body().getListLingoHomePageBigFridayResponse() &&
                                                    response.body().getListLingoHomePageBigFridayResponse().size() > 0) {
                                                for (LingoHomePageBigFridayResponse lingoHomePageBigFridayResponse : response.body().getListLingoHomePageBigFridayResponse()) {
                                                    listLingoHomePageBigFridayResponse.add(lingoHomePageBigFridayResponse);
                                                    moreBigFridayRecyclerViewAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {
                                            moreBigFriDayView.hideProcessing();
                                        }
                                    });
                                }
                            }
                        });
                        moreBigFriDayView.hideProcessing();
                    }

                }

                @Override
                public void onFailure(Throwable t) {
                    moreBigFriDayView.hideProcessing();
                }
            });
        } catch (Exception exception) {
            Log.i(TAG, exception.toString());
        }
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        moreBigFriDayView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
