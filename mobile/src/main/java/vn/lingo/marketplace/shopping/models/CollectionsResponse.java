package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 09/12/2015.
 */
public class CollectionsResponse implements Serializable {

    @SerializedName("data")
    private List<CollectionResponse> listCollection;

    @SerializedName("display")
    private List<CollectionDisplay> collectionDisplay;

    public List<CollectionResponse> getListCollection() {
        return listCollection;
    }

    public void setListCollection(List<CollectionResponse> listCollection) {
        this.listCollection = listCollection;
    }

    public List<CollectionDisplay> getCollectionDisplay() {
        return collectionDisplay;
    }

    public void setCollectionDisplay(List<CollectionDisplay> collectionDisplay) {
        this.collectionDisplay = collectionDisplay;
    }
}
