package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 26/12/2015.
 */
public class GiftProductsResponse implements Serializable {

    @SerializedName("PRODUCT_ID")
    private String productId;
    @SerializedName("PRODUCT_NAME")
    private String productName;
    @SerializedName("AVATAR")
    private String avatar;
    @SerializedName("GIFT_NAME")
    private String giftName;
    @SerializedName("PRODUCT_PRICE_FORMAT")
    private String productPriceFormat;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getProductPriceFormat() {
        return productPriceFormat;
    }

    public void setProductPriceFormat(String productPriceFormat) {
        this.productPriceFormat = productPriceFormat;
    }
}
