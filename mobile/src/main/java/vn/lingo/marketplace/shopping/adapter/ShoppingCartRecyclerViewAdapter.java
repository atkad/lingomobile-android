package vn.lingo.marketplace.shopping.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.github.amlcurran.showcaseview.ShotStateStore;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.listeners.ShoppingCartListener;
import vn.lingo.marketplace.shopping.models.ShoppingCartEntity;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.viewholder.ShoppingCartRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class ShoppingCartRecyclerViewAdapter extends RecyclerView.Adapter<ShoppingCartRecyclerViewHolder> {

    private final String TAG = ShoppingCartRecyclerViewAdapter.class.getName();

    private final String SHOWCASE_ID = ShoppingCartRecyclerViewAdapter.class.getName();
    private List<ShoppingCartEntity> itemList;
    private Activity activity;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private ShoppingCartListener shoppingCartListener;
    private DisplayImageOptions displayImageOptions;

    public ShoppingCartRecyclerViewAdapter(Activity activity, List<ShoppingCartEntity> itemList,
                                           RecyclerViewItemClickListener recyclerViewItemClickListener,
                                           ShoppingCartListener shoppingCartListener,
                                           DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.activity = activity;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        this.shoppingCartListener = shoppingCartListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public ShoppingCartRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_cart_item_layout, null);
        ShoppingCartRecyclerViewHolder viewHolder = new ShoppingCartRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ShoppingCartRecyclerViewHolder holder, final int position) {
        final ShoppingCartEntity shoppingCartEntity = itemList.get(position);
        holder.appCompatCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                shoppingCartEntity.setIsOrdered(isChecked ? 1 : 0);
                shoppingCartListener.setOnListener(Constant.ON_CLICK_PROCESS_ORDERED, position, shoppingCartEntity);
            }
        });
        holder.appCompatImageViewDeletedOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoppingCartListener.setOnListener(Constant.ON_CLICK_DELETED_ORDER, position, shoppingCartEntity);
            }
        });
        holder.appCompatCheckBox.setChecked(shoppingCartEntity.getIsOrdered() == Constant.ORDERED_VALUES ? true : false);
        holder.appCompatTextViewDescription.setText(shoppingCartEntity.getProductName() == null ? Constant.EMPTY_VALUES : shoppingCartEntity.getProductName());
        holder.appCompatTextViewDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, shoppingCartEntity.getCartID(), Integer.MAX_VALUE, Integer.MAX_VALUE,
                            shoppingCartEntity.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
        holder.appCompatTextViewMarketPrice.setText(shoppingCartEntity.getMarketPriceFormat());
        holder.appCompatTextViewMarketPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, shoppingCartEntity.getCartID(), Integer.MAX_VALUE, Integer.MAX_VALUE,
                            shoppingCartEntity.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
        holder.appCompatTextViewPrice.setText(shoppingCartEntity.getProductPriceFormat());
        holder.appCompatTextViewPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, shoppingCartEntity.getCartID(), Integer.MAX_VALUE, Integer.MAX_VALUE,
                            shoppingCartEntity.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
        holder.appCompatTextViewPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, shoppingCartEntity.getCartID(), Integer.MAX_VALUE, Integer.MAX_VALUE,
                            shoppingCartEntity.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
        holder.appCompatTextViewLabelQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, shoppingCartEntity.getCartID(), Integer.MAX_VALUE, Integer.MAX_VALUE,
                            shoppingCartEntity.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
        holder.appCompatTextViewLabelQuantity.setText(String.valueOf(shoppingCartEntity.getOrderQuantity()));
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, shoppingCartEntity.getCartID(), Integer.MAX_VALUE, Integer.MAX_VALUE,
                            shoppingCartEntity.getProductName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
//        ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + shoppingCartEntity.getAvatar(),
//                holder.imageView, displayImageOptions);
        String urlAvatar = Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + shoppingCartEntity.getAvatar();
        try {
            ImageSize targetSize = new ImageSize(256, 256);
            ImageLoader.getInstance().loadImage(urlAvatar, targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.imageView.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            Log.e(TAG, outOfMemoryError.toString());
        }
        holder.discreteSeekBar.setProgress(shoppingCartEntity.getOrderQuantity());
        holder.discreteSeekBar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @Override
            public int transform(int value) {
                return value;
            }
        });
        holder.discreteSeekBar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                shoppingCartEntity.setOrderQuantity(value);
                holder.appCompatTextViewLabelQuantity.setText(String.valueOf(value));
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                shoppingCartEntity.setIsOrdered(holder.appCompatCheckBox.isChecked() ? 1 : 0);
                shoppingCartListener.setOnListener(Constant.ON_CLICK_PROCESS_ORDERED, position, shoppingCartEntity);
            }
        });
        if (position == 0) {
            presentShowcaseSequence(holder.discreteSeekBar);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    /***
     *
     */
    private void presentShowcaseSequence(View target) {
        ShotStateStore shotStateStore = new ShotStateStore(activity);
        shotStateStore.setSingleShot(0x444);
        if (!shotStateStore.hasShot()) {
            showcaseViewShoppingCartItemQuantity(target);
        }
    }

    private void showcaseViewShoppingCartItemQuantity(View target) {
        new ShowcaseView.Builder(activity)
                .withMaterialShowcase()
                .singleShot(0x444)
                .setTarget(new ViewTarget(target))
                .setStyle(R.style.CustomShowcaseTheme2)
                .setContentTitle("Thay đổi số lượng của sản phẩm.")
                .setContentText("Nếu bạn muốn mua nhiều hơn 1 sản phẩn, xin vui lòng thay đổi giá trị tại đây, kéo sang phải để thay đổi giá trị.")
                .build()
                .show();
    }
}
