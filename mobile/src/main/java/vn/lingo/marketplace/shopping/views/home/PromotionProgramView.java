package vn.lingo.marketplace.shopping.views.home;

import android.support.v7.widget.RecyclerView;

import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by longtran on 06/12/2015.
 */
public interface PromotionProgramView extends AbstractView {

    public RecyclerView getPromotionProgramRecyclerView();
}
