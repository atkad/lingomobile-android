package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.presenters.MoreProductReviewsPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.MoreProductReviewsView;

/**
 * Created by longtran on 23/12/2015.
 */
public class MoreProductReviewsActivity extends AbstractAppCompatActivity implements MoreProductReviewsView {

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.activity_more_product_review_layout_recycle_view_id)
    RecyclerView recyclerView;

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    private MoreProductReviewsPresenter moreProductReviewsPresenter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_product_review_layout);
        ButterKnife.bind(this);
        moreProductReviewsPresenter = new MoreProductReviewsPresenter(this);
        try {
            int productId = Integer.parseInt(getIntent().getStringExtra(Constant.ProductDetails.PRODUCT_DETAILS_KEY));
            moreProductReviewsPresenter.fetchingMoreProductReviews(productId);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public RecyclerView getRecyclerView() {
        recyclerView.addItemDecoration(new DividerItemDecoration(1));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        return recyclerView;
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        super.onResume();
    }
}
