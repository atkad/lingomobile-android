package vn.lingo.marketplace.shopping.utils;

import android.os.Build;

/**
 * Created by longtran on 14/11/2015.
 */
public class SystemUtils {

    private static SystemUtils systemUtils;

    private static final Object SYNCHRONIZED = new Object();

    public static SystemUtils getInstance() {
        synchronized (SYNCHRONIZED) {
            if (null == systemUtils) {
                systemUtils = new SystemUtils();
            }
            return systemUtils;
        }
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }
}
