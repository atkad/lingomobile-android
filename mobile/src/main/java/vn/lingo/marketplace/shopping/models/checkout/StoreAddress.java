package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lenhan on 01/04/2016.
 */
public class StoreAddress implements Serializable {
    @SerializedName("STORE_ID")
    String storeID;
    @SerializedName("STORE_NAME")
    String storeName;
    @SerializedName("STORE_ADDRESS")
    String storeAddress;

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }
}
