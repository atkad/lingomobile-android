package vn.lingo.marketplace.shopping.views;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flurry.android.FlurryAgent;

import butterknife.ButterKnife;

/**
 * Base fragment created to be extended by every fragment in this application. This class provides
 * dependency injection configuration, ButterKnife Android library configuration and some methods
 * common to every fragment.
 * Created by longtran on 03/11/2015.
 */
public abstract class AbstractFragment extends Fragment {

    private final String TAG = AbstractFragment.class.getName();

    public AbstractFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
        FlurryAgent.onPageView();
    }

    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this method when extends BaseFragment.
     */
    protected abstract int getFragmentLayout();

    /**
     * Replace every field annotated with ButterKnife annotations like @InjectView with the proper
     * value.
     *
     * @param view to extract each widget injected in the fragment.
     */
    private void injectViews(final View view) {
        ButterKnife.bind(this, view);
    }

    public void switchFragments(Fragment newFragment, Bundle args, String fragmentByTag) {
        if (null != args) {
            newFragment.setArguments(args);
        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragmentPrevious = getFragmentManager().findFragmentByTag(fragmentByTag);
        if (fragmentPrevious != null) {
            transaction.remove(fragmentPrevious);
        }
        Log.i(TAG, "getFragmentViewId() : " + getFragmentViewId());
        /***
         * Replace whatever is in the fragment_container view with this fragment,
         * and add the transaction to the back stack so the user can navigate back
         */
        transaction.replace(getFragmentViewId(), newFragment, fragmentByTag);
        transaction.addToBackStack(null);
        /***
         * Commit the transaction
         */
        transaction.commit();
    }

    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this method when extends BaseFragment.
     */
    public abstract int getFragmentViewId();

    /**
     * Method to get internet connection status
     *
     * @param context Context of the current activity
     * @return true     if {@link java.lang.Boolean} internet connection is established else false
     */
    public boolean isInternetConnected(Context context) {
        boolean isConnected;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = (activeNetwork != null)
                && (activeNetwork.isConnectedOrConnecting());
        return isConnected;
    }
}
