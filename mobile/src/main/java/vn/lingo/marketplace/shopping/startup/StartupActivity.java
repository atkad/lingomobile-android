package vn.lingo.marketplace.shopping.startup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ShortcutInformationEntity;

/**
 * Created by longtran on 22/09/2015.
 */
public class StartupActivity extends Activity {

    private final StartupSequenceMediator startupSequenceMediator;

    public StartupActivity() {
        startupSequenceMediator = new StartupSequenceMediator(this);
    }

    public StartupSequenceMediator getStartupMediator() {
        return this.startupSequenceMediator;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Realm realm = Realm.getDefaultInstance();
//        realm.beginTransaction();
//        RealmQuery<ShortcutInformationEntity> realmQuery = realm.where(ShortcutInformationEntity.class);
//        List<ShortcutInformationEntity> listShortcutInformationEntity = realmQuery.findAll();
//        if (listShortcutInformationEntity.size() > 0) {
//            Iterator<ShortcutInformationEntity> iteratorShortcutInformationEntity = listShortcutInformationEntity.iterator();
//            while (iteratorShortcutInformationEntity.hasNext()) {
//                ShortcutInformationEntity shortcutInformationEntity = (ShortcutInformationEntity) iteratorShortcutInformationEntity.next();
//                if (shortcutInformationEntity.getShortcut() == 0) {
//                    removeShortcut();
//                    addShortcut();
//                    ShortcutInformationEntity shortcutInformationEntityUpdate = new ShortcutInformationEntity();
//                    shortcutInformationEntityUpdate.setShortcutId(1984);
//                    shortcutInformationEntityUpdate.setShortcut(1);
//                    realm.copyToRealmOrUpdate(shortcutInformationEntityUpdate);
//                    return;
//                }
//            }
//        } else {
//            ShortcutInformationEntity shortcutInformationEntityUpdate = new ShortcutInformationEntity();
//            shortcutInformationEntityUpdate.setShortcutId(1984);
//            shortcutInformationEntityUpdate.setShortcut(1);
//            realm.copyToRealmOrUpdate(shortcutInformationEntityUpdate);
//            removeShortcut();
//            addShortcut();
//        }
//        realm.commitTransaction();
        setContentView(new StartupView(this));
    }

    /****
     *
     */
    private void addShortcut() {
        //Adding shortcut for MainActivity on Home screen
//        Intent shortcutIntent = new Intent(getApplicationContext(),
//                StartupActivity.class);
//        shortcutIntent.setAction(Intent.ACTION_MAIN);
//        Intent addIntent = new Intent();
//        addIntent
//                .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
//        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
//        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
//                Intent.ShortcutIconResource.fromContext(getApplicationContext(),
//                        R.mipmap.ic_launcher));
//        addIntent
//                .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
//        getApplicationContext().sendBroadcast(addIntent);

        Intent shortcutIntent = new Intent(getApplicationContext(),
                StartupActivity.class);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        Intent addIntent = new Intent();
        addIntent.putExtra("duplicate", false);
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_launcher));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        getApplicationContext().sendBroadcast(addIntent);
    }

    /***
     *
     */
    private void removeShortcut() {
        //Deleting shortcut for MainActivity on Home screen
        Intent shortcutIntent = new Intent(getApplicationContext(),
                StartupActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        Intent addIntent = new Intent();
        addIntent
                .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                        R.mipmap.ic_launcher));
        addIntent
                .setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
    }
}
