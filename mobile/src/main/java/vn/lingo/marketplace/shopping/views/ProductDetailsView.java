package vn.lingo.marketplace.shopping.views;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.viewpagerindicator.CirclePageIndicator;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;
import net.sourceforge.widgets.CountdownView;

import retrofit.Retrofit;

/**
 * Created by longtran on 09/12/2015.
 */
public interface ProductDetailsView extends AbstractView {

    public AppCompatActivity getAppCompatActivity();

    public Toolbar getToolbar();

    public AutoScrollViewPager getAutoScrollViewPager();

    public CirclePageIndicator getCirclePageIndicator();

    public AppCompatTextView getProductDiscount();

    public AppCompatTextView getProductBrand();

    public AppCompatTextView getProductDetailsDescription();

    public CountdownView getCountdownViewGoldenHour();

    public AppCompatTextView getProductCode();

    public AppCompatTextView getProductPrice();

    public AppCompatTextView getProductPriceGoldenHourLabel();

    public AppCompatTextView getProductMarketPrice();

    public RatingBar getRatingBar();

    public AppCompatTextView getAppCompatTextViewScores();

    public RecyclerView getRecyclerViewRelatedProducts();

    public RecyclerView getRecyclerViewProductDetailsGeneralProductInformation();

    public RecyclerView getRecyclerViewProductForReview();

    public AppCompatButton getAppCompatButtonBuyNow();

    public AppCompatButton getAppCompatButtonAddToCart();

    public Retrofit getRetrofitProductCheckout();

    public MaterialDialog getMaterialDialogAlert(AppCompatActivity appCompatActivity, String title, String content, String positiveText);

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);

    public AppCompatTextView getAppCompatTextViewDetailedInformation();

    public AutoScrollViewPager getAutoScrollViewPagerGiftProducts();

    public LinearLayout getLinearLayoutCountDownGoldenHour();

    public LinearLayout getLinearLayoutViewPagerAutoScrollGiftProducts();

    public LinearLayout getLinearLayoutMoreProductProductReviews();

    public AppCompatTextView getAppCompatTextViewProductDetailedInformationViewMore();
}
