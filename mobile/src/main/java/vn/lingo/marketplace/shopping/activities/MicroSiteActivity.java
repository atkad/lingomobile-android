package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionParcelable;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.presenters.MicroSitePresenter;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.MicroSiteView;

/**
 * Created by longtran on 25/11/2015.
 */
public class MicroSiteActivity extends AbstractAppCompatActivity implements MicroSiteView {

    private static final String TAG = MicroSiteActivity.class.getName();

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.linear_layout_micro_site_id)
    LinearLayout linearLayoutMicroSite;

    private MicroSitePresenter microSitePresenter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_deal_fix_deals_collection_screen);
        ButterKnife.bind(this);
        microSitePresenter = new MicroSitePresenter(this);
        EventDealFixDealsCollectionParcelable eventDealFixDealsCollectionParcelable = getIntent().getParcelableExtra(
                Constant.EventDeal.EVENT_DEAL_FIX_DEALS_COLLECTION);
        if (null != eventDealFixDealsCollectionParcelable) {
            microSitePresenter.fetchingEventDealFixDealsCollection(eventDealFixDealsCollectionParcelable);
        }
    }

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    public LinearLayout getLinearLayoutMicroSite() {
        return linearLayoutMicroSite;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Intent intent = new Intent();
        intent.setClassName(this, ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }
}
