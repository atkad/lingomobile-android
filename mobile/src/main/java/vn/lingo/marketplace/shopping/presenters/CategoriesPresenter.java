package vn.lingo.marketplace.shopping.presenters;

import android.graphics.Color;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.adapter.CategoriesRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.models.IndustryAPI;
import vn.lingo.marketplace.shopping.models.IndustryResponse;
import vn.lingo.marketplace.shopping.views.CategoriesView;

/**
 * Created by longtran on 12/11/2015.
 */
public class CategoriesPresenter implements RecyclerViewItemClickListener {

    private CategoriesView categoriesView;

    public CategoriesPresenter(final CategoriesView categoriesView) {
        this.categoriesView = categoriesView;
    }

    public void setAdapter(CategoriesParcelable categoriesParcelable) {
        categoriesView.showProcessing();
        long parentId = categoriesParcelable.getId();
        categoriesView.getToolbar().setTitleTextColor(Color.WHITE);
        categoriesView.getAppCompatActivity().setSupportActionBar(categoriesView.getToolbar());
        categoriesView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        categoriesView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        IndustryAPI industryAPI = categoriesView.getRetrofit().create(IndustryAPI.class);
        Call<List<IndustryResponse>> call = null;
        if (parentId == 0 && categoriesParcelable.getZoneLevel() == 0) {
            call = industryAPI.getIndustryFull();
        } else if (categoriesParcelable.getZoneLevel() == 1) {
            call = industryAPI.getDetailIndustry(parentId);
        } else if (categoriesParcelable.getZoneLevel() == 2) {
            call = industryAPI.getGroupProduct(parentId);
        }
        if (null == call) {
            return;
        }
        call.enqueue(new Callback<List<IndustryResponse>>() {
            @Override
            public void onResponse(Response<List<IndustryResponse>> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    CategoriesRecyclerViewAdapter homeHotDealsRecyclerViewAdapter = new CategoriesRecyclerViewAdapter(categoriesView.getContext(), response.body(),
                            CategoriesPresenter.this, categoriesView.getDisplayImageOptions());
                    categoriesView.getRecyclerViewCategories().setAdapter(homeHotDealsRecyclerViewAdapter);
                } else {

                }
                categoriesView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                categoriesView.hideProcessing();
            }
        });
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        categoriesView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
