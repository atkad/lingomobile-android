package vn.lingo.marketplace.shopping.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by longtran on 12/11/2015.
 */
public class ProductDetailsParcelable implements Parcelable {

    private int id;

    private String title = "Lingo";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static final Creator<ProductDetailsParcelable> CREATOR = new Creator<ProductDetailsParcelable>() {
        public ProductDetailsParcelable createFromParcel(Parcel source) {
            ProductDetailsParcelable categoriesParcelable = new ProductDetailsParcelable();
            categoriesParcelable.id = source.readInt();
            categoriesParcelable.title = source.readString();
            return categoriesParcelable;
        }

        public ProductDetailsParcelable[] newArray(int size) {
            return new ProductDetailsParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
    }
}
