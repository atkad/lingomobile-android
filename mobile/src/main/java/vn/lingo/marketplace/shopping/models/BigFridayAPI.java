package vn.lingo.marketplace.shopping.models;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 24/12/2015.
 */
public interface BigFridayAPI {
    @GET("/v1/mobile/Home/GetBigFriday")
    Call<LingoHomePageAllBigFridayResponse> getBigFriday(@Query("i_current_page") int i_current_page, @Query("i_page_size") int i_page_size);
}
