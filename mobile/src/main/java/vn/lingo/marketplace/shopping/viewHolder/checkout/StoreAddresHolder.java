package vn.lingo.marketplace.shopping.viewHolder.checkout;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by lenhan on 01/04/2016.
 */
public class StoreAddresHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.custom_radiobutton_image_view)
    public ImageView imageView;
    @Bind(R.id.custom_radiobutton_text_view)
    public AppCompatTextView textViewStoreAddress;

    @Bind(R.id.custom_radiobutton_layout)
    public LinearLayout linearLayoutItemStoreAddress;

    public StoreAddresHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
