package vn.lingo.marketplace.shopping.adapter.checkout;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.City;
import vn.lingo.marketplace.shopping.models.checkout.BankVAT;

/**
 * Created by zon on 31/03/2016.
 */
public class BankVATAdapter extends ArrayAdapter<String>{

    private Context context;
    private List<String> lstBankVATs;
    private LayoutInflater layoutInflater;

    public BankVATAdapter(Context context, int resource, List<String> lstBankVATs) {
        super(context, resource, lstBankVATs);

        this.context = context;
        this.lstBankVATs = lstBankVATs;
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getCustomView(int position, View convertView, ViewGroup parents){
        Holder holder;
        if(convertView == null){
            holder = new Holder();
            convertView = layoutInflater.inflate(R.layout.layout_item_ship_type, null);
            holder.textViewCity = (AppCompatTextView) convertView.findViewById(R.id.item_ship_type_text_view);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        String bankVAT = lstBankVATs.get(position);
        holder.textViewCity.setText(bankVAT);
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    class Holder{
        AppCompatTextView textViewCity;
    }
}
