package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 25/12/2015.
 */
public class ProductReviewResponse implements Serializable {

    @SerializedName("TOTALLIKE")
    private String totalLike;
    @SerializedName("COMMENT_ID")
    private String commentId;
    @SerializedName("FULLNAME")
    private String fullName;
    @SerializedName("COMMENT_TITLE")
    private String commentTitle;
    @SerializedName("MEMBERID")
    private String memberId;
    @SerializedName("R")
    private String r;
    @SerializedName("EMAIL")
    private String email;
    @SerializedName("USERNAME")
    private String username;
    @SerializedName("DATECREATED")
    private String dateCreated;
    @SerializedName("PRODUCTID")
    private String productId;
    @SerializedName("COMMENTCONTENT")
    private String commentContent;

    public String getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(String totalLike) {
        this.totalLike = totalLike;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCommentTitle() {
        return commentTitle;
    }

    public void setCommentTitle(String commentTitle) {
        this.commentTitle = commentTitle;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }
}
