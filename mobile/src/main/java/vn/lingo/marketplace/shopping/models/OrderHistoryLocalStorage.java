package vn.lingo.marketplace.shopping.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by longtran on 01/01/2016.
 */
public class OrderHistoryLocalStorage extends RealmObject {

    @PrimaryKey
    private String orderNumberId;
    private String orderNumber;
    private String orderNumberEndpoint;
    private long timeStamp;

    public String getOrderNumberId() {
        return orderNumberId;
    }

    public void setOrderNumberId(String orderNumberId) {
        this.orderNumberId = orderNumberId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumberEndpoint() {
        return orderNumberEndpoint;
    }

    public void setOrderNumberEndpoint(String orderNumberEndpoint) {
        this.orderNumberEndpoint = orderNumberEndpoint;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
