package vn.lingo.marketplace.shopping.views.home;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.utils.JVMRuntime;
import vn.lingo.marketplace.shopping.views.AbstractFragment;

/**
 * Created by longtran on 12/11/2015.
 */
public class HomePageSubCategoriesFragment extends AbstractFragment {

    private final String TAG = HomePageSubCategoriesFragment.class.getName();


    public HomePageSubCategoriesFragment() {
        super();
        Log.i(TAG, "HomePageSubCategoriesFragment");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home_page_sub_categories_layout;
    }

    @Override
    public int getFragmentViewId() {
        return R.id.fragment_home_page_sub_categories_layout_id;
    }


    @Override
    public void onDestroy() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onDestroy();
    }

//    @Override
//    public CharSequence getTitle(Resources r) {
//        return "HomePageSubCategoriesFragment";
//    }

//    @Override
//    public String getSelfTag() {
//        return TAG;
//    }
//
//    @Override
//    public boolean canScrollVertically(int direction) {
//        return false;
//    }
}
