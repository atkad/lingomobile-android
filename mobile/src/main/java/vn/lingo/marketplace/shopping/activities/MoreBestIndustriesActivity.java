package vn.lingo.marketplace.shopping.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.presenters.MoreBestIndustriesPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.MoreBestIndustriesView;

/**
 * Created by longtran on 23/12/2015.
 */
public class MoreBestIndustriesActivity extends AbstractAppCompatActivity implements MoreBestIndustriesView {

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.activity_more_best_industries_layout_recycle_view_id)
    RecyclerView recyclerView;

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    private MoreBestIndustriesPresenter moreBestIndustriesPresenter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_best_industries_layout);
        ButterKnife.bind(this);
        moreBestIndustriesPresenter = new MoreBestIndustriesPresenter(this);
        int eventId = getIntent().getIntExtra(Constant.EVENT_ID, 5);
        moreBestIndustriesPresenter.fetchingMoreBestIndustries(eventId);
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public RecyclerView getRecyclerView() {
        recyclerView.addItemDecoration(new DividerItemDecoration(1));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        return recyclerView;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Intent intent = new Intent();
        intent.setClassName(this, ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        moreBestIndustriesPresenter.invalidateToolbar();
        super.onResume();
    }
}
