package vn.lingo.marketplace.shopping.presenters;

import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.adapter.MoreBestIndustriesRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.adapter.home.LingoHomePageBestIndustryServiceRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.OnRcvScrollListener;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.EventByTypesAPI;
import vn.lingo.marketplace.shopping.models.HomeEventResponse;
import vn.lingo.marketplace.shopping.models.ListHomeEventsResponse;
import vn.lingo.marketplace.shopping.views.MoreBestIndustriesView;

/**
 * Created by longtran on 06/12/2015.
 */
public class MoreBestIndustriesPresenter implements RecyclerViewItemClickListener {

    private String TAG = MoreBestIndustriesPresenter.class.getName();
    private final int totalDocumentOfPage = 15;
    private int currentPage = 1;
    private MoreBestIndustriesView moreBestIndustriesView;

    /***
     * @param moreBestIndustriesView
     */
    public MoreBestIndustriesPresenter(MoreBestIndustriesView moreBestIndustriesView) {
        this.moreBestIndustriesView = moreBestIndustriesView;

    }

    /***
     *
     */
    public void fetchingMoreBestIndustries(int eventId) {
        moreBestIndustriesView.showProcessing();
        moreBestIndustriesView.getToolbar().setTitle("");
        moreBestIndustriesView.getToolbar().setTitleTextColor(moreBestIndustriesView.getContext().getResources().getColor(R.color.primary));
        moreBestIndustriesView.getAppCompatActivity().setSupportActionBar(moreBestIndustriesView.getToolbar());
        moreBestIndustriesView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        moreBestIndustriesView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        moreBestIndustriesView.getToolbar().invalidate();// restore toolbar
        fetchingMoreBestIndustriesRecyclerViewAdapter(eventId);

    }

    /***
     *
     */
    public void invalidateToolbar() {
        //mainView.getToolbar().setLogo(R.drawable.lingo_logo_tool_bar);
        moreBestIndustriesView.getToolbar().setTitle("");
        moreBestIndustriesView.getToolbar().setTitleTextColor(moreBestIndustriesView.getContext().getResources().getColor(R.color.primary));
        moreBestIndustriesView.getToolbar().invalidate();// restore toolbar
    }
    /****
     *
     */
    private void fetchingMoreBestIndustriesRecyclerViewAdapter(final int eventId) {
        try {
            final EventByTypesAPI eventByTypesAPI = moreBestIndustriesView.getRetrofit().create(EventByTypesAPI.class);
            Call<ListHomeEventsResponse> callListHomeEventsResponse = null;
            if (eventId == 5) {
                callListHomeEventsResponse = eventByTypesAPI.getDetailItemInAllEvent(eventId, totalDocumentOfPage, currentPage);
            } else {
                callListHomeEventsResponse = eventByTypesAPI.getDetailItemInOneEvent(eventId, totalDocumentOfPage, currentPage);
            }
            callListHomeEventsResponse.enqueue(new Callback<ListHomeEventsResponse>() {
                @Override
                public void onResponse(Response<ListHomeEventsResponse> response, Retrofit retrofit) {
                    if (response.code() == 200 && null != response.body().getListHomeEventResponse() &&
                            response.body().getListHomeEventResponse().size() > 0) {
                        final List<HomeEventResponse> listHomeEventResponse = new ArrayList<HomeEventResponse>();
                        final int count = response.body().getCount();
                        for (HomeEventResponse hotDealFestivalResponse : response.body().getListHomeEventResponse()) {
                            listHomeEventResponse.add(hotDealFestivalResponse);
                        }
                        final MoreBestIndustriesRecyclerViewAdapter moreBestIndustriesRecyclerViewAdapter
                                = new MoreBestIndustriesRecyclerViewAdapter(moreBestIndustriesView.getContext(), listHomeEventResponse,
                                MoreBestIndustriesPresenter.this, moreBestIndustriesView.getDisplayImageOptions());
                        moreBestIndustriesView.getRecyclerView().setAdapter(moreBestIndustriesRecyclerViewAdapter);
                        moreBestIndustriesView.getRecyclerView().addOnScrollListener(new OnRcvScrollListener() {
                            @Override
                            public void onLoadMore(int totalItemCount) {
                                super.onLoadMore(totalItemCount);
                                if ((currentPage * totalDocumentOfPage) < count) {
                                    currentPage++;
                                    listHomeEventResponse.add(null);
                                    moreBestIndustriesRecyclerViewAdapter.notifyItemInserted(listHomeEventResponse.size() - 1);
                                    Call<ListHomeEventsResponse> callListHomeEventsResponseLoadMore = null;
                                    if (eventId == 5) {
                                        callListHomeEventsResponseLoadMore = eventByTypesAPI.getDetailItemInAllEvent(eventId, totalDocumentOfPage, currentPage);
                                    } else {
                                        callListHomeEventsResponseLoadMore = eventByTypesAPI.getDetailItemInOneEvent(eventId, totalDocumentOfPage, currentPage);
                                    }
                                    callListHomeEventsResponseLoadMore.enqueue(new Callback<ListHomeEventsResponse>() {
                                        @Override
                                        public void onResponse(Response<ListHomeEventsResponse> response, Retrofit retrofit) {
                                            listHomeEventResponse.remove(null);
                                            moreBestIndustriesRecyclerViewAdapter.notifyDataSetChanged();
                                            if (response.code() == 200 && null != response.body().getListHomeEventResponse() &&
                                                    response.body().getListHomeEventResponse().size() > 0) {
                                                for (HomeEventResponse homeEventResponse : response.body().getListHomeEventResponse()) {
                                                    listHomeEventResponse.add(homeEventResponse);
                                                    moreBestIndustriesRecyclerViewAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {
                                            moreBestIndustriesView.hideProcessing();
                                        }
                                    });
                                }
                            }
                        });
                        moreBestIndustriesView.hideProcessing();
                    }

                }

                @Override
                public void onFailure(Throwable t) {
                    moreBestIndustriesView.hideProcessing();
                }
            });
        } catch (Exception exception) {
            Log.i(TAG, exception.toString());
        }
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        moreBestIndustriesView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
