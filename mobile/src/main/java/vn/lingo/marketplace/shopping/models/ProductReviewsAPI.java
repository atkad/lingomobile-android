package vn.lingo.marketplace.shopping.models;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 25/12/2015.
 */
public interface ProductReviewsAPI {
    @GET("/v1/mobile/Product/GetListComments")
    Call<ProductReviewsResponse> getListComments(@Query("i_current_page") int i_current_page, @Query("i_page_size") int i_page_size, @Query("i_productid") int i_productid);
}
