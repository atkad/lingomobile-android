package vn.lingo.marketplace.shopping.views.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class ProductDetailsRelatedProductsRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.product_details_related_products_recycler_view_item_layout_id)
    public RelativeLayout relativeLayoutRowItem;

    @Bind(R.id.product_details_related_products_image_view_meta_data_id)
    public ImageView productDetailsRelatedProductsImageViewMetaData;

    @Bind(R.id.product_details_related_products_title)
    public AppCompatTextView productDetailsRelatedProductsTitle;

    @Bind(R.id.product_details_related_products_prices)
    public AppCompatTextView productDetailsRelatedProductsPrices;

    @Bind(R.id.product_details_related_products_market_price)
    public AppCompatTextView productDetailsRelatedProductsMarketPrice;

    @Bind(R.id.product_details_related_products_item_linear_layout_percent_id)
    public LinearLayout linearLayoutPercent;

    @Bind(R.id.product_details_related_products_item_percent_id)
    public AppCompatTextView appCompatTextViewPercent;

    @Bind(R.id.product_details_related_products_item_linear_layout_happy_hour_discount_id)
    public LinearLayout linearLayoutHappyHourDiscount;

    @Bind(R.id.product_details_related_products_item_happy_hour_discount_id)
    public AppCompatTextView appCompatTextViewPercentHappyHourDiscount;

    @Bind(R.id.product_details_related_products_item_linear_layout_discount_gift_cards_id)
    public LinearLayout linearLayoutGiftCards;

    @Bind(R.id.product_details_related_products_item_discount_gift_cards_id)
    public AppCompatTextView appCompatTextViewGiftCards;


    public ProductDetailsRelatedProductsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        productDetailsRelatedProductsMarketPrice.setPaintFlags(productDetailsRelatedProductsMarketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
