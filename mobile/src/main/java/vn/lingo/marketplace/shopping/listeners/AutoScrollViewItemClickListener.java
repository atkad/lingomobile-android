package vn.lingo.marketplace.shopping.listeners;

/**
 * Created by longtran on 13/12/2015.
 */
public interface AutoScrollViewItemClickListener {
    public void onItemClicked(int position, int id, String title);
}
