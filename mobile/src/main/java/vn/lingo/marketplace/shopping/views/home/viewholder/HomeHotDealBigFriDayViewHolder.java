package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 16/11/2015.
 */
public class HomeHotDealBigFriDayViewHolder {

    @Bind(R.id.home_hot_deal_big_fri_day_image_view_id)
    public ImageView imageView;

    @Bind(R.id.panel_big_fri_day)
    public LinearLayout linearLayout;
    
//    @Bind(R.id.home_hot_deal_big_fri_day_app_compat_text_view_title_id)
//    public AppCompatTextView appCompatTextViewTitle;
//
//    @Bind(R.id.home_hot_deal_big_fri_day_app_compat_text_view_description_id)
//    public AppCompatTextView appCompatTextViewDescription;

    public HomeHotDealBigFriDayViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }

}
