package vn.lingo.marketplace.shopping.adapter.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.local.HomeEventResponseLocalStorage;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.views.home.viewholder.LingoHomePageBestIndustryServiceRecyclerViewHolder;
import vn.lingo.marketplace.shopping.views.home.viewholder.ProgressViewHolder;


/**
 * Created by longtran on 09/11/2015.
 */
public class LingoHomePageBestIndustryServiceRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    private List<HomeEventResponseLocalStorage> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    public LingoHomePageBestIndustryServiceRecyclerViewAdapter(Context context, List<HomeEventResponseLocalStorage> itemList,
                                                               RecyclerViewItemClickListener recyclerViewItemClickListener,
                                                               DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        if (null != context) {
            this.context = context;
            this.displayImageOptions = displayImageOptions;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lingo_home_page_best_industry_recycler_view_item_layout, null);
            LingoHomePageBestIndustryServiceRecyclerViewHolder viewHolder =
                    new LingoHomePageBestIndustryServiceRecyclerViewHolder(view);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);
            ProgressViewHolder progressViewHolder = new ProgressViewHolder(view);
            return progressViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof LingoHomePageBestIndustryServiceRecyclerViewHolder) {
            final LingoHomePageBestIndustryServiceRecyclerViewHolder homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder
                    = (LingoHomePageBestIndustryServiceRecyclerViewHolder) holder;
            final HomeEventResponseLocalStorage homeEventResponse = itemList.get(position);
            try {
                ImageSize targetSize = new ImageSize(256, 256); // result Bitmap will be fit to this size
                ImageLoader.getInstance().loadImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + homeEventResponse.getAvatar(), targetSize,
                        displayImageOptions, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                super.onLoadingStarted(imageUri, view);
                                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                super.onLoadingComplete(imageUri, view, loadedImage);
                                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.imageView.setImageBitmap(loadedImage);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                super.onLoadingFailed(imageUri, view, failReason);
                                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                super.onLoadingCancelled(imageUri, view);
                                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }
                        });
            } catch (OutOfMemoryError outOfMemoryError) {
                outOfMemoryError.printStackTrace();
            }
//            ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + homeEventResponse.getAvatar(),
//                    homeFragmentHotDealsFestivalRecyclerViewHolder.imageView, displayImageOptions);
            if (null != context) {
                AnalyticsHelper.logEvent(context, homeEventResponse.getProductName(), homeEventResponse.getProductId());
            }
            homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.appCompatTextViewDescription.setText(homeEventResponse.getProductName());
            if (!StringUtils.isBlank(homeEventResponse.getProductPriceFormat())) {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.price.setVisibility(View.VISIBLE);
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.price.setText(homeEventResponse.getProductPriceFormat());
            } else {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.price.setVisibility(View.GONE);
            }

            if (!StringUtils.isBlank(homeEventResponse.getMarketPriceFormat())) {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.marketPrice.setVisibility(View.VISIBLE);
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.marketPrice.setText(homeEventResponse.getMarketPriceFormat());
            } else {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.marketPrice.setVisibility(View.GONE);
            }

            if (!StringUtils.isBlank(homeEventResponse.getPromotionPercent())) {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.linearLayoutPercent.setVisibility(View.VISIBLE);
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.appCompatTextViewPercent.setText(homeEventResponse.getPromotionPercent());
            } else {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.linearLayoutPercent.setVisibility(View.GONE);
            }

            if (!StringUtils.isBlank(homeEventResponse.getHappyHourDiscount())) {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.VISIBLE);
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.appCompatTextViewHappyHourDiscount.setText("Giờ vàng");
            } else {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.GONE);
            }

            if (!StringUtils.isBlank(homeEventResponse.getGiftCard())) {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.linearLayoutDiscountGiftCards.setVisibility(View.VISIBLE);
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.appCompatTextViewDiscountGiftCards.setText("Kèm theo quà tặng");
            } else {
                homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.linearLayoutDiscountGiftCards.setVisibility(View.GONE);
            }
            homeBestDealEverydayHouseholdEssentialsElectricalAppliancesRecyclerViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(homeEventResponse.getProductId()),
                                Integer.MAX_VALUE, Integer.MAX_VALUE,
                                homeEventResponse.getProductName());
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            });
        } else {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBarLoadMore.setIndeterminate(true);
        }
    }
}
