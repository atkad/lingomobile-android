package vn.lingo.marketplace.shopping.adapter.search;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.search.SearchResultsResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.views.home.viewholder.ProgressViewHolder;
import vn.lingo.marketplace.shopping.views.viewholder.search.SearchResultsRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class SearchResultsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String TAG = SearchResultsRecyclerViewAdapter.class.getName();
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    private List<SearchResultsResponse> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    /***
     * @param context
     * @param itemList
     * @param recyclerViewItemClickListener
     */
    public SearchResultsRecyclerViewAdapter(Context context, List<SearchResultsResponse> itemList,
                                            RecyclerViewItemClickListener recyclerViewItemClickListener,
                                            DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.context = context;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_results_item_layout, null);
            SearchResultsRecyclerViewHolder viewHolder = new SearchResultsRecyclerViewHolder(view);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);
            ProgressViewHolder progressViewHolder = new ProgressViewHolder(view);
            return progressViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof SearchResultsRecyclerViewHolder) {
            final SearchResultsRecyclerViewHolder searchResultsRecyclerViewHolder = (SearchResultsRecyclerViewHolder) holder;
            final SearchResultsResponse searchResultsResponse = itemList.get(position);
            try {
                searchResultsRecyclerViewHolder.searchResultsProductionTitle.setText(searchResultsResponse.getProductName());
                searchResultsRecyclerViewHolder.searchResultsPrices.setText(String.format(context.getResources().getString(R.string.prices),
                        searchResultsResponse.getProductPriceFormat()));
                if (StringUtils.isBlank(searchResultsResponse.getMarketPriceFormat())) {
                    searchResultsRecyclerViewHolder.searchResultsMarketPrice.setVisibility(View.GONE);
                } else {
                    searchResultsRecyclerViewHolder.searchResultsMarketPrice.setVisibility(View.VISIBLE);
                    searchResultsRecyclerViewHolder.searchResultsMarketPrice.setText(String.format(context.getResources().getString(R.string.market_price),
                            searchResultsResponse.getMarketPriceFormat()));
                }
                searchResultsRecyclerViewHolder.searchResultsRatingBar.setNumStars(Constant.TOTAL_STAR);
                searchResultsRecyclerViewHolder.searchResultsRatingBar.setRating(Float.parseFloat(searchResultsResponse.getRatingStar()));
            } catch (Exception exception) {
                Log.e(TAG, exception.toString());
            }
//            ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + searchResultsResponse.getAvatar(),
//                    searchResultsRecyclerViewHolder.imageView, displayImageOptions);
            String urlAvatar = Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + searchResultsResponse.getAvatar();
            try {
                ImageSize targetSize = new ImageSize(256, 256);
                ImageLoader.getInstance().loadImage(urlAvatar, targetSize,
                        displayImageOptions, new SimpleImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                super.onLoadingStarted(imageUri, view);
                                searchResultsRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                super.onLoadingFailed(imageUri, view, failReason);
                                searchResultsRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                super.onLoadingCancelled(imageUri, view);
                                searchResultsRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                searchResultsRecyclerViewHolder.imageView.setImageBitmap(loadedImage);
                            }
                        });
            } catch (OutOfMemoryError outOfMemoryError) {
                Log.e(TAG, outOfMemoryError.toString());
            }

            searchResultsRecyclerViewHolder.relativeLayoutRowItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(searchResultsResponse.getProductId()),
                                Integer.MAX_VALUE, Integer.MAX_VALUE, searchResultsResponse.getProductName());
                    } catch (Exception exception) {
                        Log.e(TAG, exception.toString());
                    }
                }
            });
        } else {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBarLoadMore.setIndeterminate(true);
        }
    }
}
