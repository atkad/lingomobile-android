package vn.lingo.marketplace.shopping.views.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class LeafChildrenCategoryRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.leaf_children_category_item_layout_id)
    public RelativeLayout relativeLayoutRowItem;

    @Bind(R.id.image_view_leaf_children_category)
    public ImageView imageView;

    @Bind(R.id.leaf_children_category_production_title)
    public AppCompatTextView leafChildrenCategoryProductionTitle;

    @Bind(R.id.leaf_children_categories_prices)
    public AppCompatTextView leafChildrenCategoriesPrices;

    @Bind(R.id.leaf_children_categories_market_price)
    public AppCompatTextView leafChildrenCategoriesMarketPrice;

    @Bind(R.id.leaf_children_category_rating_bar)
    public RatingBar leafChildrenCategoryRatingBar;

    @Bind(R.id.linear_layout_percent)
    public LinearLayout leafChildrenCategoryLinearLayoutPercent;

    @Bind(R.id.app_compat_text_view_item_percent)
    public AppCompatTextView leafChildrenCategoryPercent;

    public LeafChildrenCategoryRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        leafChildrenCategoriesMarketPrice.setPaintFlags(leafChildrenCategoriesMarketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
