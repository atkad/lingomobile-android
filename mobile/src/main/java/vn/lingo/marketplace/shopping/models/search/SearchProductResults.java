package vn.lingo.marketplace.shopping.models.search;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 13/12/2015.
 */
public class SearchProductResults implements Serializable {

    @SerializedName("count")
    public int count;
    @SerializedName("items")
    public List<SearchResultsResponse> items;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<SearchResultsResponse> getItems() {
        return items;
    }

    public void setItems(List<SearchResultsResponse> items) {
        this.items = items;
    }

}
