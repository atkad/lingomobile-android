package vn.lingo.marketplace.shopping.presenters;

import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.adapter.LeafChildrenCategoryFilterAdapter;
import vn.lingo.marketplace.shopping.adapter.LeafChildrenCategoryRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.OnRcvScrollListener;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.models.LeafChildrenCategoryAPI;
import vn.lingo.marketplace.shopping.models.LeafChildrenCategoryResponse;
import vn.lingo.marketplace.shopping.models.LeafChildrenProductsByCategoryResponse;
import vn.lingo.marketplace.shopping.models.ProductionFilterEntity;
import vn.lingo.marketplace.shopping.views.LeafChildrenCategoryView;

/**
 * Created by longtran on 06/12/2015.
 */
public class LeafChildrenCategoryPresenter implements RecyclerViewItemClickListener {

    private final int totalDocumentOfPage = 15;
    private int currentPage = 1;
    private LeafChildrenCategoryView leafChildrenCategoryView;
    private int categoriesItemId;
    private int mappingZoneId;
    private String sortName;
    private String sortType;
    private int count;
    private List<LeafChildrenCategoryResponse> listLeafChildrenCategoryResponseGlobal;
    private LeafChildrenCategoryRecyclerViewAdapter leafChildrenCategoryRecyclerViewAdapter;
    private LeafChildrenCategoryAPI leafChildrenCategoryAPI;

    /***
     * @param leafChildrenCategoryView
     */
    public LeafChildrenCategoryPresenter(final LeafChildrenCategoryView leafChildrenCategoryView) {
        this.leafChildrenCategoryView = leafChildrenCategoryView;
        leafChildrenCategoryAPI = leafChildrenCategoryView.getRetrofit().create(LeafChildrenCategoryAPI.class);
        listLeafChildrenCategoryResponseGlobal = new ArrayList<LeafChildrenCategoryResponse>();
        leafChildrenCategoryRecyclerViewAdapter = new LeafChildrenCategoryRecyclerViewAdapter(
                leafChildrenCategoryView.getContext(), listLeafChildrenCategoryResponseGlobal, LeafChildrenCategoryPresenter.this,
                leafChildrenCategoryView.getDisplayImageOptions());
        leafChildrenCategoryView.getRecyclerViewLeafChildrenCategory().setAdapter(leafChildrenCategoryRecyclerViewAdapter);
    }

    /***
     *
     */
    public void fetchingLeafChildrenCategory(final CategoriesParcelable categoriesParcelable) {
        mappingZoneId = categoriesParcelable.getMappingZoneId();
        categoriesItemId = categoriesParcelable.getId();
        leafChildrenCategoryView.getToolbar().setTitleTextColor(Color.WHITE);
        leafChildrenCategoryView.getAppCompatActivity().setSupportActionBar(leafChildrenCategoryView.getToolbar());
        leafChildrenCategoryView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        leafChildrenCategoryView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        List<ProductionFilterEntity> listProductionFilterEntity = new ArrayList<ProductionFilterEntity>();

        ProductionFilterEntity productionFilterEntityAscending = new ProductionFilterEntity();
        productionFilterEntityAscending.setTitle("Giá tăng dần");
        productionFilterEntityAscending.setSortName("PRODUCT_PRICE");
        productionFilterEntityAscending.setSortType("ASC");
        listProductionFilterEntity.add(productionFilterEntityAscending);

        ProductionFilterEntity productionFilterEntityDescending = new ProductionFilterEntity();
        productionFilterEntityDescending.setTitle("Giá giảm dần");
        productionFilterEntityDescending.setSortName("PRODUCT_PRICE");
        productionFilterEntityDescending.setSortType("DESC");
        listProductionFilterEntity.add(productionFilterEntityDescending);

        ProductionFilterEntity productionFilterCreatedOnAscending = new ProductionFilterEntity();
        productionFilterCreatedOnAscending.setTitle("Theo ngày tăng dần");
        productionFilterCreatedOnAscending.setSortName("CREATEDON");
        productionFilterCreatedOnAscending.setSortType("ASC");
        listProductionFilterEntity.add(productionFilterCreatedOnAscending);

        ProductionFilterEntity productionFilterCreatedOnDescending = new ProductionFilterEntity();
        productionFilterCreatedOnDescending.setTitle("Theo ngày giảm dần");
        productionFilterCreatedOnDescending.setSortName("CREATEDON");
        productionFilterCreatedOnDescending.setSortType("DESC");
        listProductionFilterEntity.add(productionFilterCreatedOnDescending);

        LeafChildrenCategoryFilterAdapter leafChildrenCategoryFilterAdapter = new LeafChildrenCategoryFilterAdapter(leafChildrenCategoryView.getContext(),
                listProductionFilterEntity);
        leafChildrenCategoryView.getAppCompatSpinnerDataFilter().setAdapter(leafChildrenCategoryFilterAdapter);
        leafChildrenCategoryView.getAppCompatSpinnerDataFilter().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listLeafChildrenCategoryResponseGlobal.clear();
                leafChildrenCategoryView.showProcessing();
                ProductionFilterEntity productionFilterEntity = (ProductionFilterEntity) parent.getItemAtPosition(position);
                sortName = productionFilterEntity.getSortName();
                sortType = productionFilterEntity.getSortType();
                Call<LeafChildrenProductsByCategoryResponse> call = leafChildrenCategoryAPI.getProductsByCategory(mappingZoneId, sortName, sortType,
                        totalDocumentOfPage, currentPage);
                call.enqueue(new Callback<LeafChildrenProductsByCategoryResponse>() {
                    @Override
                    public void onResponse(Response<LeafChildrenProductsByCategoryResponse> response, Retrofit retrofit) {
                        if (response.code() == 200 && response.body().getItems().size() > 0) {
                            count = response.body().getCount();
                            List<LeafChildrenCategoryResponse> listLeafChildrenCategoryResponse = response.body().getItems();
                            Iterator<LeafChildrenCategoryResponse> iteratorLeafChildrenCategoryResponse = listLeafChildrenCategoryResponse.iterator();
                            while (iteratorLeafChildrenCategoryResponse.hasNext()) {
                                listLeafChildrenCategoryResponseGlobal.add(iteratorLeafChildrenCategoryResponse.next());
                                leafChildrenCategoryRecyclerViewAdapter.notifyDataSetChanged();
                            }
                        }
                        leafChildrenCategoryView.hideProcessing();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        leafChildrenCategoryView.hideProcessing();
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                leafChildrenCategoryView.hideProcessing();
            }
        });

        leafChildrenCategoryView.getRecyclerViewLeafChildrenCategory().addOnScrollListener(new OnRcvScrollListener() {
            @Override
            public void onLoadMore(int totalItemCount) {
                super.onLoadMore(totalItemCount);
                if ((currentPage * totalDocumentOfPage) < count) {
                    currentPage++;
                    listLeafChildrenCategoryResponseGlobal.add(null);
                    leafChildrenCategoryRecyclerViewAdapter.notifyItemInserted(listLeafChildrenCategoryResponseGlobal.size() - 1);
                    Call<LeafChildrenProductsByCategoryResponse> call = leafChildrenCategoryAPI.getProductsByCategory(mappingZoneId, sortName,
                            sortType, totalDocumentOfPage, currentPage);
                    call.enqueue(new Callback<LeafChildrenProductsByCategoryResponse>() {
                        @Override
                        public void onResponse(Response<LeafChildrenProductsByCategoryResponse> response, Retrofit retrofit) {
                            listLeafChildrenCategoryResponseGlobal.remove(null);
                            leafChildrenCategoryRecyclerViewAdapter.notifyDataSetChanged();
                            if (response.code() == 200 && response.body().getItems().size() > 0) {
                                List<LeafChildrenCategoryResponse> listLeafChildrenCategoryResponse = response.body().getItems();
                                Iterator<LeafChildrenCategoryResponse> iteratorLeafChildrenCategoryResponse = listLeafChildrenCategoryResponse.iterator();
                                while (iteratorLeafChildrenCategoryResponse.hasNext()) {
                                    listLeafChildrenCategoryResponseGlobal.add(iteratorLeafChildrenCategoryResponse.next());
                                    leafChildrenCategoryRecyclerViewAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            listLeafChildrenCategoryResponseGlobal.remove(null);
                            leafChildrenCategoryRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });

    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        leafChildrenCategoryView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
