package vn.lingo.marketplace.shopping.models;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 10/11/2015.
 */
public interface HotDealAPI {

    @GET("/v1/mobile/advertisement/hotdeal")
    Call<List<HotDealResponse>> getHotDeals();

    @GET("/v1/mobile/event/ProductAllEventHomePage")
    Call<List<HomeEventResponse>> getListHotDealFestivalResponse();

    @GET("/v1/mobile/product/ProductsInEventHotDeal")
    Call<ListHomeEventsResponse> getListProductsOnHotDealsFestivalResponse(@Query("P_EVENTID")  int P_EVENTID,
                                                                           @Query("P_CATID")    int P_CATID,
                                                                           @Query("P_MINPRICE") int P_MINPRICE,
                                                                           @Query("P_MAXPRICE") int P_MAXPRICE,
                                                                           @Query("P_PAGESIZE") int P_PAGESIZE,
                                                                           @Query("P_CURPAGE")  int P_CURPAGE);
    @GET("/v1/mobile/advertisement/BannerMegaSale")
    Call<HomeHotDealsFestivalResponse> getMegaSaleAds();

    @GET("/v1/mobile/advertisement/BannerDailyDeal")
    Call<HomeHotDealsFestivalResponse> getDailyDeal();

    @GET("/v1/mobile/advertisement/BannerBigFriday")
    Call<HomeHotDealsFestivalResponse> getBigFridayAds();
}