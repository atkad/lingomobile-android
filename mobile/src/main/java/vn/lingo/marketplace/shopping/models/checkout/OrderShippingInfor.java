package vn.lingo.marketplace.shopping.models.checkout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lenhan on 05/04/2016.
 */
public class OrderShippingInfor implements Serializable {
    @SerializedName("USER_LOGIN_ID")
    int userLoginID;
    @SerializedName("RECEIVE_ORDER_TYPE")
    int receiveOrdeType;
    @SerializedName("RECEIVE_CITY_ID")
    int receiveCityID;
    @SerializedName("RECEIVE_DISTRICT_ID")
    int receiveDistrictID;
    List<ProductShippingInfo> listProductInfor;
    @SerializedName("totalAmount")
    int totalAmount;
    @SerializedName("shipType")
    int shipType;

    public OrderShippingInfor(int userLoginID, int receiveOrdeType, int receiveCityID
            , int receiveDistrictID, List<ProductShippingInfo> listProductInfor, int totalAmount, int shipType) {
        this.userLoginID = userLoginID;
        this.receiveOrdeType = receiveOrdeType;
        this.receiveCityID = receiveCityID;
        this.receiveDistrictID = receiveDistrictID;
        this.listProductInfor = listProductInfor;
        this.totalAmount = totalAmount;
        this.shipType = shipType;
    }

    public int getUserLoginID() {
        return userLoginID;
    }

    public void setUserLoginID(int userLoginID) {
        this.userLoginID = userLoginID;
    }

    public int getReceiveOrdeType() {
        return receiveOrdeType;
    }

    public void setReceiveOrdeType(int receiveOrdeType) {
        this.receiveOrdeType = receiveOrdeType;
    }

    public int getReceiveCityID() {
        return receiveCityID;
    }

    public void setReceiveCityID(int receiveCityID) {
        this.receiveCityID = receiveCityID;
    }

    public int getReceiveDistrictID() {
        return receiveDistrictID;
    }

    public void setReceiveDistrictID(int receiveDistrictID) {
        this.receiveDistrictID = receiveDistrictID;
    }

    public List<ProductShippingInfo> getListProductInfor() {
        return listProductInfor;
    }

    public void setListProductInfor(List<ProductShippingInfo> listProductInfor) {
        this.listProductInfor = listProductInfor;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getShipType() {
        return shipType;
    }

    public void setShipType(int shipType) {
        this.shipType = shipType;
    }
}
