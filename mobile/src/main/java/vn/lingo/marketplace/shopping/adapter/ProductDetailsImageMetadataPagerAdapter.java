package vn.lingo.marketplace.shopping.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.sourceforge.android.view.autoscrollviewpager.RecyclingPagerAdapter;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ProductInformationImageMetadata;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.ProductDetailsImageMetadataViewHolder;

/**
 * Created by longtran on 04/11/2015.
 */
public class ProductDetailsImageMetadataPagerAdapter extends RecyclingPagerAdapter {

    private Context context;
    private List<ProductInformationImageMetadata> listImageMetadata;
    private DisplayImageOptions displayImageOptions;

    public ProductDetailsImageMetadataPagerAdapter(Context context, List<ProductInformationImageMetadata> listImageMetadata,
                                                   DisplayImageOptions displayImageOptions) {
        this.context = context;
        this.listImageMetadata = listImageMetadata;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public int getCount() {
        return listImageMetadata.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        ProductDetailsImageMetadataViewHolder productDetailsImageMetadataViewHolder;
        if (convertView == null) {
            /***
             * inflate the layout
             */
            convertView = LayoutInflater.from(container.getContext())
                    .inflate(R.layout.product_details_image_metadata_view_pager_item_image, container, false);
            /***
             * well set up the HomeSliderBannerViewHolder
             */
            productDetailsImageMetadataViewHolder = new ProductDetailsImageMetadataViewHolder(convertView);
            /***
             * store the holder with the view.
             */
            convertView.setTag(productDetailsImageMetadataViewHolder);
        } else {
            /***
             * we've just avoided calling findViewById() on resource everytimejust use the viewHolder
             */
            productDetailsImageMetadataViewHolder = (ProductDetailsImageMetadataViewHolder) convertView.getTag();
        }
        ProductInformationImageMetadata productInformationImageMetadata = listImageMetadata.get(position);
        ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + productInformationImageMetadata.getImagePath(),
                productDetailsImageMetadataViewHolder.imageView, displayImageOptions);
        return convertView;
    }
}
