package vn.lingo.marketplace.shopping.models;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 09/12/2015.
 */
public interface ProductsAPI {

    @GET("/v1/mobile/Home/GetFullProDetail")
    Call<ProductDetailsResponse> getProductDetails(@Query("i_productid") int i_productid);

    @GET("/v1/mobile/Product/GetGiftProduct")
    Call<ProductDetailsResponse> getGiftProduct(@Query("i_product_id") int i_product_id);
}
