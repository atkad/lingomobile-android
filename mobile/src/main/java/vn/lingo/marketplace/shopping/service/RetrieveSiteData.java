package vn.lingo.marketplace.shopping.service;

import android.os.AsyncTask;
import android.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import io.realm.Realm;
import vn.lingo.marketplace.shopping.models.OrderHistoryLocalStorage;
import vn.lingo.marketplace.shopping.utils.Utility;

/**
 * Created by longtran on 01/01/2016.
 */
public class RetrieveSiteData extends AsyncTask<String, Integer, String> {

    private final String TAG = RetrieveSiteData.class.getName();
    private String orderNumberEndpoint;

    @Override
    protected String doInBackground(String... params) {
        try {
            // Build and set timeout values for the request.
            orderNumberEndpoint = params[0];
            URLConnection connection = (new URL(orderNumberEndpoint)).openConnection();
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            // Read and store the result line by line then return the entire string.
            InputStream in = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder html = new StringBuilder();
            for (String line; (line = reader.readLine()) != null; ) {
                html.append(line);
            }
            in.close();
            return html.toString();
        } catch (Exception exception) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (!StringUtil.isBlank(s)) {
            byte[] byteBase64 = orderNumberEndpoint.getBytes();
            String value = Base64.encodeToString(byteBase64, Base64.URL_SAFE | android.util.Base64.NO_WRAP);
            OrderHistoryLocalStorage orderHistoryLocalStorage = new OrderHistoryLocalStorage();
            if (!StringUtils.isBlank(value) &&
                    !StringUtils.isBlank(Utility.getInstance().getOrderNumber(s))
                    && !StringUtils.isBlank(value)) {
                orderHistoryLocalStorage.setOrderNumberEndpoint(value);
                orderHistoryLocalStorage.setOrderNumber(Utility.getInstance().getOrderNumber(s));
                orderHistoryLocalStorage.setOrderNumberId(value);
                orderHistoryLocalStorage.setTimeStamp(System.currentTimeMillis());
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(orderHistoryLocalStorage);
                realm.commitTransaction();
            }
        }
    }
}
