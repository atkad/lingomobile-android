package vn.lingo.marketplace.shopping.presenters;

import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.adapter.ProductDetailsProductReviewsRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.OnRcvScrollListener;
import vn.lingo.marketplace.shopping.models.ProductReviewResponse;
import vn.lingo.marketplace.shopping.models.ProductReviewsAPI;
import vn.lingo.marketplace.shopping.models.ProductReviewsResponse;
import vn.lingo.marketplace.shopping.views.MoreProductReviewsView;

/**
 * Created by longtran on 06/12/2015.
 */
public class MoreProductReviewsPresenter {

    private String TAG = MoreProductReviewsPresenter.class.getName();
    private final int totalDocumentOfPage = 15;
    private int currentPage = 1;
    private MoreProductReviewsView moreProductReviewsView;

    /***
     * @param moreProductReviewsView
     */
    public MoreProductReviewsPresenter(MoreProductReviewsView moreProductReviewsView) {
        this.moreProductReviewsView = moreProductReviewsView;

    }

    /***
     *
     */
    public void fetchingMoreProductReviews(int productId) {
        moreProductReviewsView.showProcessing();
        moreProductReviewsView.getToolbar().setTitleTextColor(Color.WHITE);
        moreProductReviewsView.getAppCompatActivity().setSupportActionBar(moreProductReviewsView.getToolbar());
        moreProductReviewsView.getAppCompatActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        moreProductReviewsView.getAppCompatActivity().getSupportActionBar().setHomeButtonEnabled(false);
        fetchingMoreProductReviewsRecyclerViewAdapter(productId);

    }

    /****
     *
     */
    private void fetchingMoreProductReviewsRecyclerViewAdapter(final int productId) {
        try {
            final ProductReviewsAPI productReviewsAPI = moreProductReviewsView.getRetrofit().create(ProductReviewsAPI.class);
            Call<ProductReviewsResponse> callProductReviewsResponse = productReviewsAPI.getListComments(currentPage, totalDocumentOfPage, productId);
            callProductReviewsResponse.enqueue(new Callback<ProductReviewsResponse>() {
                @Override
                public void onResponse(Response<ProductReviewsResponse> response, Retrofit retrofit) {
                    if (response.code() == 200 && null != response.body().getLisProductReviewsResponse() &&
                            response.body().getLisProductReviewsResponse().size() > 0) {
                        final List<ProductReviewResponse> listHomeEventResponse = new ArrayList<ProductReviewResponse>();
                        final int count = Integer.parseInt(response.body().getTotalRecord());
                        Iterator<ProductReviewResponse> iteratorProductReviewResponse = response.body().getLisProductReviewsResponse().iterator();
                        while (iteratorProductReviewResponse.hasNext()) {
                            listHomeEventResponse.add(iteratorProductReviewResponse.next());
                        }
                        final ProductDetailsProductReviewsRecyclerViewAdapter moreBestIndustriesRecyclerViewAdapter
                                = new ProductDetailsProductReviewsRecyclerViewAdapter(moreProductReviewsView.getContext(), listHomeEventResponse,
                                moreProductReviewsView.getDisplayImageOptions());
                        moreProductReviewsView.getRecyclerView().setAdapter(moreBestIndustriesRecyclerViewAdapter);
                        moreProductReviewsView.getRecyclerView().addOnScrollListener(new OnRcvScrollListener() {
                            @Override
                            public void onLoadMore(int totalItemCount) {
                                super.onLoadMore(totalItemCount);
                                if ((currentPage * totalDocumentOfPage) < count) {
                                    currentPage++;
                                    listHomeEventResponse.add(null);
                                    moreBestIndustriesRecyclerViewAdapter.notifyItemInserted(listHomeEventResponse.size() - 1);
                                    Call<ProductReviewsResponse> callListHomeEventsResponseLoadMore = productReviewsAPI.getListComments(currentPage, totalDocumentOfPage,
                                            productId);
                                    callListHomeEventsResponseLoadMore.enqueue(new Callback<ProductReviewsResponse>() {
                                        @Override
                                        public void onResponse(Response<ProductReviewsResponse> response, Retrofit retrofit) {
                                            listHomeEventResponse.remove(null);
                                            moreBestIndustriesRecyclerViewAdapter.notifyDataSetChanged();
                                            if (response.code() == 200 && null != response.body().getLisProductReviewsResponse() &&
                                                    response.body().getLisProductReviewsResponse().size() > 0) {
                                                Iterator<ProductReviewResponse> iteratorHomeEventResponseLoadMore = response.body().getLisProductReviewsResponse().iterator();
                                                while (iteratorHomeEventResponseLoadMore.hasNext()) {
                                                    listHomeEventResponse.add(iteratorHomeEventResponseLoadMore.next());
                                                    moreBestIndustriesRecyclerViewAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {
                                            moreProductReviewsView.hideProcessing();
                                        }
                                    });
                                }
                            }
                        });
                        moreProductReviewsView.hideProcessing();
                    }

                }

                @Override
                public void onFailure(Throwable t) {
                    moreProductReviewsView.hideProcessing();
                }
            });
        } catch (Exception exception) {
            Log.i(TAG, exception.toString());
        }
    }
}
