package vn.lingo.marketplace.shopping.activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.utils.JVMRuntime;

/**
 * Created by longtran on 12/12/2015.
 */
public class AbstractAppCompatActivityNoToolbar extends AppCompatActivity {

    private static final String SHOWCASE_ID = "AbstractAppCompatActivityNoToolbar";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the Currency
        //AppsFlyerLib.setCurrencyCode("VND");
        // The Dev key cab be set here or in the manifest.xml
        //AppsFlyerLib.setAppsFlyerKey("wyrVieWjHQUKx3HtBnVwoc");
        //AppsFlyerLib.sendTracking(this);
    }

    /***
     *
     */
    public void presentShowcaseSequence(View target) {
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOWCASE_ID);
        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
                //Toast.makeText(itemView.getContext(), "Item #" + position, Toast.LENGTH_SHORT).show();
            }
        });
        sequence.setConfig(config);
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(target)
                        .setDismissText("GOT IT")
                        .setContentText("This is button three")
                        .withRectangleShape()
                        .build()
        );
        sequence.start();
    }

    /***
     * @return
     */
    public MaterialDialog getMaterialDialog() {
        MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                //.title(R.string.progress_dialog)
                //.content(R.string.please_wait)
                .customView(R.layout.progress_indicator_layout, true)
                //.progress(true, 0)
                .autoDismiss(false)
                .cancelable(false)
                .backgroundColorRes(R.color.transparent)
                .titleColorRes(R.color.transparent)
                .contentColor(Color.TRANSPARENT)
                .progressIndeterminateStyle(false)
                .show();
        materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return materialDialog;
    }

    /***
     * @param appCompatActivity
     * @param title
     * @param content
     * @param positiveText
     * @return
     */
    public MaterialDialog getMaterialDialog(AppCompatActivity appCompatActivity, String title, String content, String positiveText) {
        return new MaterialDialog.Builder(appCompatActivity)
                .cancelable(true)
                .backgroundColorRes(R.color.white)
                .title(title)
                .titleColorRes(R.color.black)
                .content(content)
                .contentColor(Color.BLACK)
                .positiveText(positiveText)
                .positiveColorRes(R.color.black)
                .onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onStop() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        JVMRuntime.getVMRuntime().clearGrowthLimit();
        super.onDestroy();
    }

    /***
     * @return
     */
    public DisplayImageOptions getDisplayImageOptions() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getDisplayImageOptions();
    }
}
