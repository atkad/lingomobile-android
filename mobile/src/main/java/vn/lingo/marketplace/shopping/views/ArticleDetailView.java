package vn.lingo.marketplace.shopping.views;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;

/**
 * Created by longtran on 14/12/2015.
 */
public interface ArticleDetailView extends AbstractView {

    public AppCompatActivity getAppCompatActivity();

    public Toolbar getToolbar();

    public AppCompatTextView getAppCompatTextViewHeadLine();

    public AppCompatTextView getAppCompatTextViewTeaser();

    public AppCompatTextView getAppCompatTextViewBody();

    public AppCompatImageView getAppCompatTextViewAvatar();

    public AppCompatTextView getAppCompatTextViewSource();

    public AppCompatTextView getAppCompatTextViewAuthor();
}
