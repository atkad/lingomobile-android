package vn.lingo.marketplace.shopping.utils;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 15/12/2015.
 */
public class MarginDecoration extends RecyclerView.ItemDecoration {

    private int margin;

    public MarginDecoration(Context context) {
        if (null != context && null != context.getResources()) {
            margin = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
        }
    }

    @Override
    public void getItemOffsets(
            Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(margin, margin, margin, margin);
    }
}
