package vn.lingo.marketplace.shopping.presenters.home;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.adapter.CategoriesRecyclerViewAdapter;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.CategoriesParcelable;
import vn.lingo.marketplace.shopping.models.IndustryAPI;
import vn.lingo.marketplace.shopping.models.IndustryResponse;
import vn.lingo.marketplace.shopping.views.home.HomePageSubIndustriesView;

/**
 * Created by longtran on 13/11/2015.
 */
public class HomePageCategoriesPresenter implements RecyclerViewItemClickListener {

    private String TAG = HomePageCategoriesPresenter.class.getName();
    private HomePageSubIndustriesView homePageSubIndustriesView;

    /****
     * @param homePageSubIndustriesView
     */
    public HomePageCategoriesPresenter(HomePageSubIndustriesView homePageSubIndustriesView) {
        this.homePageSubIndustriesView = homePageSubIndustriesView;
        homePageSubIndustriesView.showProcessing();
    }

    /***
     *
     */
    public void fetchingHomePageSubIndustries(CategoriesParcelable categoriesParcelable) {
        long parentId = categoriesParcelable.getId();
        IndustryAPI industryAPI = homePageSubIndustriesView.getRetrofit().create(IndustryAPI.class);
        Call<List<IndustryResponse>> call = null;
        if (parentId == 0 && categoriesParcelable.getZoneLevel() == 0) {
            call = industryAPI.getIndustryFull();
        } else if (categoriesParcelable.getZoneLevel() == 1) {
            call = industryAPI.getDetailIndustry(parentId);
        } else if (categoriesParcelable.getZoneLevel() == 2) {
            call = industryAPI.getGroupProduct(parentId);
        }
        if (null == call) {
            return;
        }
        call.enqueue(new Callback<List<IndustryResponse>>() {
            @Override
            public void onResponse(Response<List<IndustryResponse>> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    CategoriesRecyclerViewAdapter homeHotDealsRecyclerViewAdapter = new CategoriesRecyclerViewAdapter(homePageSubIndustriesView.getContext(), response.body(),
                            HomePageCategoriesPresenter.this, homePageSubIndustriesView.getDisplayImageOptions());
                    homePageSubIndustriesView.getRecyclerViewSubIndustries().setAdapter(homeHotDealsRecyclerViewAdapter);
                } else {

                }
                homePageSubIndustriesView.hideProcessing();
            }

            @Override
            public void onFailure(Throwable t) {
                homePageSubIndustriesView.hideProcessing();
            }
        });
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        homePageSubIndustriesView.onItemClicked(position, id, zoneLevel, mappingZoneId, title);
    }
}
