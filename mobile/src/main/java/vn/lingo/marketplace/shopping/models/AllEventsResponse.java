package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 08/12/2015.
 */
public class AllEventsResponse implements Serializable {

    @SerializedName("o_cur_items")
    private List<EventDealResponse> listEventDealResponse;

    public List<EventDealResponse> getListEventDealResponse() {
        return listEventDealResponse;
    }

    public void setListEventDealResponse(List<EventDealResponse> listEventDealResponse) {
        this.listEventDealResponse = listEventDealResponse;
    }
}
