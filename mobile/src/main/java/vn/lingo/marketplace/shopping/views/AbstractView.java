package vn.lingo.marketplace.shopping.views;

import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import retrofit.Retrofit;

/**
 * Created by longtran on 04/11/2015.
 */
public interface AbstractView {
    
    public Context getContext();

    public void setMessageError(String error);

    public void showProcessing();

    public void hideProcessing();

    public Retrofit getRetrofit();

    public DisplayImageOptions getDisplayImageOptions();
}
