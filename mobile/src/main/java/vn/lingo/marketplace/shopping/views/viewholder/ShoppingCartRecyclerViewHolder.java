package vn.lingo.marketplace.shopping.views.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class ShoppingCartRecyclerViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.process_ordered)
    public AppCompatCheckBox appCompatCheckBox;

    @Bind(R.id.deleted_order)
    public AppCompatImageView appCompatImageViewDeletedOrder;

    @Bind(R.id.shopping_cart_image_view)
    public ImageView imageView;

    @Bind(R.id.shopping_cart_description)
    public AppCompatTextView appCompatTextViewDescription;

    @Bind(R.id.shopping_cart_market_price)
    public AppCompatTextView appCompatTextViewMarketPrice;

    @Bind(R.id.shopping_cart_price)
    public AppCompatTextView appCompatTextViewPrice;

    @Bind(R.id.shopping_cart_label_quantity)
    public AppCompatTextView appCompatTextViewLabelQuantity;

    @Bind(R.id.all_shopping_cart_row_item)
    public RelativeLayout relativeLayoutRowItem;

    @Bind(R.id.product_quantity)
    public DiscreteSeekBar discreteSeekBar;

    public ShoppingCartRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        discreteSeekBar.setMax(5);
        discreteSeekBar.setMin(1);
        appCompatTextViewMarketPrice.setPaintFlags(appCompatTextViewMarketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
