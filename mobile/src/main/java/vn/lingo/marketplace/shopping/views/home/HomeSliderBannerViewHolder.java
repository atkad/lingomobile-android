package vn.lingo.marketplace.shopping.views.home;

import android.support.v7.widget.AppCompatImageView;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 05/11/2015.
 */
public class HomeSliderBannerViewHolder {

    @Bind(R.id.fragment_home_view_pager_slider_banner_item_image_layout_item_id)
    public View view;

    @Bind(R.id.fragment_home_view_pager_slider_banner_item_image_layout_item_image_id)
    public AppCompatImageView imageView;

    public HomeSliderBannerViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }
}
