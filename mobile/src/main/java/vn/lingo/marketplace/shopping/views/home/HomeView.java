package vn.lingo.marketplace.shopping.views.home;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.viewpagerindicator.CirclePageIndicator;

import net.sourceforge.android.view.autoscrollviewpager.AutoScrollViewPager;

import me.relex.seamlessviewpagerheader.widget.TouchCallbackLayout;
import vn.lingo.marketplace.shopping.views.AbstractFragment;
import vn.lingo.marketplace.shopping.views.AbstractView;
import vn.lingo.marketplace.shopping.widget.HackyViewPager;

/**
 * Created by longtran on 05/11/2015.
 */
public interface HomeView extends AbstractView {

    public RelativeLayout getRelativeLayoutLoading();

    public FragmentActivity getFragmentActivity();

    public LinearLayout getParallaxTopHeaderLinearLayout();

    public TouchCallbackLayout getTouchCallbackLayout();

    public SmartTabLayout getSmartTabLayoutIndustry();

    public AutoScrollViewPager getAutoScrollViewPager();

    public CirclePageIndicator getCirclePageIndicator();

    public HackyViewPager getHackyViewPagerIndustry();

    public AbstractFragment getFragment();

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);

    public void onItemClickedEventDealFixDealsEventByTypes(int position, int id, String micrositeKey, String micrositeType);

    public void onItemClickedMoreAllEvents();

    public RelativeLayout getRelativeLayoutLingoAdvertising();

    public AppCompatImageView getAppCompatImageViewClose();

    public AppCompatImageView getAppCompatImageViewLingoAdvertising();
}
