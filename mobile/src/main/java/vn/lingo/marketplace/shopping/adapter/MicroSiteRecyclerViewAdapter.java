package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.BoxInfoAndItemInfoListItem;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.viewholder.MicroSiteViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class MicroSiteRecyclerViewAdapter extends RecyclerView.Adapter<MicroSiteViewHolder> {
    private final String TAG = MicroSiteRecyclerViewAdapter.class.getName();
    private List<BoxInfoAndItemInfoListItem> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    public MicroSiteRecyclerViewAdapter(Context context, List<BoxInfoAndItemInfoListItem> itemList,
                                        RecyclerViewItemClickListener recyclerViewItemClickListener,
                                        DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.context = context;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public MicroSiteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_event_box_item_layout, null);
        MicroSiteViewHolder viewHolder = new MicroSiteViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MicroSiteViewHolder holder, final int position) {
        /***
         * cancel any loading images on this view
         */
        final BoxInfoAndItemInfoListItem boxInfoAndItemInfoListItem = itemList.get(position);
        try {
            ImageSize targetSize = new ImageSize(256, 256); // result Bitmap will be fit to this size
            holder.imageView.setImageResource(0);
            holder.imageView.setImageDrawable(null);
            holder.imageView.setImageBitmap(null);
            holder.imageView.destroyDrawingCache();
            ImageLoader.getInstance().loadImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + boxInfoAndItemInfoListItem.getAvatar(), targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.imageView.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            outOfMemoryError.printStackTrace();
        }
        holder.appCompatTextViewDescription.setText(boxInfoAndItemInfoListItem.getProductName());

        if (!StringUtils.isBlank(boxInfoAndItemInfoListItem.getProductPriceFormat())) {
            holder.price.setVisibility(View.VISIBLE);
            holder.price.setText(boxInfoAndItemInfoListItem.getProductPriceFormat());
        } else {
            holder.price.setVisibility(View.GONE);
        }

        if (!StringUtils.isBlank(boxInfoAndItemInfoListItem.getMarketPriceFormat())) {
            holder.marketPrice.setVisibility(View.VISIBLE);
            holder.marketPrice.setText(boxInfoAndItemInfoListItem.getMarketPriceFormat());
        } else {
            holder.marketPrice.setVisibility(View.GONE);
        }

        if (!StringUtils.isBlank(boxInfoAndItemInfoListItem.getPromotionPercent())) {
            holder.linearLayoutPercent.setVisibility(View.VISIBLE);
            holder.appCompatTextViewPercent.setText(boxInfoAndItemInfoListItem.getPromotionPercent());
        } else {
            holder.linearLayoutPercent.setVisibility(View.GONE);
        }

        if (!StringUtils.isBlank(boxInfoAndItemInfoListItem.getHappyHourDiscount())) {
            holder.linearLayoutHappyHourDiscount.setVisibility(View.VISIBLE);
            holder.appCompatTextViewHappyHourDiscount.setText("Giờ vàng");
        } else {
            holder.linearLayoutHappyHourDiscount.setVisibility(View.GONE);
        }

        if (!StringUtils.isBlank(boxInfoAndItemInfoListItem.getGiftCard())) {
            holder.linearLayoutDiscountGiftCards.setVisibility(View.VISIBLE);
            holder.appCompatTextViewDiscountGiftCards.setText("Kèm theo quà tặng");
        } else {
            holder.linearLayoutDiscountGiftCards.setVisibility(View.GONE);
        }
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(boxInfoAndItemInfoListItem.getProductId()),
                            Integer.MAX_VALUE, Integer.MAX_VALUE, boxInfoAndItemInfoListItem.getProductName());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}
