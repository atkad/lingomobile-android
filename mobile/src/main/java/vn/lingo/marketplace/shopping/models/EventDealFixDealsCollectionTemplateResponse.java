package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 09/12/2015.
 */
public class EventDealFixDealsCollectionTemplateResponse implements Serializable {

    @SerializedName("microinfo")
    private List<EventDealFixDealsCollectionMicroInfo> listMicroInfo;

    @SerializedName("lstboxsetting")
    private List<EventDealFixDealsCollectionListBoxSettings> listBoxSettings;

    public List<EventDealFixDealsCollectionMicroInfo> getListMicroInfo() {
        return listMicroInfo;
    }

    public void setListMicroInfo(List<EventDealFixDealsCollectionMicroInfo> listMicroInfo) {
        this.listMicroInfo = listMicroInfo;
    }

    public List<EventDealFixDealsCollectionListBoxSettings> getListBoxSettings() {
        return listBoxSettings;
    }

    public void setListBoxSettings(List<EventDealFixDealsCollectionListBoxSettings> listBoxSettings) {
        this.listBoxSettings = listBoxSettings;
    }
}
