package vn.lingo.marketplace.shopping.models;

import java.io.Serializable;

/**
 * Created by longtran on 16/12/2015.
 */
public class ProductionFilterEntity implements Serializable {

    private int id;
    private String title;
    private String description;
    private String sortName;
    private String sortType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }
}
