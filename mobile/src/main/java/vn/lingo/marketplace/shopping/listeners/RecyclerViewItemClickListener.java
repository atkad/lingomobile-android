package vn.lingo.marketplace.shopping.listeners;

/**
 * Created by longtran on 13/11/2015.
 */
public interface RecyclerViewItemClickListener {
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);
}
