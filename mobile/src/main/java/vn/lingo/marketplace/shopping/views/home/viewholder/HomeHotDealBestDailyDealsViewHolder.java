package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 16/11/2015.
 */
public class HomeHotDealBestDailyDealsViewHolder {

    @Bind(R.id.home_hot_deal_best_daily_deals_image_view_id)
    public ImageView imageView;

    @Bind(R.id.home_hot_deal_best_daily_deals_id)
    public LinearLayout linearLayout;

//    @Bind(R.id.home_hot_deal_best_daily_deals_app_compat_text_view_title_id)
//    public AppCompatTextView appCompatTextViewTitle;
//
//    @Bind(R.id.home_hot_deal_best_daily_deals_app_compat_text_view_description_id)
//    public AppCompatTextView appCompatTextViewDescription;

    public HomeHotDealBestDailyDealsViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }

}
