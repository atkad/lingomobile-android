package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.HomeEventResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.home.viewholder.ProgressViewHolder;
import vn.lingo.marketplace.shopping.views.viewholder.MoreBestIndustriesRecyclerViewHolder;


/**
 * Created by longtran on 09/11/2015.
 */
public class MoreBestIndustriesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String TAG = MoreBestIndustriesRecyclerViewAdapter.class.getName();
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    private List<HomeEventResponse> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    public MoreBestIndustriesRecyclerViewAdapter(Context context, List<HomeEventResponse> itemList,
                                                 RecyclerViewItemClickListener recyclerViewItemClickListener,
                                                 DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        if (null != context) {
            this.context = context;
            this.displayImageOptions = displayImageOptions;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_best_industries_recycler_view_item_layout, null);
            MoreBestIndustriesRecyclerViewHolder viewHolder =
                    new MoreBestIndustriesRecyclerViewHolder(view);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);
            ProgressViewHolder progressViewHolder = new ProgressViewHolder(view);
            return progressViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MoreBestIndustriesRecyclerViewHolder) {
            final MoreBestIndustriesRecyclerViewHolder moreBestIndustriesRecyclerViewHolder
                    = (MoreBestIndustriesRecyclerViewHolder) holder;
            final HomeEventResponse homeEventResponse = itemList.get(position);
            try {
                ImageSize targetSize = new ImageSize((int) context.getResources().getDimension(R.dimen.lingo_home_page_big_fri_day_image_height),
                        (int) context.getResources().getDimension(R.dimen.lingo_home_page_big_fri_day_image_height));
                ImageLoader.getInstance().loadImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + homeEventResponse.getAvatar(), targetSize,
                        displayImageOptions, new SimpleImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                super.onLoadingStarted(imageUri, view);
                                moreBestIndustriesRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                super.onLoadingFailed(imageUri, view, failReason);
                                moreBestIndustriesRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                super.onLoadingCancelled(imageUri, view);
                                moreBestIndustriesRecyclerViewHolder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                moreBestIndustriesRecyclerViewHolder.imageView.setImageBitmap(loadedImage);
                            }
                        });
            } catch (OutOfMemoryError outOfMemoryError) {
                Log.e(TAG, outOfMemoryError.toString());
            }
//            ImageLoader.getInstance().displayImage(Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + homeEventResponse.getAvatar(),
//                    homeFragmentHotDealsFestivalRecyclerViewHolder.imageView, displayImageOptions);
            moreBestIndustriesRecyclerViewHolder.appCompatTextViewDescription.setText(homeEventResponse.getProductName());
            moreBestIndustriesRecyclerViewHolder.price.setText(homeEventResponse.getProductPriceFormat());
            if (StringUtils.isBlank(homeEventResponse.getMarketPriceFormat())) {
                moreBestIndustriesRecyclerViewHolder.marketPrice.setVisibility(View.GONE);
            } else {
                moreBestIndustriesRecyclerViewHolder.marketPrice.setVisibility(View.VISIBLE);
                moreBestIndustriesRecyclerViewHolder.marketPrice.setText(homeEventResponse.getMarketPriceFormat());
            }
            if (!StringUtils.isBlank(homeEventResponse.getPromotionPercent())) {
                moreBestIndustriesRecyclerViewHolder.percent.setVisibility(View.VISIBLE);
                moreBestIndustriesRecyclerViewHolder.percent.setText(homeEventResponse.getPromotionPercent());
            } else {
                moreBestIndustriesRecyclerViewHolder.percent.setVisibility(View.GONE);
            }
            if (!StringUtils.isBlank(homeEventResponse.getHappyHourDiscount())) {
                moreBestIndustriesRecyclerViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.VISIBLE);
                moreBestIndustriesRecyclerViewHolder.appCompatTextViewHappyHourDiscount.setText("Giờ vàng");

            } else {
                moreBestIndustriesRecyclerViewHolder.linearLayoutHappyHourDiscount.setVisibility(View.GONE);
            }
            if (!StringUtils.isBlank(homeEventResponse.getGiftCard())) {
                moreBestIndustriesRecyclerViewHolder.linearLayoutDiscountGiftCards.setVisibility(View.VISIBLE);
                moreBestIndustriesRecyclerViewHolder.appCompatTextViewDiscountGiftCards.setText("Kèm theo quà tặng");
            } else {
                moreBestIndustriesRecyclerViewHolder.linearLayoutDiscountGiftCards.setVisibility(View.GONE);
            }

            moreBestIndustriesRecyclerViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(homeEventResponse.getProductId()),
                                Integer.MAX_VALUE, Integer.MAX_VALUE,
                                homeEventResponse.getProductName());
                    } catch (Exception exception) {
                        Log.e(TAG, exception.toString());
                    }
                }
            });
        } else {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            progressViewHolder.progressBarLoadMore.setIndeterminate(true);
        }
    }
}
