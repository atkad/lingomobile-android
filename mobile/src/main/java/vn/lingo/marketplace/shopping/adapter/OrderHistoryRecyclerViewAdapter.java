package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.OrderHistoryRecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.OrderHistoryLocalStorage;
import vn.lingo.marketplace.shopping.views.viewholder.OrderHistoryRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class OrderHistoryRecyclerViewAdapter extends RecyclerView.Adapter<OrderHistoryRecyclerViewHolder> {
    private String TAG = OrderHistoryRecyclerViewAdapter.class.getName();
    private List<OrderHistoryLocalStorage> itemList;
    private Context context;
    private OrderHistoryRecyclerViewItemClickListener orderHistoryRecyclerViewItemClickListener;

    public OrderHistoryRecyclerViewAdapter(Context context, List<OrderHistoryLocalStorage> itemList,
                                           OrderHistoryRecyclerViewItemClickListener orderHistoryRecyclerViewItemClickListener) {
        this.itemList = itemList;
        this.context = context;
        this.orderHistoryRecyclerViewItemClickListener = orderHistoryRecyclerViewItemClickListener;
    }

    @Override
    public OrderHistoryRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_item_layout, null);
        OrderHistoryRecyclerViewHolder viewHolder = new OrderHistoryRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OrderHistoryRecyclerViewHolder holder, final int position) {
        final OrderHistoryLocalStorage orderHistoryLocalStorage = itemList.get(position);
        if (!StringUtils.isBlank(orderHistoryLocalStorage.getOrderNumber())) {
            holder.appCompatTextViewDescription.setVisibility(View.VISIBLE);
            holder.appCompatTextViewDescription.setText(orderHistoryLocalStorage.getOrderNumber());
        } else {
            holder.appCompatTextViewDescription.setVisibility(View.INVISIBLE);
        }
        String orderNumberEndpoint = new String(Base64.decode(orderHistoryLocalStorage.getOrderNumberEndpoint().getBytes(),
                Base64.URL_SAFE | android.util.Base64.NO_WRAP));
        try {
            holder.appCompatTextViewOrderTime.setVisibility(View.VISIBLE);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date netDate = (new Date(orderHistoryLocalStorage.getTimeStamp()));
            holder.appCompatTextViewOrderTime.setText(dateFormat.format(netDate));
        } catch (Exception ex) {
            holder.appCompatTextViewOrderTime.setVisibility(View.GONE);
        }
        holder.relativeLayoutRowItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderHistoryRecyclerViewItemClickListener.onItemClicked(position, orderHistoryLocalStorage);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}
