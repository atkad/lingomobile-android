package vn.lingo.marketplace.shopping.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.RecyclerViewItemClickListener;
import vn.lingo.marketplace.shopping.models.IndustryResponse;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.viewholder.CategoriesRecyclerViewHolder;

/**
 * Created by longtran on 09/11/2015.
 */
public class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<CategoriesRecyclerViewHolder> {
    private String TAG = CategoriesRecyclerViewAdapter.class.getName();
    private List<IndustryResponse> itemList;
    private Context context;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private DisplayImageOptions displayImageOptions;

    public CategoriesRecyclerViewAdapter(Context context, List<IndustryResponse> itemList, RecyclerViewItemClickListener recyclerViewItemClickListener,
                                         DisplayImageOptions displayImageOptions) {
        this.itemList = itemList;
        this.context = context;
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public CategoriesRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_item_layout, null);
        CategoriesRecyclerViewHolder viewHolder = new CategoriesRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CategoriesRecyclerViewHolder holder, final int position) {
        final IndustryResponse industryResponse = itemList.get(position);
        String urlAvatar = Endpoint.LINGO_SIMPLE_STORAGE_SERVICE + industryResponse.getZoneAvatar();
        try {
            ImageSize targetSize = new ImageSize(256, 256);
            ImageLoader.getInstance().loadImage(urlAvatar, targetSize,
                    displayImageOptions, new SimpleImageLoadingListener() {

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            super.onLoadingFailed(imageUri, view, failReason);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                            holder.imageView.setImageResource(R.drawable.icon_loading_throbber);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.imageView.setImageBitmap(loadedImage);
                        }
                    });
        } catch (OutOfMemoryError outOfMemoryError) {
            Log.e(TAG, outOfMemoryError.toString());
        }
        if (!StringUtils.isBlank(industryResponse.getZoneName())) {
            holder.appCompatTextViewDescription.setVisibility(View.VISIBLE);
            holder.appCompatTextViewDescription.setText(industryResponse.getZoneName());
        } else {
            holder.appCompatTextViewDescription.setVisibility(View.INVISIBLE);
        }
        if (!StringUtils.isBlank(industryResponse.getZoneTitle())) {
            holder.appCompatTextViewShortDescription.setVisibility(View.VISIBLE);
            holder.appCompatTextViewShortDescription.setText(industryResponse.getZoneTitle());
        } else {
            holder.appCompatTextViewShortDescription.setVisibility(View.INVISIBLE);
        }
        holder.relativeLayoutRowItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    recyclerViewItemClickListener.onItemClicked(position, Integer.parseInt(industryResponse.getZoneId()),
                            Integer.parseInt(industryResponse.getZoneLevel()), Integer.parseInt(industryResponse.getMappingZoneId()),
                            industryResponse.getZoneName());
                } catch (Exception exception) {
                    Log.e(TAG, exception.toString());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}