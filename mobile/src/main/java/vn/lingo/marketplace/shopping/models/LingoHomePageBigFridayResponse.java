package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 23/12/2015.
 */
public class LingoHomePageBigFridayResponse implements Serializable {

    @SerializedName("PRODUCT_PRICE_LAST")
    private String productPriceLast;
    @SerializedName("AVATAR")
    private String avatar;
    @SerializedName("IS_PAUSE_SAVEBUY")
    private String isPauseSaveBuy;
    @SerializedName("PRODUCT_DISCOUNT_LAST")
    private String productDiscountLast;
    @SerializedName("DISCOUNTS_TYPE")
    private String discountsType;
    @SerializedName("PRODUCT_ID")
    private String productId;
    @SerializedName("IS_STOP_SAVEBUY")
    private String isStopSaveBuy;
    @SerializedName("PRODUCT_NAME")
    private String productName;
    @SerializedName("PRODUCT_QUANTITY")
    private String productQuantity;
    @SerializedName("MARKET_PRICE")
    private String marketPrice;
    @SerializedName("TOTALORDER")
    private String totalOrder;
    @SerializedName("MARKET_PRICE_FORMAT")
    private String marketPriceFormat;
    @SerializedName("PRODUCT_PRICE_LAST_FORMAT")
    private String productPriceLastFormat;
    @SerializedName("M_GOLD_HOUR")
    private String happyHourDiscount;
    @SerializedName("M_GIFT")
    private String giftCard;

    public String getProductPriceLast() {
        return productPriceLast;
    }

    public void setProductPriceLast(String productPriceLast) {
        this.productPriceLast = productPriceLast;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIsPauseSaveBuy() {
        return isPauseSaveBuy;
    }

    public void setIsPauseSaveBuy(String isPauseSaveBuy) {
        this.isPauseSaveBuy = isPauseSaveBuy;
    }

    public String getProductDiscountLast() {
        return productDiscountLast;
    }

    public void setProductDiscountLast(String productDiscountLast) {
        this.productDiscountLast = productDiscountLast;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getIsStopSaveBuy() {
        return isStopSaveBuy;
    }

    public void setIsStopSaveBuy(String isStopSaveBuy) {
        this.isStopSaveBuy = isStopSaveBuy;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(String totalOrder) {
        this.totalOrder = totalOrder;
    }

    public String getMarketPriceFormat() {
        return marketPriceFormat;
    }

    public void setMarketPriceFormat(String marketPriceFormat) {
        this.marketPriceFormat = marketPriceFormat;
    }

    public String getProductPriceLastFormat() {
        return productPriceLastFormat;
    }

    public void setProductPriceLastFormat(String productPriceLastFormat) {
        this.productPriceLastFormat = productPriceLastFormat;
    }

    public String getHappyHourDiscount() {
        return happyHourDiscount;
    }

    public void setHappyHourDiscount(String happyHourDiscount) {
        this.happyHourDiscount = happyHourDiscount;
    }

    public String getGiftCard() {
        return giftCard;
    }

    public void setGiftCard(String giftCard) {
        this.giftCard = giftCard;
    }
}
