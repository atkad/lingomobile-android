package vn.lingo.marketplace.shopping.views.home;

import android.view.WindowManager;
import android.widget.ImageView;

import net.sourceforge.widgets.CountdownView;

import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by longtran on 06/11/2015.
 */
public interface BigFriDayView extends AbstractView {
    public CountdownView getCountdownViewBigFriDay();

    public ImageView getImageViewBigFridayBanner();

    public WindowManager getWindowManager();
}
