package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 09/12/2015.
 */
public class ProductDetailsResponse implements Serializable {

    @SerializedName("o_pro_info")
    private List<ProductInformationEntity> listProductInformationEntity;

    @SerializedName("o_lst_imgs")
    private List<ProductInformationImageMetadata> productDetailsImageMetadata;

    @SerializedName("o_lst_other")
    private List<ProductDetailsRelatedProductsEntity> productDetailsRelatedProductsEntity;

    @SerializedName("o_lst_group")
    private List<ProductDetailsGeneralProductInformation> productDetailsGeneralProductInformation;

    @SerializedName("gift_items")
    private List<GiftProductsResponse> giftProductsResponse;

    public List<ProductInformationEntity> getListProductInformationEntity() {
        return listProductInformationEntity;
    }

    public void setListProductInformationEntity(List<ProductInformationEntity> listProductInformationEntity) {
        this.listProductInformationEntity = listProductInformationEntity;
    }

    public List<ProductInformationImageMetadata> getProductDetailsImageMetadata() {
        return productDetailsImageMetadata;
    }

    public void setProductDetailsImageMetadata(List<ProductInformationImageMetadata> productDetailsImageMetadata) {
        this.productDetailsImageMetadata = productDetailsImageMetadata;
    }

    public List<ProductDetailsRelatedProductsEntity> getProductDetailsRelatedProductsEntity() {
        return productDetailsRelatedProductsEntity;
    }

    public void setProductDetailsRelatedProductsEntity(List<ProductDetailsRelatedProductsEntity> productDetailsRelatedProductsEntity) {
        this.productDetailsRelatedProductsEntity = productDetailsRelatedProductsEntity;
    }

    public List<ProductDetailsGeneralProductInformation> getProductDetailsGeneralProductInformation() {
        return productDetailsGeneralProductInformation;
    }

    public void setProductDetailsGeneralProductInformation(List<ProductDetailsGeneralProductInformation> productDetailsGeneralProductInformation) {
        this.productDetailsGeneralProductInformation = productDetailsGeneralProductInformation;
    }

    public List<GiftProductsResponse> getGiftProductsResponse() {
        return giftProductsResponse;
    }

    public void setGiftProductsResponse(List<GiftProductsResponse> giftProductsResponse) {
        this.giftProductsResponse = giftProductsResponse;
    }

    /****
     *
     */
    public class ProductDetailsGeneralProductInformation implements Serializable {

        @SerializedName("groupName")
        private String groupName;

        @SerializedName("items")
        private List<GeneralProductInformationNameValue> listGeneralProductInformationNameValue;

        public List<GeneralProductInformationNameValue> getListGeneralProductInformationNameValue() {
            return listGeneralProductInformationNameValue;
        }

        public void setListGeneralProductInformationNameValue(List<GeneralProductInformationNameValue> listGeneralProductInformationNameValue) {
            this.listGeneralProductInformationNameValue = listGeneralProductInformationNameValue;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }
    }

    /***
     *
     */
    public class GeneralProductInformationNameValue implements Serializable {

        @SerializedName("NAME")
        private String name;
        @SerializedName("VALUE")
        private String value;
        @SerializedName("ATTRIBUTES_KEY")
        private String attributesKey;

        public String getAttributesKey() {
            return attributesKey;
        }

        public void setAttributesKey(String attributesKey) {
            this.attributesKey = attributesKey;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
