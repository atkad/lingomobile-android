package vn.lingo.marketplace.shopping.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import net.sourceforge.android.view.autoscrollviewpager.RecyclingPagerAdapter;

import java.util.List;

import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.listeners.EventDealFixDealsEventByTypesListener;
import vn.lingo.marketplace.shopping.models.local.HomeSliderBannerResponseLocalStorage;
import vn.lingo.marketplace.shopping.network.Endpoint;
import vn.lingo.marketplace.shopping.views.home.HomeSliderBannerViewHolder;

/**
 * Created by longtran on 04/11/2015.
 */
public class HomeSliderBannerPagerAdapter extends RecyclingPagerAdapter {

    private final String TAG = HomeSliderBannerPagerAdapter.class.getName();
    private Context context;
    private List<HomeSliderBannerResponseLocalStorage> listCarouselFeaturesRollOutRegularly;
    private EventDealFixDealsEventByTypesListener eventDealFixDealsEventByTypesListener;
    private DisplayImageOptions displayImageOptions;

    public HomeSliderBannerPagerAdapter(Context context, List<HomeSliderBannerResponseLocalStorage> listCarouselFeaturesRollOutRegularly,
                                        EventDealFixDealsEventByTypesListener eventDealFixDealsEventByTypesListener,
                                        DisplayImageOptions displayImageOptions) {
        this.context = context;
        this.listCarouselFeaturesRollOutRegularly = listCarouselFeaturesRollOutRegularly;
        this.eventDealFixDealsEventByTypesListener = eventDealFixDealsEventByTypesListener;
        this.displayImageOptions = displayImageOptions;
    }

    @Override
    public int getCount() {
        return listCarouselFeaturesRollOutRegularly.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup container) {
        final HomeSliderBannerViewHolder carouselFeaturesRollOutRegularlyViewHolder;
        if (convertView == null) {
            /***
             * inflate the layout
             */
            convertView = LayoutInflater.from(container.getContext())
                    .inflate(R.layout.fragment_home_view_pager_slider_banner_item_image_layout, container, false);
            /***
             * well set up the HomeSliderBannerViewHolder
             */
            carouselFeaturesRollOutRegularlyViewHolder = new HomeSliderBannerViewHolder(convertView);
            /***
             * store the holder with the view.
             */
            convertView.setTag(carouselFeaturesRollOutRegularlyViewHolder);
        } else {
            /***
             * we've just avoided calling findViewById() on resource everytimejust use the viewHolder
             */
            carouselFeaturesRollOutRegularlyViewHolder = (HomeSliderBannerViewHolder) convertView.getTag();
        }
        final HomeSliderBannerResponseLocalStorage homeSliderBannerResponseLocalStorage = listCarouselFeaturesRollOutRegularly.get(position);
        ImageLoader.getInstance().displayImage(Endpoint.LINGO_HOME_PAGE_SIMPLE_STORAGE_SERVICE + homeSliderBannerResponseLocalStorage.getImagePath(),
                carouselFeaturesRollOutRegularlyViewHolder.imageView, displayImageOptions, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        carouselFeaturesRollOutRegularlyViewHolder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        carouselFeaturesRollOutRegularlyViewHolder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                        if (ScreenUtils.getAspectRatio(loadedImage.getWidth(), loadedImage.getHeight()).equalsIgnoreCase("80:37")) {
                            carouselFeaturesRollOutRegularlyViewHolder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//                        } else {
//                            carouselFeaturesRollOutRegularlyViewHolder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                        }
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        carouselFeaturesRollOutRegularlyViewHolder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    }
                });
        carouselFeaturesRollOutRegularlyViewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventDealFixDealsEventByTypesListener.onItemClickedEventDealFixDealsEventByTypes(position,
                        Integer.parseInt(homeSliderBannerResponseLocalStorage.getGalleryId()), homeSliderBannerResponseLocalStorage.getParameter1(), "1");
            }
        });
        return convertView;
    }
}
