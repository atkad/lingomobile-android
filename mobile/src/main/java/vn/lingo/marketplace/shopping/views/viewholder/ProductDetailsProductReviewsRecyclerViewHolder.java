package vn.lingo.marketplace.shopping.views.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class ProductDetailsProductReviewsRecyclerViewHolder extends RecyclerView.ViewHolder {


    @Bind(R.id.product_details_product_reviews_recycler_view_item_layout_comment_title_id)
    public AppCompatTextView appCompatTextViewCommentTitle;

    @Bind(R.id.product_details_product_reviews_recycler_view_item_layout_comment_content_id)
    public AppCompatTextView appCompatTextViewCommentContent;

    @Bind(R.id.product_details_product_reviews_recycler_view_item_layout_full_name_id)
    public AppCompatTextView appCompatTextViewFullName;

    @Bind(R.id.product_details_product_reviews_recycler_view_item_layout_date_created_id)
    public AppCompatTextView appCompatTextViewDateCreated;

    @Bind(R.id.product_details_product_reviews_rating_bar)
    public RatingBar ratingBar;

    public ProductDetailsProductReviewsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
