package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 10/12/2015.
 */
public class ProductCheckoutResponse implements Serializable {

    @SerializedName("orderTokenKey")
    private String orderTokenKey;
    @SerializedName("shopCartId")
    private String shopCartId;
    @SerializedName("morId")
    private String morId;
    @SerializedName("redirectWapURL")
    private String redirectWapURL;
    @SerializedName("shopCreateTime")
    private String shopCreateTime;
    @SerializedName("reqID")
    private String reqID;
    @SerializedName("noted")
    private String noted;
    @SerializedName("shopOrderStatus")
    private String shopOrderStatus;
    @SerializedName("shopUser")
    private String shopUser;
    @SerializedName("shopOrderId")
    private String shopOrderId;
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;

    public String getOrderTokenKey() {
        return orderTokenKey;
    }

    public void setOrderTokenKey(String orderTokenKey) {
        this.orderTokenKey = orderTokenKey;
    }

    public String getShopCartId() {
        return shopCartId;
    }

    public void setShopCartId(String shopCartId) {
        this.shopCartId = shopCartId;
    }

    public String getMorId() {
        return morId;
    }

    public void setMorId(String morId) {
        this.morId = morId;
    }

    public String getRedirectWapURL() {
        return redirectWapURL;
    }

    public void setRedirectWapURL(String redirectWapURL) {
        this.redirectWapURL = redirectWapURL;
    }

    public String getShopCreateTime() {
        return shopCreateTime;
    }

    public void setShopCreateTime(String shopCreateTime) {
        this.shopCreateTime = shopCreateTime;
    }

    public String getReqID() {
        return reqID;
    }

    public void setReqID(String reqID) {
        this.reqID = reqID;
    }

    public String getNoted() {
        return noted;
    }

    public void setNoted(String noted) {
        this.noted = noted;
    }

    public String getShopOrderStatus() {
        return shopOrderStatus;
    }

    public void setShopOrderStatus(String shopOrderStatus) {
        this.shopOrderStatus = shopOrderStatus;
    }

    public String getShopUser() {
        return shopUser;
    }

    public void setShopUser(String shopUser) {
        this.shopUser = shopUser;
    }

    public String getShopOrderId() {
        return shopOrderId;
    }

    public void setShopOrderId(String shopOrderId) {
        this.shopOrderId = shopOrderId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
