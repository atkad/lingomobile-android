package vn.lingo.marketplace.shopping.views.home;

import android.support.v7.widget.RecyclerView;

import vn.lingo.marketplace.shopping.views.AbstractView;

/**
 * Created by longtran on 13/11/2015.
 */
public interface HomePageSubIndustriesView extends AbstractView {

    public RecyclerView getRecyclerViewSubIndustries();

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);
}
