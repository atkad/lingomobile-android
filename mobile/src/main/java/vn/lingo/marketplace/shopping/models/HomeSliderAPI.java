package vn.lingo.marketplace.shopping.models;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 11/11/2015.
 */
public interface HomeSliderAPI {
    @GET("/v1/mobile/Home/GetSlides")
    Call<List<HomeSliderBannerResponse>> getHomeSlider();//@Query("p_key") String primitiveKey
}
