package vn.lingo.marketplace.shopping.utils;

import android.util.Log;

import org.jsoup.helper.StringUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by longtran on 01/01/2016.
 */
public class Utility {

    private final String TAG = Utility.class.getName();

    private static Utility utility;

    private static final Object SYNCHRONIZED = new Object();

    public static Utility getInstance() {
        synchronized (SYNCHRONIZED) {
            if (null == utility) {
                utility = new Utility();
            }
            return utility;
        }
    }

    /**
     *
     */
    public String getPurchaseOrderNumber(String input) {
        String purchaseOrderNumber = null;
        if (StringUtil.isBlank(input)) {
            return purchaseOrderNumber;
        }
        try {
            String patternString = ".*/thanh-cong/(\\d+|\\D+)";
            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(input);
            while (matcher.find()) {
                purchaseOrderNumber = matcher.group(1);
            }
        } catch (Exception exception) {
            Log.i(TAG, exception.toString());
        }
        return purchaseOrderNumber;
    }

    /**
     *
     */
    public String getOrderNumber(String input) {
        String purchaseOrderNumber = null;
        if (StringUtil.isBlank(input)) {
            return purchaseOrderNumber;
        }
        try {
            String patternString = ".*/theo-doi-don-hang/(\\d+-\\d+)";
            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = pattern.matcher(input);
            while (matcher.find()) {
                purchaseOrderNumber = matcher.group(1);
            }
        } catch (Exception exception) {
            Log.i(TAG, exception.toString());
        }
        return purchaseOrderNumber;
    }
}
