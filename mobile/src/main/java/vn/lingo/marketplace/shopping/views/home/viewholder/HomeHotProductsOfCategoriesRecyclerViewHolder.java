package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 09/11/2015.
 */
public class HomeHotProductsOfCategoriesRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @Bind(R.id.app_compat_text_view_description)
    public AppCompatTextView appCompatTextView;

    @Bind(R.id.banner_background_image)
    public ImageView imageView;

    @Bind(R.id.hot_products_of_categories_item_id)
    public CardView cardViewHotProductsOfCategoriesItemId;

    public HomeHotProductsOfCategoriesRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }
}
