package vn.lingo.marketplace.shopping.views.home.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 23/12/2015.
 */
public class LingoHomePageBigFridayViewHolder {

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_image_view_metadata_id)
    public AppCompatImageView imageView;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_description_id)
    public AppCompatTextView appCompatTextViewDescription;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_item_price_id)
    public AppCompatTextView appCompatTextViewPrice;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_item_market_price_id)
    public AppCompatTextView appCompatTextViewMarketPrice;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_item_percent_id)
    public AppCompatTextView appCompatTextViewPercent;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_item_happy_hour_discount_id)
    public AppCompatTextView appCompatTextViewHappyHourDiscount;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_item_discount_gift_card_id)
    public AppCompatTextView appCompatTextViewDiscountGiftCard;

    @Bind(R.id.lingo_home_page_industry_view_pager_big_fri_day_layout_item_id)
    public View view;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_item_linear_layout_happy_hour_discount_id)
    public LinearLayout linearLayoutHappyHourDiscount;

    @Bind(R.id.lingo_home_page_industry_fragment_view_pager_item_big_fri_day_item_linear_layout_discount_gift_card_id)
    public LinearLayout linearLayoutDiscountGiftCard;

    public LingoHomePageBigFridayViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
        appCompatTextViewMarketPrice.setPaintFlags(appCompatTextViewMarketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
