package vn.lingo.marketplace.shopping.utils;

import android.os.Handler;

/**
 * Created by longtran on 17/11/2015.
 */
public class JHandler extends Handler {
    private static JHandler jHandler;
    private static final Object SYNCHRONIZED = new Object();

    public static JHandler getInstall() {
        synchronized (SYNCHRONIZED) {
            jHandler = new JHandler();
        }
        return jHandler;
    }


}
