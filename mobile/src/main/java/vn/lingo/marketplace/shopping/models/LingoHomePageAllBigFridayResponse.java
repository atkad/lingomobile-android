package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 23/12/2015.
 */
public class LingoHomePageAllBigFridayResponse implements Serializable {

    @SerializedName("o_cur_items")
    private List<LingoHomePageBigFridayResponse> listLingoHomePageBigFridayResponse;
    @SerializedName("o_total_record")
    private int totalRecord;
    @SerializedName("label")
    private String label;

    public List<LingoHomePageBigFridayResponse> getListLingoHomePageBigFridayResponse() {
        return listLingoHomePageBigFridayResponse;
    }

    public void setListLingoHomePageBigFridayResponse(List<LingoHomePageBigFridayResponse> listLingoHomePageBigFridayResponse) {
        this.listLingoHomePageBigFridayResponse = listLingoHomePageBigFridayResponse;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
