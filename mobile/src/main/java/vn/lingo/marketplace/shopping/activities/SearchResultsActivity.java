package vn.lingo.marketplace.shopping.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;

import org.apache.commons.lang3.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Retrofit;
import vn.lingo.marketplace.shopping.LingoApplication;
import vn.lingo.marketplace.shopping.R;
import vn.lingo.marketplace.shopping.models.ProductDetailsParcelable;
import vn.lingo.marketplace.shopping.models.search.CustomSearchableConstants;
import vn.lingo.marketplace.shopping.models.search.ResultItem;
import vn.lingo.marketplace.shopping.presenters.SearchResultsPresenter;
import vn.lingo.marketplace.shopping.utils.AnalyticsHelper;
import vn.lingo.marketplace.shopping.utils.Constant;
import vn.lingo.marketplace.shopping.utils.DividerItemDecoration;
import vn.lingo.marketplace.shopping.views.SearchResultsView;

/**
 * Created by longtran on 13/12/2015.
 */
public class SearchResultsActivity extends AbstractAppCompatActivity implements SearchResultsView {

    private final String TAG = SearchResultsActivity.class.getName();

    @Bind(R.id.toolbar_container)
    Toolbar toolbar;

    @Bind(R.id.activity_search_results_layout_search_results_list)
    RecyclerView recyclerViewSearchResults;

    @Bind(R.id.not_found_error_relative_layout_id)
    RelativeLayout relativeLayoutNotFound;

    @Bind(R.id.not_found_error_description)
    AppCompatTextView appCompatTextViewNotFoundErrorDescription;

    @Bind(R.id.activity_search_data_filter_spinner_id)
    AppCompatSpinner appCompatSpinnerDataFilter;

    private MaterialDialog materialDialog;
    private SearchResultsPresenter searchResultsPresenter;

    @Override
    public int getFragmentContainerViewId() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results_layout);
        ButterKnife.bind(this);
        searchResultsPresenter = new SearchResultsPresenter(this);
        Intent intent = getIntent();
        handleIntent(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /***
     * Handles the intent that carries user's choice in the Search Interface
     *
     * @param intent
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.i(TAG, "Received query: " + query);
            if (!StringUtils.isBlank(query)) {
                searchResultsPresenter.fetchingFilter(query);
                AnalyticsHelper.logEvent("TRACKING_USER_ACTIVITY_SEARCH", query, true);
                AnalyticsHelper.logEvent(getContext(), "TRACKING_USER_ACTIVITY_SEARCH", query);
            }
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Bundle bundle = this.getIntent().getExtras();
            assert (bundle != null);
            if (bundle != null) {
                ResultItem receivedItem = bundle.getParcelable(CustomSearchableConstants.CLICKED_RESULT_ITEM);
                Log.i(TAG + ".header", receivedItem.getHeader());
                Log.i(TAG + ".subHeader", receivedItem.getSubHeader());
                Log.i(TAG + ".leftIcon", receivedItem.getLeftIcon().toString());
                Log.i(TAG + ".rightIcon", receivedItem.getRightIcon().toString());
            }
        }
    }

    @Override
    public AppCompatSpinner getAppCompatSpinnerDataFilter() {
        return appCompatSpinnerDataFilter;
    }

    @Override
    public AppCompatActivity getAppCompatActivity() {
        return this;
    }

    @Override
    public RecyclerView getRecyclerSearchResults() {
        recyclerViewSearchResults.addItemDecoration(new DividerItemDecoration(1));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewSearchResults.setLayoutManager(linearLayoutManager);
        return recyclerViewSearchResults;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title) {
        ProductDetailsParcelable productDetailsParcelable = new ProductDetailsParcelable();
        productDetailsParcelable.setId(id);
        productDetailsParcelable.setTitle(title);
        Intent intent = new Intent();
        intent.setClassName(this, ProductDetailsActivity.class.getName());
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.ProductDetails.PRODUCT_DETAILS_KEY, productDetailsParcelable);
        intent.putExtras(bundle);
        this.startActivity(intent);
    }

    @Override
    public RelativeLayout getRelativeLayoutNotFound() {
        return relativeLayoutNotFound;
    }

    @Override
    public AppCompatTextView getAppCompatTextViewNotFoundErrorDescription() {
        return appCompatTextViewNotFoundErrorDescription;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setMessageError(String error) {

    }

    @Override
    public void showProcessing() {
        materialDialog = getMaterialDialog();
    }

    @Override
    public void hideProcessing() {
        materialDialog.dismiss();
    }

    @Override
    public Retrofit getRetrofit() {
        LingoApplication lingoApplication = (LingoApplication) getApplication();
        return lingoApplication.getRetrofit();
    }

    @Override
    protected void onResume() {
        AnalyticsHelper.logPageViews();
        searchResultsPresenter.invalidateToolbar();
        super.onResume();
    }
}
