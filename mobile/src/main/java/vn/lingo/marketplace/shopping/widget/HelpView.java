package vn.lingo.marketplace.shopping.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class HelpView extends View {
    private List<CycleView> mListCycle;
    private List<BitmapView> mListBitmapViews;

    private Paint paintBackground = new Paint();
    private Paint transparentPaint = new Paint();
    private Paint paintBitmap = new Paint();
    private Paint borderPaint = new Paint();
    private RectF rectPaintBitmap = new RectF();
    private Xfermode mXfermode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);

    private int defaultBorderCycleWidth;
    private int defaultBorderColor;

    public HelpView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }

    public HelpView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public HelpView(Context context) {
        super(context);
        initPaint();
    }

    public CycleView createACycleView() {
        return new CycleView();
    }

    public BitmapView createABitmapView() {
        return new BitmapView();
    }

    public void clear() {
        if (mListBitmapViews != null) {
            mListBitmapViews.clear();
        }

        if (mListCycle != null) {
            mListCycle.clear();
        }
        invalidate();
    }

    public void setBackgroundColor(int color) {
        paintBackground.setColor(color);
        invalidate();
    }

    private void initPaint() {

        paintBackground.setStyle(Paint.Style.FILL);
        paintBackground.setColor(Color.parseColor("#CC000000"));
        borderPaint.setAntiAlias(true);
        borderPaint.setStyle(Paint.Style.STROKE);

        transparentPaint.setColor(getResources().getColor(android.R.color.transparent));
        transparentPaint.setXfermode(mXfermode);
        transparentPaint.setAntiAlias(true);
        transparentPaint.setDither(true);

        paintBitmap.setAntiAlias(true);
        paintBitmap.setDither(true);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawPaint(paintBackground);

        drawListCycleItems(canvas);
        drawListBitmapItems(canvas);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mListBitmapViews != null) {
            mListBitmapViews.clear();
        }

        if (mListCycle != null) {
            mListCycle.clear();
        }
    }

    private void drawListBitmapItems(Canvas canvas) {
        if (mListBitmapViews != null) {
            for (BitmapView item : mListBitmapViews) {
                rectPaintBitmap.set(item.position.x, item.position.y, item.position.x + item.width,
                        item.position.y + item.height);
                canvas.drawBitmap(item.getBitmap(), null, rectPaintBitmap, paintBitmap);
            }

        }
    }

    private void drawListCycleItems(Canvas canvas) {
        if (mListCycle != null) {
            for (CycleView item : mListCycle) {
                if (item.isTransparent()) {
                    transparentPaint.setColor(getResources().getColor(android.R.color.transparent));
                    transparentPaint.setXfermode(mXfermode);
                } else {
                    transparentPaint.setColor(Color.WHITE);
                    transparentPaint.setXfermode(null);

                }
                if (item.borderWidth > 0) {
                    borderPaint.setStrokeWidth(item.borderWidth);
                    borderPaint.setColor(item.borderColor);
                    canvas.drawCircle(item.getPosition().x, item.getPosition().y, item.getRadius(),
                            borderPaint);
                }
                canvas.drawCircle(item.getPosition().x, item.getPosition().y, item.getRadius(),
                        transparentPaint);
            }
        }
    }

    public List<CycleView> getListCycle() {
        return mListCycle;
    }

    public void setListCycle(List<CycleView> mListCycle) {
        if (this.mListCycle != null) {

            this.mListCycle.clear();
        }

        this.mListCycle = mListCycle;
        invalidate();
    }

    public void addCycle(CycleView cycle) {
        if (cycle == null) {
            return;
        }
        if (mListCycle == null) {
            mListCycle = new ArrayList<HelpView.CycleView>();
        }
        mListCycle.add(cycle);
        invalidate();
    }

    public void addBitmapView(BitmapView bitmapView) {

        if (bitmapView == null) {
            return;
        }
        if (mListBitmapViews == null) {
            mListBitmapViews = new ArrayList<HelpView.BitmapView>();
        }
        mListBitmapViews.add(bitmapView);
        invalidate();
    }

    public CycleView getCycle(String nameSearch) {
        if (nameSearch == null) {
            return null;
        }

        if (mListCycle != null) {
            for (CycleView cycle : mListCycle) {
                String cycleName = cycle.getName();

                if (cycleName != null && cycleName.equals(nameSearch)) {
                    return cycle;
                }
            }
        }
        return null;

    }

    public List<BitmapView> getBitmapViews() {
        return mListBitmapViews;

    }

    public void setBitmapViews(List<BitmapView> mBitmapViews) {
        if (this.mListBitmapViews != null) {
            for (BitmapView item : mBitmapViews) {
                item.clear();
            }
            this.mListBitmapViews.clear();
        }
        this.mListBitmapViews = mBitmapViews;
        invalidate();
    }

    public int getDefaultBorderCycleWidth() {
        return defaultBorderCycleWidth;
    }

    public void setDefaultBorderCycleWidth(int defaultBorderCycleWidth) {
        this.defaultBorderCycleWidth = defaultBorderCycleWidth;
    }

    public int getDefaultBorderColor() {
        return defaultBorderColor;
    }

    public void setDefaultBorderColor(int defaultBorderColor) {
        this.defaultBorderColor = defaultBorderColor;
    }

    public class BitmapView {
        private Bitmap mBitmap;
        private PointF position;
        private float width;
        private float height;
        private String name;

        public Bitmap getBitmap() {
            return mBitmap;
        }

        public void setBitmap(Bitmap mBitmap) {
            this.mBitmap = mBitmap;
        }

        public PointF getPosition() {
            return position;
        }

        public void setPosition(PointF position) {
            this.position = position;
        }

        protected void clear() {
            if (mBitmap != null) {
                mBitmap.recycle();
            }
            mBitmap = null;
        }

        public float getWidth() {
            return width;
        }

        public void setWidth(float width) {
            this.width = width;
        }

        public float getHeight() {
            return height;
        }

        public void setHeight(float height) {
            this.height = height;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class CycleView {
        private PointF position;
        private float radius = 70;
        private boolean transparent = true;
        private String name;
        private int borderWidth;

        public int getBorderWidth() {
            return borderWidth;
        }

        public void setBorderWidth(int borderWidth) {
            this.borderWidth = borderWidth;
        }

        public int getBorderColor() {
            return borderColor;
        }

        public void setBorderColor(int borderColor) {
            this.borderColor = borderColor;
        }

        private int borderColor;

        public CycleView() {
            borderWidth = defaultBorderCycleWidth;
            borderColor = defaultBorderColor;
        }

        public float getRadius() {
            return radius;
        }

        public void setRadius(float radius) {
            this.radius = radius;
        }

        public PointF getPosition() {
            return position;
        }

        public void setPosition(PointF position) {
            this.position = position;
        }

        public boolean isTransparent() {
            return transparent;
        }

        public void setTransparent(boolean transparent) {
            this.transparent = transparent;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
