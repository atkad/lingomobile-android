package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by longtran on 12/12/2015.
 */
public class BoxInfoAndItemInfoTemplateInfo implements Serializable {

    @SerializedName("ITEM_HEIGHT")
    private String itemHeight;
    @SerializedName("BOX_FOOTER")
    private String boxFooter;
    @SerializedName("BOX_TEMPLATE_ID")
    private String boxTemplateId;
    @SerializedName("BOX_NAME")
    private String boxName;
    @SerializedName("BOX_HEADER")
    private String boxHeader;
    @SerializedName("BOX_ITEM")
    private String boxItem;
    @SerializedName("IS_PRODUCT")
    private String isProduct;
    @SerializedName("IS_AUTO")
    private String isAuto;
    @SerializedName("TEMPLATE_ID")
    private String templateId;
    @SerializedName("CREATEDBY")
    private String createdBy;
    @SerializedName("BOX_AVATAR")
    private String boxAvatar;
    @SerializedName("ITEM_QUANTITY")
    private String itemQuantity;
    @SerializedName("CREATEDON")
    private String createDon;
    @SerializedName("MODIFYBY")
    private String modifyBy;
    @SerializedName("ITEM_WIDTH")
    private String itemWidth;
    @SerializedName("MODIYON")
    private String modiyOn;

    public String getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(String itemHeight) {
        this.itemHeight = itemHeight;
    }

    public String getBoxFooter() {
        return boxFooter;
    }

    public void setBoxFooter(String boxFooter) {
        this.boxFooter = boxFooter;
    }

    public String getBoxTemplateId() {
        return boxTemplateId;
    }

    public void setBoxTemplateId(String boxTemplateId) {
        this.boxTemplateId = boxTemplateId;
    }

    public String getBoxName() {
        return boxName;
    }

    public void setBoxName(String boxName) {
        this.boxName = boxName;
    }

    public String getBoxHeader() {
        return boxHeader;
    }

    public void setBoxHeader(String boxHeader) {
        this.boxHeader = boxHeader;
    }

    public String getBoxItem() {
        return boxItem;
    }

    public void setBoxItem(String boxItem) {
        this.boxItem = boxItem;
    }

    public String getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(String isProduct) {
        this.isProduct = isProduct;
    }

    public String getIsAuto() {
        return isAuto;
    }

    public void setIsAuto(String isAuto) {
        this.isAuto = isAuto;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getBoxAvatar() {
        return boxAvatar;
    }

    public void setBoxAvatar(String boxAvatar) {
        this.boxAvatar = boxAvatar;
    }

    public String getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(String itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public String getCreateDon() {
        return createDon;
    }

    public void setCreateDon(String createDon) {
        this.createDon = createDon;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public String getModiyOn() {
        return modiyOn;
    }

    public void setModiyOn(String modiyOn) {
        this.modiyOn = modiyOn;
    }
}
