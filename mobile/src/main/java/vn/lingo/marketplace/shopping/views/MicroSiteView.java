package vn.lingo.marketplace.shopping.views;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import vn.lingo.marketplace.shopping.models.EventDealFixDealsCollectionListBoxSettings;

/**
 * Created by longtran on 06/12/2015.
 */
public interface MicroSiteView extends AbstractView {

    public AppCompatActivity getAppCompatActivity();

    public Toolbar getToolbar();

    public LinearLayout getLinearLayoutMicroSite();

    public void onItemClicked(int position, int id, int zoneLevel, int mappingZoneId, String title);
}
