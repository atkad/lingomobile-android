package vn.lingo.marketplace.shopping.utils;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

/**
 * http://developer.samsung.com/technical-doc/view.do?v=T000000103
 * Created by longtran on 30/12/2015.
 */
public class JSystemProperties {

    private TelephonyManager telephonyManager;
    private Context context;
    private static JSystemProperties instance = null;

    private JSystemProperties(Context context) {
        this.context = context;
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    /***
     *
     * @param context
     * @return
     */
    public static JSystemProperties getInstance(Context context) {
        if (instance == null) {
            instance = new JSystemProperties(context);
        }
        return instance;
    }

    /*
     * getDeviceId() function Returns the unique device ID.
     * for example,the IMEI for GSM and the MEID or ESN for CDMA phones.
     * return IMEI No :
     */
    public String getDeviceId() {
        return telephonyManager.getDeviceId();
    }

    /*
     * getSubscriberId() function Returns the unique subscriber ID,
     * for example, the IMSI for a GSM phone.
     * return IMSI No:
     */
    public String getSubscriberId() {
        return telephonyManager.getSubscriberId();
    }

    /*
     * System Property ro.serialno returns the serial number as unique number
     * Works for Android 2.3 and above
     */
    public String getSimSerialNumber() {
        String serialNumber = null;
        try {
            Class<?> classSystemProperties = Class.forName("android.os.SystemProperties");
            Method get = classSystemProperties.getMethod("get", String.class, String.class);
            serialNumber = (String) (get.invoke(classSystemProperties, "ro.serialno", "unknown"));
            //("serial : " + serialnum + "\n");
        } catch (Exception ignored) {

        }
        String serialNumberSecond = null;
        try {
            Class classSystemPropertiesSecond = Class.forName("android.os.SystemProperties");
            Method[] methods = classSystemPropertiesSecond.getMethods();
            Object[] params = new Object[]{new String("ro.serialno"), new String(
                    "Unknown")};
            serialNumberSecond = (String) (methods[2].invoke(classSystemPropertiesSecond, params));
            //append("serial2 : " + serialnum2 + "\n");
        } catch (Exception ignored) {

        }
        return serialNumber == null ? (serialNumberSecond == null ? serialNumber : serialNumberSecond) : serialNumber;
    }

    /*
     * Settings.Secure.ANDROID_ID returns the unique DeviceID
     * Works for Android 2.2 and above
     */
    public String getDeviceID() {
        String androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return androidId;
    }

    public String getIMEIMEIDESNIMSI() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getDeviceId());
        stringBuffer.append("-");
        stringBuffer.append(getSubscriberId());
        stringBuffer.append("-");
        stringBuffer.append(getSimSerialNumber());
        stringBuffer.append("-");
        stringBuffer.append(getDeviceID());
        return stringBuffer.toString();
    }
}
