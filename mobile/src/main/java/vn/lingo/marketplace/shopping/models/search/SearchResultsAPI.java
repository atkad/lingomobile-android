package vn.lingo.marketplace.shopping.models.search;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by longtran on 13/12/2015.
 */
public interface SearchResultsAPI {

    @GET("/v1/mobile/Home/SearchProduct")
    Call<SearchProductResults> searchProduct(@Query("i_name") String i_name, @Query("i_size") int i_size,
                                             @Query("i_page") int i_page, @Query("i_sort_name") String i_sort_name,
                                             @Query("i_sort_type") String i_sort_type);

}
