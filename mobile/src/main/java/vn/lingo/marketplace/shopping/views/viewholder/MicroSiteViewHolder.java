package vn.lingo.marketplace.shopping.views.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import vn.lingo.marketplace.shopping.R;

/**
 * Created by longtran on 06/12/2015.
 */
public class MicroSiteViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_card_view_item_layout_id)
    public RelativeLayout cardView;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_card_view_item_image_view_id)
    public ImageView imageView;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_market_price_id)
    public AppCompatTextView marketPrice;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_price_id)
    public AppCompatTextView price;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_description_id)
    public AppCompatTextView appCompatTextViewDescription;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_linear_layout_percent_id)
    public LinearLayout linearLayoutPercent;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_percent_id)
    public AppCompatTextView appCompatTextViewPercent;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_linear_layout_happy_hour_discount_id)
    public LinearLayout linearLayoutHappyHourDiscount;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_happy_hour_discount_id)
    public AppCompatTextView appCompatTextViewHappyHourDiscount;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_linear_layout_discount_gift_cards_id)
    public LinearLayout linearLayoutDiscountGiftCards;

    @Bind(R.id.lingo_home_page_industry_fragment_event_box_item_discount_gift_cards_id)
    public AppCompatTextView appCompatTextViewDiscountGiftCards;

    public MicroSiteViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        marketPrice.setPaintFlags(marketPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
