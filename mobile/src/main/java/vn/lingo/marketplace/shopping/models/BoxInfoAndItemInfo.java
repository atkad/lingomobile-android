package vn.lingo.marketplace.shopping.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by longtran on 12/12/2015.
 */
public class BoxInfoAndItemInfo implements Serializable {

    @SerializedName("templateinfo")
    private List<BoxInfoAndItemInfoTemplateInfo> listBoxInfoAndItemInfoTemplateInfo;
    @SerializedName("lstitem")
    private List<BoxInfoAndItemInfoListItem> listBoxInfoAndItemInfoListItem;

    public List<BoxInfoAndItemInfoTemplateInfo> getListBoxInfoAndItemInfoTemplateInfo() {
        return listBoxInfoAndItemInfoTemplateInfo;
    }

    public void setListBoxInfoAndItemInfoTemplateInfo(List<BoxInfoAndItemInfoTemplateInfo> listBoxInfoAndItemInfoTemplateInfo) {
        this.listBoxInfoAndItemInfoTemplateInfo = listBoxInfoAndItemInfoTemplateInfo;
    }

    public List<BoxInfoAndItemInfoListItem> getListBoxInfoAndItemInfoListItem() {
        return listBoxInfoAndItemInfoListItem;
    }

    public void setListBoxInfoAndItemInfoListItem(List<BoxInfoAndItemInfoListItem> listBoxInfoAndItemInfoListItem) {
        this.listBoxInfoAndItemInfoListItem = listBoxInfoAndItemInfoListItem;
    }
}
